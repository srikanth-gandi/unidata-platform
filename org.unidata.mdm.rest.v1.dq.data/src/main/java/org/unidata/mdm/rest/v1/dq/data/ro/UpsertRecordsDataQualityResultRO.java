package org.unidata.mdm.rest.v1.dq.data.ro;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

import org.unidata.mdm.rest.system.ro.DetailedOutputRO;

/**
 * @author Mikhail Mikhailov on Apr 1, 2021
 */
public class UpsertRecordsDataQualityResultRO extends DetailedOutputRO {
    /**
     * Periods, holding DQ data.
     */
    private List<PeriodDataQualityRO> periods;
    /**
     * Constructor.
     */
    public UpsertRecordsDataQualityResultRO() {
        super();
    }
    /**
     * @return the periods
     */
    public List<PeriodDataQualityRO> getPeriods() {
        return Objects.isNull(periods) ? Collections.emptyList() : periods;
    }
    /**
     * @param periods the periods to set
     */
    public void setPeriods(List<PeriodDataQualityRO> periods) {
        this.periods = periods;
    }
}

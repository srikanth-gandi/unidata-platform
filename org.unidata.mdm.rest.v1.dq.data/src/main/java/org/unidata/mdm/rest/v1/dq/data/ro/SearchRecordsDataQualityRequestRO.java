/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.v1.dq.data.ro;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Mikhail Mikhailov on Apr 2, 2021
 * Simple DQ filtering.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SearchRecordsDataQualityRequestRO {
    /**
     * Filter by the name of the rule set.
     */
    private List<String> setNames;
    /**
     * Filter by the name of the rule.
     */
    private List<String> ruleNames;
    /**
     * Filter by the name of the function.
     */
    private List<String> functionNames;
    /**
     * Filter by the error message(s) from all errors,
     * indexed analyzed for searches by error content.
     */
    private String message;
    /**
     * Filter by severity indicator(s) from all errors.
     */
    private String severity;
    /**
     * Category/is from all errors.
     */
    private String category;
    /**
     * Overall score (sum of all scores from all errors).
     */
    private int totalScore;
    /**
     * Filter all records, having DQ errors.
     */
    private boolean matchAll;
    /**
     * Constructor.
     */
    public SearchRecordsDataQualityRequestRO() {
        super();
    }
    /**
     * @return the setNameFilter
     */
    public List<String> getSetNames() {
        return setNames;
    }
    /**
     * @param setNames the setNameFilter to set
     */
    public void setSetNames(List<String> setNames) {
        this.setNames = setNames;
    }
    /**
     * @return the ruleName
     */
    public List<String> getRuleNames() {
        return ruleNames;
    }
    /**
     * @param ruleNames the ruleName to set
     */
    public void setRuleNames(List<String> ruleNames) {
        this.ruleNames = ruleNames;
    }
    /**
     * @return the functionName
     */
    public List<String> getFunctionNames() {
        return functionNames;
    }
    /**
     * @param functionNames the functionName to set
     */
    public void setFunctionNames(List<String> functionNames) {
        this.functionNames = functionNames;
    }
    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }
    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }
    /**
     * @return the severity
     */
    public String getSeverity() {
        return severity;
    }
    /**
     * @param severity the severity to set
     */
    public void setSeverity(String severity) {
        this.severity = severity;
    }
    /**
     * @return the totalScore
     */
    public int getTotalScore() {
        return totalScore;
    }
    /**
     * @param totalScore the totalScore to set
     */
    public void setTotalScore(int totalScore) {
        this.totalScore = totalScore;
    }
    /**
     * @return the category
     */
    public String getCategory() {
        return category;
    }
    /**
     * @param category the category to set
     */
    public void setCategory(String category) {
        this.category = category;
    }
    /**
     * @return the matchAll
     */
    public boolean isMatchAll() {
        return matchAll;
    }
    /**
     * @param matchAll the matchAll to set
     */
    public void setMatchAll(boolean matchAll) {
        this.matchAll = matchAll;
    }
}

CREATE TABLE IF NOT EXISTS sandbox_data_records
(
    id          SERIAL PRIMARY KEY,
    entity_name VARCHAR(256) NOT NULL,
    data        BYTEA        NOT NULL
);
CREATE INDEX ix_sandbox_data_records_entity_name ON sandbox_data_records USING BTREE (entity_name);

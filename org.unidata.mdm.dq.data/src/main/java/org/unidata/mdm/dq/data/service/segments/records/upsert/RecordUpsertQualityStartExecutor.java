/*
 *
 *  * Unidata Platform
 *  * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *  *
 *  * Commercial License
 *  * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *  *
 *  * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 *  * For clarification or additional options, please contact: info@unidata-platform.com
 *  * -------
 *  * Disclaimer:
 *  * -------
 *  * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 *  * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 *  * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 *  * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 *
 */

package org.unidata.mdm.dq.data.service.segments.records.upsert;

import java.util.Objects;

import javax.annotation.Nonnull;

import org.springframework.stereotype.Component;
import org.unidata.mdm.core.type.timeline.Timeline;
import org.unidata.mdm.data.type.data.OriginRecord;
import org.unidata.mdm.data.type.keys.RecordKeys;
import org.unidata.mdm.dq.data.context.RecordQualityContext;
import org.unidata.mdm.dq.data.dto.RecordUpsertQualityResult;
import org.unidata.mdm.dq.data.module.DataQualityDataModule;
import org.unidata.mdm.system.type.pipeline.Start;

/**
 * @author Alexey Tsarapkin
 */
@Component(RecordUpsertQualityStartExecutor.SEGMENT_ID)
public class RecordUpsertQualityStartExecutor extends Start<RecordQualityContext, RecordUpsertQualityResult> {
    /**
     * This segment ID.
     */
    public static final String SEGMENT_ID = DataQualityDataModule.MODULE_ID + "[RECORD_UPSERT_QUALITY_START]";
    /**
     * This segment description.
     */
    private static final String SEGMENT_DESCRIPTION = DataQualityDataModule.MODULE_ID + ".record.upsert.quality.start.description";
    /**
     * Constructor.
     */
    public RecordUpsertQualityStartExecutor() {
        super(SEGMENT_ID, SEGMENT_DESCRIPTION, RecordQualityContext.class, RecordUpsertQualityResult.class);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void start(@Nonnull RecordQualityContext ctx) {
        // Nothing
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String subject(RecordQualityContext ctx) {
        Timeline<OriginRecord> t = ctx.nextTimeline();
        return Objects.nonNull(t) && Objects.nonNull(t.getKeys()) ? t.<RecordKeys>getKeys().getEntityName() : null;
    }
}

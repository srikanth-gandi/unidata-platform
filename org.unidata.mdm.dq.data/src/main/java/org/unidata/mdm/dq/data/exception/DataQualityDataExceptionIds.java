/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.dq.data.exception;

import org.unidata.mdm.dq.data.module.DataQualityDataModule;
import org.unidata.mdm.system.exception.ExceptionId;

/**
 * @author Mikhail Mikhailov on Sep 7, 2021
 */
public class DataQualityDataExceptionIds {
    /**
     * Constructor.
     */
    private DataQualityDataExceptionIds() {
        super();
    }
    /**
     * Code attribute attr type mismatch.
     */
    public static final ExceptionId EX_DQ_DATA_CODE_ATTRIBUTE_TYPE_MISMATCH =
            new ExceptionId("EX_DQ_DATA_CODE_ATTRIBUTE_TYPE_MISMATCH", DataQualityDataModule.MODULE_ID + ".code.attribute.type.mismatch");
    /**
     * Code attribute data type mismatch.
     */
    public static final ExceptionId EX_DQ_DATA_CODE_DATA_TYPE_MISMATCH =
            new ExceptionId("EX_DQ_DATA_CODE_DATA_TYPE_MISMATCH", DataQualityDataModule.MODULE_ID + ".code.data.type.mismatch");
}

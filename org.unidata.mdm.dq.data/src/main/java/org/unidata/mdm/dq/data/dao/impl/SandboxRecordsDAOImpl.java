/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.dq.data.dao.impl;

import java.util.Collections;
import java.util.List;
import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.unidata.mdm.core.util.Maps;
import org.unidata.mdm.dq.data.dao.SandboxRecordsDAO;
import org.unidata.mdm.dq.data.dao.po.SandboxRecordPO;
import org.unidata.mdm.system.dao.impl.BaseDAOImpl;

@Repository
public class SandboxRecordsDAOImpl extends BaseDAOImpl implements SandboxRecordsDAO {

    private static final RowMapper<SandboxRecordPO> SANDBOX_RECORD_ROW_MAPPER = (rs, rowNum) -> new SandboxRecordPO(
            rs.getLong("id"),
            rs.getString("entity_name"),
            rs.getBytes("data"));

    private final String insertRecordQuery;
    private final String updateRecordQuery;
    private final String findByIdQuery;
    private final String deleteByIdsQuery;
    private final String deleteByEntityNameQuery;
    private final String searchRecordsQuery;
    private final String findRecordsByIdsQuery;
    private final String countRecordsByEntityNameQuery;

    @Autowired
    public SandboxRecordsDAOImpl(
            @Qualifier("dataQualityDataSource") final DataSource dataSource,
            @Qualifier("sandbox-sql") final Properties sql
    ) {
        super(dataSource);
        insertRecordQuery = sql.getProperty("INSERT_DATA_RECORD");
        updateRecordQuery = sql.getProperty("UPDATE_DATA_RECORD");
        findByIdQuery = sql.getProperty("SELECT_BY_ID");
        deleteByIdsQuery = sql.getProperty("DELETE_BY_IDS");
        deleteByEntityNameQuery = sql.getProperty("DELETE_BY_ENTITY_NAME");
        searchRecordsQuery = sql.getProperty("SELECT_BY_ENTITY_NAME");
        findRecordsByIdsQuery = sql.getProperty("SELECT_BY_RECORDS_IDS");
        countRecordsByEntityNameQuery = sql.getProperty("COUNT_IDS_BY_ENTITY_NAME");
    }

    @Override
    public long save(final SandboxRecordPO sandboxRecord) {
        if (sandboxRecord.getId() == null) {
            return getNamedJdbcTemplate().queryForObject(
                    insertRecordQuery,
                    Maps.of("entityName", sandboxRecord.getEntityName(), "data", sandboxRecord.getData()),
                    Long.class
            );
        }
        else {
            getNamedJdbcTemplate().update(
                    updateRecordQuery,
                    Maps.of("id", sandboxRecord.getId(), "entityName", sandboxRecord.getEntityName(), "data", sandboxRecord.getData())
            );
            return sandboxRecord.getId();
        }

    }

    @Override
    public SandboxRecordPO findRecordById(final long recordId) {
        return getNamedJdbcTemplate().queryForObject(
                findByIdQuery,
                Collections.singletonMap("id", recordId),
                SANDBOX_RECORD_ROW_MAPPER
        );
    }

    @Override
    public void deleteByIds(final List<Long> recordsIds) {
        getNamedJdbcTemplate().update(deleteByIdsQuery, Collections.singletonMap("ids", recordsIds));
    }

    @Override
    public void deleteByEntityName(final String entityName) {
        getNamedJdbcTemplate().update(deleteByEntityNameQuery, Collections.singletonMap("entityName", entityName));
    }

    @Override
    public List<SandboxRecordPO> find(final String entity, final int page, final int count) {
        return getNamedJdbcTemplate().query(
                searchRecordsQuery,
                Maps.of("entityName", entity, "limit", count, "offset", page * count),
                SANDBOX_RECORD_ROW_MAPPER
        );
    }

    @Override
    public List<SandboxRecordPO> findByRecordsIds(List<Long> recordsIds) {
        return getNamedJdbcTemplate().query(
                findRecordsByIdsQuery,
                Collections.singletonMap("recordsIds", recordsIds),
                SANDBOX_RECORD_ROW_MAPPER
        );
    }

    @Override
    public long count(final String entityName) {
        return getNamedJdbcTemplate().queryForObject(
                countRecordsByEntityNameQuery,
                Collections.singletonMap("entityName", entityName),
                Long.class
        );
    }
}

/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.dq.data.dto;

import java.util.Objects;

import org.unidata.mdm.dq.core.dto.DataQualityResult;
import org.unidata.mdm.system.dto.ExecutionResult;
import org.unidata.mdm.system.type.pipeline.PipelineOutput;
import org.unidata.mdm.system.type.pipeline.fragment.FragmentId;
import org.unidata.mdm.system.type.pipeline.fragment.OutputFragment;

/**
 * @author Alexey Tsarapkin
 * On GET operation only errors and spots are returned (spots do not contain attribute values).
 */
public class RecordGetQualityResult implements OutputFragment<RecordGetQualityResult>, PipelineOutput, ExecutionResult {
    /**
     * Fragment id.
     */
    public static final FragmentId<RecordGetQualityResult> ID = new FragmentId<>("GET_DATA_QUALITY_RECORDS_RESULT");
    /**
     * Execution result by rules.
     */
    private DataQualityResult payload;
    /**
     * Constructor.
     */
    public RecordGetQualityResult() {
        super();
    }
    /**
     * Constructor.
     * @param payload the results
     */
    public RecordGetQualityResult(DataQualityResult payload) {
        this();
        this.payload = payload;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public FragmentId<RecordGetQualityResult> fragmentId() {
        return ID;
    }
    /**
     * Returns true, if has some payload.
     * @return true, if has some payload
     */
    public boolean hasPayload() {
        return Objects.nonNull(payload);
    }
    /**
     * Gets rules excution state.
     * @return rules excution state
     */
    public DataQualityResult getPayload() {
        return payload;
    }
    /**
     * Sets DQ rules execution results.
     * @param payload  the results
     */
    public void setPayload(DataQualityResult payload) {
        this.payload = payload;
    }
}

/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.data.service.segments.records.batch;

import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import org.unidata.mdm.matching.data.dto.RecordsMatchingBulkResult;
import org.unidata.mdm.matching.data.module.MatchingDataModule;
import org.unidata.mdm.matching.data.type.apply.batch.RecordMatchingDeleteBatchSetAccumulator;
import org.unidata.mdm.system.type.pipeline.batch.BatchedStart;

/**
 * @author Sergey Murskiy on 13.09.2021
 */
@Component(RecordsDeleteMatchingStartExecutor.SEGMENT_ID)
public class RecordsDeleteMatchingStartExecutor extends BatchedStart<RecordMatchingDeleteBatchSetAccumulator,
        RecordsMatchingBulkResult> {

    /**
     * This segment ID.
     */
    public static final String SEGMENT_ID = MatchingDataModule.MODULE_ID + "[BATCH_RECORD_DELETE_MATCHING_START]";
    /**
     * Localized message code.
     */
    public static final String SEGMENT_DESCRIPTION = MatchingDataModule.MODULE_ID + ".batch.record.delete.matching.start.description";

    /**
     * Constructor.
     */
    public RecordsDeleteMatchingStartExecutor() {
        super(SEGMENT_ID, SEGMENT_DESCRIPTION, RecordMatchingDeleteBatchSetAccumulator.class,
                RecordsMatchingBulkResult.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void start(@NonNull RecordMatchingDeleteBatchSetAccumulator ctx) {
        // Nothing.
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String subject(RecordMatchingDeleteBatchSetAccumulator ctx) {
        // No subjects for batched segments so far
        return null;
    }
}

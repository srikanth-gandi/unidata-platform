/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.data.service.segments;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.core.service.UPathService;
import org.unidata.mdm.core.type.data.Attribute;
import org.unidata.mdm.core.type.data.DataRecord;
import org.unidata.mdm.core.type.upath.UPathResult;
import org.unidata.mdm.matching.core.type.data.MatchingRecord;
import org.unidata.mdm.matching.core.type.data.MatchingRecordKey;
import org.unidata.mdm.matching.core.type.data.field.MatchingRecordField;
import org.unidata.mdm.matching.core.type.model.instance.MatchingColumnMappingElement;
import org.unidata.mdm.matching.core.type.model.instance.MatchingTableMappingElement;
import org.unidata.mdm.matching.core.type.model.instance.NamespaceAssignmentElement;
import org.unidata.mdm.matching.data.context.DataMatchingCalculationContext;
import org.unidata.mdm.matching.data.converter.MatchingFieldConverter;
import org.unidata.mdm.matching.data.module.MatchingDataModule;
import org.unidata.mdm.matching.data.type.state.RecordMatchingState;
import org.unidata.mdm.system.type.pipeline.Point;
import org.unidata.mdm.system.type.pipeline.Start;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * @author Sergey Murskiy on 21.08.2021
 */
@Component(DataRecordMappingExecutor.SEGMENT_ID)
public class DataRecordMappingExecutor<T extends DataMatchingCalculationContext<?>> extends Point<T> {
    /**
     * This segment ID.
     */
    public static final String SEGMENT_ID = MatchingDataModule.MODULE_ID + "[DATA_RECORD_MATCHING_MAPPING]";
    /**
     * Localized message code.
     */
    public static final String SEGMENT_DESCRIPTION = MatchingDataModule.MODULE_ID + ".record.matching.mapping.description";

    /**
     * Constructor.
     */
    public DataRecordMappingExecutor() {
        super(SEGMENT_ID, SEGMENT_DESCRIPTION);
    }

    /**
     * Upath service.
     */
    @Autowired
    private UPathService uPathService;

    /**
     * {@inheritDoc}
     */
    @Override
    public void point(T ctx) {

        RecordMatchingState state = ctx.matchingState();
        NamespaceAssignmentElement assignment = ctx.getAssignment();

        for (Map.Entry<MatchingRecordKey, DataRecord> entry : state.getCalculations().entrySet()) {
            for (MatchingTableMappingElement mapping : assignment.getAssignedMappings()) {

                MatchingRecord matchingRecord = new MatchingRecord();
                matchingRecord.setKey(entry.getKey());

                Map<String, MatchingRecordField<?>> fields = new HashMap<>();

                for (MatchingColumnMappingElement fieldMapping : mapping.getMappings()) {
                    UPathResult uPathResult = uPathService.upathGet(fieldMapping.getPath(), entry.getValue());

                    if (CollectionUtils.isEmpty(uPathResult.getAttributes())) {
                        continue;
                    }

                    Attribute attribute = uPathResult.getAttributes().get(0);

                    if (Objects.nonNull(attribute)) {
                        fields.put(fieldMapping.getColumnName(),
                                MatchingFieldConverter.to(fieldMapping.getColumnName(), attribute));
                    }
                }

                matchingRecord.setFields(fields);

                state.putInsert(mapping.getMatchingTableName(), matchingRecord);
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean supports(Start<?, ?> start) {
        return DataMatchingCalculationContext.class.isAssignableFrom(start.getInputTypeClass());
    }
}

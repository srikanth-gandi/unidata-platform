/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.data.type.apply.batch;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.unidata.mdm.data.type.apply.batch.AbstractBatchSetAccumulator;
import org.unidata.mdm.matching.core.type.data.MatchingRecord;
import org.unidata.mdm.matching.core.type.data.MatchingRecordKey;
import org.unidata.mdm.matching.core.type.execution.MatchingRealTimeInput;
import org.unidata.mdm.matching.core.type.model.instance.MatchingRuleSetElement;
import org.unidata.mdm.matching.data.context.DataMatchingContext;
import org.unidata.mdm.matching.data.type.apply.RecordMatchingUpsertChangeSet;
import org.unidata.mdm.system.context.InputFragmentHolder;
import org.unidata.mdm.system.dto.ExecutionResult;
import org.unidata.mdm.system.type.pipeline.fragment.FragmentId;
import org.unidata.mdm.system.type.pipeline.fragment.InputFragment;
import org.unidata.mdm.system.type.pipeline.fragment.InputFragmentCollector;
import org.unidata.mdm.system.type.pipeline.fragment.InputFragmentContainer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Supplier;

/**
 * @author Sergey Murskiy on 02.09.2021
 */
public abstract class AbstractMatchingBatchSetAccumulator<T extends DataMatchingContext, O extends ExecutionResult,
        X extends AbstractMatchingBatchSetAccumulator<T, O, X>> extends AbstractBatchSetAccumulator<T, O>
        implements InputFragmentCollector<X>, InputFragmentContainer {

    /**
     * Matching inputs.
     */
    protected final Map<MatchingRuleSetElement, MatchingRealTimeInput> matchingInputs;
    /**
     * Matching inserts.
     */
    protected final Map<String, List<MatchingRecord>> inserts;
    /**
     * Matching deletes.
     */
    protected final Map<String, List<MatchingRecordKey>> deletes;
    /**
     * Fragments map.
     */
    protected Map<FragmentId<? extends InputFragment<?>>, InputFragmentHolder> fragments;

    /**
     * Constructor.
     *
     * @param commitSize commit size
     */
    protected AbstractMatchingBatchSetAccumulator(int commitSize) {
        super(commitSize);
        this.matchingInputs = new HashMap<>();
        this.inserts = new HashMap<>();
        this.deletes = new HashMap<>();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void discharge() {
        super.discharge();
        matchingInputs.clear();
        inserts.clear();
        deletes.clear();
    }

    /**
     * Accumulates objects.
     *
     * @param ctx the batch set
     */
    @Override
    public void accumulate(T ctx) {

        RecordMatchingUpsertChangeSet batchSet = ctx.changeSet();

        if (batchSet.isEmpty()) {
            return;
        }

        MatchingRealTimeInput matchingInput = batchSet.getMatchingInput();

        if (Objects.nonNull(matchingInput) && !matchingInput.isEmpty()) {
            batchSet.getMatchingSets().forEach(set ->
                    matchingInputs.computeIfAbsent(set, k -> new MatchingRealTimeInput()).append(batchSet.getMatchingInput())
            );
        }

        if (MapUtils.isNotEmpty(batchSet.getDeletes())) {
            batchSet.getDeletes().forEach((matchingTableName, keys) ->
                    deletes.computeIfAbsent(matchingTableName, k -> new ArrayList<>()).addAll(keys)
            );
        }

        if (MapUtils.isNotEmpty(batchSet.getInserts())) {
            batchSet.getInserts().forEach((matchingTableName, keys) ->
                    inserts.computeIfAbsent(matchingTableName, k -> new ArrayList<>()).addAll(keys));
        }

        batchSet.clear();
    }

    /**
     * Adds a singleton fragment for an ID using a supplier.
     *
     * @param s the supplier
     * @return self
     */
    @Override
    public X fragment(Supplier<? extends InputFragment<?>> s) {
        return fragment(s.get());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public X fragment(InputFragment<?> f) {
        if (Objects.nonNull(f)) {

            if (fragments == null) {
                fragments = new IdentityHashMap<>();
            }

            fragments.put(f.fragmentId(), InputFragmentHolder.of(f));
        }
        return self();
    }

    /**
     * Adds a fragment collection for an ID using a supplier.
     *
     * @param s the supplier
     * @return self
     */
    @Override
    public X fragments(Supplier<Collection<? extends InputFragment<?>>> s) {
        return fragments(s.get());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public X fragments(Collection<? extends InputFragment<?>> fc) {
        if (CollectionUtils.isNotEmpty(fc)) {

            if (fragments == null) {
                fragments = new IdentityHashMap<>();
            }

            fragments.put(fc.iterator().next().fragmentId(), InputFragmentHolder.of(fc));
        }
        return self();
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    @Override
    public <C extends InputFragment<C>> C fragment(FragmentId<C> f) {

        if (MapUtils.isEmpty(fragments)) {
            return null;
        }

        InputFragmentHolder h = fragments.get(f);
        if (Objects.isNull(h) || !h.isSingle()) {
            return null;
        }

        return (C) h.getSingle();
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings({"unchecked", "DuplicatedCode"})
    @Override
    public <C extends InputFragment<C>> Collection<C> fragments(FragmentId<C> f) {

        if (MapUtils.isEmpty(fragments)) {
            return Collections.emptyList();
        }

        InputFragmentHolder h = fragments.get(f);
        if (Objects.isNull(h) || !h.isMultiple()) {
            return Collections.emptyList();
        }

        return (Collection<C>) h.getMultiple();
    }

    /**
     * Gets matching inputs.
     *
     * @return inputs.
     */
    public Map<MatchingRuleSetElement, MatchingRealTimeInput> getMatchingInputs() {
        return matchingInputs;
    }

    /**
     * Gets matching record deletes.
     *
     * @return deletes
     */
    public Map<String, List<MatchingRecordKey>> getDeletes() {
        return deletes;
    }

    /**
     * Gets matching record inserts.
     *
     * @return inserts
     */
    public Map<String, List<MatchingRecord>> getInserts() {
        return inserts;
    }

    /**
     * This cast trick.
     *
     * @return self
     */
    @SuppressWarnings("unchecked")
    protected X self() {
        return (X) this;
    }
}

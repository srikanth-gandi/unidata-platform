/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.data.dto;

import org.unidata.mdm.matching.core.dto.MatchingResult;
import org.unidata.mdm.system.dto.ExecutionResult;
import org.unidata.mdm.system.type.pipeline.PipelineOutput;
import org.unidata.mdm.system.type.pipeline.fragment.FragmentId;
import org.unidata.mdm.system.type.pipeline.fragment.OutputFragment;

import java.util.Objects;

/**
 * @author Sergey Murskiy on 22.08.2021
 */
public class RecordMatchingGetResult implements OutputFragment<RecordMatchingGetResult>, PipelineOutput, ExecutionResult {
    /**
     * Fragment id.
     */
    public static final FragmentId<RecordMatchingGetResult> ID = new FragmentId<>("GET_MATCHING_RECORDS_RESULT");
    /**
     * Execution result by rules.
     */
    private MatchingResult payload;
    /**
     * Constructor.
     */
    public RecordMatchingGetResult() {
        super();
    }

    /**
     * Constructor.
     * @param payload the results
     */
    public RecordMatchingGetResult(MatchingResult payload) {
        this();
        this.payload = payload;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FragmentId<RecordMatchingGetResult> fragmentId() {
        return ID;
    }

    /**
     * Returns true, if has some payload.
     * @return true, if has some payload
     */
    public boolean hasPayload() {
        return Objects.nonNull(payload);
    }

    /**
     * Gets rules execution state.
     * @return rules execution state
     */
    public MatchingResult getPayload() {
        return payload;
    }

    /**
     * Sets matching rules execution results.
     * @param payload the results
     */
    public void setPayload(MatchingResult payload) {
        this.payload = payload;
    }
}

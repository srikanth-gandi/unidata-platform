/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.data.service.segments.records.upsert;

import org.springframework.stereotype.Component;
import org.unidata.mdm.core.type.timeline.TimeInterval;
import org.unidata.mdm.core.type.timeline.Timeline;
import org.unidata.mdm.data.context.UpsertRequestContext;
import org.unidata.mdm.data.type.data.EtalonRecord;
import org.unidata.mdm.data.type.data.OriginRecord;
import org.unidata.mdm.data.type.keys.RecordKeys;
import org.unidata.mdm.matching.core.type.data.MatchingRecordKey;
import org.unidata.mdm.matching.core.type.model.instance.NamespaceAssignmentElement;
import org.unidata.mdm.matching.data.context.RecordMatchingUpsertContext;
import org.unidata.mdm.matching.data.module.MatchingDataModule;
import org.unidata.mdm.matching.data.type.state.RecordMatchingState;
import org.unidata.mdm.system.type.pipeline.Point;
import org.unidata.mdm.system.type.pipeline.Start;
import org.unidata.mdm.system.util.TimeBoundaryUtils;

import java.util.Date;

/**
 * @author Sergey Murskiy on 07.08.2021
 */
@Component(RecordUpsertMatchingPrepareExecutor.SEGMENT_ID)
public class RecordUpsertMatchingPrepareExecutor extends Point<RecordMatchingUpsertContext> {
    /**
     * This segment ID.
     */
    public static final String SEGMENT_ID = MatchingDataModule.MODULE_ID + "[RECORD_UPSERT_MATCHING_PREPARE]";
    /**
     * Localized message code.
     */
    public static final String SEGMENT_DESCRIPTION = MatchingDataModule.MODULE_ID + ".record.upsert.matching.prepare.description";

    /**
     * Constructor.
     */
    public RecordUpsertMatchingPrepareExecutor() {
        super(SEGMENT_ID, SEGMENT_DESCRIPTION);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void point(RecordMatchingUpsertContext ctx) {

        Timeline<OriginRecord> current = ctx.currentTimeline();
        Timeline<OriginRecord> next = ctx.nextTimeline();

        collectUpdates(current, next, ctx);
        collectDeletes(current, ctx);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean supports(Start<?, ?> start) {
        return RecordMatchingUpsertContext.class.isAssignableFrom(start.getInputTypeClass());
    }

    private void collectDeletes(Timeline<OriginRecord> current, RecordMatchingUpsertContext ctx) {

        if (current.isEmpty()) {
            return;
        }

        RecordMatchingState state = ctx.matchingState();
        UpsertRequestContext payload = ctx.getPayload();

        RecordKeys keys = current.getKeys();
        Timeline<OriginRecord> reduced = current.reduceBy(
                current.earliestFrom(payload.getValidFrom()),
                current.latestTo(payload.getValidTo()));

        reduced.forEach(interval -> state.putDelete(MatchingRecordKey.of(
                ctx.getNamespace().getId(),
                keys.getEntityName(),
                keys.getEtalonKey().getId(),
                interval.getValidFrom(),
                interval.getValidTo())));
    }

    private void collectUpdates(Timeline<OriginRecord> current, Timeline<OriginRecord> next,
                                RecordMatchingUpsertContext ctx) {

        if (next.isEmpty()) {
            return;
        }

        NamespaceAssignmentElement assignment = ctx.getAssignment();
        RecordMatchingState state = ctx.matchingState();

        Timeline<OriginRecord> selected = selectFrame(current, next, ctx);
        for (TimeInterval<OriginRecord> i : selected) {

            if (!i.hasCalculationResult()) {
                continue;
            }

            EtalonRecord calculationResult = i.getCalculationResult();
            MatchingRecordKey key = MatchingRecordKey.of(
                    assignment.getNameSpace().getId(),
                    assignment.getTypeName(),
                    calculationResult.getInfoSection().getEtalonKey().getId(),
                    calculationResult.getInfoSection().getValidFrom(),
                    calculationResult.getInfoSection().getValidTo()
            );

            state.putCalculation(i, key, calculationResult);
        }
    }

    @SuppressWarnings("DuplicatedCode")
    private Timeline<OriginRecord> selectFrame(Timeline<OriginRecord> current, Timeline<OriginRecord> next,
                                               RecordMatchingUpsertContext ctx) {

        UpsertRequestContext payload = ctx.getPayload();
        Timeline<OriginRecord> reduced = current.reduceBy(
                current.earliestFrom(payload.getValidFrom()),
                current.latestTo(payload.getValidTo()));

        Date from = TimeBoundaryUtils.selectEarliest(reduced.isEmpty() ? payload.getValidFrom() :
                reduced.first().getValidFrom(), payload.getValidFrom());
        Date to = TimeBoundaryUtils.selectLatest(reduced.isEmpty() ? payload.getValidTo() : reduced.last().getValidTo(),
                payload.getValidTo());

        return next.reduceBy(next.earliestFrom(from), next.latestTo(to));
    }
}

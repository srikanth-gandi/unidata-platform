/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.data.context;

import org.unidata.mdm.data.type.data.OriginRecord;
import org.unidata.mdm.matching.data.service.segments.records.delete.RecordDeleteMatchingStartExecutor;

/**
 * @author Sergey Murskiy on 21.08.2021
 */
public class RecordMatchingDeleteContext extends DataMatchingContext implements DataMatchingCalculationContext<OriginRecord> {
    /**
     * SVUID.
     */
    private static final long serialVersionUID = -468886243461568875L;
    /**
     * Physically remove data validFrom the storage.
     */
    private final boolean wipe;
    /**
     * A version for inactive period should be put above all.
     */
    private final boolean inactivatePeriod;
    /**
     * Inactivate origin flag.
     */
    private final boolean inactivateOrigin;
    /**
     * Inactivate etalon flag.
     */
    private final boolean inactivateEtalon;

    /**
     * Constructor.
     *
     * @param b the builder
     */
    private RecordMatchingDeleteContext(RecordMatchingDeleteContextBuilder b) {
        super(b);

        this.wipe = b.wipe;
        this.inactivateEtalon = b.inactivateEtalon;
        this.inactivateOrigin = b.inactivateOrigin;
        this.inactivatePeriod = b.inactivatePeriod;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getStartTypeId() {
        return RecordDeleteMatchingStartExecutor.SEGMENT_ID;
    }

    /**
     * Gets 'wipe' flag.
     *
     * @return is wipe
     */
    public boolean isWipe() {
        return wipe;
    }

    /**
     * Gets 'inactivatePeriod' flag
     *
     * @return is inactivate period
     */
    public boolean isInactivatePeriod() {
        return inactivatePeriod;
    }

    /**
     * Gets 'inactivateOrigin' flag.
     *
     * @return is inactivate origin
     */
    public boolean isInactivateOrigin() {
        return inactivateOrigin;
    }

    /**
     * Gets 'inactivateEtalon' flag.
     *
     * @return is inactivate etalon
     */
    public boolean isInactivateEtalon() {
        return inactivateEtalon;
    }

    /**
     * Builder shorthand.
     *
     * @return builder
     */
    public static RecordMatchingDeleteContextBuilder builder() {
        return new RecordMatchingDeleteContextBuilder();
    }

    /**
     * Builder for the context.
     */
    public static class RecordMatchingDeleteContextBuilder extends DataMatchingContextBuilder<RecordMatchingDeleteContextBuilder> {
        /**
         * Physically remove data validFrom the storage.
         */
        private boolean wipe;
        /**
         * A version for inactive period should be put above all.
         */
        private boolean inactivatePeriod;
        /**
         * Inactivate origin flag.
         */
        private boolean inactivateOrigin;
        /**
         * Inactivate etalon flag.
         */
        private boolean inactivateEtalon;

        /**
         * Sets wipe flag.
         *
         * @param wipe is wipe
         * @return self
         */
        public RecordMatchingDeleteContextBuilder wipe(boolean wipe) {
            this.wipe = wipe;
            return self();
        }

        /**
         * Sets inactivatePeriod flag.
         *
         * @param inactivatePeriod is inactivate period
         * @return self
         */
        public RecordMatchingDeleteContextBuilder inactivatePeriod(boolean inactivatePeriod) {
            this.inactivatePeriod = inactivatePeriod;
            return self();
        }

        /**
         * Sets inactivate origin.
         *
         * @param inactivateOrigin is inactivate origin
         * @return self
         */
        public RecordMatchingDeleteContextBuilder inactivateOrigin(boolean inactivateOrigin) {
            this.inactivateOrigin = inactivateOrigin;
            return self();
        }

        /**
         * Sets inactivate etalon.
         * @param inactivateEtalon is inactivate etalon
         * @return self
         */
        public RecordMatchingDeleteContextBuilder inactivateEtalon(boolean inactivateEtalon) {
            this.inactivateEtalon = inactivateEtalon;
            return self();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public RecordMatchingDeleteContext build() {
            return new RecordMatchingDeleteContext(this);
        }
    }
}

/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.data.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.matching.core.context.MatchingContext;
import org.unidata.mdm.matching.core.service.MatchingService;
import org.unidata.mdm.matching.data.configuration.MatchingDataConfigurationConstants;
import org.unidata.mdm.matching.data.service.RecordMatchingBatchSetProcessor;
import org.unidata.mdm.matching.data.type.apply.batch.AbstractMatchingBatchSetAccumulator;
import org.unidata.mdm.matching.data.type.apply.batch.RecordMatchingDeleteBatchSetAccumulator;
import org.unidata.mdm.matching.data.type.apply.batch.RecordMatchingUpsertBatchSetAccumulator;
import org.unidata.mdm.system.type.annotation.ConfigurationRef;
import org.unidata.mdm.system.type.configuration.ConfigurationValue;

/**
 * @author Sergey Murskiy on 13.09.2021
 */
@Component
public class RecordMatchingBatchSetProcessorImpl implements RecordMatchingBatchSetProcessor {

    /**
     * Is real-time matching enabled.
     */
    @ConfigurationRef(MatchingDataConfigurationConstants.PROPERTY_REAL_TIME_MATCHING_ENABLED)
    private ConfigurationValue<Boolean> realTimeMatchingEnabled;

    /**
     * Matching service.
     */
    @Autowired
    private MatchingService matchingService;

    /**
     * {@inheritDoc}
     */
    @Override
    public void apply(RecordMatchingUpsertBatchSetAccumulator bsa) {
        applyMatching(bsa);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void apply(RecordMatchingDeleteBatchSetAccumulator bsa) {
        applyMatching(bsa);
    }

    private void applyMatching(AbstractMatchingBatchSetAccumulator<?, ?, ?> bsa) {

        matchingService.update(MatchingContext.builder()
                .insert(bsa.getInserts())
                .delete(bsa.getDeletes())
                .build());

        if (Boolean.TRUE.equals(realTimeMatchingEnabled.getValue())) {
            bsa.getMatchingInputs().forEach((set, input) -> matchingService.match(MatchingContext.builder()
                    .matchingInput(input)
                    .matchingSets(set)
                    .skipResultCalculation(true)
                    .build()));
        }
    }
}

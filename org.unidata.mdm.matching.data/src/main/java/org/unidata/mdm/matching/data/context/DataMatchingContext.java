/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.data.context;

import org.unidata.mdm.data.context.AbstractRecordIdentityContext;
import org.unidata.mdm.matching.core.type.model.instance.NamespaceAssignmentElement;
import org.unidata.mdm.system.context.CommonRequestContext;
import org.unidata.mdm.system.type.namespace.NameSpace;

import java.util.Objects;

/**
 * @author Sergey Murskiy on 12.09.2021
 */
public abstract class DataMatchingContext extends CommonRequestContext implements DataMatchingWriteContext {
    /**
     * SVUID.
     */
    private static final long serialVersionUID = -1620841570311231187L;

    /**
     * Assignment.
     */
    private final transient NamespaceAssignmentElement assignment;
    /**
     * Namespace.
     */
    private final transient NameSpace namespace;
    /**
     * The actual context, being processed.
     */
    private final AbstractRecordIdentityContext payload;

    /**
     * Constructor.
     *
     * @param b the builder
     */
    protected DataMatchingContext(DataMatchingContextBuilder<?> b) {
        super(b);
        this.assignment = b.assignment;
        this.payload = b.payload;
        this.namespace = b.namespace;
    }

    /**
     * Returns true, if the context contains assignment.
     *
     * @return true, if the context contains assignment
     */
    public boolean hasAssignment() {
        return Objects.nonNull(assignment);
    }

    /**
     * @return the assignment
     */
    public NamespaceAssignmentElement getAssignment() {
        return assignment;
    }

    /**
     * @return the payload
     */
    @SuppressWarnings("unchecked")
    public <X extends AbstractRecordIdentityContext> X getPayload() {
        return (X) payload;
    }

    public NameSpace getNamespace() {
        return namespace;
    }

    /**
     * Builder for the context.
     */
    public abstract static class DataMatchingContextBuilder<X extends DataMatchingContextBuilder<X>> extends CommonRequestContextBuilder<X> {
        /**
         * Assignment.
         */
        private NamespaceAssignmentElement assignment;
        /**
         * The actual context, being processed.
         */
        private AbstractRecordIdentityContext payload;
        /**
         * Namespace.
         */
        private NameSpace namespace;

        /**
         * Sets assignment.
         *
         * @param assignment the assignment
         * @return self
         */
        public X assignment(NamespaceAssignmentElement assignment) {
            this.assignment = assignment;
            return self();
        }

        /**
         * Sets payload.
         *
         * @param payload the payload
         * @return self
         */
        public <V extends AbstractRecordIdentityContext> X payload(V payload) {
            this.payload = payload;
            return self();
        }

        /**
         * Sets namespace.
         *
         * @param namespace the namespace
         * @return self
         */
        public X namespace(NameSpace namespace) {
            this.namespace = namespace;
            return self();
        }
    }
}

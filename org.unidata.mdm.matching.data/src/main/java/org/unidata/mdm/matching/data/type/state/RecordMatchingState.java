/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.data.type.state;

import org.unidata.mdm.core.type.data.DataRecord;
import org.unidata.mdm.core.type.timeline.TimeInterval;
import org.unidata.mdm.data.type.data.OriginRecord;
import org.unidata.mdm.matching.core.type.data.MatchingRecord;
import org.unidata.mdm.matching.core.type.data.MatchingRecordKey;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Sergey Murskiy on 22.08.2021
 */
public class RecordMatchingState {
    /**
     * Data record calculations.
     */
    private final Map<MatchingRecordKey, DataRecord> calculations = new HashMap<>();
    /**
     * Identity map.
     */
    private final Map<TimeInterval<OriginRecord>, MatchingRecordKey> identity = new HashMap<>();
    /**
     * Matching inserts.
     */
    private final Map<String, List<MatchingRecord>> inserts = new HashMap<>();
    /**
     * Matching deletes.
     */
    private final List<MatchingRecordKey> deletes = new ArrayList<>();

    /**
     * Gets calculations.
     *
     * @return calculations
     */
    public Map<MatchingRecordKey, DataRecord> getCalculations() {
        return calculations;
    }

    /**
     * Gets identity map.
     *
     * @return identity map
     */
    public Map<TimeInterval<OriginRecord>, MatchingRecordKey> getIdentity() {
        return identity;
    }

    /**
     * Gets matching updates.
     *
     * @return updates
     */
    public Map<String, List<MatchingRecord>> getInserts() {
        return inserts;
    }

    /**
     * Gets matching deletes.
     *
     * @return deletes
     */
    public List<MatchingRecordKey> getDeletes() {
        return deletes;
    }

    /**
     * Puts calculation.
     *
     * @param interval   time interval
     * @param key        matching key
     * @param dataRecord data record
     */
    public void putCalculation(TimeInterval<OriginRecord> interval, MatchingRecordKey key, DataRecord dataRecord) {
        identity.put(interval, key);
        calculations.put(key, dataRecord);
    }

    /**
     * Puts delete.
     *
     * @param key matching key
     */
    public void putDelete(MatchingRecordKey key) {
        deletes.add(key);
    }

    /**
     * Puts update.
     *
     * @param matchingTableName matching table name
     * @param matchingRecord    matching record
     */
    public void putInsert(String matchingTableName, MatchingRecord matchingRecord) {
        inserts.computeIfAbsent(matchingTableName, k -> new ArrayList<>())
                .add(matchingRecord);
    }
}

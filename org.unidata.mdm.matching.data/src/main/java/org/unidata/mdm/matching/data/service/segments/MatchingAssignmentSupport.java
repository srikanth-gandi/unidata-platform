/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.data.service.segments;

import org.unidata.mdm.core.service.MetaModelService;
import org.unidata.mdm.core.type.model.EntityElement;
import org.unidata.mdm.data.configuration.DataNamespace;
import org.unidata.mdm.data.type.keys.RecordKeys;
import org.unidata.mdm.matching.core.configuration.MatchingDescriptors;
import org.unidata.mdm.matching.core.type.model.instance.MatchingModelInstance;
import org.unidata.mdm.matching.core.type.model.instance.NamespaceAssignmentElement;
import org.unidata.mdm.meta.configuration.Descriptors;
import org.unidata.mdm.meta.type.instance.DataModelInstance;
import org.unidata.mdm.system.type.namespace.NameSpace;
import org.unidata.mdm.system.util.NameSpaceUtils;

import java.util.Objects;

/**
 * @author Sergey Murskiy on 13.09.2021
 */
public interface MatchingAssignmentSupport {

    /**
     * Gets metamodel service.
     *
     * @return metamodel service
     */
    MetaModelService metaModelService();

    /**
     * Gets matching assignments.
     *
     * @param keys record keys
     * @return namespace assignment
     */
    default NamespaceAssignmentElement getAssignment(RecordKeys keys) {

        if (Objects.isNull(keys)) {
            return null;
        }

        DataModelInstance dataModelInstance = metaModelService().instance(Descriptors.DATA);
        MatchingModelInstance matchingModelInstance = metaModelService().instance(MatchingDescriptors.MATCHING);

        return getAssignment(keys, dataModelInstance, matchingModelInstance);
    }

    /**
     * Gets matching assignments.
     *
     * @param keys record keys
     * @return namespace assignment
     */
    default NamespaceAssignmentElement getAssignment(RecordKeys keys, DataModelInstance dataModelInstance,
                                                     MatchingModelInstance matchingModelInstance) {

        if (Objects.isNull(keys)) {
            return null;
        }

        EntityElement el = dataModelInstance.getElement(keys.getEntityName());

        if (Objects.isNull(el)) {
            return null;
        }

        NameSpace nameSpace = null;
        if (el.isLookup()) {
            nameSpace = DataNamespace.LOOKUP;
        } else if (el.isRegister()) {
            nameSpace = DataNamespace.REGISTER;
        }

        return matchingModelInstance.getAssignment(NameSpaceUtils.join(nameSpace, keys.getEntityName()));
    }
}

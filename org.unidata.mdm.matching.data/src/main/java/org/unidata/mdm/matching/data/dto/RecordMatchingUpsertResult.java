/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.data.dto;

import org.apache.commons.collections4.MapUtils;
import org.unidata.mdm.core.type.timeline.TimeInterval;
import org.unidata.mdm.data.type.data.OriginRecord;
import org.unidata.mdm.matching.core.dto.MatchingResult;
import org.unidata.mdm.system.dto.ExecutionResult;
import org.unidata.mdm.system.type.pipeline.PipelineOutput;
import org.unidata.mdm.system.type.pipeline.fragment.FragmentId;
import org.unidata.mdm.system.type.pipeline.fragment.OutputFragment;

import java.util.Collections;
import java.util.Map;
import java.util.Objects;

/**
 * @author Sergey Murskiy on 07.08.2021
 */
public class RecordMatchingUpsertResult implements OutputFragment<RecordMatchingUpsertResult>, PipelineOutput, ExecutionResult {
    /**
     * Fragment id.
     */
    public static final FragmentId<RecordMatchingUpsertResult> ID = new FragmentId<>("UPSERT_DATA_MATCHING_RECORDS_RESULT");
    /**
     * Execution result by rules.
     */
    private Map<TimeInterval<OriginRecord>, MatchingResult> payload;

    /**
     * Constructor.
     */
    public RecordMatchingUpsertResult() {
        super();
    }

    /**
     * Constructor.
     * @param payload the results
     */
    public RecordMatchingUpsertResult(Map<TimeInterval<OriginRecord>, MatchingResult> payload) {
        this();
        this.payload = payload;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FragmentId<RecordMatchingUpsertResult> fragmentId() {
        return ID;
    }

    /**
     * Returns true, if has some payload.
     * @return true, if has some payload
     */
    public boolean hasPayload() {
        return MapUtils.isEmpty(payload);
    }

    /**
     * Gets rules execution state.
     * @return rules execution state
     */
    public Map<TimeInterval<OriginRecord>, MatchingResult> getPayload() {
        return Objects.isNull(payload) ? Collections.emptyMap() : payload;
    }
    /**
     * Sets matching rules execution results.
     * @param payload  the results
     */
    public void setPayload(Map<TimeInterval<OriginRecord>, MatchingResult> payload) {
        this.payload = payload;
    }

}

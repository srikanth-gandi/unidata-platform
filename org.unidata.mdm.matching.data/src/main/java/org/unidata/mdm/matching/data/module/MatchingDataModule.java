/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.data.module;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.unidata.mdm.matching.data.configuration.MatchingDataConfiguration;
import org.unidata.mdm.matching.data.configuration.MatchingDataConfigurationProperty;
import org.unidata.mdm.system.type.configuration.ConfigurationProperty;
import org.unidata.mdm.system.type.module.AbstractModule;
import org.unidata.mdm.system.type.module.Dependency;
import org.unidata.mdm.system.type.pipeline.Segment;

import java.util.Arrays;
import java.util.Collection;

/**
 * @author Sergey Murskiy on 21.07.2021
 */
public class MatchingDataModule extends AbstractModule {
    /**
     * Logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(MatchingDataModule.class);

    /**
     * Module id.
     */
    public static final String MODULE_ID = "org.unidata.mdm.matching.data";

    /**
     * Module dependencies.
     */
    private static final Collection<Dependency> DEPENDENCIES = Arrays.asList(
            new Dependency("org.unidata.mdm.core", "6.0"),
            new Dependency("org.unidata.mdm.system", "6.0"),
            new Dependency("org.unidata.mdm.data", "6.0"),
            new Dependency("org.unidata.mdm.matching.core", "6.3"));

    /**
     * Matching core configuration.
     */
    @Autowired
    private MatchingDataConfiguration matchingDataConfiguration;

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<Dependency> getDependencies() {
        return DEPENDENCIES;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ConfigurationProperty<?>[] getConfigurationProperties() {
        return MatchingDataConfigurationProperty.values();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String[] getResourceBundleBasenames() {
        return new String[]{"matching_data_messages"};
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getId() {
        return MODULE_ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getVersion() {
        return "6.3";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getName() {
        return "Matching Data";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getDescription() {
        return "Matching Data module";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void install() {
        LOGGER.info("Install");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void uninstall() {
        LOGGER.info("Uninstall");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void start() {
        LOGGER.info("Start");

        addSegments(matchingDataConfiguration.getBeansOfType(Segment.class).values());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void stop() {
        LOGGER.info("Stop");
    }
}

/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.data.service.segments.records.upsert;

import org.apache.commons.collections4.MapUtils;
import org.springframework.stereotype.Component;
import org.unidata.mdm.core.type.timeline.TimeInterval;
import org.unidata.mdm.data.type.data.OriginRecord;
import org.unidata.mdm.matching.core.dto.MatchingResult;
import org.unidata.mdm.matching.core.type.data.MatchingRecordKey;
import org.unidata.mdm.matching.data.context.RecordMatchingUpsertContext;
import org.unidata.mdm.matching.data.dto.RecordMatchingUpsertResult;
import org.unidata.mdm.matching.data.module.MatchingDataModule;
import org.unidata.mdm.matching.data.type.state.RecordMatchingState;
import org.unidata.mdm.system.type.pipeline.Finish;
import org.unidata.mdm.system.type.pipeline.Start;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Sergey Murskiy on 22.08.2021
 */
@Component(RecordUpsertMatchingFinishExecutor.SEGMENT_ID)
public class RecordUpsertMatchingFinishExecutor extends Finish<RecordMatchingUpsertContext, RecordMatchingUpsertResult> {
    /**
     * This segment ID.
     */
    public static final String SEGMENT_ID = MatchingDataModule.MODULE_ID + "[RECORD_UPSERT_MATCHING_FINISH]";
    /**
     * Localized message code.
     */
    public static final String SEGMENT_DESCRIPTION = MatchingDataModule.MODULE_ID + ".record.upsert.matching.finish.description";

    /**
     * Constructor.
     */
    public RecordUpsertMatchingFinishExecutor() {
        super(SEGMENT_ID, SEGMENT_DESCRIPTION, RecordMatchingUpsertResult.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RecordMatchingUpsertResult finish(RecordMatchingUpsertContext ctx) {

        Map<MatchingRecordKey, MatchingResult> matchingResult = ctx.matchingResult();

        if (MapUtils.isEmpty(matchingResult)) {
            return new RecordMatchingUpsertResult();
        }

        RecordMatchingState state = ctx.matchingState();
        Map<TimeInterval<OriginRecord>, MatchingResult> payload = new HashMap<>(matchingResult.size());
        state.getIdentity().forEach((interval, key) -> payload.put(interval, matchingResult.get(key)));

        return new RecordMatchingUpsertResult(payload);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean supports(Start<?, ?> start) {
        return RecordMatchingUpsertContext.class.isAssignableFrom(start.getInputTypeClass());
    }
}

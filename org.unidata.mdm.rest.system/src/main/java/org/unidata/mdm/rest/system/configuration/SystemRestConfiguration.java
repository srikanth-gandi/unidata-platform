/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.system.configuration;

import java.util.EnumSet;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import javax.servlet.ServletContext;
import javax.servlet.ServletRegistration;

import org.apache.cxf.bus.spring.SpringBus;
import org.apache.cxf.transport.servlet.CXFServlet;
import org.javasimon.console.SimonConsoleServlet;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.GenericBeanDefinition;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.ContextLoaderListener;
import org.unidata.mdm.rest.system.exception.RestExceptionMapper;
import org.unidata.mdm.rest.system.service.ReceiveInInterceptor;
import org.unidata.mdm.rest.system.service.impl.ReceiveInInterceptorImpl;
import org.unidata.mdm.rest.system.service.impl.spring.ModularAnnotationConfigWebApplicationContext;
import org.unidata.mdm.system.configuration.ModuleConfiguration;
import org.unidata.mdm.system.configuration.SystemConfiguration;
import org.unidata.mdm.system.module.SystemModule;
import org.unidata.mdm.system.service.ModularPostProcessingRegistrar;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;

/**
 * @author Mikhail Mikhailov on Apr 24, 2020
 */
@Configuration
public class SystemRestConfiguration implements InitializingBean {

    @Bean
    public JacksonJaxbJsonProvider jacksonJaxbJsonProvider(ObjectMapper objectMapper) {
        return new JacksonJaxbJsonProvider(objectMapper, JacksonJaxbJsonProvider.DEFAULT_ANNOTATIONS);
    }

    @Bean
    public ReceiveInInterceptor receiveInInterceptor(final ModularPostProcessingRegistrar registrar) {
        ReceiveInInterceptorImpl inInterceptor = new ReceiveInInterceptorImpl();
        registrar.registerBeanPostProcessor(inInterceptor);
        return inInterceptor;
    }

    @Bean
    public RestExceptionMapper restExceptionMapper() {
        return new RestExceptionMapper();
    }
    /**
     * Starts the platform as a WEB application.
     * Under the hood it creates Spring context and registers servlets.
     * @param servletContext the servlet context
     */
    public static void start(final ServletContext servletContext) {

        final ModularAnnotationConfigWebApplicationContext context = new ModularAnnotationConfigWebApplicationContext();
        context.setId(SystemModule.MODULE_ID);
        context.register(SystemConfiguration.class);

        servletContext.addListener(new ContextLoaderListener(context));

        // SpringBus will be searched at runtime. See below.
        final ServletRegistration.Dynamic dispatcher = servletContext.addServlet("CXFServlet", new CXFServlet());
        dispatcher.setLoadOnStartup(2);
        dispatcher.addMapping("/api/*");

        final ServletRegistration.Dynamic simon = servletContext.addServlet("SimonConsoleServlet", new SimonConsoleServlet());
        simon.setLoadOnStartup(3);
        simon.setInitParameter("url-prefix", "/javasimon-console");
        simon.addMapping("/javasimon-console/*");

        final FilterRegistration.Dynamic corsFilter = servletContext.addFilter("CorsFilter", "org.apache.catalina.filters.CorsFilter");
        corsFilter.setInitParameter("cors.allowed.origins", "*");
        corsFilter.setInitParameter("cors.allowed.methods", "GET,POST,HEAD,OPTIONS,PUT,DELETE");
        corsFilter.setInitParameter("cors.allowed.headers", "Content-Type,X-Requested-With,accept,Origin,Access-Control-Request-Method,Access-Control-Request-Headers,Authorization,PROLONG_TTL");
        corsFilter.setInitParameter("cors.exposed.headers", "Access-Control-Allow-Origin,Access-Control-Allow-Credentials,Content-Disposition,Authorization,PROLONG_TTL");
        corsFilter.addMappingForServletNames(EnumSet.allOf(DispatcherType.class), true, "CXFServlet", "SimonConsoleServlet");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void afterPropertiesSet() throws Exception {

        // We have to add SpringBus bean to the ROOT context at runtime
        // since it will be searched there by the CXFServlet at context start stage.
        ApplicationContext ctx = ModuleConfiguration.getApplicationContext(SystemConfiguration.ID);

        AutowireCapableBeanFactory acbf = ctx.getAutowireCapableBeanFactory();
        BeanDefinitionRegistry bdr = (BeanDefinitionRegistry) acbf;

        GenericBeanDefinition gbd = new GenericBeanDefinition();
        gbd.setBeanClass(SpringBus.class);
        gbd.setScope(BeanDefinition.SCOPE_SINGLETON);

        bdr.registerBeanDefinition("cxf", gbd);
    }
}

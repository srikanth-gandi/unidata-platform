/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
/**
 * Date: 18.03.2016
 */

package org.unidata.mdm.rest.core.ro.job.descriptor;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

import org.unidata.mdm.rest.core.ro.touch.TouchRO;

/**
 * @author amagdenko
 * REST job descriptor type.
 */
public class JobDesriptorRO {
    /**
     * Param name.
     */
    private String jobName;
    /**
     * Display name.
     */
    private String displayName;
    /**
     * Display description supplier (can be used for i18n purposes).
     */
    private String description;
    /**
     * Modularity mark.
     */
    private boolean modular;
    /**
     * Modularity mark.
     */
    private boolean system;
    /**
     * Parameters.
     */
    private List<JobParameterDescriptorRO> parameters;
    /**
     * Possibly defined touches.
     */
    private List<TouchRO> touches;
    /**
     * Discovered job fractions.
     */
    private List<JobFractionRO> fractions;
    /**
     * Constructor.
     */
    public JobDesriptorRO() {
        super();
    }
    /**
     * @return the jobName
     */
    public String getJobName() {
        return jobName;
    }
    /**
     * @param jobName the jobName to set
     */
    public void setJobName(String jobName) {
        this.jobName = jobName;
    }
    /**
     * @return the displayName
     */
    public String getDisplayName() {
        return displayName;
    }
    /**
     * @param displayName the displayName to set
     */
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }
    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }
    /**
     * @return the modular
     */
    public boolean isModular() {
        return modular;
    }
    /**
     * @param modular the modular to set
     */
    public void setModular(boolean modular) {
        this.modular = modular;
    }
    /**
     * @return the system
     */
    public boolean isSystem() {
        return system;
    }
    /**
     * @param system the system to set
     */
    public void setSystem(boolean system) {
        this.system = system;
    }
    /**
     * @return parameters
     */
    public List<JobParameterDescriptorRO> getParameters() {
        return Objects.isNull(parameters) ? Collections.emptyList() : parameters;
    }
    /**
     * Sets parameters.
     * @param parameters
     */
    public void setParameters(List<JobParameterDescriptorRO> parameters) {
        this.parameters = parameters;
    }
    /**
     * @return the touches
     */
    public List<TouchRO> getTouches() {
        return Objects.isNull(touches) ? Collections.emptyList() : touches;
    }
    /**
     * @param touches the touches to set
     */
    public void setTouches(List<TouchRO> touches) {
        this.touches = touches;
    }
    /**
     * @return the fractions
     */
    public List<JobFractionRO> getFractions() {
        return fractions;
    }
    /**
     * @param fractions the fractions to set
     */
    public void setFractions(List<JobFractionRO> fractions) {
        this.fractions = fractions;
    }
}

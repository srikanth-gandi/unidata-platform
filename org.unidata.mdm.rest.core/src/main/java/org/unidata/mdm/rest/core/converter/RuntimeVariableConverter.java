/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.core.converter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;

import org.unidata.mdm.rest.core.ro.configuration.ConfigurationPropertyAvailableValuePO;
import org.unidata.mdm.rest.core.ro.configuration.ConfigurationPropertyMetaRO;
import org.unidata.mdm.rest.core.ro.configuration.ConfigurationPropertyRO;
import org.unidata.mdm.system.convert.Converter;
import org.unidata.mdm.system.type.configuration.ConfigurationProperty;
import org.unidata.mdm.system.type.configuration.ConfigurationValue;
import org.unidata.mdm.system.util.TextUtils;

/**
 * @author Mikhail Mikhailov on May 17, 2021
 * Runtime property value converter.
 */
public class RuntimeVariableConverter extends Converter<ConfigurationValue<?>, ConfigurationPropertyRO> {
    /**
     * Constructor.
     * @param to
     * @param from
     */
    public RuntimeVariableConverter() {
        super(RuntimeVariableConverter::convert, null);
    }

    private static ConfigurationPropertyRO convert(ConfigurationValue<?> p) {

        final ConfigurationProperty<?> property = p.getProperty();
        final Collection<ConfigurationPropertyAvailableValuePO> availableValues =
                property.getAvailableValues().entrySet().stream()
                        .map(v -> new ConfigurationPropertyAvailableValuePO(v.getKey(), v.getValue()))
                        .collect(Collectors.toCollection(ArrayList::new));

        String propertyKey = property.getKey();
        String propertyGroupKey = property.getGroupKey();
        return new ConfigurationPropertyRO(
                propertyKey,
                TextUtils.getText(propertyKey),
                propertyGroupKey,
                TextUtils.getText(propertyGroupKey),
                property.getPropertyType().value(),
                p.serialize(),
                new ConfigurationPropertyMetaRO(availableValues, property.isRequired(), property.isReadOnly()));
    }
}

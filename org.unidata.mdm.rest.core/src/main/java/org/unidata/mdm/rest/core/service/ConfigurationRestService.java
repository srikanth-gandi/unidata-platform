/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.core.service;

import static org.unidata.mdm.core.util.SecurityUtils.ADMIN_SYSTEM_MANAGEMENT;
import static org.unidata.mdm.core.util.SecurityUtils.PLATFORM_PARAMETERS_MANAGEMENT;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;
import java.util.stream.Collectors;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.apache.cxf.jaxrs.ext.multipart.Multipart;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.unidata.mdm.core.context.UpsertLargeObjectContext;
import org.unidata.mdm.core.context.UpsertUserEventRequestContext;
import org.unidata.mdm.core.context.UpsertUserEventRequestContext.UpsertUserEventRequestContextBuilder;
import org.unidata.mdm.core.dto.UserEventDTO;
import org.unidata.mdm.core.service.LargeObjectsService;
import org.unidata.mdm.core.service.UserService;
import org.unidata.mdm.core.type.lob.LargeObjectAcceptance;
import org.unidata.mdm.core.util.FileUtils;
import org.unidata.mdm.core.util.SecurityUtils;
import org.unidata.mdm.rest.core.converter.RuntimeVariableConverter;
import org.unidata.mdm.rest.core.ro.configuration.ConfigurationPropertyRO;
import org.unidata.mdm.rest.system.ro.ErrorResponse;
import org.unidata.mdm.rest.system.ro.RestResponse;
import org.unidata.mdm.rest.system.service.AbstractRestService;
import org.unidata.mdm.system.service.RuntimePropertiesService;
import org.unidata.mdm.system.type.configuration.ConfigurationValue;
import org.unidata.mdm.system.util.TextUtils;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

/*
 * FIXME: Refactor return types.
 */
@Path("/configuration")
@Consumes({"application/json"})
@Produces({"application/json"})
public class ConfigurationRestService extends AbstractRestService {
    /**
     * Logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(ConfigurationRestService.class);
    /**
     * Export success translation code.
     */
    private static final String CONFIG_EXPORT_SUCCESS = "app.user.events.export.config.success";
    /**
     * Export failure translation code.
     */
    private static final String CONFIG_EXPORT_FAIL = "app.user.events.export.config.fail";
    /**
     * Import success translation code.
     */
    private static final String CONFIG_IMPORT_SUCCESS = "app.user.events.import.config.success";
    /**
     * Import failure translation code.
     */
    private static final String CONFIG_IMPORT_FAIL = "app.user.events.import.config.fail";
    /**
     * User export action name.
     */
    private static final String EXPORT_ACTION_NAME = "CONFIG_EXPORT";
    /**
     * User import action name.
     */
    private static final String IMPORT_ACTION_NAME = "CONFIG_IMPORT";
    /**
     * The Constant DATA_PARAM_FILE.
     */
    private static final String DATA_PARAM_FILE = "file";
    /**
     * Runtime prop presentation comparator.
     */
    private static final Comparator<ConfigurationPropertyRO> PRESENTATION_COMPARATOR = (o1, o2) -> {

        int result = o1.getGroupCode().compareTo(o2.getGroupCode());
        if (result == 0) {
            return o1.getName().compareTo(o2.getName());
        }

        return result;
    };

    @Autowired
    private RuntimePropertiesService runtimePropertiesService;

    @Autowired
    private UserService userService;

    @Autowired
    private LargeObjectsService largeObjectsService;


    private static final RuntimeVariableConverter RUNTIME_VARIABLE_CONVERTER = new RuntimeVariableConverter();

    @GET
    @PreAuthorize("T(org.unidata.mdm.core.util.security.SecurityUtils).isAdminUser()"
            + " or"
            + " T(org.unidata.mdm.core.util.security.SecurityUtils).isReadRightsForResource('"
            + ADMIN_SYSTEM_MANAGEMENT + "," + PLATFORM_PARAMETERS_MANAGEMENT + "')")
    @Operation(
        description = "Вернуть список всех настроек.",
        method = "GET",
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = Collection.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
        }
    )
    public Response getAll() {
        return ok(runtimePropertiesService.getAll().stream()
                    .map(RUNTIME_VARIABLE_CONVERTER::to)
                    .sorted(PRESENTATION_COMPARATOR)
                    .collect(Collectors.toList()));
    }

    @GET
    @Path("{name}")
    @Operation(
        description = "Вернуть список всех настроек для группы.",
        method = "GET",
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = Collection.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
        }
    )
    public Response getByName(@Parameter(description = "Код группы") @PathParam("name") String name) {
        return ok(runtimePropertiesService.getByGroup(name).stream()
                    .map(RUNTIME_VARIABLE_CONVERTER::to)
                    .sorted(PRESENTATION_COMPARATOR)
                    .collect(Collectors.toList()));
    }

    @PUT
    @PreAuthorize("T(org.unidata.mdm.core.util.security.SecurityUtils).isAdminUser()"
            + " or"
            + " T(org.unidata.mdm.core.util.security.SecurityUtils).isUpdateRightsForResource('"
            + ADMIN_SYSTEM_MANAGEMENT + "," + PLATFORM_PARAMETERS_MANAGEMENT + "')")
    @Operation(
        description = "Обновить настройки приложения.",
        method = "PUT",
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = Collection.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
        }
    )
    public Response upsert(@Parameter(description = "Список настроек") Map<String, String> properties) {
        runtimePropertiesService.update(properties);
        return ok(Collections.emptyList());
    }

    /**
     * Export backend configuration.
     *
     * @return the response
     * @throws Exception the exception
     */
    @GET
    @PreAuthorize("T(org.unidata.mdm.core.util.security.SecurityUtils).isAdminUser()"
            + " or"
            + " T(org.unidata.mdm.core.util.security.SecurityUtils).isReadRightsForResource('"
            + ADMIN_SYSTEM_MANAGEMENT + "," + PLATFORM_PARAMETERS_MANAGEMENT + "')")
    @Path(value = "/export-config")
    @Operation(
        description = "Экспорт конфигурации.",
        method = "GET",
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = Boolean.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
        }
    )
    public Response exportProperties() {

        final String data = runtimePropertiesService.getAll().stream()
                .collect(Collectors.groupingBy(p -> p.getProperty().getGroupKey()))
                .entrySet().stream()
                .sorted(Map.Entry.comparingByKey())
                .map(this::generateGroupString)
                .collect(Collectors.joining("\n"));

        publishExportData(data, SecurityUtils.getCurrentUserName());
        return ok(new RestResponse<>(true));
    }

    private String generateGroupString(Map.Entry<String, List<ConfigurationValue<?>>> e) {
        return "# " + e.getKey() + "\n" + e.getValue().stream()
                .sorted(Comparator.comparing(p -> p.getProperty().getKey()))
                .map(this::generatePropertyString)
                .collect(Collectors.joining("\n"));
    }

    private String generatePropertyString(ConfigurationValue<?> p) {
        return "## " + TextUtils.getText(p.getProperty().getKey()) + "\n"
                + p.getProperty().getKey() + "="
                + prepareValue(p);
    }

    private String generateFileName() {
        try {

            return URLEncoder.encode("config_"
                            + DateFormatUtils.format(System.currentTimeMillis(), "yyyy-MM-dd_HH-mm-ss")
                            + ".properties",
                    StandardCharsets.UTF_8.name());

        } catch (UnsupportedEncodingException e) {
            LOGGER.error("Error generating properties file name", e);
        }
        return "backend.properties";
    }

    private String prepareValue(ConfigurationValue<?> value) {

        if (!value.hasValue()) {
            return StringUtils.EMPTY;
        }

        String raw = value.serialize();
        if (StringUtils.isBlank(raw)) {
            return StringUtils.EMPTY;
        }

        boolean multiLine = raw.indexOf('\n') != -1;
        if (multiLine) {

            StringBuilder b = new StringBuilder();
            try (StringReader sr = new StringReader(raw);
                 BufferedReader br = new BufferedReader(sr)) {

                String l;
                while ((l = br.readLine()) != null) {
                    b.append(l)
                     .append(' ')
                     .append('\\')
                     .append('\n');
                }

            } catch (IOException e) {
                LOGGER.warn("Property prepare ", e);
            }

            return b.toString();
        }

        return raw;
    }

    private void publishExportData(final String data, final String currentUserName) {

        final UpsertUserEventRequestContextBuilder eCtxb = UpsertUserEventRequestContext.builder()
                        .login(currentUserName)
                        .type(EXPORT_ACTION_NAME);

        try (final InputStream is = new ByteArrayInputStream(data.getBytes())) {

            final UpsertUserEventRequestContext eCtx = eCtxb
                            .content(TextUtils.getText(CONFIG_EXPORT_SUCCESS))
                            .build();

            final UserEventDTO userEventDTO = userService.upsert(eCtx);
            final UpsertLargeObjectContext ctx = UpsertLargeObjectContext.builder()
                            .subjectId(userEventDTO.getId())
                            .mimeType("text/plain")
                            .binary(false)
                            .input(is)
                            .filename(generateFileName())
                            .acceptance(LargeObjectAcceptance.ACCEPTED)
                            .build();

            largeObjectsService.saveLargeObject(ctx);

        } catch (IOException e) {

            LOGGER.error("Can't export backend configuration file", e);
            final UpsertUserEventRequestContext eCtx = eCtxb
                            .content(TextUtils.getText(CONFIG_EXPORT_FAIL))
                            .build();

            userService.upsert(eCtx);
        }
    }

    /**
     * Import backend configuration.
     *
     * @param fileAttachment JSON file with jobs definitions.
     * @return the response
     * @throws Exception the exception
     */
    @POST
    @PreAuthorize("T(org.unidata.mdm.core.util.security.SecurityUtils).isAdminUser()"
            + " or"
            + " T(org.unidata.mdm.core.util.security.SecurityUtils).isUpdateRightsForResource('"
            + ADMIN_SYSTEM_MANAGEMENT + "," + PLATFORM_PARAMETERS_MANAGEMENT + "')")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Path(value = "/import-config")
    @Operation(
        description = "Импорт конфигурации.",
        method = "POST",
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = Boolean.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
        }
    )
    public Response importProperties(@Parameter(description = "Импортируемый файл") @Multipart(value = DATA_PARAM_FILE) final Attachment fileAttachment) {

        if (Objects.isNull(fileAttachment)) {
            return okOrNotFound(null);
        }

        final java.nio.file.Path path = FileUtils.saveFileTempFolder(fileAttachment);
        final String currentUserName = SecurityUtils.getCurrentUserName();
        final UpsertUserEventRequestContext upsertUserEventRequestContext = UpsertUserEventRequestContext.builder()
                .login(currentUserName)
                .type(IMPORT_ACTION_NAME)
                .content(TextUtils.getText(importFile(path)))
                .build();

        userService.upsert(upsertUserEventRequestContext);

        return ok(new RestResponse<>(true));
    }

    private String importFile(java.nio.file.Path path) {

        final Properties p = new Properties();
        try (final InputStream is = new FileInputStream(path.toFile())) {

            p.load(is);
            runtimePropertiesService.update(p.entrySet().stream()
                            .collect(Collectors.toMap(e -> e.getKey().toString(), e -> e.getValue().toString())));

            return CONFIG_IMPORT_SUCCESS;
        } catch (Exception e) {
            LOGGER.error("Can't import backend configuration file", e);
            return CONFIG_IMPORT_FAIL;
        } finally {
            try {
                Files.delete(path);
            } catch (Exception e) {
                LOGGER.warn("Unable to delete temporary file [{}].", path, e);
            }
        }
    }
}

/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.core.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.core.type.job.JobFraction;
import org.unidata.mdm.rest.core.ro.job.descriptor.JobFractionRO;
import org.unidata.mdm.system.convert.Converter;

/**
 * @author Mikhail Mikhailov on Jun 18, 2021
 */
@Component
public class JobFractionConverter extends Converter<JobFraction, JobFractionRO> {

    @Autowired
    private JobParameterDescriptorConverter jobParameterDescriptorConverter;
    /**
     * Constructor.
     */
    public JobFractionConverter() {
        super();
        this.to = this::convert;
    }

    private JobFractionRO convert(JobFraction source) {

        JobFractionRO target = new JobFractionRO();

        target.setDefaultOrder(source.getOrder());
        target.setDescription(source.getDescription());
        target.setDisplayName(source.getDisplayName());
        target.setId(source.getId());
        target.setParameters(jobParameterDescriptorConverter.to(source.getParametersCollection()));

        return target;
    }
}

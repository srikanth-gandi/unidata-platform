/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
/**
 * Date: 10.03.2016
 */

package org.unidata.mdm.rest.core.ro.job.execution;

import java.time.Instant;
import java.util.List;

import org.unidata.mdm.rest.core.ro.job.ExecutionStateRO;

/**
 * FIXDOC: add file description.
 *
 * @author amagdenko
 */
public class JobExecutionRO {

    private Long id;

    private Long jobDefinitionId;

    private String jobName;

    private Instant startTime;

    private Instant endTime;

    private ExecutionStateRO state;

    private boolean restartable;

    private List<StepExecutionRO> stepExecutions;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getJobDefinitionId() {
        return jobDefinitionId;
    }

    public void setJobDefinitionId(Long jobId) {
        this.jobDefinitionId = jobId;
    }

    /**
     * @return the jobName
     */
    public String getJobName() {
        return jobName;
    }

    /**
     * @param jobName the jobName to set
     */
    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public ExecutionStateRO getState() {
        return state;
    }

    public void setState(ExecutionStateRO status) {
        this.state = status;
    }

    public Instant getStartTime() {
        return startTime;
    }

    public void setStartTime(Instant startTime) {
        this.startTime = startTime;
    }

    public Instant getEndTime() {
        return endTime;
    }

    public void setEndTime(Instant endTime) {
        this.endTime = endTime;
    }

    public List<StepExecutionRO> getStepExecutions() {
        return stepExecutions;
    }

    public void setStepExecutions(List<StepExecutionRO> stepExecutions) {
        this.stepExecutions = stepExecutions;
    }

    public boolean isRestartable() {
        return restartable;
    }

    public void setRestartable(boolean restartable) {
        this.restartable = restartable;
    }
}

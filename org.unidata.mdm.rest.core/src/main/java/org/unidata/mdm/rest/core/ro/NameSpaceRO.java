/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.core.ro;

/**
 * @author Mikhail Mikhailov on Mar 4, 2021
 */
public class NameSpaceRO {
    /**
     * Name space ID.
     */
    private String id;
    /**
     * Name space display name.
     */
    private String displayName;
    /**
     * Name space description.
     */
    private String decsription;
    /**
     * Holder module id.
     */
    private String moduleId;
    /**
     * Constructor.
     */
    public NameSpaceRO() {
        super();
    }
    /**
     * Constructor.
     * @param id the ID
     * @param displayName the DS
     * @param description the description
     * @param moduleId the owning module ID
     */
    public NameSpaceRO(String id, String displayName, String description, String moduleId) {
        super();
        this.id = id;
        this.displayName = displayName;
        this.decsription = description;
        this.moduleId = moduleId;
    }
    /**
     * @return the id
     */
    public String getId() {
        return id;
    }
    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }
    /**
     * @return the displayName
     */
    public String getDisplayName() {
        return displayName;
    }
    /**
     * @param displayName the displayName to set
     */
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
    /**
     * @return the decsription
     */
    public String getDecsription() {
        return decsription;
    }
    /**
     * @param decsription the decsription to set
     */
    public void setDecsription(String decsription) {
        this.decsription = decsription;
    }
    /**
     * @return the moduleId
     */
    public String getModuleId() {
        return moduleId;
    }
    /**
     * @param moduleId the moduleId to set
     */
    public void setModuleId(String moduleId) {
        this.moduleId = moduleId;
    }
}

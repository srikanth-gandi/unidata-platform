/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.core.ro.job.definition;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * @author Mikhail Mikhailov on Apr 29, 2021
 * Used for import/export operations.
 */
public class JobDefinitionsRO {

    private List<JobDefinitionRO> definitions;
    /**
     * Constructor.
     */
    public JobDefinitionsRO(List<JobDefinitionRO> jobs) {
        super();
        this.definitions = jobs;
    }
    /**
     * Constructor.
     */
    public JobDefinitionsRO() {
        super();
    }
    /**
     * @return the definitions
     */
    public List<JobDefinitionRO> getDefinitions() {
        return Objects.isNull(definitions) ? Collections.emptyList() : definitions;
    }
    /**
     * @param definitions the definitions to set
     */
    public void setDefinitions(List<JobDefinitionRO> jobs) {
        this.definitions = jobs;
    }
}

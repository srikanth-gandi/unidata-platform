/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.core.converter;

import java.util.stream.Collectors;

import org.unidata.mdm.core.type.load.DataImportHandlerInfo;
import org.unidata.mdm.rest.core.ro.ie.DataImportHandlerInfoRO;
import org.unidata.mdm.system.convert.Converter;

/**
 * @author Mikhail Mikhailov on May 24, 2021
 */
public class DataImportConverter extends Converter<DataImportHandlerInfo, DataImportHandlerInfoRO> {
    /**
     * Constructor.
     * @param to
     * @param from
     */
    public DataImportConverter() {
        super(DataImportConverter::convert, null);
    }

    private static DataImportHandlerInfoRO convert(DataImportHandlerInfo source) {

        DataImportHandlerInfoRO target = new DataImportHandlerInfoRO();
        target.setId(source.getId());
        target.setDescription(source.getDescription());
        target.setFormats(source.getFormats().stream().map(Enum::name).collect(Collectors.toList()));

        return target;
    }
}

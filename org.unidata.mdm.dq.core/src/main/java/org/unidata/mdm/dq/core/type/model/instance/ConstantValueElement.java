/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.dq.core.type.model.instance;

import org.unidata.mdm.dq.core.type.cleanse.CleanseFunctionInputParam;
import org.unidata.mdm.dq.core.type.cleanse.CleanseFunctionOutputParam;

/**
 * @author Mikhail Mikhailov on Feb 28, 2021
 * Base constant
 */
public interface ConstantValueElement {
    /**
     * Returns true, if this constant is a single value constant.
     * @return true, if this constant is a single value constant
     */
    default boolean isSingle() {
        return false;
    }
    /**
     * Gets the single value element view.
     * @return the single value element view
     */
    default SingleValueElement getSingle() {
        return null;
    }
    /**
     * Returns true, if this constant is a array value constant.
     * @return true, if this constant is a array value constant
     */
    default boolean isArray() {
        return false;
    }
    /**
     * Gets the array value element view.
     * @return the array value element view
     */
    default ArrayValueElement getArray() {
        return null;
    }

    default boolean isInput() {
        return false;
    }

    default InputConstantElement getInput() {
        return null;
    }

    default boolean isOutput() {
        return false;
    }

    default OutputConstantElement getOutput() {
        return null;
    }

    /**
     * @author Mikhail Mikhailov on Mar 10, 2021
     * Input constant value view.
     */
    public interface InputConstantElement {
        /**
         * Gets self as input param.
         * @return self as input param
         */
        CleanseFunctionInputParam toInput();
    }
    /**
     * @author Mikhail Mikhailov on Mar 9, 2021
     * Output constant value view.
     */
    public interface OutputConstantElement {
        /**
         * Gets self as output param.
         * @return self as output param
         */
        CleanseFunctionOutputParam toOutput();
    }
}

package org.unidata.mdm.dq.core.type.model.source.constant;

import java.time.LocalDate;

import org.unidata.mdm.dq.core.type.constant.SingleValueConstantType;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Mikhail Mikhailov on Jan 22, 2021
 */
public class DateSingleValueConstant extends AbstractSingleValueConstant<DateSingleValueConstant, LocalDate> {
    /**
     * GSVUID.
     */
    private static final long serialVersionUID = -5753827310209100933L;
    /**
     * Constructor.
     */
    public DateSingleValueConstant() {
        super();
    }

    @JsonProperty("type")
    @Override
    public SingleValueConstantType getType() {
        return SingleValueConstantType.DATE;
    }
}

package org.unidata.mdm.dq.core.type.model.source.constant;

import org.unidata.mdm.dq.core.type.constant.SingleValueConstantType;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Mikhail Mikhailov on Jan 22, 2021
 */
public class StringSingleValueConstant extends AbstractSingleValueConstant<StringSingleValueConstant, String> {
    /**
     * GSVUID.
     */
    private static final long serialVersionUID = -4648668791494240977L;
    /**
     * Constructor.
     */
    public StringSingleValueConstant() {
        super();
    }
    /**
     * {@inheritDoc}
     */
    @JsonProperty("type")
    @Override
    public SingleValueConstantType getType() {
        return SingleValueConstantType.STRING;
    }
}

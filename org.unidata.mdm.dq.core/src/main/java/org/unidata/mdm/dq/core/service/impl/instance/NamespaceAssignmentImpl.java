/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.dq.core.service.impl.instance;

import java.util.Collection;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.unidata.mdm.dq.core.type.model.instance.MappingSetElement;
import org.unidata.mdm.dq.core.type.model.instance.NamespaceAssignmentElement;
import org.unidata.mdm.dq.core.type.model.source.assignment.EntityAssignmentSource;
import org.unidata.mdm.dq.core.type.model.source.assignment.NameSpaceAssignmentSource;
import org.unidata.mdm.system.type.namespace.NameSpace;
import org.unidata.mdm.system.util.NameSpaceUtils;

/**
 * @author Mikhail Mikhailov on Mar 26, 2021
 * Entity name to mapping sets element.
 */
public class NamespaceAssignmentImpl implements NamespaceAssignmentElement {
    /**
     * The assignments.
     */
    private final Map<String, Collection<MappingSetElement>> assignments;
    /**
     * The source.
     */
    private final NameSpaceAssignmentSource source;
    /**
     * Constructor.
     */
    public NamespaceAssignmentImpl(DataQualityInstanceImpl dqi, NameSpaceAssignmentSource source) {
        super();
        this.source = source;
        this.assignments = source.getAssignments().stream()
                .collect(Collectors.toMap(EntityAssignmentSource::getEntityName,
                        a -> a.getSets().stream()
                            .map(dqi::getSet)
                            .filter(Objects::nonNull)
                            .collect(Collectors.toList())));
    }
    /**
     * Gets the source.
     * @return the source
     */
    public NameSpaceAssignmentSource getSource() {
        return source;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public NameSpace getNameSpace() {
        return NameSpaceUtils.find(getId());
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<String> getEntityNames() {
        return assignments.keySet();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getId() {
        return StringUtils.defaultIfBlank(source.getNameSpace(), NameSpace.GLOBAL_NAMESPACE_ID);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<MappingSetElement> getAsssigned(String name) {
        return assignments.get(name);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public Map<String, Collection<MappingSetElement>> getAssigned() {
        return assignments;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isAssigned(String name) {
        return assignments.containsKey(name);
    }

}

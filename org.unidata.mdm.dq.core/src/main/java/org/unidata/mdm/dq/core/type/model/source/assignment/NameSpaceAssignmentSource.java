/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.dq.core.type.model.source.assignment;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

/**
 * @author Mikhail Mikhailov on Mar 26, 2021
 * Namespace to entity names assignment.
 */
public class NameSpaceAssignmentSource implements Serializable {
    /**
     * GSVUID.
     */
    private static final long serialVersionUID = 78303096284616571L;
    /**
     * The entity name.
     */
    @JacksonXmlProperty(isAttribute = true, localName = "namespace")
    private String nameSpace;
    /**
     * The mapping sets.
     */
    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "assignment")
    private List<EntityAssignmentSource> assignments;
    /**
     * Gets the value of the nameSpace property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getNameSpace() {
        return nameSpace;
    }
    /**
     * Sets the value of the nameSpace property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setNameSpace(String value) {
        this.nameSpace = value;
    }
    /**
     * Gets the value of the 'assignments' property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the property.
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String}
     */
    public List<EntityAssignmentSource> getAssignments() {
        if (this.assignments == null) {
            this.assignments = new ArrayList<>();
        }
        return this.assignments;
    }

    public NameSpaceAssignmentSource withNameSpace(String value) {
        setNameSpace(value);
        return this;
    }

    public NameSpaceAssignmentSource withAssignments(EntityAssignmentSource... values) {
        if (ArrayUtils.isNotEmpty(values)) {
            return withAssignments(Arrays.asList(values));
        }
        return this;
    }

    public NameSpaceAssignmentSource withAssignments(Collection<EntityAssignmentSource> values) {
        if (CollectionUtils.isNotEmpty(values)) {
            getAssignments().addAll(values);
        }
        return this;
    }
}

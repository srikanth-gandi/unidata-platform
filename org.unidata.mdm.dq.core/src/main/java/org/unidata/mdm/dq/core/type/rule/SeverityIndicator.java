package org.unidata.mdm.dq.core.type.rule;

import org.apache.commons.lang3.StringUtils;

/**
 * Traffic light for overall validation state.<br>
 * <ul>
 * <li> RED marks a record as being totally garbage. Upserts, validated to RED, should be rejected.</li>
 * <li> YELLOW marks a record as being partially or non-critically inconsistent. Upserts, validated to YELLOW should be evaluated for overall yellow score.</li>
 * <li> GREEN is just OK.</li>
 * </ul>
 */
public enum SeverityIndicator {
    /**
     * RED marks a record as being totally garbage.
     * Upserts, validated to RED, should be rejected.
     */
    RED,
    /**
     * YELLOW marks a record as being partially or non-critically inconsistent.
     * Upserts, validated to YELLOW should be evaluated for overall yellow score.
     */
    YELLOW,
    /**
     * GREEN is just OK.
     */
    GREEN;

    public static SeverityIndicator fromValue(String v) {

        if (StringUtils.isNotBlank(v)) {
            for (SeverityIndicator si : SeverityIndicator.values()) {
                if (si.name().equals(v)) {
                    return si;
                }
            }
        }

        return null;
    }
}

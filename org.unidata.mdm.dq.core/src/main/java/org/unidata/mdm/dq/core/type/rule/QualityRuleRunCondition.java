/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.dq.core.type.rule;

/**
 * Rule run condition.
 */
public enum QualityRuleRunCondition {
    /**
     * The rule will be run always,
     * regardless of the completeness and validity of the input.
     */
    RUN_ALWAYS,
    /**
     * The rule will never be run.
     */
    RUN_NEVER,
    /**
     * The rule will be run, if required input is present.
     * Optional values may be missing.
     */
    RUN_ON_REQUIRED_PRESENT,
    /**
     * Only run the rule, if all values (required and optional) are present.
     */
    RUN_ON_ALL_PRESENT;
}

/*
 * Unidata Platform Community Edition Copyright (c) 2013-2020, UNIDATA LLC, All
 * rights reserved. This file is part of the Unidata Platform Community Edition
 * software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.dq.core.type.cleanse;

import java.util.Collection;
import java.util.Objects;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.apache.commons.lang3.StringUtils;
import org.unidata.mdm.core.dto.UserLibraryResult;
import org.unidata.mdm.core.type.libraries.LibraryMimeType;
import org.unidata.mdm.dq.core.type.model.instance.CleanseFunctionElement;

/**
 * @author Mikhail Mikhailov on Feb 3, 2021 Basic library container.
 */
public abstract class CleanseFunctionLibrary {
    /**
     * Type of the library.
     */
    protected final LibraryMimeType type;
    /**
     * This library (and classloader's) name.
     */
    protected final String name;
    /**
     * Constructor.
     *
     * @param type
     *            this library type.
     */
    protected CleanseFunctionLibrary(UserLibraryResult ulr) {
        super();

        Objects.requireNonNull(ulr.getMimeType(), "MIME type cannot be null.");

        this.type = ulr.getMimeType();
        this.name = toName(ulr.getStorageId(), ulr.getFilename(), ulr.getVersion());
    }
    /**
     * @return the type
     */
    @Nonnull
    public LibraryMimeType getType() {
        return type;
    }
    /**
     * Gets this library identity name.
     *
     * @return name
     */
    @Nonnull
    public String getName() {
        return name;
    }

    /**
     * Performs lookup of cleanse function implementation for the given element.
     *
     * @param el
     *            the element
     * @return implementation or null
     */
    @Nullable
    public abstract CleanseFunction lookup(CleanseFunctionElement el);
    /**
     * Returns sugestions for class/script names, that can be used in function definitions.
     *
     * @return sugestions for class/script names, that can be used in function definitions.
     */
    @Nonnull
    public abstract Collection<String> suggest();
    /**
     * Perform some cleanup before going to Valhalla.
     */
    public abstract void cleanup();

    public static String toName(UserLibraryResult ulr) {
        return toName(ulr.getStorageId(), ulr.getFilename(), ulr.getVersion());
    }

    public static String toName(String storageId, String fileName, String version) {
        return new StringBuilder().append(StringUtils.trimToEmpty(storageId)).append(':')
                .append(StringUtils.trimToEmpty(fileName)).append(':').append(StringUtils.trimToEmpty(version))
                .toString();
    }
}
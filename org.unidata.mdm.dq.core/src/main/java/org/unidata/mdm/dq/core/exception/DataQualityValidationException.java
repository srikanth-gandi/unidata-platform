/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.dq.core.exception;

import java.util.Collection;

import org.unidata.mdm.system.exception.DomainId;
import org.unidata.mdm.system.exception.ExceptionId;
import org.unidata.mdm.system.exception.PlatformValidationException;
import org.unidata.mdm.system.exception.ValidationResult;

/**
 * Data validation error.
 *
 * @author Alexander Malyshev
 */
public class DataQualityValidationException extends PlatformValidationException {
    /**
     * ABI SVUID.
     */
    private static final long serialVersionUID = -1241615204360227443L;
    /**
     * This exception domain.
     */
    private static final DomainId DATA_QUALITY_VALIDATION_EXCEPTION = () -> "DATA_QUALITY_VALIDATION_EXCEPTION";
    /**
     * Constructor.
     * @param message the message
     * @param id the id
     * @param validationResult validation result
     * @param args the arguments
     */
    public DataQualityValidationException(String message, ExceptionId id, Collection<ValidationResult> validationResult, Object... args) {
        super(message, id, validationResult, args);
    }
    /**
     * Constructor.
     * @param message the message
     * @param cause the exception cause
     * @param id exception id
     * @param validationResult validation elements
     * @param args arguments
     */
    public DataQualityValidationException(String message, Throwable cause, ExceptionId id, Collection<ValidationResult> validationResult, Object... args) {
        super(message, cause, id, validationResult, args);
    }
    /**
     * Constructor.
     * @param message the message
     * @param id exception id
     * @param validationResults validation elements
     */
    public DataQualityValidationException(String message, ExceptionId id, Collection<ValidationResult> validationResults) {
        super(message, id, validationResults);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public DomainId getDomain() {
        return DATA_QUALITY_VALIDATION_EXCEPTION;
    }
}

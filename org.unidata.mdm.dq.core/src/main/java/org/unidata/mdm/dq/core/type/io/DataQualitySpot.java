/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.dq.core.type.io;

import java.util.Objects;

import javax.annotation.Nullable;

import org.unidata.mdm.core.type.data.Attribute;
import org.unidata.mdm.core.type.data.DataRecord;
import org.unidata.mdm.dq.core.type.cleanse.CleanseFunctionInputParam;

/**
 * @author Mikhail Mikhailov on Mar 10, 2021
 * Failure/problem spot - not found/missing/malformed/invalid input attributes.
 */
public class DataQualitySpot {
    /**
     * The input.
     */
    private final CleanseFunctionInputParam input;
    /**
     * Local path in the record, being examined.
     */
    private final String path;
    /**
     * The value. May be null (for missing attributes).
     */
    private final Attribute attribute;
    /**
     * For spots, donoting incomplete paths.
     */
    private final DataRecord container;
    /**
     * Origin namespace (set by the engine).
     */
    private String nameSpace;
    /**
     * Origin type (set by the engine).
     */
    private String typeName;
    /**
     * Origin record id, if possible (set by the engine).
     */
    private String recordId;
    /**
     * Constructor.
     * @param path the path
     */
    public DataQualitySpot(String path) {
        super();
        this.input = null;
        this.path = path;
        this.attribute = null;
        this.container = null;
    }
    /**
     * Constructor.
     * @param input the input param
     * @param path the path
     * @param attribute the attribute
     */
    public DataQualitySpot(CleanseFunctionInputParam input, String path, Attribute attribute) {
        super();
        this.input = input;
        this.path = path;
        this.attribute = attribute;
        this.container = null;
    }
    /**
     * Constructor.
     * @param input the input param
     * @param path the path
     * @param container the container
     */
    public DataQualitySpot(CleanseFunctionInputParam input, String path, DataRecord container) {
        super();
        this.input = input;
        this.path = path;
        this.attribute = null;
        this.container = container;
    }
    /**
     * @return the input
     */
    public CleanseFunctionInputParam getInput() {
        return input;
    }
    /**
     * @return the path
     */
    public String getPath() {
        return path;
    }
    /**
     * @return the attribute
     */
    @Nullable
    public Attribute getAttribute() {
        return attribute;
    }
    /**
     * Returns true, if spot attribute was set.
     * @return true, if spot attribute was set
     */
    public boolean hasAttribute() {
        return Objects.nonNull(attribute);
    }
    /**
     * @return the record
     */
    @Nullable
    public DataRecord getRecord() {
        return container;
    }
    /**
     * Returns true, if spot parent record was set.
     * @return true, if spot parent record was set
     */
    public boolean hasRecord() {
        return Objects.nonNull(container);
    }
    /**
     * @return the nameSpace
     */
    public String getNameSpace() {
        return nameSpace;
    }
    /**
     * @return the typeName
     */
    public String getTypeName() {
        return typeName;
    }
    /**
     * @param nameSpace the nameSpace to set
     */
    public void setNameSpace(String nameSpace) {
        this.nameSpace = nameSpace;
    }
    /**
     * @param typeName the typeName to set
     */
    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }
    /**
     * @return the recordId
     */
    public String getRecordId() {
        return recordId;
    }
    /**
     * @param recordId the recordId to set
     */
    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return new StringBuilder()
                .append(input.getPortName())
                .append(" > ")
                .append(path)
                .append(" = ")
                .append(attribute)
                .toString();
    }
}

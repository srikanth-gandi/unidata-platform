/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.dq.core.type.cleanse;

import javax.annotation.Nullable;

import org.unidata.mdm.dq.core.context.CleanseFunctionContext;
import org.unidata.mdm.dq.core.dto.CleanseFunctionResult;

/**
 * @author Michael Yashin. Created on 09.06.2015.
 * <br>
 * Basic cleanse function imterface.
 * A class, wanting to implement this interface, must also define a no-argument constructor
 * to allow creation of instances dynamically at run time.
 * Java cleans functions can inject fields via {@literal @}Inject, {@literal @}Autowire,
 * use {@literal @}PostConstruct or Spring interfaces such as InitializingBean.
 * Cleans functions can not inject other cleans functions.
 * {@literal @}PreDestroy and DisposableBean are also supported.
 */
public interface CleanseFunction {
    /**
     * Gets the underlying implementation name.
     * This can be a class name for java cleanse functions, [script].py or [script].groovy for scripting functions.
     * @return the underlying implementation name
     */
    default String implementation() {
        return getClass().getName();
    }
    /**
     * Returns CleanseFunction definition in terms of
     * <br>
     * - name<br>
     * - display name<br>
     * - description<br>
     * - list of input ports<br>
     * - list of output ports<br>
     * <br>
     * If a function returns configuration, it is considered "autoconfigured" and
     * configuration for this element is not available on UI (set read only).
     *
     * @return configuration or null
     */
    @Nullable
    default CleanseFunctionConfiguration configure() {
        return null;
    }
    /**
     * Executes a cleanse function in the given context.
     * @param ctx the context
     * @return the result or null
     */
    @Nullable
    default CleanseFunctionResult execute(CleanseFunctionContext ctx) {
        return null;
    }
}

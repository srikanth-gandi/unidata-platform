/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.dq.core.type.model.instance;

import java.util.Collection;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.unidata.mdm.core.type.model.ModelInstance;
import org.unidata.mdm.dq.core.type.model.source.DataQualityModel;
import org.unidata.mdm.system.type.namespace.NameSpace;

/**
 * @author Mikhail Mikhailov on Jan 20, 2021
 * The DQ model instance.
 */
public interface DataQualityInstance extends ModelInstance<DataQualityModel> {
    /**
     * Gets the root function group.
     * @return the root function group
     */
    FunctionGroupElement getRootGroup();
    /**
     * Gets function groups.
     * @return function groups
     */
    @Nonnull
    Collection<FunctionGroupElement> getGroups();
    /**
     * Gets a specific function group by name.
     * @param id the function group id
     * @return function group or null.
     */
    @Nullable
    FunctionGroupElement getGroup(String id);
    /**
     * Gets all registered functions.
     * @return functions collection
     */
    @Nonnull
    Collection<CleanseFunctionElement> getFunctions();
    /**
     * Gets function by id.
     * @param id the function id
     * @return function or null, if not found
     */
    @Nullable
    CleanseFunctionElement getFunction(String id);
    /**
     * Returns true, if the function with the given id/name has been registered.
     * @param id the id
     * @return true, if the function with the given id/name has been registered.
     */
    boolean functionExists(String id);
    /**
     * Gets all registered rules.
     * @return rules collection
     */
    @Nonnull
    Collection<QualityRuleElement> getRules();
    /**
     * Gets rule by id.
     * @param id the rule id
     * @return rule or null, if not found
     */
    @Nullable
    QualityRuleElement getRule(String id);
    /**
     * Returns true, if the rule with the given id/name has been registered.
     * @param id the id
     * @return true, if the rule with the given id/name has been registered.
     */
    boolean ruleExists(String id);
    /**
     * Gets all registered rule sets.
     * @return rule sets collection
     */
    @Nonnull
    Collection<MappingSetElement> getSets();
    /**
     * Gets rule set by id.
     * @param id the rule set id
     * @return rule set or null, if not found
     */
    @Nullable
    MappingSetElement getSet(String id);
    /**
     * Returns true, if a rule set with the given id/name has been registered.
     * @param id the id
     * @return true, if a rule set with the given id/name has been registered
     */
    boolean setExists(String id);
    /**
     * Returns a collection of entity link elements assigned to a namespace.
     * @return collection of entity link elementsassigned to a namespace
     */
    @Nonnull
    Collection<NamespaceAssignmentElement> getAssignments();
    /**
     * Gets namespace assignment element, if the given namespace has an association (links).
     * @param ns the namespace
     * @return namespace link element or null
     */
    @Nullable
    NamespaceAssignmentElement getAssignment(NameSpace ns);
    /**
     * Gets namespace assignment element, if the given namespace has an association (links).
     * @param ns the namespace as string
     * @return namespace link element or null
     */
    @Nullable
    NamespaceAssignmentElement getAssignment(String ns);
    /**
     * Returns true, if the given namespace has entity to rule mapping sets associations (assignments).
     * @param ns the namespace
     * @return true, if the given namespace has entity to rule mapping sets associations (assignments)
     */
    boolean hasAssignment(NameSpace ns);
}

/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.dq.core.type.cleanse;

import javax.annotation.Nullable;

import org.unidata.mdm.core.type.upath.UPathApplicationMode;

/**
 * @author Mikhail Mikhailov
 * This is the counterpart of UPath filtering enum, controlling value filetring mode for a port.
 */
public enum CleanseFunctionPortFilteringMode {
    /**
     * Apply to all (collect all hits).
     */
    MODE_ALL,
    /**
     * Apply to all (collect all hits) and collect also incomplete paths.
     */
    MODE_ALL_WITH_INCOMPLETE,
    /**
     * Apply once (collect / set to first and exit).
     */
    MODE_ONCE,
    /**
     * Doesn't apply or undefined.
     */
    MODE_UNDEFINED;
    /**
     * Converts this enum labels to {@link UPathApplicationMode}.
     * @param m the mode
     * @return UPathApplicationMode
     */
    @Nullable
    public UPathApplicationMode toUPathMode() {

        switch (this) {
        case MODE_ALL:
            return UPathApplicationMode.MODE_ALL;
        case MODE_ALL_WITH_INCOMPLETE:
            return UPathApplicationMode.MODE_ALL_WITH_INCOMPLETE;
        case MODE_ONCE:
            return UPathApplicationMode.MODE_ONCE;
        default:
            break;
        }

        return UPathApplicationMode.MODE_ALL_WITH_INCOMPLETE;
    }
}

/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.dq.core.type.model.instance;

import java.util.Collection;

import javax.annotation.Nullable;

import org.unidata.mdm.core.type.upath.UPath;
import org.unidata.mdm.dq.core.type.cleanse.CleanseFunctionExecutionScope;

/**
 * @author Mikhail Mikhailov on Mar 4, 2021
 */
public interface RuleMappingElement {
    /**
     * Gets the rule it mapps.
     * @return rule
     */
    QualityRuleElement getRule();
    /**
     * Gets input mapping port names.
     * @return input mapping port names
     */
    Collection<String> getInputMappingPorts();
    /**
     * Gets input mappings.
     * @return input mappings
     */
    Collection<InputMappingElement> getInputMappings();
    /**
     * Gets input mapping by port name.
     * @param port the port name
     * @return input mapping by port name
     */
    InputMappingElement getInputMapping(String port);
    /**
     * Gets output mapping port names.
     * @return output mapping port names
     */
    Collection<String> getOutputMappingPorts();
    /**
     * Gets output mappings.
     * @return output mappings
     */
    Collection<OutputMappingElement> getOutputMappings();
    /**
     * Gets output mapping by port name.
     * @param port the port name
     * @return output mapping by port name
     */
    OutputMappingElement getOutputMapping(String port);
    /**
     * Gets root execution path for {@link CleanseFunctionExecutionScope#LOCAL} functions/rules.
     * Null is returned for {@link CleanseFunctionExecutionScope#GLOBAL} functions/rules.
     * @return root execution path
     */
    @Nullable
    UPath getLocalPath();
    /**
     * Returns true, if the mapping is a local one.
     * @return true, if the mapping is a local one
     */
    boolean isLocal();
    /**
     * Returns true, if this mapping is a global one.
     * @return true, if this mapping is a global one
     */
    default boolean isGlobal() {
        return !isLocal();
    }
}

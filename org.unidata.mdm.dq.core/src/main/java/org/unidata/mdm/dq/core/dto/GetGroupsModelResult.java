/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.dq.core.dto;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.annotation.Nonnull;

import org.unidata.mdm.dq.core.type.model.source.AbstractCleanseFunctionSource;
import org.unidata.mdm.dq.core.type.model.source.CleanseFunctionGroup;

/**
 * @author Mikhail Mikhailov
 * CF groups transfer object.
 */
public class GetGroupsModelResult {
    /**
     * Groups.
     */
    private Map<String, CleanseFunctionGroup> groups;
    /**
     * Nested.
     */
    private Map<CleanseFunctionGroup, List<AbstractCleanseFunctionSource<?>>> nested;
    /**
     * Constructor.
     * @param groups groups
     * @param nested nested
     */
    public GetGroupsModelResult(
            Map<String, CleanseFunctionGroup> groups,
            Map<CleanseFunctionGroup, List<AbstractCleanseFunctionSource<?>>> nested) {
        super();
        this.groups = groups;
        this.nested = nested;
    }
    /**
     * @return the groups
     */
    @Nonnull
    public Map<String, CleanseFunctionGroup> getGroups() {
        return Objects.isNull(groups) ? Collections.emptyMap() : groups;
    }
    /**
     * @return the nested
     */
    @Nonnull
    public Map<CleanseFunctionGroup, List<AbstractCleanseFunctionSource<?>>> getNested() {
        return Objects.isNull(nested) ? Collections.emptyMap() : nested;
    }

    public List<AbstractCleanseFunctionSource<?>> getContent(CleanseFunctionGroup def) {

        if (Objects.isNull(def)) {
            return Collections.emptyList();
        }

        List<AbstractCleanseFunctionSource<?>> hits = getNested().get(def);
        return Objects.isNull(hits) ? Collections.emptyList() : hits;
    }

    public List<AbstractCleanseFunctionSource<?>> getContent(String id) {
        return getContent(getGroups().get(id));
    }
}

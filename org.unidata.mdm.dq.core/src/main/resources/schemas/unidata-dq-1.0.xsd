<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xs:schema [
        <!ELEMENT xs:schema (xs:import|xs:simpleType|xs:complexType|xs:element)*>
        <!ATTLIST xs:schema
                xmlns:xs CDATA #REQUIRED
                xmlns:dq CDATA #REQUIRED
                xmlns:meta CDATA #REQUIRED
                targetNamespace CDATA #REQUIRED
                elementFormDefault CDATA #REQUIRED
                xmlns:jaxb CDATA #REQUIRED
                jaxb:version CDATA #REQUIRED>
        <!ELEMENT xs:import (#PCDATA)>
        <!ATTLIST xs:import
                namespace CDATA #REQUIRED
                schemaLocation CDATA #REQUIRED>
        <!ELEMENT xs:simpleType (xs:restriction)*>
        <!ATTLIST xs:simpleType
                name CDATA #REQUIRED>
        <!ELEMENT xs:restriction (xs:enumeration)*>
        <!ATTLIST xs:restriction
                base CDATA #REQUIRED>
        <!ELEMENT xs:enumeration (#PCDATA)>
        <!ATTLIST xs:enumeration
                value CDATA #REQUIRED>
        <!ELEMENT xs:complexType (xs:annotation|xs:sequence|xs:attribute|xs:complexContent)*>
        <!ATTLIST xs:complexType
                name CDATA #IMPLIED>
        <!ELEMENT xs:annotation (xs:appinfo)*>
        <!ELEMENT xs:appinfo (jaxb:class)*>
        <!ELEMENT jaxb:class (#PCDATA)>
        <!ATTLIST jaxb:class
                implClass CDATA #REQUIRED>
        <!ELEMENT xs:sequence (xs:choice|xs:element)*>
        <!ELEMENT xs:choice (xs:element)*>
        <!ATTLIST xs:choice
                maxOccurs CDATA #IMPLIED>
        <!ELEMENT xs:element (xs:complexType)*>
        <!ATTLIST xs:element
                default CDATA #IMPLIED
                maxOccurs CDATA #IMPLIED
                minOccurs CDATA #IMPLIED
                name CDATA #REQUIRED
                type CDATA #IMPLIED>
        <!ELEMENT xs:attribute (#PCDATA)>
        <!ATTLIST xs:attribute
                default CDATA #IMPLIED
                name CDATA #REQUIRED
                type CDATA #IMPLIED
                use CDATA #IMPLIED>
        <!ELEMENT xs:complexContent (xs:extension)*>
        <!ELEMENT xs:extension (xs:sequence|xs:attribute|xs:choice)*>
        <!ATTLIST xs:extension
                base CDATA #REQUIRED>
        ]>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           xmlns:dq="http://dq.mdm.unidata.com/"
           xmlns:meta="http://meta.mdm.unidata.com/"
           targetNamespace="http://dq.mdm.unidata.com/"
           elementFormDefault="qualified"
           xmlns:jaxb="http://java.sun.com/xml/ns/jaxb"
           jaxb:version="2.0">

    <!--    <xs:import namespace="http://meta.mdm.unidata.com/" schemaLocation="../support/unidata-meta-1.0.xsd"/>-->

    <!-- TODO @Modules Duplicate from unidata-common meta -->
    <xs:simpleType name="DQDataType">
        <xs:restriction base="xs:string">
            <!-- Date without time and timezone. -->
            <xs:enumeration value="Date"/>
            <!-- Time without date and timezone. -->
            <xs:enumeration value="Time"/>
            <!-- Timestamp without timezone. -->
            <xs:enumeration value="Timestamp"/>
            <!-- Plain string. -->
            <xs:enumeration value="String"/>
            <!-- 64bit integer -->
            <xs:enumeration value="Integer"/>
            <!-- 64bit FP number. -->
            <xs:enumeration value="Number"/>
            <!-- Plain boolean -->
            <xs:enumeration value="Boolean"/>
            <!-- Binary large object, containing file name, size etc. attributes. Used for attachments. -->
            <xs:enumeration value="Blob"/>
            <!-- Character large object, containing file name, size etc. attributes. Used for attachments. -->
            <xs:enumeration value="Clob"/>
            <!-- 64bit FP number with measurement value options -->
            <xs:enumeration value="Measured"/>
            <!-- Link to a value in a enumeration (EnumerationDataType code value) -->
            <!--
            <xs:enumeration value="Enum"/>
            -->
            <!-- Special type, containing a template, where attribute names can be replaced with their current values. -->
            <!--
            <xs:enumeration value="Link"/>
            -->
            <!-- Link to a code/alias attribute(s) value in a lookup entity (LookupEntityDef code/alias attribute(s))-->
            <!--
            <xs:enumeration value="Code"/>
            -->
            <!-- Guess the type -->
            <xs:enumeration value="Any"/>
        </xs:restriction>
    </xs:simpleType>

    <xs:simpleType name="ConstantValueType">
        <xs:restriction base="dq:DQDataType">
            <!-- Date without time and timezone. -->
            <xs:enumeration value="Date"/>
            <!-- Time without date and timezone. -->
            <xs:enumeration value="Time"/>
            <!-- Timestamp without timezone. -->
            <xs:enumeration value="Timestamp"/>
            <!-- Plain string. -->
            <xs:enumeration value="String"/>
            <!-- 64bit integer -->
            <xs:enumeration value="Integer"/>
            <!-- 64bit FP number. -->
            <xs:enumeration value="Number"/>
            <!-- Plain boolean -->
            <xs:enumeration value="Boolean"/>
        </xs:restriction>
    </xs:simpleType>

    <xs:complexType name="ConstantValueDef">
        <xs:annotation>
            <xs:appinfo>
                <jaxb:class implClass="org.unidata.mdm.dq.core.type.serialization.ConstantValueDefImpl"/>
            </xs:appinfo>
        </xs:annotation>
        <xs:sequence>
            <xs:choice>
                <xs:element name="intValue" type="xs:long"/>
                <xs:element name="dateValue" type="xs:date"/>
                <xs:element name="timeValue" type="xs:time"/>
                <xs:element name="timestampValue" type="xs:dateTime"/>
                <xs:element name="stringValue" type="xs:string"/>
                <xs:element name="numberValue" type="xs:double"/>
                <xs:element name="boolValue" type="xs:boolean"/>
            </xs:choice>
        </xs:sequence>
        <xs:attribute name="type" type="dq:ConstantValueType" use="required"/>
    </xs:complexType>

    <xs:simpleType name="SeverityType">
        <xs:restriction base="xs:string">
            <xs:enumeration value="CRITICAL"/>
            <xs:enumeration value="HIGH"/>
            <xs:enumeration value="NORMAL"/>
            <xs:enumeration value="LOW"/>
        </xs:restriction>
    </xs:simpleType>

    <xs:simpleType name="DQRPhaseType">
        <xs:restriction base="xs:string">
            <xs:enumeration value="BEFORE_UPSERT"/>
            <xs:enumeration value="AFTER_UPSERT"/>
            <xs:enumeration value="AFTER_MERGE"/>
        </xs:restriction>
    </xs:simpleType>

    <xs:simpleType name="DQRuleExecutionContext">
        <xs:restriction base="xs:string">
            <xs:enumeration value="GLOBAL"/> <!-- allow filtering for input and output of any depth. -->
            <xs:enumeration
                    value="LOCAL"/> <!-- execute for filtered records, their direct ancestors and their leaves.  -->
        </xs:restriction>
    </xs:simpleType>

    <xs:simpleType name="DQRuleRunType">
        <xs:restriction base="xs:string">
            <xs:enumeration value="RUN_ALWAYS"/> <!-- Skip port value validation -->
            <xs:enumeration value="RUN_NEVER"/> <!-- Disabled -->
            <xs:enumeration
                    value="RUN_ON_REQUIRED_PRESENT"/> <!-- Run only if required ports have values. Other ports may miss values. -->
            <xs:enumeration value="RUN_ON_ALL_PRESENT"/> <!-- Run only if all ports have values. Skip run otherwise. -->
        </xs:restriction>
    </xs:simpleType>

    <xs:simpleType name="DQRActionType">
        <xs:restriction base="xs:string">
            <xs:enumeration value="CREATE_NEW"/>
            <xs:enumeration value="UPDATE_CURRENT"/>
        </xs:restriction>
    </xs:simpleType>

    <xs:simpleType name="DQRuleType">
        <xs:restriction base="xs:string">
            <xs:enumeration value="VALIDATE"/>
            <xs:enumeration value="ENRICH"/>
        </xs:restriction>
    </xs:simpleType>

    <xs:simpleType name="DQRuleClass">
        <xs:restriction base="xs:string">
            <xs:enumeration value="SYSTEM"/>
            <xs:enumeration value="USER_DEFINED"/>
        </xs:restriction>
    </xs:simpleType>

    <xs:simpleType name="DQCleanseFunctionPortApplicationMode">
        <xs:restriction base="xs:string">
            <xs:enumeration value="MODE_ALL"/>
            <xs:enumeration value="MODE_ALL_WITH_INCOMPLETE"/>
            <xs:enumeration value="MODE_ONCE"/>
        </xs:restriction>
    </xs:simpleType>

    <xs:simpleType name="DQApplicableType">
        <xs:restriction base="xs:string">
            <xs:enumeration value="ORIGIN"/>
            <xs:enumeration value="ETALON"/>
        </xs:restriction>
    </xs:simpleType>

    <xs:simpleType name="KeyAttribute">
        <xs:restriction base="xs:string">
            <xs:minLength value="1"/>
            <xs:pattern value=".*[^\s].*"/>
        </xs:restriction>
    </xs:simpleType>

    <xs:complexType name="DQRMappingDef">
        <xs:annotation>
            <xs:appinfo>
                <jaxb:class implClass="org.unidata.mdm.dq.core.type.serialization.DQRMappingDefImpl"/>
            </xs:appinfo>
        </xs:annotation>
        <xs:sequence>
            <xs:element name="attributeConstantValue" type="dq:ConstantValueDef" minOccurs="0"/>
        </xs:sequence>
        <xs:attribute name="attributeName" type="dq:KeyAttribute" use="required"/>
        <xs:attribute name="inputPort" type="xs:string"/>
        <xs:attribute name="outputPort" type="xs:string"/>
    </xs:complexType>

    <xs:complexType name="DQREnrichDef">
        <xs:attribute name="sourceSystem" type="xs:string"/>
        <xs:attribute name="action" type="dq:DQRActionType"/>
        <xs:attribute name="phase" type="dq:DQRPhaseType"/>
    </xs:complexType>

    <xs:complexType name="DQRRaiseDef">
        <xs:attribute name="functionRaiseErrorPort" type="xs:string" use="required"/>
        <xs:attribute name="messagePort" type="xs:string"/>
        <xs:attribute name="messageText" type="xs:string"/>
        <xs:attribute name="severityPort" type="xs:string"/>
        <xs:attribute name="severityValue" type="dq:SeverityType"/>
        <xs:attribute name="categoryPort" type="xs:string"/>
        <xs:attribute name="categoryText" type="xs:string"/>
        <xs:attribute name="pathsPort" type="xs:string"/>
        <xs:attribute name="phase" type="dq:DQRPhaseType"/>
    </xs:complexType>

    <xs:complexType name="DQRSourceSystemRef">
        <xs:attribute name="name" type="xs:string" use="required"/>
    </xs:complexType>

    <xs:complexType name="DQROriginsDef">
        <xs:sequence>
            <xs:element name="sourceSystem" type="dq:DQRSourceSystemRef" minOccurs="0" maxOccurs="unbounded"/>
        </xs:sequence>
        <xs:attribute name="all" type="xs:boolean" use="required"/>
    </xs:complexType>

    <!-- TODO @Modules Duplicate from unidata-common meta -->
    <xs:complexType name="CustomPropertyDef">
        <xs:annotation>
            <xs:documentation>Дополнительное свойству сущности</xs:documentation>
        </xs:annotation>
        <xs:attribute type="xs:string" name="name"/>
        <xs:attribute type="xs:string" name="value"/>
    </xs:complexType>

    <xs:complexType name="DQRuleDef">
        <xs:annotation>
            <xs:appinfo>
                <jaxb:class implClass="org.unidata.mdm.dq.core.type.serialization.DQRuleDefImpl"/>
            </xs:appinfo>
        </xs:annotation>
        <xs:sequence>
            <xs:element name="dqrMapping" type="dq:DQRMappingDef" maxOccurs="unbounded"/>
            <xs:element name="origins" type="dq:DQROriginsDef"/>
            <xs:element name="raise" type="dq:DQRRaiseDef" minOccurs="0"/>
            <xs:element name="enrich" type="dq:DQREnrichDef" minOccurs="0"/>
            <xs:element name="type" type="dq:DQRuleType" maxOccurs="2"/>
            <xs:element name="rClass" type="dq:DQRuleClass" minOccurs="0"/>
            <xs:element name="applicable" type="dq:DQApplicableType" minOccurs="0" maxOccurs="2"/>
            <xs:element name="customProperties" type="dq:CustomPropertyDef" minOccurs="0" maxOccurs="unbounded"/>
            <xs:element name="tags" type="xs:string" minOccurs="0" maxOccurs="unbounded"/>
        </xs:sequence>
        <xs:attribute name="name" type="dq:KeyAttribute" use="required"/>
        <xs:attribute name="cleanseFunctionName" type="xs:string" use="required"/>
        <xs:attribute name="description" type="xs:string"/>
        <xs:attribute name="order" type="xs:integer" use="required"/>
        <xs:attribute name="special" type="xs:boolean" default="false"/>
        <xs:attribute name="runType" type="dq:DQRuleRunType" default="RUN_ON_REQUIRED_PRESENT"/>
        <xs:attribute name="executionContextPath" type="xs:string"/>
        <xs:attribute name="executionContext" type="dq:DQRuleExecutionContext" default="GLOBAL"/>
    </xs:complexType>

    <xs:simpleType name="PhaseDef">
        <xs:restriction base="xs:string">
            <xs:enumeration value="BEFORE_UPSERT"/>
            <xs:enumeration value="AFTER_UPSERT"/>
            <xs:enumeration value="AFTER_MERGE"/>
        </xs:restriction>
    </xs:simpleType>

    <xs:complexType name="EntityDataQualityDef">
        <xs:sequence>
            <xs:element name="dqRule" type="dq:DQRuleDef" minOccurs="0" maxOccurs="unbounded"/>
        </xs:sequence>
    </xs:complexType>

    <xs:complexType name="RulesPO">
        <xs:sequence>
            <xs:element name="rules" type="dq:DQRuleDef" minOccurs="0" maxOccurs="unbounded"/>
        </xs:sequence>
    </xs:complexType>

    <xs:complexType name="Port">
        <xs:attribute name="name" type="xs:string" use="required"/>
        <xs:attribute name="dataType" type="dq:DQDataType" use="required"/>
        <xs:attribute name="required" type="xs:boolean" use="required"/>
        <xs:attribute name="description" type="xs:string"/>
        <xs:attribute name="applicationMode" type="dq:DQCleanseFunctionPortApplicationMode" default="MODE_ALL"/>
    </xs:complexType>

    <xs:complexType name="CleansePortList">
        <xs:sequence>
            <xs:element name="port" type="dq:Port" maxOccurs="unbounded"/>
        </xs:sequence>
    </xs:complexType>

    <xs:simpleType name="CompositeCleanseFunctionNodeType">
        <xs:restriction base="xs:string">
            <xs:enumeration value="Constant"/>
            <xs:enumeration value="ifthenelse"/>
            <xs:enumeration value="OutputTrue"/>
            <xs:enumeration value="OutputFalse"/>
            <xs:enumeration value="Condition"/>
            <xs:enumeration value="Input"/>
            <xs:enumeration value="Output"/>
            <xs:enumeration value="InputPorts"/>
            <xs:enumeration value="OutputPorts"/>
            <xs:enumeration value="Function"/>
            <xs:enumeration value="Switch"/>
        </xs:restriction>
    </xs:simpleType>

    <xs:complexType name="CleanseFunctionConstant">
        <!-- Взаимо-исключающий набор атрибутов -->
        <xs:attribute name="intValue" type="xs:int"/>
        <xs:attribute name="dateValue" type="xs:date"/>
        <xs:attribute name="stringValue" type="xs:string"/>
        <xs:attribute name="numberValue" type="xs:double"/>
        <xs:attribute name="boolValue" type="xs:boolean"/>
    </xs:complexType>

    <xs:complexType name="CompositeCleanseFunctionLogic">
        <xs:sequence>
            <xs:element name="nodes">
                <xs:complexType>
                    <xs:sequence>
                        <xs:element name="node" minOccurs="2" maxOccurs="unbounded">
                            <xs:complexType>
                                <xs:sequence>
                                    <xs:element name="constant" type="dq:CleanseFunctionConstant" minOccurs="0"/>
                                    <xs:element name="value" type="dq:ConstantValueDef" minOccurs="0"/>
                                </xs:sequence>
                                <xs:attribute name="nodeId" type="xs:integer" use="required"/>
                                <xs:attribute name="nodeType"
                                              type="dq:CompositeCleanseFunctionNodeType" use="required"/>
                                <xs:attribute name="functionName" type="xs:string"/>
                            </xs:complexType>
                        </xs:element>
                    </xs:sequence>
                </xs:complexType>
            </xs:element>
            <xs:element name="links">
                <xs:complexType>
                    <xs:sequence>
                        <xs:element name="nodeLink" minOccurs="0" maxOccurs="unbounded">
                            <xs:complexType>
                                <xs:attribute name="fromNodeId" type="xs:integer" use="required"/>
                                <xs:attribute name="fromPort" type="xs:string" use="required"/>
                                <xs:attribute name="fromPortType" type="dq:CompositeCleanseFunctionNodeType"/>
                                <xs:attribute name="toNodeId" type="xs:integer" use="required"/>
                                <xs:attribute name="toPort" type="xs:string" use="required"/>
                                <xs:attribute name="toPortType" type="dq:CompositeCleanseFunctionNodeType"/>
                            </xs:complexType>
                        </xs:element>
                    </xs:sequence>
                </xs:complexType>
            </xs:element>
        </xs:sequence>
    </xs:complexType>

    <xs:complexType name="CleanseFunctionDef">
        <xs:sequence>
            <xs:element name="supportedExecutionContexts" type="dq:DQRuleExecutionContext"
                        default="GLOBAL" minOccurs="0" maxOccurs="unbounded"/>
        </xs:sequence>
        <xs:attribute name="functionName" type="xs:string" use="required"/>
        <xs:attribute name="javaClass" type="xs:string"/>
        <xs:attribute name="description" type="xs:string"/>
    </xs:complexType>

    <xs:complexType name="CleanseFunctionExtendedDef">
        <xs:complexContent>
            <xs:extension base="dq:CleanseFunctionDef">
                <xs:sequence>
                    <xs:element name="inputPorts" type="dq:CleansePortList"/>
                    <xs:element name="outputPorts" type="dq:CleansePortList"/>
                </xs:sequence>
                <xs:attribute name="updatedAt" type="xs:dateTime"/>
                <xs:attribute name="createdAt" type="xs:dateTime"/>
                <xs:attribute name="updatedBy" type="xs:string"/>
                <xs:attribute name="createdBy" type="xs:string"/>
            </xs:extension>
        </xs:complexContent>
    </xs:complexType>

    <xs:complexType name="CompositeCleanseFunctionDef">
        <xs:complexContent>
            <xs:extension base="dq:CleanseFunctionExtendedDef">
                <xs:sequence>
                    <xs:element name="logic" type="dq:CompositeCleanseFunctionLogic"
                    />
                </xs:sequence>
                <!--<xs:attribute name="javaClass" type="xs:string" use="optional"/> -->
            </xs:extension>
        </xs:complexContent>

    </xs:complexType>

    <!-- TODO @Modules Duplicate from unidata-common meta -->
    <xs:complexType name="VersionedObjectDef">
        <xs:attribute name="version" type="xs:long"/>
        <xs:attribute name="updatedAt" type="xs:dateTime"/>
        <xs:attribute name="createAt" type="xs:dateTime"/>
    </xs:complexType>

    <xs:complexType name="CleanseFunctionGroupDef">
        <xs:complexContent>
            <xs:extension base="dq:VersionedObjectDef">
                <xs:choice maxOccurs="unbounded">
                    <xs:element name="group" type="dq:CleanseFunctionGroupDef"/>
                    <xs:element name="cleanseFunction" type="dq:CleanseFunctionDef"/>
                    <xs:element name="compositeCleanseFunction" type="dq:CompositeCleanseFunctionDef"/>
                </xs:choice>
                <xs:attribute name="groupName" type="xs:string" use="required"/>
                <xs:attribute name="description" type="xs:string"/>
            </xs:extension>
        </xs:complexContent>
    </xs:complexType>

    <xs:complexType name="CleanseFunctionPO">
        <xs:choice maxOccurs="1">
            <xs:element name="group" type="dq:CleanseFunctionGroupDef"/>
            <xs:element name="cleanseFunction" type="dq:CleanseFunctionDef"/>
            <xs:element name="compositeCleanseFunction" type="dq:CompositeCleanseFunctionDef"/>
        </xs:choice>
    </xs:complexType>

    <xs:complexType name="ListOfCleanseFunctions">
        <xs:sequence>
            <xs:element name="group" type="dq:CleanseFunctionGroupDef"/>
        </xs:sequence>
    </xs:complexType>

    <xs:complexType name="EntitiesDataQualityRulesDef">
        <xs:sequence>
            <xs:element name="dqRule" type="dq:DQRuleDef" maxOccurs="unbounded"/>
        </xs:sequence>
        <xs:attribute name="name" use="required"/>
    </xs:complexType>

    <xs:element name="listOfEntitiesDataQualityRulesDef">
        <xs:complexType>
            <xs:sequence>
                <xs:element name="entitiesDataQualityRules" type="dq:EntitiesDataQualityRulesDef"
                            maxOccurs="unbounded"/>
            </xs:sequence>
        </xs:complexType>
    </xs:element>
</xs:schema>
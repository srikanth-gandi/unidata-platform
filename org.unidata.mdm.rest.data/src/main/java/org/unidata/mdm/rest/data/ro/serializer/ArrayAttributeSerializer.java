/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.data.ro.serializer;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Objects;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.apache.commons.lang3.time.FastDateFormat;
import org.unidata.mdm.rest.data.ro.ArrayAttributeRO;
import org.unidata.mdm.rest.data.ro.ArrayObjectRO;
import org.unidata.mdm.rest.system.serializer.AbstractJsonSerializer;

public class ArrayAttributeSerializer extends AbstractJsonSerializer<ArrayAttributeRO> {

    private static final FastDateFormat DEFAULT_TIMESTAMP
            = FastDateFormat.getInstance("yyyy-MM-dd'T'HH:mm:ss.SSS");

    @Override
    public void serialize(ArrayAttributeRO value, JsonGenerator jgen, SerializerProvider serializers) throws IOException {

        jgen.writeStartObject();
        jgen.writeStringField("name", value.getName());
        jgen.writeStringField("type", value.getType().value());
        jgen.writeArrayFieldStart("value");



        if (Objects.nonNull(value.getValue())) {
            for (ArrayObjectRO val : value.getValue()) {
                jgen.writeStartObject();

                switch (value.getType()) {
                    case DATE:
                        jgen.writeStringField("value", formatLocalDate((LocalDate) val.getValue()));
                        break;
                    case TIMESTAMP:
                        jgen.writeStringField("value", formatLocalDateTime((LocalDateTime) val.getValue()));
                        break;
                    case TIME:
                        jgen.writeStringField("value", formatLocalTime((LocalTime) val.getValue()));
                        break;
                    default:
                        jgen.writeObjectField("value", val.getValue());
                }

                jgen.writeStringField("displayValue", val.getDisplayValue());
                jgen.writeStringField("targetEtalonId", val.getTargetEtalonId());
                jgen.writeEndObject();
            }
        }

        jgen.writeEndArray();
        jgen.writeEndObject();
    }
}

alter table character_data drop column if exists classifier_id;
alter table character_data drop column if exists record_id;
alter table character_data drop column if exists event_id;
alter table character_data drop column if exists field;
alter table character_data drop column if exists status;

alter table binary_data drop column if exists classifier_id;
alter table binary_data drop column if exists record_id;
alter table binary_data drop column if exists event_id;
alter table binary_data drop column if exists field;
alter table binary_data drop column if exists status;

drop type if exists approval_state;

create type acceptance as enum ('PENDING', 'ACCEPTED');

alter table character_data add column if not exists subject text;
alter table character_data add column if not exists state acceptance not null default 'ACCEPTED'::acceptance;
alter table character_data add column if not exists tags text[];

create index if not exists ix_character_data_subject on character_data using hash (subject);
create index if not exists ix_character_data_tags on character_data using gin (tags);

alter table binary_data add column if not exists subject text;
alter table binary_data add column if not exists state acceptance not null default 'ACCEPTED'::acceptance;
alter table binary_data add column if not exists tags text[];

create index if not exists ix_binary_data_subject on binary_data using hash (subject);
create index if not exists ix_binary_data_tags on binary_data using gin (tags);

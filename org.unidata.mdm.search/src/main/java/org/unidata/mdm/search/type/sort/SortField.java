/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.search.type.sort;


import javax.annotation.Nonnull;

import org.unidata.mdm.search.type.FieldType;
import org.unidata.mdm.search.type.GenericIndexField;
import org.unidata.mdm.search.type.IndexField;

public class SortField extends GenericIndexField {

    private final SortOrder sortOrder;

    private SortField(FieldType type, @Nonnull String name, @Nonnull String path, boolean isAnalyzed, @Nonnull SortOrder sortOrder) {
        super(type, name, path, isAnalyzed);
        this.sortOrder = sortOrder;
    }

    public static SortField of(IndexField f, SortOrder order) {
        return new SortField(f.getFieldType(), f.getName(), f.getPath(), f.isAnalyzed(), order);
    }

    public static SortField of(String path, boolean analyzed, SortOrder order) {
        return new SortField(FieldType.ANY, path, path, analyzed, order);
    }

    @Nonnull
    public SortOrder getSortOrder() {
        return sortOrder;
    }

    public enum SortOrder {
        /**
         * Ascending order.
         */
        ASC {
            @Override
            public String toString() {
                return "asc";
            }
        },
        /**
         * Descending order.
         */
        DESC {
            @Override
            public String toString() {
                return "desc";
            }
        }
    }

}

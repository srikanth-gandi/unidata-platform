package org.unidata.mdm.search.type.search;

import org.unidata.mdm.search.type.IndexField;

/**
 * Marker interface for specifically search fields
 * @author Mikhail Mikhailov on Feb 27, 2020
 */
public interface SearchField extends IndexField {}

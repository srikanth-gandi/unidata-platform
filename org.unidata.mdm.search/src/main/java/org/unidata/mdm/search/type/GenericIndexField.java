package org.unidata.mdm.search.type;

import java.util.Objects;

import javax.annotation.Nonnull;

/**
 * Basic index field.
 * @author Mikhail Mikhailov on Feb 20, 2020
 */
public class GenericIndexField implements IndexField {
    /**
     * The name.
     */
    protected final String name;
    /**
     * Field path.
     */
    @Nonnull
    protected final String path;
    /**
     * The field value type.
     */
    protected final FieldType type;
    /**
     * Analyzed mark.
     */
    protected final boolean analyzed;
    /**
     * Constructor.
     * @param type the field type
     * @param path the field path
     * @param analyzed default string analyzed value
     */
    protected GenericIndexField(@Nonnull FieldType type, @Nonnull String path, boolean analyzed) {
        super();

        Objects.requireNonNull(type, "Field type can not be null");
        Objects.requireNonNull(path, "Field path can not be null");

        this.type = type;
        this.path = path;
        this.name = path;
        this.analyzed = type == FieldType.STRING && analyzed;
    }
    /**
     * Constructor.
     * @param type the field type
     * @param name the field name
     * @param path the field path
     * @param analyzed default string analyzed value
     */
    protected GenericIndexField(@Nonnull FieldType type, String name, @Nonnull String path, boolean analyzed) {
        super();

        Objects.requireNonNull(type, "Field type can not be null");
        Objects.requireNonNull(path, "Field path can not be null");

        this.type = type;
        this.name = name;
        this.path = path;
        this.analyzed = type == FieldType.STRING && analyzed;
    }

    public static GenericIndexField of(FieldType type, String path) {
        return of(type, path, true);
    }

    public static GenericIndexField of(FieldType type, String path, boolean analyzed) {
        return of(type, path, path, analyzed);
    }

    public static GenericIndexField of(FieldType type, String name, String path) {
        return of(type, name, path, true);
    }

    public static GenericIndexField of(FieldType type, String name, String path, boolean analyzed) {
        return new GenericIndexField(type, name, path, analyzed);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getName() {
        return name;
    }
    /**
     * @return the path
     */
    @Override
    public String getPath() {
        return path;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public FieldType getFieldType() {
        return type;
    }
    /**
     * @return the analyzed
     */
    @Override
    public boolean isAnalyzed() {
        return analyzed;
    }
}

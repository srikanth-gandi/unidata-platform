/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.search.type.indexing.impl;

import org.unidata.mdm.search.type.FieldType;

/**
 * @author Mikhail Mikhailov on Oct 7, 2019
 * String.
 */
public final class StringIndexingField extends AbstractValueIndexingField<String, StringIndexingField> {
    /**
     * Analyzed ?.
     */
    private boolean analyzed;
    /**
     * Constructor.
     */
    public StringIndexingField(String name) {
        super(name);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public FieldType getFieldType() {
        return FieldType.STRING;
    }
    /**
     * @return the analyzed
     */
    @Override
    public boolean isAnalyzed() {
        return analyzed;
    }
    /**
     * @param analyzed the analyzed to set
     */
    public void setAnalyzed(boolean analyzed) {
        this.analyzed = analyzed;
    }
    /**
     * @param value the value to set
     */
    public StringIndexingField withAnalyzed(boolean value) {
        setAnalyzed(value);
        return self();
    }
}

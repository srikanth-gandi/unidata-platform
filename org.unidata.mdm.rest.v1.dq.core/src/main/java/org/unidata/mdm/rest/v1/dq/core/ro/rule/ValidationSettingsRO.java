/*
 *
 *  * Unidata Platform
 *  * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *  *
 *  * Commercial License
 *  * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *  *
 *  * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 *  * For clarification or additional options, please contact: info@unidata-platform.com
 *  * -------
 *  * Disclaimer:
 *  * -------
 *  * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 *  * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 *  * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 *  * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 *
 */
package org.unidata.mdm.rest.v1.dq.core.ro.rule;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * @author Michael Yashin. Created on 11.06.2015.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ValidationSettingsRO {

    protected String raisePort;

    protected String messagePort;

    protected String messageText;

    protected String severityPort;

    @Schema(allowableValues = {"RED", "YELLOW", "GREEN"})
    protected String severityIndicator;

    protected int severityScore;

    protected String categoryPort;

    protected String categoryText;

    public String getRaisePort() {
        return raisePort;
    }

    public void setRaisePort(String functionRaiseErrorPort) {
        this.raisePort = functionRaiseErrorPort;
    }

    public String getMessagePort() {
        return messagePort;
    }

    public void setMessagePort(String messagePort) {
        this.messagePort = messagePort;
    }

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public String getSeverityPort() {
        return severityPort;
    }

    public void setSeverityPort(String severityPort) {
        this.severityPort = severityPort;
    }

    public String getSeverityIndicator() {
        return severityIndicator;
    }

    public void setSeverityIndicator(String severityValue) {
        this.severityIndicator = severityValue;
    }

    /**
     * @return the severityScore
     */
    public int getSeverityScore() {
        return severityScore;
    }

    /**
     * @param severityScore the severityScore to set
     */
    public void setSeverityScore(int severityScore) {
        this.severityScore = severityScore;
    }

    public String getCategoryPort() {
        return categoryPort;
    }

    public void setCategoryPort(String categoryPort) {
        this.categoryPort = categoryPort;
    }

    public String getCategoryText() {
        return categoryText;
    }

    public void setCategoryText(String categoryText) {
        this.categoryText = categoryText;
    }
}

package org.unidata.mdm.rest.v1.dq.core.ro.functions;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * @author Mikhail Mikhailov on Feb 7, 2021
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class JavaCleanseFunctionRO extends CleanseFunctionRO {

    private String javaClass;

    private String libraryName;

    private String libraryVersion;
    /**
     * Constructor.
     */
    public JavaCleanseFunctionRO() {
        super();
    }
    /**
     * {@inheritDoc}
     */
    @Schema(description = "JAVA for Java functions.")
    @Override
    public CleanseFunctionType getType() {
        return CleanseFunctionType.JAVA;
    }
    /**
     * @return the libraryName
     */
    public String getLibraryName() {
        return libraryName;
    }
    /**
     * @param libraryName the libraryName to set
     */
    public void setLibraryName(String libraryName) {
        this.libraryName = libraryName;
    }
    public String getJavaClass() {
        return javaClass;
    }

    public void setJavaClass(String javaClass) {
        this.javaClass = javaClass;
    }
    /**
     * @return the libraryVersion
     */
    public String getLibraryVersion() {
        return libraryVersion;
    }
    /**
     * @param libraryVersion the libraryVersion to set
     */
    public void setLibraryVersion(String libraryVersion) {
        this.libraryVersion = libraryVersion;
    }

}

/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.v1.dq.core.ro.functions;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * @author Mikhail Mikhailov on Feb 7, 2021
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PythonCleanseFunctionRO extends CleanseFunctionRO {

    private String libraryName;

    private String libraryVersion;
    /**
     * Constructor.
     */
    public PythonCleanseFunctionRO() {
        super();
    }
    /**
     * {@inheritDoc}
     */
    @Schema(description = "PYTHON for Python functions.")
    @Override
    public CleanseFunctionType getType() {
        return CleanseFunctionType.PYTHON;
    }
    /**
     * @return the libraryName
     */
    public String getLibraryName() {
        return libraryName;
    }
    /**
     * @param libraryName the libraryName to set
     */
    public void setLibraryName(String libraryName) {
        this.libraryName = libraryName;
    }
    /**
     * @return the libraryVersion
     */
    public String getLibraryVersion() {
        return libraryVersion;
    }
    /**
     * @param libraryVersion the libraryVersion to set
     */
    public void setLibraryVersion(String libraryVersion) {
        this.libraryVersion = libraryVersion;
    }
}

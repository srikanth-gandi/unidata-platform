/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.v1.dq.core.converter;

import org.unidata.mdm.dq.core.type.io.DataQualitySpot;
import org.unidata.mdm.rest.v1.dq.core.ro.result.DataQualitySpotRO;
import org.unidata.mdm.system.convert.Converter;

/**
 * @author Mikhail Mikhailov on Apr 1, 2021
 * One way DQS -> DQS.
 */
public class DataQualitySpotConverter extends Converter<DataQualitySpot, DataQualitySpotRO> {

    public DataQualitySpotConverter() {
        super(DataQualitySpotConverter::convert, null);
    }

    private static DataQualitySpotRO convert(DataQualitySpot source) {

        DataQualitySpotRO target = new DataQualitySpotRO();

        target.setPath(source.getPath());
        target.setNameSpace(source.getNameSpace());
        target.setTypeName(source.getTypeName());
        target.setRecordId(source.getRecordId());

        return target;
    }
}

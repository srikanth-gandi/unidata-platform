/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.v1.dq.core.ro.rule;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Mikhail Mikhailov on Mar 27, 2021
 * Entity to sets assignment.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class EntityAssignmentRO {
    /**
     * The entity name.
     */
    private String entityName;
    /**
     * The set names.
     */
    private List<String> sets;
    /**
     * Constructor.
     */
    public EntityAssignmentRO() {
        super();
    }
    /**
     * @return the entityName
     */
    public String getEntityName() {
        return entityName;
    }
    /**
     * @param entityName the entityName to set
     */
    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }
    /**
     * @return the sets
     */
    public List<String> getSets() {
        return Objects.isNull(sets) ? Collections.emptyList() : sets;
    }
    /**
     * @param sets the sets to set
     */
    public void setSets(List<String> sets) {
        this.sets = sets;
    }
}

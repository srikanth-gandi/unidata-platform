/*
 *
 *  * Unidata Platform
 *  * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *  *
 *  * Commercial License
 *  * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *  *
 *  * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 *  * For clarification or additional options, please contact: info@unidata-platform.com
 *  * -------
 *  * Disclaimer:
 *  * -------
 *  * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 *  * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 *  * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 *  * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 *
 */
package org.unidata.mdm.rest.v1.dq.core.ro.result;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * The Class DQErrorRO.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DataQualityErrorRO {
    /**
     * The function name.
     */
    private String functionName;
    /**
     * The function name.
     */
    private String functionDisplayName;
    /**
     * The message.
     */
    private String message;
    /**
     * The severity.
     */
    @Schema(allowableValues = { "RED", "YELLOW", "GREEN" })
    private String severity;
    /**
     * Yellow score.
     */
    private int score;
    /**
     * The category.
     */
    private String category;
    /**
     * Gets the severity.
     *
     * @return the severity
     */
    public String getSeverity() {
        return severity;
    }
    /**
     * Sets the severity.
     *
     * @param severity the severity to set
     */
    public void setSeverity(String severity) {
        this.severity = severity;
    }
    /**
     * Gets the category.
     *
     * @return the category
     */
    public String getCategory() {
        return category;
    }
    /**
     * Sets the category.
     *
     * @param category the category to set
     */
    public void setCategory(String category) {
        this.category = category;
    }
    /**
     * Gets the message.
     *
     * @return the message
     */
    public String getMessage() {
        return message;
    }
    /**
     * Sets the message.
     *
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }
    /**
     * @return the functionName
     */
    public String getFunctionName() {
        return functionName;
    }
    /**
     * @param functionName the functionName to set
     */
    public void setFunctionName(String functionName) {
        this.functionName = functionName;
    }
    /**
     * @return the functionDisplayName
     */
    public String getFunctionDisplayName() {
        return functionDisplayName;
    }
    /**
     * @param functionDisplayName the functionDisplayName to set
     */
    public void setFunctionDisplayName(String functionDisplayName) {
        this.functionDisplayName = functionDisplayName;
    }
    /**
     * @return the score
     */
    public int getScore() {
        return score;
    }
    /**
     * @param score the score to set
     */
    public void setScore(int score) {
        this.score = score;
    }
}

/*
 *
 *  * Unidata Platform
 *  * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *  *
 *  * Commercial License
 *  * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *  *
 *  * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 *  * For clarification or additional options, please contact: info@unidata-platform.com
 *  * -------
 *  * Disclaimer:
 *  * -------
 *  * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 *  * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 *  * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 *  * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 *
 */
package org.unidata.mdm.rest.v1.dq.core.ro.functions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import org.unidata.mdm.rest.core.ro.CustomPropertyRO;
import org.unidata.mdm.rest.v1.dq.core.ro.AbstractQualityElementRO;
import org.unidata.mdm.rest.v1.dq.core.ro.FunctionExecutionScopeRO;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import io.swagger.v3.oas.annotations.media.DiscriminatorMapping;
import io.swagger.v3.oas.annotations.media.Schema;


/**
 * @author Michael Yashin. Created on 20.05.2015.
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.EXISTING_PROPERTY, property = "type", visible = false)
@JsonSubTypes({
    @Type(value = JavaCleanseFunctionRO.class, name = "JAVA"),
    @Type(value = CompositeCleanseFunctionRO.class, name = "COMPOSITE"),
    @Type(value = GroovyCleanseFunctionRO.class, name = "GROOVY"),
    @Type(value = PythonCleanseFunctionRO.class, name = "PYTHON")
})
@Schema(type = "object",
        subTypes = {JavaCleanseFunctionRO.class, CompositeCleanseFunctionRO.class, GroovyCleanseFunctionRO.class, PythonCleanseFunctionRO.class},
        discriminatorMapping = {
                @DiscriminatorMapping(value = "JAVA", schema = JavaCleanseFunctionRO.class),
                @DiscriminatorMapping(value = "COMPOSITE", schema = CompositeCleanseFunctionRO.class),
                @DiscriminatorMapping(value = "GROOVY", schema = GroovyCleanseFunctionRO.class),
                @DiscriminatorMapping(value = "PYTHON", schema = PythonCleanseFunctionRO.class)
        },
        discriminatorProperty = "type")
public abstract class CleanseFunctionRO extends AbstractQualityElementRO {
    /**
     * CP.
     */
    protected List<CustomPropertyRO> customProperties;
    /**
     * Filtering mode.
     */
    protected List<FunctionExecutionScopeRO> supportedExecutionScopes;
    /**
     * The input ports.
     */
    protected List<PortRO> inputPorts = new ArrayList<>();
    /**
     * The output ports.
     */
    protected List<PortRO> outputPorts = new ArrayList<>();
    /**
     * Group name.
     */
    protected String groupName;
    /**
     * Current cleanse function state indicator/annotation.
     */
    protected String note;
    /*
     * This field is for disassembling only.
     * Not marshalled to DB.
     */
    protected boolean system;
    /*
     * This field is for disassembling only.
     * Not marshalled to DB.
     */
    protected boolean ready;
    /*
     * This field is for disassembling only.
     * Not marshalled to DB.
     */
    protected boolean configurable;
    /*
     * This field is for disassembling only.
     * Not marshalled to DB.
     */
    protected boolean editable;
    /**
     * Static funaction type.
     * @return function type
     */
    public abstract CleanseFunctionType getType();
    /**
     * @return the supportedExecutionScopes
     */
    public List<FunctionExecutionScopeRO> getSupportedExecutionScopes() {
        return Objects.isNull(supportedExecutionScopes) ? Collections.emptyList() : supportedExecutionScopes;
    }
    /**
     * @param supportedExecutionScopes the supportedExecutionScopes to set
     */
    public void setSupportedExecutionScopes(List<FunctionExecutionScopeRO> supportedExecutionContexts) {
        this.supportedExecutionScopes = supportedExecutionContexts;
    }
    /**
     * Gets the input ports.
     *
     * @return the input ports
     */
    public List<PortRO> getInputPorts() {
        return inputPorts;
    }
    /**
     * Sets the input ports.
     *
     * @param inputPorts the new input ports
     */
    public void setInputPorts(List<PortRO> inputPorts) {
        this.inputPorts = inputPorts;
    }
    /**
     * Gets the output ports.
     *
     * @return the output ports
     */
    public List<PortRO> getOutputPorts() {
        return outputPorts;
    }
    /**
     * Sets the output ports.
     *
     * @param outputPorts the new output ports
     */
    public void setOutputPorts(List<PortRO> outputPorts) {
        this.outputPorts = outputPorts;
    }
    /**
     * @return the customProperties
     */
    public List<CustomPropertyRO> getCustomProperties() {
        return customProperties;
    }
    /**
     * @param customProperties the customProperties to set
     */
    public void setCustomProperties(List<CustomPropertyRO> customProperties) {
        this.customProperties = customProperties;
    }
    /**
     * @return the groupName
     */
    public String getGroupName() {
        return groupName;
    }
    /**
     * @param groupName the groupName to set
     */
    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }
    /**
     * @return the note
     */
    public String getNote() {
        return note;
    }
    /**
     * @param note the note to set
     */
    public void setNote(String note) {
        this.note = note;
    }
    /**
     * @return the system
     */
    public boolean isSystem() {
        return system;
    }
    /**
     * @param system the system to set
     */
    public void setSystem(boolean system) {
        this.system = system;
    }
    /**
     * @return the ready
     */
    public boolean isReady() {
        return ready;
    }
    /**
     * @param ready the ready to set
     */
    public void setReady(boolean ready) {
        this.ready = ready;
    }
    /**
     * @return the configurable
     */
    public boolean isConfigurable() {
        return configurable;
    }
    /**
     * @param configurable the configurable to set
     */
    public void setConfigurable(boolean configurable) {
        this.configurable = configurable;
    }
    /**
     * @return the editable
     */
    public boolean isEditable() {
        return editable;
    }
    /**
     * @param editable the editable to set
     */
    public void setEditable(boolean editable) {
        this.editable = editable;
    }
}

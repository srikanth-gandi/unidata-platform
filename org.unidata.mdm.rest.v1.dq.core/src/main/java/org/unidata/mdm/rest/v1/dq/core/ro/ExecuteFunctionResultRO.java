package org.unidata.mdm.rest.v1.dq.core.ro;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

import org.unidata.mdm.rest.system.ro.DetailedOutputRO;
import org.unidata.mdm.rest.v1.dq.core.ro.execute.FunctionExecutionParamRO;
import org.unidata.mdm.rest.v1.dq.core.ro.result.DataQualityErrorRO;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Mikhail Mikhailov on Feb 9, 2021
 */
public class ExecuteFunctionResultRO extends DetailedOutputRO {
    /**
     * Success mark.
     */
    private boolean success;
    /**
     * The param/result containers.
     */
    @JsonProperty(value = "params")
    private List<FunctionExecutionParamRO> results;
    /**
     * Spots.
     */
    private List<DataQualityErrorRO> errors;
    /**
     * Constructor.
     */
    public ExecuteFunctionResultRO() {
        super();
    }
    /**
     * Constructor.
     */
    public ExecuteFunctionResultRO(boolean success) {
        super();
        this.success = success;
    }
    /**
     * @return the success
     */
    public boolean isSuccess() {
        return success;
    }
    /**
     * @param success the success to set
     */
    public void setSuccess(boolean success) {
        this.success = success;
    }
    /**
     * Gets the simple attributes.
     *
     * @return the simpleAttributes
     */
    public List<FunctionExecutionParamRO> getResults() {
        return results;
    }
    /**
     * Sets the input params.
     *
     * @param params the input params to set
     */
    public void setResults(List<FunctionExecutionParamRO> params) {
        this.results = params;
    }
    /**
     * @return the errors
     */
    public List<DataQualityErrorRO> getErrors() {
        return Objects.isNull(errors) ? Collections.emptyList() : errors;
    }
    /**
     * @param errors the errors to set
     */
    public void setErrors(List<DataQualityErrorRO> errors) {
        this.errors = errors;
    }
}

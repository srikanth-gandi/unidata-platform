/*
 *
 *  * Unidata Platform
 *  * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *  *
 *  * Commercial License
 *  * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *  *
 *  * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 *  * For clarification or additional options, please contact: info@unidata-platform.com
 *  * -------
 *  * Disclaimer:
 *  * -------
 *  * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 *  * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 *  * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 *  * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 *
 */
package org.unidata.mdm.rest.v1.dq.core.ro.functions;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * The Class ComplexCleanseFunction.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CompositeCFDefinition {

    /** The name. */
    private String name;

    /** The description. */
    private String description;

    /** The input ports. */
    private List<PortRO> inputPorts;

    /** The output ports. */
    private List<PortRO> outputPorts;

    /** The logic. */
    private CompositeFunctionLogicRO logic;

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name.
     *
     * @param name
     *            the new name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the description.
     *
     * @param description
     *            the new description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets the input ports.
     *
     * @return the input ports
     */
    public List<PortRO> getInputPorts() {
        return inputPorts;
    }

    /**
     * Sets the input ports.
     *
     * @param inputPorts
     *            the new input ports
     */
    public void setInputPorts(List<PortRO> inputPorts) {
        this.inputPorts = inputPorts;
    }

    /**
     * Gets the output ports.
     *
     * @return the output ports
     */
    public List<PortRO> getOutputPorts() {
        return outputPorts;
    }

    /**
     * Sets the output ports.
     *
     * @param outputPorts
     *            the new output ports
     */
    public void setOutputPorts(List<PortRO> outputPorts) {
        this.outputPorts = outputPorts;
    }

    /**
     * Gets the logic.
     *
     * @return the logic
     */
    public CompositeFunctionLogicRO getLogic() {
        return logic;
    }

    /**
     * Sets the logic.
     *
     * @param logic
     *            the new logic
     */
    public void setLogic(CompositeFunctionLogicRO logic) {
        this.logic = logic;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((description == null) ? 0 : description.hashCode());
        result = prime * result + ((inputPorts == null) ? 0 : inputPorts.hashCode());
        result = prime * result + ((logic == null) ? 0 : logic.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((outputPorts == null) ? 0 : outputPorts.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        CompositeCFDefinition other = (CompositeCFDefinition) obj;
        if (description == null) {
            if (other.description != null)
                return false;
        } else if (!description.equals(other.description))
            return false;
        if (inputPorts == null) {
            if (other.inputPorts != null)
                return false;
        } else if (!inputPorts.equals(other.inputPorts))
            return false;
        if (logic == null) {
            if (other.logic != null)
                return false;
        } else if (!logic.equals(other.logic))
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (outputPorts == null) {
            if (other.outputPorts != null)
                return false;
        } else if (!outputPorts.equals(other.outputPorts))
            return false;
        return true;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ComplexCleanseFunction [name=");
        builder.append(name);
        builder.append(", description=");
        builder.append(description);
        builder.append(", inputPorts=");
        builder.append(inputPorts);
        builder.append(", outputPorts=");
        builder.append(outputPorts);
        builder.append(", logic=");
        builder.append(logic);
        builder.append("]");
        return builder.toString();
    }
}

/*
 *
 *  * Unidata Platform
 *  * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *  *
 *  * Commercial License
 *  * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *  *
 *  * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 *  * For clarification or additional options, please contact: info@unidata-platform.com
 *  * -------
 *  * Disclaimer:
 *  * -------
 *  * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 *  * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 *  * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 *  * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 *
 */
package org.unidata.mdm.rest.v1.dq.core.ro.rule;

import java.util.List;

import org.unidata.mdm.rest.core.ro.CustomPropertyRO;
import org.unidata.mdm.rest.v1.dq.core.ro.AbstractQualityElementRO;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * REST DQ Rule.
 * @author Michael Yashin. Created on 11.06.2015.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class QualityRuleRO extends AbstractQualityElementRO {
    /**
     * The custom properties.
     */
    private List<CustomPropertyRO> customProperties;
    /**
     * The cleanse function name.
     */
    private String cleanseFunctionName;
    /**
     * The origins.
     */
    private SelectionSettingsRO selectionSettings;
    /**
     * The raise.
     */
    private ValidationSettingsRO validationSettings;
    /**
     * The enrich.
     */
    private EnrichmentSettingsRO enrichmentSettings;
    /**
     * The run type.
     */
    @Schema(allowableValues = {"RUN_ALWAYS", "RUN_NEVER", "RUN_ON_REQUIRED_PRESENT", "RUN_ON_ALL_PRESENT"})
    private String runCondition;
    /**
     * Gets the cleanse function name.
     *
     * @return the cleanse function name
     */
    public String getCleanseFunctionName() {
        return cleanseFunctionName;
    }

    /**
     * Sets the cleanse function name.
     *
     * @param cleanseFunctionName the new cleanse function name
     */
    public void setCleanseFunctionName(String cleanseFunctionName) {
        this.cleanseFunctionName = cleanseFunctionName;
    }

    /**
     * Gets the origins.
     *
     * @return the origins
     */
    public SelectionSettingsRO getSelectionSettings() {
        return selectionSettings;
    }

    /**
     * Sets the origins.
     *
     * @param origins the new origins
     */
    public void setSelectionSettings(SelectionSettingsRO origins) {
        this.selectionSettings = origins;
    }

    /**
     * Gets the raise.
     *
     * @return the raise
     */
    public ValidationSettingsRO getValidationSettings() {
        return validationSettings;
    }

    /**
     * Sets the raise.
     *
     * @param raise the new raise
     */
    public void setValidationSettings(ValidationSettingsRO raise) {
        this.validationSettings = raise;
    }

    /**
     * Gets the enrich.
     *
     * @return the enrich
     */
    public EnrichmentSettingsRO getEnrichmentSettings() {
        return enrichmentSettings;
    }

    /**
     * Sets the enrich.
     *
     * @param enrich the new enrich
     */
    public void setEnrichmentSettings(EnrichmentSettingsRO enrich) {
        this.enrichmentSettings = enrich;
    }
    /**
     * Gets the run type.
     *
     * @return the runCondition
     */
    public String getRunCondition() {
        return runCondition;
    }

    /**
     * Sets the run type.
     *
     * @param runCondition the runCondition to set
     */
    public void setRunCondition(String runType) {
        this.runCondition = runType;
    }
    /**
     * Gets the custom properties.
     *
     * @return the custom properties
     */
    public List<CustomPropertyRO> getCustomProperties() {
        return customProperties;
    }

    /**
     * Sets the custom properties.
     *
     * @param customProperties the new custom properties
     */
    public void setCustomProperties(List<CustomPropertyRO> customProperties) {
        this.customProperties = customProperties;
    }
}

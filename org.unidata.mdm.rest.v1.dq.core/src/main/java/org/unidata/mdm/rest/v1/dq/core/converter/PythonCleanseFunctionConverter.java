/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.v1.dq.core.converter;

import org.unidata.mdm.dq.core.type.model.source.PythonCleanseFunctionSource;
import org.unidata.mdm.rest.v1.dq.core.ro.functions.PythonCleanseFunctionRO;

/**
 * @author Mikhail Mikhailov on Feb 8, 2021
 */
public class PythonCleanseFunctionConverter extends AbstractCleanseFunctionConverter<PythonCleanseFunctionSource, PythonCleanseFunctionRO> {
    /**
     * Constructor.
     * @param in
     * @param out
     */
    public PythonCleanseFunctionConverter() {
        super(PythonCleanseFunctionConverter::convert, PythonCleanseFunctionConverter::convert);
    }

    private static PythonCleanseFunctionRO convert(PythonCleanseFunctionSource source) {

        PythonCleanseFunctionRO target = new PythonCleanseFunctionRO();

        convert(source, target);

        target.setLibraryName(source.getLibraryName());
        target.setLibraryVersion(source.getLibraryVersion());

        return target;
    }

    private static PythonCleanseFunctionSource convert(PythonCleanseFunctionRO source) {

        PythonCleanseFunctionSource  target = new PythonCleanseFunctionSource ();

        convert(source, target);

        return target
                .withLibraryName(source.getLibraryName())
                .withLibraryVersion(source.getLibraryVersion());
    }
}

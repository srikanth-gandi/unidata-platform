/*
 *
 *  * Unidata Platform
 *  * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *  *
 *  * Commercial License
 *  * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *  *
 *  * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 *  * For clarification or additional options, please contact: info@unidata-platform.com
 *  * -------
 *  * Disclaimer:
 *  * -------
 *  * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 *  * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 *  * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 *  * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 *
 */
package org.unidata.mdm.rest.v1.dq.core.ro.functions;

/**
 * The Enum CleanseFunctionType.
 *
 * @author Michael Yashin. Created on 20.05.2015.
 */
public enum CleanseFunctionType {
    /**
     * The simple java function.
     */
    JAVA("JAVA"),
    /**
     * Groovy cleanse function.
     */
    GROOVY("GROOVY"),
    /**
     * Python cleanse function.
     */
    PYTHON("PYTHON"),
    /**
     * Composite function.
     */
    COMPOSITE("COMPOSITE");

    /** The value. */
    private final String value;

    /**
     * Instantiates a new cleanse function type.
     *
     * @param v
     *            the v
     */
    CleanseFunctionType(String v) {
        value = v;
    }

    /**
     * Value.
     *
     * @return the string
     */
    public String value() {
        return value;
    }

    /**
     * From value.
     *
     * @param v
     *            the v
     * @return the cleanse function type
     */
    public static CleanseFunctionType fromValue(String v) {
        for (CleanseFunctionType c : CleanseFunctionType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}

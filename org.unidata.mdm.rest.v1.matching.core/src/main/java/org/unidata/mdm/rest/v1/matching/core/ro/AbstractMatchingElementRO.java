/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.v1.matching.core.ro;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.time.OffsetDateTime;

/**
 * @author Sergey Murskiy on 08.06.2021
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class AbstractMatchingElementRO {
    /**
     * Name.
     */
    protected String name;
    /**
     * Display name.
     */
    protected String displayName;
    /**
     * Description.
     */
    protected String description;
    /**
     * Create date.
     */
    protected OffsetDateTime createDate;
    /**
     * Update date.
     */
    protected OffsetDateTime updateDate;
    /**
     * Created by.
     */
    protected String createdBy;
    /**
     * Updated by.
     */
    protected String updatedBy;

    /**
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the displayName
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * @param displayName the displayName to set
     */
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    /**
     * @return description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the createdAt
     */
    public OffsetDateTime getCreateDate() {
        return createDate;
    }

    /**
     * @param createdAt the createdAt to set
     */
    public void setCreateDate(OffsetDateTime createdAt) {
        this.createDate = createdAt;
    }

    /**
     * @return the updatedAt
     */
    public OffsetDateTime getUpdateDate() {
        return updateDate;
    }

    /**
     * @param updatedAt the updatedAt to set
     */
    public void setUpdateDate(OffsetDateTime updatedAt) {
        this.updateDate = updatedAt;
    }

    /**
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * @param createdBy the createdBy to set
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    /**
     * @return the updatedBy
     */
    public String getUpdatedBy() {
        return updatedBy;
    }

    /**
     * @param updatedBy the updatedBy to set
     */
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }
}

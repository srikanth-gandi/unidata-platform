/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.v1.matching.core.converter;

import org.springframework.stereotype.Component;
import org.unidata.mdm.matching.core.type.cluster.Cluster;
import org.unidata.mdm.matching.core.type.cluster.ClusterRecord;
import org.unidata.mdm.rest.v1.matching.core.ro.cluster.ClusterRO;
import org.unidata.mdm.rest.v1.matching.core.ro.cluster.ClusterRecordRO;
import org.unidata.mdm.system.convert.Converter;

/**
 * @author Sergey Murskiy on 22.08.2021
 */
@Component
public class ClusterConverter extends Converter<Cluster, ClusterRO> {
    /**
     * Cluster record converter.
     */
    private final ClusterRecordConverter clusterRecordConverter = new ClusterRecordConverter();

    /**
     * Constructor.
     */
    public ClusterConverter() {
        this.to = this::convert;
    }

    private ClusterRO convert(Cluster source) {
        ClusterRO target = new ClusterRO();

        target.setId(source.getId());
        target.setSetName(source.getSetName());
        target.setRuleName(source.getRuleName());
        target.setMatchingDate(source.getMatchingDate());
        target.setAdditionalParameters(source.getAdditionalParameters());
        target.setOwner(clusterRecordConverter.to(source.getOwner()));
        target.setClusterRecords(clusterRecordConverter.to(source.getClusterRecords()));
        target.setRecordsCount(source.getRecordsCount());

        return target;
    }

    /**
     * Cluster record converter.
     */
    private static class ClusterRecordConverter extends Converter<ClusterRecord, ClusterRecordRO> {
        /**
         * Constructor.
         */
        public ClusterRecordConverter() {
            this.to = this::convert;
        }

        private ClusterRecordRO convert(ClusterRecord source) {
            ClusterRecordRO target = new ClusterRecordRO();

            target.setNamespace(source.getNamespace());
            target.setTypeName(source.getTypeName());
            target.setSubjectId(source.getSubjectId());
            target.setValidFrom(source.getValidFrom());
            target.setValidTo(source.getValidTo());
            target.setMatchingRate(source.getMatchingRate());

            return target;
        }
    }
}

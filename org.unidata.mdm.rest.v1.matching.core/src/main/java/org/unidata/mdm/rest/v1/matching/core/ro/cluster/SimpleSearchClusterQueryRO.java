/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.v1.matching.core.ro.cluster;

import io.swagger.v3.oas.annotations.Parameter;
import org.unidata.mdm.rest.v1.search.ro.SearchQueryRO;
import org.unidata.mdm.rest.v1.search.ro.SearchSortFieldRO;

import java.util.List;

/**
 * @author Sergey Murskiy on 02.07.2021
 */
public class SimpleSearchClusterQueryRO extends SearchQueryRO {
    @Parameter(description = "Cluster search type")
    private SearchClusterType searchClusterType;

    @Parameter(description = "Apply the following sorting settings")
    private List<SearchSortFieldRO> sortFields;

    @Parameter(description = "Fields to return.")
    private List<String> returnFields;

    @Parameter(description = "Return total count")
    private boolean totalCount;

    @Parameter(description = "Count only or return results too")
    private boolean countOnly;

    @Parameter(description = "Shortcut for all searchable fields")
    private boolean returnAllFields;

    @Parameter(description = "Set default sort if sortFields is empty")
    private boolean defaultSort;

    @Parameter(description = "Fetch all without applying a query")
    private boolean fetchAll;

    @Parameter(description = "Search as type")
    private boolean sayt;

    @Parameter(description = "Result limit")
    private int count;

    @Parameter(description = "Request offset")
    private int page;

    public SearchClusterType getSearchClusterType() {
        return searchClusterType;
    }

    public void setSearchClusterType(SearchClusterType searchClusterType) {
        this.searchClusterType = searchClusterType;
    }

    public List<SearchSortFieldRO> getSortFields() {
        return sortFields;
    }

    public void setSortFields(List<SearchSortFieldRO> sortFields) {
        this.sortFields = sortFields;
    }

    public List<String> getReturnFields() {
        return returnFields;
    }

    public void setReturnFields(List<String> returnFields) {
        this.returnFields = returnFields;
    }

    public boolean isTotalCount() {
        return totalCount;
    }

    public void setTotalCount(boolean totalCount) {
        this.totalCount = totalCount;
    }

    public boolean isCountOnly() {
        return countOnly;
    }

    public void setCountOnly(boolean countOnly) {
        this.countOnly = countOnly;
    }

    public boolean isReturnAllFields() {
        return returnAllFields;
    }

    public void setReturnAllFields(boolean returnAllFields) {
        this.returnAllFields = returnAllFields;
    }

    public boolean isDefaultSort() {
        return defaultSort;
    }

    public void setDefaultSort(boolean defaultSort) {
        this.defaultSort = defaultSort;
    }

    public boolean isFetchAll() {
        return fetchAll;
    }

    public void setFetchAll(boolean fetchAll) {
        this.fetchAll = fetchAll;
    }

    public boolean isSayt() {
        return sayt;
    }

    public void setSayt(boolean sayt) {
        this.sayt = sayt;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }
}

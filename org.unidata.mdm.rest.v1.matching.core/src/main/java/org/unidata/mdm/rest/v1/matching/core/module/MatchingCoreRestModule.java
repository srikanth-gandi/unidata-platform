/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.v1.matching.core.module;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.unidata.mdm.rest.v1.matching.core.rendering.MatchingRenderingProvider;
import org.unidata.mdm.rest.v1.search.type.rendering.SearchRestInputRenderingAction;
import org.unidata.mdm.system.type.module.AbstractModule;
import org.unidata.mdm.system.type.module.Dependency;
import org.unidata.mdm.system.type.rendering.RenderingAction;
import org.unidata.mdm.system.type.rendering.RenderingProvider;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Matching core rest module.
 *
 * @author Sergey Murskiy on 07.06.2021
 */
public class MatchingCoreRestModule extends AbstractModule {
    /**
     * Logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(MatchingCoreRestModule.class);

    /**
     * Module dependencies.
     */
    private static final Collection<Dependency> DEPENDENCIES = Arrays.asList(
            new Dependency("org.unidata.mdm.rest.core", "6.0"),
            new Dependency("org.unidata.mdm.rest.system", "6.0"),
            new Dependency("org.unidata.mdm.system", "6.0"),
            new Dependency("org.unidata.mdm.matching.core", "6.3"));

    /**
     * Rendering actions.
     */
    private static final List<RenderingAction> RENDERING_ACTIONS = List.of(
            SearchRestInputRenderingAction.SIMPLE_SEARCH_INPUT
    );

    /**
     * Module id.
     */
    public static final String MODULE_ID = "org.unidata.mdm.rest.v1.matching.core";

    /**
     * Matching rendering provider.
     */
    @Autowired
    private MatchingRenderingProvider matchingRenderingProvider;

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<RenderingAction> getRenderingActions() {
        return RENDERING_ACTIONS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @NonNull
    public Collection<RenderingProvider> getRenderingProviders() {
        return Collections.singletonList(matchingRenderingProvider);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<Dependency> getDependencies() {
        return DEPENDENCIES;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getId() {
        return MODULE_ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getVersion() {
        return "6.3";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getName() {
        return "Matching Rest";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getDescription() {
        return "Matching Rest module";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void install() {
        LOGGER.info("Install");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void uninstall() {
        LOGGER.info("Uninstall");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void start() {
        LOGGER.info("Start");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void stop() {
        LOGGER.info("Stop");
    }
}

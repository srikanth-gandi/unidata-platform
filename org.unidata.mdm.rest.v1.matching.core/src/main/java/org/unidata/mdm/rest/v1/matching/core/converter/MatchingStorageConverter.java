/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.v1.matching.core.converter;

import org.springframework.stereotype.Component;
import org.unidata.mdm.matching.core.type.storage.MatchingStorageInfo;
import org.unidata.mdm.rest.v1.matching.core.ro.storage.MatchingStorageRO;
import org.unidata.mdm.system.convert.Converter;

/**
 * @author Sergey Murskiy on 09.09.2021
 */
@Component
public class MatchingStorageConverter extends Converter<MatchingStorageInfo, MatchingStorageRO> {

    /**
     * Constructor.
     */
    public MatchingStorageConverter() {
        this.to = this::convert;
    }

    private MatchingStorageRO convert(MatchingStorageInfo source) {
        MatchingStorageRO ro = new MatchingStorageRO();

        ro.setName(source.getName());
        ro.setDisplayName(source.getDisplayName());
        ro.setDescription(source.getDescription());

        return ro;
    }

}

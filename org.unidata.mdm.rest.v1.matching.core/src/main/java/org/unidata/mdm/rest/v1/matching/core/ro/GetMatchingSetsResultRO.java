/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.v1.matching.core.ro;

import org.unidata.mdm.rest.system.ro.DetailedOutputRO;
import org.unidata.mdm.rest.v1.matching.core.ro.rule.MatchingRuleSetRO;

import java.util.List;

/**
 * @author Sergey Murskiy on 11.06.2021
 */
public class GetMatchingSetsResultRO extends DetailedOutputRO {
    /**
     * Matching rule sets.
     */
    private List<MatchingRuleSetRO> sets;

    /**
     * Constructor.
     */
    public GetMatchingSetsResultRO() {
        super();
    }

    /**
     * Constructor.
     *
     * @param sets matching rule sets
     */
    public GetMatchingSetsResultRO(List<MatchingRuleSetRO> sets) {
        this.sets = sets;
    }

    /**
     * @return matching rule sets
     */
    public List<MatchingRuleSetRO> getSets() {
        return sets;
    }

    /**
     * @param sets matching rule sets to set
     */
    public void setSets(List<MatchingRuleSetRO> sets) {
        this.sets = sets;
    }
}

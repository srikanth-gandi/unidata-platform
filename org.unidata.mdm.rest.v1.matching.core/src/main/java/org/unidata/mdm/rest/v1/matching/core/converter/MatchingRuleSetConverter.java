/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.v1.matching.core.converter;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.unidata.mdm.core.util.SecurityUtils;
import org.unidata.mdm.matching.core.type.model.source.algorithm.AlgorithmMappingSource;
import org.unidata.mdm.matching.core.type.model.source.rule.MatchingRuleMappingSource;
import org.unidata.mdm.matching.core.type.model.source.rule.MatchingRuleSetSource;
import org.unidata.mdm.rest.core.converter.CustomPropertiesConverter;
import org.unidata.mdm.rest.v1.matching.core.ro.algorithm.AlgorithmMappingRO;
import org.unidata.mdm.rest.v1.matching.core.ro.rule.MatchingRuleMappingRO;
import org.unidata.mdm.rest.v1.matching.core.ro.rule.MatchingRuleSetRO;
import org.unidata.mdm.system.convert.Converter;

import java.time.OffsetDateTime;
import java.util.Objects;

/**
 * @author Sergey Murskiy on 23.06.2021
 */
@Component
public class MatchingRuleSetConverter extends Converter<MatchingRuleSetSource, MatchingRuleSetRO> {
    /**
     * Rule mapping converter.
     */
    private static final MatchingRuleMappingConverter RULE_MAPPING_CONVERTER = new MatchingRuleMappingConverter();

    /**
     * Constructor.
     */
    public MatchingRuleSetConverter() {
        this.to = this::convert;
        this.from = this::convert;
    }

    private MatchingRuleSetRO convert(MatchingRuleSetSource source) {
        MatchingRuleSetRO ro = new MatchingRuleSetRO();

        ro.setName(source.getName());
        ro.setDisplayName(source.getDisplayName());
        ro.setDescription(source.getDescription());
        ro.setMatchingStorageId(source.getMatchingStorageId());
        ro.setMatchingTable(source.getMatchingTable());
        ro.setMappings(RULE_MAPPING_CONVERTER.to(source.getMappings()));
        ro.setCreateDate(source.getCreateDate());
        ro.setCreatedBy(source.getCreatedBy());
        ro.setUpdateDate(source.getUpdateDate());
        ro.setUpdatedBy(source.getUpdatedBy());
        ro.setCustomProperties(CustomPropertiesConverter.to(source.getCustomProperties()));

        return ro;
    }

    private MatchingRuleSetSource convert(MatchingRuleSetRO source) {
        String user = SecurityUtils.getCurrentUserName();
        OffsetDateTime now = OffsetDateTime.now();

        return new MatchingRuleSetSource()
                .withName(source.getName())
                .withDisplayName(source.getDisplayName())
                .withDescription(source.getDescription())
                .withMatchingStorageId(source.getMatchingStorageId())
                .withMatchingTable(source.getMatchingTable())
                .withMappings(RULE_MAPPING_CONVERTER.from(source.getMappings()))
                .withCreateDate(Objects.isNull(source.getCreateDate()) ? now : source.getCreateDate())
                .withCreatedBy(StringUtils.isBlank(source.getCreatedBy()) ? user : source.getCreatedBy())
                .withUpdateDate(Objects.isNull(source.getCreateDate()) ? null : now)
                .withUpdatedBy(StringUtils.isBlank(source.getCreatedBy()) ? null : user)
                .withCustomProperties(CustomPropertiesConverter.from(source.getCustomProperties()));
    }

    private static class MatchingRuleMappingConverter extends Converter<MatchingRuleMappingSource, MatchingRuleMappingRO> {
        /**
         * Algorithm mapping converter.
         */
        private static final AlgorithmMappingConverter ALGORITHM_MAPPING_CONVERTER = new AlgorithmMappingConverter();

        /**
         * Constructor.
         */
        public MatchingRuleMappingConverter() {
            super(MatchingRuleMappingConverter::convert, MatchingRuleMappingConverter::convert);
        }

        private static MatchingRuleMappingRO convert(MatchingRuleMappingSource source) {
            MatchingRuleMappingRO ro = new MatchingRuleMappingRO();

            ro.setRuleName(source.getRuleName());
            ro.setMappings(ALGORITHM_MAPPING_CONVERTER.to(source.getMappings()));

            return ro;
        }

        private static MatchingRuleMappingSource convert(MatchingRuleMappingRO source) {
            return new MatchingRuleMappingSource()
                    .withRuleName(source.getRuleName())
                    .withMappings(ALGORITHM_MAPPING_CONVERTER.from(source.getMappings()));
        }
    }

    private static class AlgorithmMappingConverter extends Converter<AlgorithmMappingSource, AlgorithmMappingRO> {
        /**
         * Constructor.
         */
        public AlgorithmMappingConverter() {
            super(AlgorithmMappingConverter::convert, AlgorithmMappingConverter::convert);
        }

        private static AlgorithmMappingRO convert(AlgorithmMappingSource source) {
            AlgorithmMappingRO ro = new AlgorithmMappingRO();

            ro.setSettingsId(source.getSettingsId());
            ro.setColumnName(source.getColumnName());

            return ro;
        }

        private static AlgorithmMappingSource convert(AlgorithmMappingRO source) {
            return new AlgorithmMappingSource()
                    .withAlgorithmSettingsId(source.getSettingsId())
                    .withColumnName(source.getColumnName());
        }
    }
}

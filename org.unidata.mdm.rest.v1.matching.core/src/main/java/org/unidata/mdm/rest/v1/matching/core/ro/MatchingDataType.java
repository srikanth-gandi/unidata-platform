/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.v1.matching.core.ro;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * @author Sergey Murskiy on 11.06.2021
 */
public enum MatchingDataType {
    /**
     * The string type.
     */
    STRING("String"),
    /**
     * Dictionary type (attribute-local enumeration type).
     */
    DICTIONARY("Dictionary"),
    /**
     * The integer type (long 8 bytes).
     */
    INTEGER("Integer"),
    /**
     * The floating point type (double 8 bytes).
     */
    NUMBER("Number"),
    /**
     * The boolean type.
     */
    BOOLEAN("Boolean"),
    /**
     * Binary large object.
     */
    BLOB("Blob"),
    /**
     * Character large object.
     */
    CLOB("Clob"),
    /**
     * The date type.
     */
    DATE("Date"),
    /**
     * The time type.
     */
    TIME("Time"),
    /**
     * The timestamp type.
     */
    TIMESTAMP("Timestamp"),
    /**
     * Link to a global enum value.
     */
    ENUM("Enum"),
    /**
     * Special href template, processed by get post-processor, type.
     */
    LINK("Link"),
    /**
     * Special type of number.
     */
    MEASURED("Measured"),
    /**
     * Any type.
     */
    ANY("Any");

    /**
     * Value.
     */
    private final String value;

    /**
     * Constructor.
     *
     * @param value the value to set
     */
    MatchingDataType(String value) {
        this.value = value;
    }

    /**
     * @return the value
     */
    @JsonValue
    public String getValue() {
        return value;
    }

    /**
     * From value.
     *
     * @param v the value
     * @return the matching data type
     */
    @JsonCreator
    public static MatchingDataType fromValue(String v) {
        for (MatchingDataType c : MatchingDataType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}

/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.v1.matching.core.rendering;

import org.unidata.mdm.matching.core.type.search.ClusterChildIndexType;
import org.unidata.mdm.matching.core.type.search.ClusterHeadIndexType;
import org.unidata.mdm.rest.system.service.TypedRestOutputRenderer;
import org.unidata.mdm.rest.v1.matching.core.module.MatchingCoreRestModule;
import org.unidata.mdm.rest.v1.search.converter.SearchResultToRestSearchResultConverter;
import org.unidata.mdm.rest.v1.search.ro.SearchResponseRO;
import org.unidata.mdm.rest.v1.search.ro.SearchResultRO;
import org.unidata.mdm.search.dto.ComplexSearchResultDTO;
import org.unidata.mdm.search.dto.SearchOutputContainer;
import org.unidata.mdm.search.dto.SearchResultDTO;
import org.unidata.mdm.system.type.rendering.FieldDef;
import org.unidata.mdm.system.type.rendering.FragmentDef;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * @author Sergey Murskiy on 20.07.2021
 */
public class SearchClusterResponseRenderer extends TypedRestOutputRenderer<SearchResponseRO, SearchOutputContainer> {

    /**
     * Constructor.
     */
    public SearchClusterResponseRenderer() {
        super(SearchResponseRO.class, SearchOutputContainer.class,
                Collections.singletonList(FieldDef.fieldDef(MatchingCoreRestModule.MODULE_ID, SearchResultRO.class))
        );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Map<String, Object> renderFragmentFields(FragmentDef fragmentDef, SearchOutputContainer container) {
        if (container instanceof SearchResultDTO && !((SearchResultDTO) container).getIndexType()
                .isOneOf(ClusterHeadIndexType.CLUSTER, ClusterChildIndexType.CLUSTER_RECORD)) {

            return Collections.emptyMap();
        }

        if (container instanceof ComplexSearchResultDTO && !((ComplexSearchResultDTO) container).getMain().getIndexType()
                .isOneOf(ClusterHeadIndexType.CLUSTER, ClusterChildIndexType.CLUSTER_RECORD)) {

            return Collections.emptyMap();
        }

        final SearchResultRO output = new SearchResultRO();
        Map<String, Object> result = new HashMap<>();
        result.put(MatchingCoreRestModule.MODULE_ID, output);

        if (SearchOutputContainer.SearchOutputContainerType.COMPLEX == container.getContainerType()) {
            renderComplexSearchOutput((ComplexSearchResultDTO) container, output);

        } else if (SearchOutputContainer.SearchOutputContainerType.SIMPLE == container.getContainerType()) {
            renderSimpleSearchOutput((SearchResultDTO) container, output);
        }

        return result;
    }

    private void renderComplexSearchOutput(ComplexSearchResultDTO result, SearchResultRO output) {
        Objects.requireNonNull(result, "Result can't be null");
        Objects.requireNonNull(output, "Output can't be null");

        renderSimpleSearchOutput(result.getMain(), output);
    }

    private void renderSimpleSearchOutput(SearchResultDTO result, SearchResultRO output) {
        Objects.requireNonNull(result, "Result can't be null");
        Objects.requireNonNull(output, "Output can't be null");

        if (result.getIndexType().isOneOf(ClusterHeadIndexType.CLUSTER, ClusterChildIndexType.CLUSTER_RECORD)) {
            SearchResultRO local = SearchResultToRestSearchResultConverter.convert(result);
            output.getHits().addAll(local.getHits());
            output.setFields(local.getFields());
            output.setHasRecords(local.isHasRecords());
            output.setMaxScore(local.getMaxScore());
            output.setTotalCount(output.getTotalCount() + local.getTotalCount());
            output.setTotalCountLimit(local.getTotalCountLimit());
        }
    }
}

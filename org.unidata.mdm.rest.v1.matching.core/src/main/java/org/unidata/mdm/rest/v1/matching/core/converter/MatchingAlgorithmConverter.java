/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.v1.matching.core.converter;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.core.util.SecurityUtils;
import org.unidata.mdm.matching.core.type.model.source.algorithm.AlgorithmParamConfigurationSource;
import org.unidata.mdm.matching.core.type.model.source.algorithm.EnumValueSource;
import org.unidata.mdm.matching.core.type.model.source.algorithm.MatchingAlgorithmSource;
import org.unidata.mdm.rest.core.converter.CustomPropertiesConverter;
import org.unidata.mdm.rest.v1.matching.core.ro.AlgorithmParameterType;
import org.unidata.mdm.rest.v1.matching.core.ro.algorithm.AlgorithmParamConfigurationRO;
import org.unidata.mdm.rest.v1.matching.core.ro.algorithm.EnumValueRO;
import org.unidata.mdm.rest.v1.matching.core.ro.algorithm.MatchingAlgorithmRO;
import org.unidata.mdm.system.convert.Converter;

import java.time.OffsetDateTime;
import java.util.Objects;

/**
 * @author Sergey Murskiy on 23.06.2021
 */
@Component
public class MatchingAlgorithmConverter extends Converter<MatchingAlgorithmSource, MatchingAlgorithmRO> {

    /**
     * Parameter converter.
     */
    @Autowired
    private AlgorithmParameterConverter parameterConverter;

    /**
     * Parameter configuration converter.
     */
    private final AlgorithmParamConfigurationConverter paramConfigurationConverter = new AlgorithmParamConfigurationConverter();

    /**
     * Constructor.
     */
    public MatchingAlgorithmConverter() {
        this.to = this::convert;
        this.from = this::convert;
    }

    private MatchingAlgorithmRO convert(MatchingAlgorithmSource source) {
        MatchingAlgorithmRO ro = new MatchingAlgorithmRO();

        ro.setName(source.getName());
        ro.setDisplayName(source.getDisplayName());
        ro.setDescription(source.getDescription());
        ro.setMatchingStorageId(source.getMatchingStorageId());
        ro.setJavaClass(source.getJavaClass());
        ro.setLibraryName(source.getLibraryName());
        ro.setLibraryVersion(source.getLibraryVersion());
        ro.setParameters(paramConfigurationConverter.to(source.getParameters()));
        ro.setCreateDate(source.getCreateDate());
        ro.setCreatedBy(source.getCreatedBy());
        ro.setUpdateDate(source.getUpdateDate());
        ro.setUpdatedBy(source.getUpdatedBy());
        ro.setSystem(source.isSystem());
        ro.setConfigurable(source.isConfigurable());
        ro.setEditable(source.isEditable());
        ro.setReady(source.isReady());
        ro.setNote(source.getNote());
        ro.setCustomProperties(CustomPropertiesConverter.to(source.getCustomProperties()));

        return ro;
    }

    private MatchingAlgorithmSource convert(MatchingAlgorithmRO source) {
        String user = SecurityUtils.getCurrentUserName();
        OffsetDateTime now = OffsetDateTime.now();

        return new MatchingAlgorithmSource()
                .withName(source.getName())
                .withDisplayName(source.getDisplayName())
                .withDescription(source.getDescription())
                .withMatchingStorageId(source.getMatchingStorageId())
                .withJavaClass(source.getJavaClass())
                .withLibraryName(source.getLibraryName())
                .withLibraryVersion(source.getLibraryVersion())
                .withParameters(paramConfigurationConverter.from(source.getParameters()))
                .withCreateDate(Objects.isNull(source.getCreateDate()) ? now : source.getCreateDate())
                .withCreatedBy(StringUtils.isBlank(source.getCreatedBy()) ? user : source.getCreatedBy())
                .withUpdateDate(Objects.isNull(source.getCreateDate()) ? null : now)
                .withUpdatedBy(StringUtils.isBlank(source.getCreatedBy()) ? null : user)
                .withCustomProperties(CustomPropertiesConverter.from(source.getCustomProperties()));
    }

    private class AlgorithmParamConfigurationConverter extends Converter<AlgorithmParamConfigurationSource, AlgorithmParamConfigurationRO> {
        /**
         * Enum value converter.
         */
        private final EnumValueConverter enumValueConverter = new EnumValueConverter();

        /**
         * Constructor.
         */
        public AlgorithmParamConfigurationConverter() {
            this.to = this::convert;
            this.from = this::convert;
        }

        private AlgorithmParamConfigurationRO convert(AlgorithmParamConfigurationSource source) {
            AlgorithmParamConfigurationRO ro = new AlgorithmParamConfigurationRO();

            ro.setName(source.getName());
            ro.setDisplayName(source.getDisplayName());
            ro.setDescription(source.getDescription());
            ro.setType(AlgorithmParameterType.fromValue(source.getType().getValue()));
            ro.setDefaultValue(parameterConverter.to(source.getDefaultValue()));
            ro.setEnumValues(enumValueConverter.to(source.getEnumValues()));
            ro.setRequired(source.isRequired());

            return ro;
        }

        private AlgorithmParamConfigurationSource convert(AlgorithmParamConfigurationRO ro) {
            return new AlgorithmParamConfigurationSource()
                    .withName(ro.getName())
                    .withDisplayName(ro.getDisplayName())
                    .withDescription(ro.getDescription())
                    .withType(org.unidata.mdm.matching.core.type.algorithm.AlgorithmParameterType.fromValue(ro.getType().getValue()))
                    .withDefaultValue(parameterConverter.from(ro.getDefaultValue()))
                    .withEnumValues(enumValueConverter.from(ro.getEnumValues()))
                    .withRequired(ro.isRequired());
        }
    }

    private static class EnumValueConverter extends Converter<EnumValueSource, EnumValueRO> {
        /**
         * Constructor.
         */
        public EnumValueConverter() {
            this.to = this::convert;
            this.from = this::convert;
        }

        private EnumValueRO convert(EnumValueSource source) {
            EnumValueRO ro = new EnumValueRO();

            ro.setName(source.getName());
            ro.setDisplayName(source.getDisplayName());

            return ro;
        }

        private EnumValueSource convert(EnumValueRO ro) {
            return new EnumValueSource()
                    .withName(ro.getName())
                    .withDisplayName(ro.getDisplayName());
        }
    }
}

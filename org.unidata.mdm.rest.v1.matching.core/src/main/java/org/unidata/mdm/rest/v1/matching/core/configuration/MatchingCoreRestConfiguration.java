/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.v1.matching.core.configuration;

import java.util.Arrays;
import java.util.Collections;

import javax.ws.rs.ext.RuntimeDelegate;

import org.apache.cxf.Bus;
import org.apache.cxf.endpoint.Server;
import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.apache.cxf.jaxrs.openapi.OpenApiFeature;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.unidata.mdm.rest.system.exception.DetailedRestExceptionMapper;
import org.unidata.mdm.rest.system.service.ReceiveInInterceptor;
import org.unidata.mdm.rest.system.util.OpenApiMetadataFactory;
import org.unidata.mdm.rest.v1.matching.core.service.ClusterRestService;
import org.unidata.mdm.rest.v1.matching.core.service.MatchingAlgorithmRestService;
import org.unidata.mdm.rest.v1.matching.core.service.MatchingAssignmentRestService;
import org.unidata.mdm.rest.v1.matching.core.service.MatchingModelRestService;
import org.unidata.mdm.rest.v1.matching.core.service.MatchingRestApplication;
import org.unidata.mdm.rest.v1.matching.core.service.MatchingRuleRestService;
import org.unidata.mdm.rest.v1.matching.core.service.MatchingRuleSetRestService;
import org.unidata.mdm.rest.v1.matching.core.service.MatchingTableRestService;
import org.unidata.mdm.system.configuration.ModuleConfiguration;

import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;

/**
 * Matching rest core configuration.
 *
 * @author Sergey Murskiy on 08.06.2021
 */
@Configuration
public class MatchingCoreRestConfiguration extends ModuleConfiguration {
    /**
     * Configuration id.
     */
    public static final ConfigurationId ID = () -> "MATCHING_CORE_REST_CONFIGURATION";
    /**
     * {@inheritDoc}
     */
    @Override
    public ConfigurationId getId() {
        return ID;
    }

    @Bean
    public MatchingRestApplication matchingRestApplication() {
        return new MatchingRestApplication();
    }

    @Bean
    public MatchingRuleRestService matchingRuleRestService() {
        return new MatchingRuleRestService();
    }

    @Bean
    public MatchingRuleSetRestService matchingRuleSetRestService() {
        return new MatchingRuleSetRestService();
    }

    @Bean
    public MatchingAlgorithmRestService matchingAlgorithmRestService() {
        return new MatchingAlgorithmRestService();
    }

    @Bean
    public MatchingTableRestService matchingTableRestService() {
        return new MatchingTableRestService();
    }

    @Bean
    public MatchingAssignmentRestService matchingAssignmentRestService() {
        return new MatchingAssignmentRestService();
    }

    @Bean
    public ClusterRestService clusterRestService() {
        return new ClusterRestService();
    }

    @Bean
    public MatchingModelRestService matchingModelRestService() {
        return new MatchingModelRestService();
    }

    @Bean
    public Server server(
            final Bus cxf,
            final JacksonJaxbJsonProvider jacksonJaxbJsonProvider,
            final DetailedRestExceptionMapper restExceptionMapper,
            final ReceiveInInterceptor receiveInInterceptor,
            final MatchingRestApplication matchingRestApplication,
            final MatchingRuleRestService matchingRuleRestService,
            final MatchingAlgorithmRestService matchingAlgorithmRestService,
            final MatchingTableRestService matchingTableRestService,
            final MatchingRuleSetRestService matchingRuleSetRestService,
            final MatchingAssignmentRestService matchingAssignmentRestService,
            final ClusterRestService clusterRestService,
            final MatchingModelRestService matchingModelRestService) {

        final JAXRSServerFactoryBean jaxrsServerFactoryBean = RuntimeDelegate.getInstance()
                .createEndpoint(matchingRestApplication, JAXRSServerFactoryBean.class);

        final OpenApiFeature matchingOpenApiFeature = OpenApiMetadataFactory.openApiFeature(
                "Unidata Matching Core API",
                "Unidata Matching Core REST API operations",
                matchingRestApplication, cxf,
                "org.unidata.mdm.rest.v1.matching.core.service", "org.unidata.mdm.rest.v1.matching.core.ro");

        jaxrsServerFactoryBean.setFeatures(Collections.singletonList(matchingOpenApiFeature));
        jaxrsServerFactoryBean.setProviders(Arrays.asList(jacksonJaxbJsonProvider, restExceptionMapper));
        jaxrsServerFactoryBean.setInInterceptors(Collections.singletonList(receiveInInterceptor));
        jaxrsServerFactoryBean.setServiceBeans(Arrays.asList(
                matchingRuleRestService,
                matchingAlgorithmRestService,
                matchingTableRestService,
                matchingRuleSetRestService,
                matchingAssignmentRestService,
                clusterRestService,
                matchingModelRestService));

        return jaxrsServerFactoryBean.create();
    }
}

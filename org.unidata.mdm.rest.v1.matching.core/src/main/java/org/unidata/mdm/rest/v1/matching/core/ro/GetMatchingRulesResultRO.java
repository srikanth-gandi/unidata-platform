/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.v1.matching.core.ro;

import org.unidata.mdm.rest.system.ro.DetailedOutputRO;
import org.unidata.mdm.rest.v1.matching.core.ro.rule.MatchingRuleRO;

import java.util.List;

/**
 * @author Sergey Murskiy on 11.06.2021
 */
public class GetMatchingRulesResultRO extends DetailedOutputRO {
    /**
     * Matching rules.
     */
    private List<MatchingRuleRO> rules;

    /**
     * Constructor.
     */
    public GetMatchingRulesResultRO() {
        super();
    }

    /**
     * Constructor.
     *
     * @param rules matching rules to set
     */
    public GetMatchingRulesResultRO(List<MatchingRuleRO> rules) {
        this.rules = rules;
    }

    /**
     * @return rules
     */
    public List<MatchingRuleRO> getRules() {
        return rules;
    }

    /**
     * @param rules rules to set
     */
    public void setRules(List<MatchingRuleRO> rules) {
        this.rules = rules;
    }
}

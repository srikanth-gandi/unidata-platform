/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.v1.matching.core.service;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.unidata.mdm.core.service.MetaModelService;
import org.unidata.mdm.matching.core.context.MatchingModelGetContext;
import org.unidata.mdm.matching.core.context.MatchingModelUpsertContext;
import org.unidata.mdm.matching.core.dto.MatchingModelGetResult;
import org.unidata.mdm.matching.core.dto.RulesModelGetResult;
import org.unidata.mdm.matching.core.type.model.source.rule.MatchingRuleSource;
import org.unidata.mdm.rest.system.ro.DetailedErrorResponseRO;
import org.unidata.mdm.rest.system.service.AbstractRestService;
import org.unidata.mdm.rest.v1.matching.core.converter.MatchingRuleConverter;
import org.unidata.mdm.rest.v1.matching.core.ro.DeleteMatchingRuleResultRO;
import org.unidata.mdm.rest.v1.matching.core.ro.GetMatchingRuleResultRO;
import org.unidata.mdm.rest.v1.matching.core.ro.GetMatchingRulesResultRO;
import org.unidata.mdm.rest.v1.matching.core.ro.UpsertMatchingRuleRequestRO;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.Collections;
import java.util.Objects;

/**
 * @author Sergey Murskiy on 08.06.2021
 */
@Path(MatchingRuleRestService.SERVICE_PATH)
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class MatchingRuleRestService extends AbstractRestService {
    /**
     * Service path.
     */
    public static final String SERVICE_PATH = "rules";
    /**
     * Tag.
     */
    public static final String SERVICE_TAG = "rules";

    /**
     * Matching rule converter.
     */
    @Autowired
    private MatchingRuleConverter matchingRuleConverter;

    /**
     * The MMS.
     */
    @Autowired
    private MetaModelService metaModelService;

    @GET
    @Operation(
            description = "Get all defined matching rules.",
            method = HttpMethod.GET,
            tags = SERVICE_TAG,
            parameters = {
                    @Parameter(description = "Draft id. Optional.", in = ParameterIn.QUERY, name = "draftId")
            },
            responses = {
                    @ApiResponse(content = @Content(schema = @Schema(implementation = GetMatchingRulesResultRO.class)), responseCode = "200"),
                    @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "500")
            }
    )
    public GetMatchingRulesResultRO getAll(@QueryParam("draftId") @DefaultValue("0") Long draftId) {
        MatchingModelGetResult getResult = metaModelService.get(MatchingModelGetContext.builder()
                .allMatchingRules(true)
                .draftId(draftId)
                .build());

        return new GetMatchingRulesResultRO(Objects.isNull(getResult.getRules())
                ? Collections.emptyList()
                : matchingRuleConverter.to(getResult.getRules().getRules()));
    }

    @GET
    @Path("{id}")
    @Operation(
            description = "Get a matching rule definition by id.",
            method = HttpMethod.GET,
            tags = SERVICE_TAG,
            parameters = {
                    @Parameter(description = "The matching rule id (name). Required.", in = ParameterIn.PATH, name = "id"),
                    @Parameter(description = "Draft id. Optional.", in = ParameterIn.QUERY, name = "draftId")
            },
            responses = {
                    @ApiResponse(content = @Content(schema = @Schema(implementation = GetMatchingRuleResultRO.class)), responseCode = "200"),
                    @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "500")
            }
    )
    public GetMatchingRuleResultRO getById(@PathParam("id") String id,
                                           @QueryParam("draftId") @DefaultValue("0") Long draftId) {

        MatchingModelGetResult modelGetResult = metaModelService.get(MatchingModelGetContext.builder()
                .matchingRuleIds(Collections.singletonList(id))
                .draftId(draftId)
                .build());

        GetMatchingRuleResultRO result = new GetMatchingRuleResultRO();

        RulesModelGetResult rmr = modelGetResult.getRules();
        if (CollectionUtils.isNotEmpty(rmr.getRules()) && rmr.getRules().size() == 1) {
            result.setRule(matchingRuleConverter.to(rmr.singleValue()));
        }

        return result;
    }

    @POST
    @Operation(
            description = "Create a matching rule definition.",
            method = HttpMethod.POST,
            tags = SERVICE_TAG,
            parameters = {
                    @Parameter(description = "Draft id. Optional.", in = ParameterIn.QUERY, name = "draftId")
            },
            requestBody = @RequestBody(
                    content = @Content(schema = @Schema(implementation = UpsertMatchingRuleRequestRO.class)),
                    description = "Matching rule definition."),
            responses = {
                    @ApiResponse(content = @Content(schema = @Schema(implementation = GetMatchingRuleResultRO.class)), responseCode = "200"),
                    @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "500")
            }
    )
    public GetMatchingRuleResultRO create(@QueryParam("draftId") @DefaultValue("0") Long draftId,
                                          UpsertMatchingRuleRequestRO request) {

        metaModelService.upsert(MatchingModelUpsertContext.builder()
                .rulesUpdate(matchingRuleConverter.from(request.getRule()))
                .draftId(draftId)
                .waitForFinish(true)
                .build());

        return getById(request.getRule().getName(), draftId);
    }

    @PUT
    @Path("{id}")
    @Operation(
            description = "Update a matching rule definition.",
            method = HttpMethod.POST,
            tags = SERVICE_TAG,
            parameters = {
                    @Parameter(description = "The matching rule id (name). Required.", in = ParameterIn.PATH, name = "id"),
                    @Parameter(description = "Draft id. Optional.", in = ParameterIn.QUERY, name = "draftId")
            },
            requestBody = @RequestBody(
                    content = @Content(schema = @Schema(implementation = UpsertMatchingRuleRequestRO.class)),
                    description = "Matching rule definition."),
            responses = {
                    @ApiResponse(content = @Content(schema = @Schema(implementation = GetMatchingRuleResultRO.class)), responseCode = "200"),
                    @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "500")
            }
    )
    public GetMatchingRuleResultRO update(@PathParam("id") String id,
                                          @QueryParam("draftId") @DefaultValue("0") Long draftId,
                                          UpsertMatchingRuleRequestRO request) {

        MatchingRuleSource source = matchingRuleConverter.from(request.getRule());

        metaModelService.upsert(MatchingModelUpsertContext.builder()
                .rulesUpdate(source)
                .rulesDelete(!StringUtils.equals(id, source.getName()) ? id : null)
                .draftId(draftId)
                .waitForFinish(true)
                .build());

        return getById(source.getName(), draftId);
    }

    @DELETE
    @Path("{id}")
    @Operation(
            description = "Delete an existing matching rule definition by id.",
            method = HttpMethod.DELETE,
            tags = SERVICE_TAG,
            parameters = {
                    @Parameter(description = "The matching rule id (name). Required.", in = ParameterIn.PATH, name = "id"),
                    @Parameter(description = "Draft id. Optional.", in = ParameterIn.QUERY, name = "draftId")
            },
            responses = {
                    @ApiResponse(content = @Content(schema = @Schema(implementation = DeleteMatchingRuleResultRO.class)), responseCode = "200"),
                    @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "500")
            }
    )
    public DeleteMatchingRuleResultRO delete(@PathParam("id") String id,
                                             @QueryParam("draftId") @DefaultValue("0") Long draftId) {

        metaModelService.upsert(MatchingModelUpsertContext.builder()
                .rulesDelete(id)
                .draftId(draftId)
                .build());

        return new DeleteMatchingRuleResultRO();
    }
}

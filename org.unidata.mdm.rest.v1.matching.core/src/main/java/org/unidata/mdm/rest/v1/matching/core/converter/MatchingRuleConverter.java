/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.v1.matching.core.converter;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.core.util.SecurityUtils;
import org.unidata.mdm.matching.core.type.model.source.algorithm.AlgorithmSettingsSource;
import org.unidata.mdm.matching.core.type.model.source.rule.MatchingRuleSource;
import org.unidata.mdm.rest.core.converter.CustomPropertiesConverter;
import org.unidata.mdm.rest.v1.matching.core.ro.algorithm.AlgorithmSettingsRO;
import org.unidata.mdm.rest.v1.matching.core.ro.rule.MatchingRuleRO;
import org.unidata.mdm.system.convert.Converter;

import java.time.OffsetDateTime;
import java.util.Objects;

/**
 * @author Sergey Murskiy on 23.06.2021
 */
@Component
public class MatchingRuleConverter extends Converter<MatchingRuleSource, MatchingRuleRO> {

    /**
     * Parameter converter.
     */
    @Autowired
    private AlgorithmParameterConverter parameterConverter;

    /**
     * Algorithm settings converter.
     */
    private final AlgorithmSettingsConverter algorithmSettingsConverter = new AlgorithmSettingsConverter();

    /**
     * Constructor.
     */
    public MatchingRuleConverter() {
        this.to = this::convert;
        this.from = this::convert;
    }

    private MatchingRuleRO convert(MatchingRuleSource source) {
        MatchingRuleRO ro = new MatchingRuleRO();

        ro.setName(source.getName());
        ro.setDisplayName(source.getDisplayName());
        ro.setDescription(source.getDescription());
        ro.setMatchingStorageId(source.getMatchingStorageId());
        ro.setAlgorithms(algorithmSettingsConverter.to(source.getAlgorithms()));
        ro.setCreateDate(source.getCreateDate());
        ro.setCreatedBy(source.getCreatedBy());
        ro.setUpdateDate(source.getUpdateDate());
        ro.setUpdatedBy(source.getUpdatedBy());
        ro.setCustomProperties(CustomPropertiesConverter.to(source.getCustomProperties()));

        return ro;
    }

    private MatchingRuleSource convert(MatchingRuleRO source) {
        String user = SecurityUtils.getCurrentUserName();
        OffsetDateTime now = OffsetDateTime.now();

        return new MatchingRuleSource()
                .withName(source.getName())
                .withDisplayName(source.getDisplayName())
                .withDescription(source.getDescription())
                .withMatchingStorageId(source.getMatchingStorageId())
                .withAlgorithms(algorithmSettingsConverter.from(source.getAlgorithms()))
                .withCreateDate(Objects.isNull(source.getCreateDate()) ? now : source.getCreateDate())
                .withCreatedBy(StringUtils.isBlank(source.getCreatedBy()) ? user : source.getCreatedBy())
                .withUpdateDate(Objects.isNull(source.getCreateDate()) ? null : now)
                .withUpdatedBy(StringUtils.isBlank(source.getCreatedBy()) ? null : user)
                .withCustomProperties(CustomPropertiesConverter.from(source.getCustomProperties()));
    }

    private class AlgorithmSettingsConverter extends Converter<AlgorithmSettingsSource, AlgorithmSettingsRO> {

        /**
         * Constructor.
         */
        public AlgorithmSettingsConverter() {
            this.to = this::convert;
            this.from = this::convert;
        }

        private AlgorithmSettingsRO convert(AlgorithmSettingsSource source) {
            AlgorithmSettingsRO ro = new AlgorithmSettingsRO();

            ro.setAlgorithmName(source.getAlgorithmName());
            ro.setId(source.getId());
            ro.setParameters(parameterConverter.to(source.getParameters()));

            return ro;
        }

        private AlgorithmSettingsSource convert(AlgorithmSettingsRO source) {
            return new AlgorithmSettingsSource()
                    .withAlgorithmName(source.getAlgorithmName())
                    .withParameters(parameterConverter.from(source.getParameters()));
        }
    }
}

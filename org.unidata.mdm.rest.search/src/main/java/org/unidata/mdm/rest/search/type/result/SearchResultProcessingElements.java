package org.unidata.mdm.rest.search.type.result;

public enum SearchResultProcessingElements {
    LOOKUP, MEASURED, ENUM, DATE
}
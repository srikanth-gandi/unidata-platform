/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.v1.matching.data.ro;

import org.unidata.mdm.rest.system.ro.DetailedOutputRO;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * @author Sergey Murskiy on 22.08.2021
 */
public class UpsertRecordsMatchingResultRO extends DetailedOutputRO {
    /**
     * Periods, holding matching data.
     */
    private List<PeriodMatchingResultRO> periods;

    /**
     * Constructor.
     */
    public UpsertRecordsMatchingResultRO() {
        super();
    }

    /**
     * @return the periods
     */
    public List<PeriodMatchingResultRO> getPeriods() {
        return Objects.isNull(periods) ? Collections.emptyList() : periods;
    }
    /**
     * @param periods the periods to set
     */
    public void setPeriods(List<PeriodMatchingResultRO> periods) {
        this.periods = periods;
    }
}

package org.unidata.mdm.rest.v1.data.ro.keys;

/**
 * Object with external id
 *
 * @author Alexandr Serov
 * @since 20.11.2020
 **/
public interface ExternalIdSupport {

    RecordExternalIdRO getExternalId();

}

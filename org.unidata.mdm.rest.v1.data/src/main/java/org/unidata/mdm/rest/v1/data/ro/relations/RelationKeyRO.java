package org.unidata.mdm.rest.v1.data.ro.relations;

import org.unidata.mdm.rest.v1.data.ro.keys.RecordOriginKeyRO;

/**
 * Relation key rest object
 *
 * @author Alexandr Serov
 * @since 20.10.2020
 **/
public class RelationKeyRO {

    private RecordOriginKeyRO from;
    private RecordOriginKeyRO to;

    public RecordOriginKeyRO getFrom() {
        return from;
    }

    public void setFrom(RecordOriginKeyRO from) {
        this.from = from;
    }

    public RecordOriginKeyRO getTo() {
        return to;
    }

    public void setTo(RecordOriginKeyRO to) {
        this.to = to;
    }
}

package org.unidata.mdm.rest.v1.data.ro.favorite;

import java.util.List;
import java.util.UUID;

import org.unidata.mdm.rest.system.ro.DetailedOutputRO;

/**
 * Get favorite etalons result
 *
 * @author Alexandr Serov
 * @since 19.10.2020
 **/
public class GetFavoriteResultRO extends DetailedOutputRO {

    private List<UUID> etalons;

    public List<UUID> getEtalons() {
        return etalons;
    }

    public void setEtalons(List<UUID> etalons) {
        this.etalons = etalons;
    }
}

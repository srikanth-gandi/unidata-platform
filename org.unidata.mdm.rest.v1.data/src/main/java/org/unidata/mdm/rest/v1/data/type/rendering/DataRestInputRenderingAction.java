/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.v1.data.type.rendering;

import org.unidata.mdm.system.type.rendering.InputRenderingAction;

/**
 * @author Mikhail Mikhailov on Jan 16, 2020
 */
public enum DataRestInputRenderingAction implements InputRenderingAction {
    /**
     * Atomic upsert action support.
     */
    ATOMIC_UPSERT_INPUT,
    /**
     * Get op.
     */
    ATOMIC_GET_INPUT,
    /**
     * Delete input op.
     */
    ATOMIC_DELETE_INPUT,
    /**
     * Render complex search input.
     */
    SEARCH_INPUT,
    /**
     * Render marge input.
     */
    MERGE_INPUT,
    /**
     * Render marge input.
     */
    PREVIEW_INPUT,
    /**
     * Record restore input.
     */
    RECORD_RESTORE_INPUT,
    /**
     * Period restore input.
     */
    PERIOD_RESTORE_INPUT;
}

/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.v1.data.converter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.tuple.Triple;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.core.type.data.Attribute;
import org.unidata.mdm.core.type.data.impl.SerializableDataRecord;
import org.unidata.mdm.data.type.data.EtalonRelation;
import org.unidata.mdm.data.type.data.impl.EtalonRelationImpl;
import org.unidata.mdm.rest.v1.data.ro.attributes.ComplexAttributeRO;
import org.unidata.mdm.rest.v1.data.ro.attributes.SimpleAttributeRO;
import org.unidata.mdm.rest.v1.data.ro.records.EtalonRelationToRO;
import org.unidata.mdm.rest.v1.data.service.search.SearchResultHitModifier;
import org.unidata.mdm.system.convert.Converter;
import org.unidata.mdm.system.util.ConvertUtils;

/**
 * @author Mikhail Mikhailov
 * Converts {@link EtalonRelationToRO} type relations.
 */
@Component
public class RelationToEtalonConverter extends Converter<EtalonRelation, EtalonRelationToRO> {

    @Autowired
    private SearchResultHitModifier searchResultHitModifier;

    /**
     * Constructor.
     */
    private RelationToEtalonConverter() {
        super();
        this.to = this::convert;
        this.from = this::convert;
    }

    /**
     * Converts to the REST type.
     * @param source the source
     * @return target
     */
    private EtalonRelationToRO convert(EtalonRelation source) {

        if (source == null) {
            return null;
        }

        EtalonRelationToRO target = new EtalonRelationToRO();

        List<ComplexAttributeRO> complexAttributes = new ArrayList<>();
        ComplexAttributeConverter.to(source.getComplexAttributes(), complexAttributes);

        List<SimpleAttributeRO> simpleAttributes = new ArrayList<>();
        SimpleAttributeConverter.to(source.getSimpleAttributes(), simpleAttributes);

        target.setComplexAttributes(complexAttributes);
        target.setSimpleAttributes(simpleAttributes);

        Map<String, List<Triple<String, Date, Date>>> displayNames =
                searchResultHitModifier.getDisplayNamesForRelationTo(
                    source.getInfoSection().getRelationName(),
                    source.getInfoSection().getFromEtalonKey().getId(),
                    null,
                    source.getInfoSection().getValidFrom(),
                    source.getInfoSection().getValidTo());

        target.setEtalonIdTo(source.getInfoSection().getToEtalonKey() != null ? source.getInfoSection().getToEtalonKey().getId() : null);
        target.setEtalonDisplayNameTo(searchResultHitModifier.extractDisplayName(displayNames,
                source.getInfoSection().getToEtalonKey().getId(),
                source.getInfoSection().getValidFrom(),
                source.getInfoSection().getValidTo(),
                new Date()));

        target.setRelName(source.getInfoSection().getRelationName());
        target.setCreateDate(source.getInfoSection() != null ? source.getInfoSection().getCreateDate() : null);
        target.setCreatedBy(source.getInfoSection() != null ? source.getInfoSection().getCreatedBy() : null);
        target.setUpdateDate(source.getInfoSection() != null ? source.getInfoSection().getUpdateDate() : null);
        target.setUpdatedBy(source.getInfoSection() != null ? source.getInfoSection().getUpdatedBy() : null);
        target.setStatus(source.getInfoSection() != null ? source.getInfoSection().getStatus().value() : null);
        target.setValidFrom(source.getInfoSection() != null ? ConvertUtils.date2LocalDateTime(source.getInfoSection().getValidFrom()) : null);
        target.setValidTo(source.getInfoSection() != null ? ConvertUtils.date2LocalDateTime(source.getInfoSection().getValidTo()) : null);

        target.setEtalonId(source.getInfoSection() != null ? source.getInfoSection().getRelationEtalonKey() : null);

        return target;
    }

    /**
     * Converts from the REST type.
     * @param source the source
     * @return target
     */
    private EtalonRelation convert(EtalonRelationToRO source) {

        SerializableDataRecord result = new SerializableDataRecord(
                source.getComplexAttributes().size() +
                source.getSimpleAttributes().size());

        List<Attribute> attributes = new ArrayList<>(
                source.getComplexAttributes().size() +
                source.getSimpleAttributes().size());

        attributes.addAll(ComplexAttributeConverter.from(source.getComplexAttributes()));
        attributes.addAll(SimpleAttributeConverter.from(source.getSimpleAttributes()));

        result.addAll(attributes);

        return new EtalonRelationImpl().withDataRecord(result);
    }
}

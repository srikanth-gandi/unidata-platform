package org.unidata.mdm.rest.v1.data.ro.relations;

import java.time.LocalDateTime;

/**
 * Get relation timeline request
 *
 * @author Alexandr Serov
 * @since 23.10.2020
 **/
public class GetRelationTimelineRequestRO {

    private Long draftId;
    private String relationEtalonKey;
    private LocalDateTime dateFor;
    private LocalDateTime validFrom;
    private LocalDateTime validTo;

    /**
     * @return the draftId
     */
    public Long getDraftId() {
        return draftId;
    }

    /**
     * @param draftId the draftId to set
     */
    public void setDraftId(Long draftId) {
        this.draftId = draftId;
    }

    public String getRelationEtalonKey() {
        return relationEtalonKey;
    }

    public void setRelationEtalonKey(String relationEtalonKey) {
        this.relationEtalonKey = relationEtalonKey;
    }

    public LocalDateTime getDateFor() {
        return dateFor;
    }

    public void setDateFor(LocalDateTime dateFor) {
        this.dateFor = dateFor;
    }

    public LocalDateTime getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(LocalDateTime validFrom) {
        this.validFrom = validFrom;
    }

    public LocalDateTime getValidTo() {
        return validTo;
    }

    public void setValidTo(LocalDateTime validTo) {
        this.validTo = validTo;
    }
}

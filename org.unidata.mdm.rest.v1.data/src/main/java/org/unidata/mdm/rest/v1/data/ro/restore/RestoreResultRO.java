package org.unidata.mdm.rest.v1.data.ro.restore;

import org.unidata.mdm.rest.system.ro.DetailedOutputRO;
import org.unidata.mdm.rest.v1.data.ro.records.EtalonRecordRO;

/**
 * Restore result
 *
 * @author Alexandr Serov
 * @since 19.10.2020
 **/
public class RestoreResultRO extends DetailedOutputRO {

    private EtalonRecordRO record;

    public EtalonRecordRO getRecord() {
        return record;
    }

    public void setRecord(EtalonRecordRO record) {
        this.record = record;
    }
}

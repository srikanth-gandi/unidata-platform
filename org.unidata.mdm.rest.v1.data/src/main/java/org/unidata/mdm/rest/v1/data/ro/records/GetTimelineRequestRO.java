package org.unidata.mdm.rest.v1.data.ro.records;

/**
 * Get timeline request
 *
 * @author Alexandr Serov
 * @since 14.10.2020
 **/
public class GetTimelineRequestRO extends AbstractRecordRequestRO {

    private Long draftId;
    private boolean includeDrafts;

    public GetTimelineRequestRO() {}

    public GetTimelineRequestRO(Long draftId, boolean includeDrafts) {
        this.draftId = draftId;
        this.includeDrafts = includeDrafts;
    }

    public boolean isIncludeDrafts() {
        return includeDrafts;
    }

    public void setIncludeDrafts(boolean includeDrafts) {
        this.includeDrafts = includeDrafts;
    }

    /**
     * @return the draftId
     */
    public Long getDraftId() {
        return draftId;
    }

    /**
     * @param draftId the draftId to set
     */
    public void setDraftId(Long draftId) {
        this.draftId = draftId;
    }
}

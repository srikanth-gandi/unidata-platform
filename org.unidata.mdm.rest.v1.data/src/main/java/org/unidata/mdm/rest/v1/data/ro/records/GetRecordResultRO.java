package org.unidata.mdm.rest.v1.data.ro.records;

/**
 * Get entity result
 *
 * @author Alexandr Serov
 * @since 07.10.2020
 **/
public class GetRecordResultRO extends RecordKeysSupportResultRO {

    private EtalonRecordRO record;

    public EtalonRecordRO getRecord() {
        return record;
    }

    public void setRecord(EtalonRecordRO record) {
        this.record = record;
    }
}

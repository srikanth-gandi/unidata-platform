/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.v1.data.ro.records;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import org.unidata.mdm.rest.v1.data.ro.attributes.ArrayAttributeRO;
import org.unidata.mdm.rest.v1.data.ro.attributes.ComplexAttributeRO;
import org.unidata.mdm.rest.v1.data.ro.attributes.SimpleAttributeRO;

/**
 * @author Michael Yashin. Created on 02.06.2015.
 */
public class NestedRecordRO {

    /**
     * Simple attributes.
     */
    protected List<SimpleAttributeRO> simpleAttributes = new ArrayList<>();
    /**
     * Simple attributes.
     */
    protected List<ArrayAttributeRO> arrayAttributes = new ArrayList<>();
    /**
     * Complex attributes.
     */
    protected List<ComplexAttributeRO> complexAttributes = new ArrayList<>();
    /**
     * Gets simple attributes.
     * @return list
     */
    public List<SimpleAttributeRO> getSimpleAttributes() {
        return Objects.isNull(simpleAttributes) ? Collections.emptyList() : simpleAttributes;
    }
    /**
     * Sets simple attributes.
     * @param simpleAttributes attributes list
     */
    public void setSimpleAttributes(List<SimpleAttributeRO> simpleAttributes) {
        this.simpleAttributes = simpleAttributes;
    }
    /**
     * @return the arrayAttributes
     */
    public List<ArrayAttributeRO> getArrayAttributes() {
        return Objects.isNull(arrayAttributes) ? Collections.emptyList() : arrayAttributes;
    }
    /**
     * @param arrayAttributes the arrayAttributes to set
     */
    public void setArrayAttributes(List<ArrayAttributeRO> arrayAttributes) {
        this.arrayAttributes = arrayAttributes;
    }
    /**
     * Gets simple attributes.
     * @return complex attributes list
     */
    public List<ComplexAttributeRO> getComplexAttributes() {
        return Objects.isNull(complexAttributes) ? Collections.emptyList() : complexAttributes;
    }
    /**
     * Sets simple attributes.
     * @param complexAttributes complex attributes list
     */
    public void setComplexAttributes(List<ComplexAttributeRO> complexAttributes) {
        this.complexAttributes = complexAttributes;
    }

}

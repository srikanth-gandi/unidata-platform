package org.unidata.mdm.rest.v1.data.ro.records;

import java.util.List;

/**
 * Filtered query result
 *
 * @author Alexandr Serov
 * @since 14.10.2020
 **/
public class FilterByCriteriaResultRO {

    private List<String> etalonsWithoutHoles;

    private FilterByCriteriaResultRO() {}

    public FilterByCriteriaResultRO(List<String> etalonsWithoutHoles) {
        this.etalonsWithoutHoles = etalonsWithoutHoles;
    }

    public List<String> getEtalonsWithoutHoles() {
        return etalonsWithoutHoles;
    }

    public void setEtalonsWithoutHoles(List<String> etalonsWithoutHoles) {
        this.etalonsWithoutHoles = etalonsWithoutHoles;
    }
}

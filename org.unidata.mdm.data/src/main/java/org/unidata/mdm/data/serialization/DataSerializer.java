/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.data.serialization;

import org.unidata.mdm.core.serialization.CoreSerializer;
import org.unidata.mdm.core.type.data.DataRecord;

/**
 * @author Mikhail Mikhailov on Oct 21, 2019
 */
public final class DataSerializer {
    /**
     * Constructor.
     */
    private DataSerializer() {
        super();
    }
    /**
     * Dumps to Protostuff.
     * @param record the record
     * @return bytes
     */
    public static byte[] toProtostuff(DataRecord record) {
        return CoreSerializer.recordToProtostuff(record);
    }
    /**
     * Restores from Protostuff.
     * @param buf the buffer to restore.
     * @return record
     */
    public static DataRecord fromProtostuff(byte[] buf) {
        return CoreSerializer.recordFromProtostuff(buf);
    }

}

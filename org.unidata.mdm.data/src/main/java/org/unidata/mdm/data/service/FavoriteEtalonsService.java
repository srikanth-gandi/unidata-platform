package org.unidata.mdm.data.service;

import java.util.List;
import java.util.Map;
import java.util.UUID;

public interface FavoriteEtalonsService {

    void add(Map<String, List<UUID>> favorites);

    void remove(UUID favorite);

    void exclude(UUID favorite);

    void exclude(List<UUID> favorites);

    void excludeByEntity(String entityName, String userName);

    void removeAllByEntity(String entityName);

    boolean isFavorite(UUID uuid);

    List<UUID> getEtalons();

    List<UUID> getEtalons(String entityName);

    Map<String, List<UUID>> getEtalonsMap();

    Map<String, List<UUID>> getEtalonsMap(String entityName);

    void removeAbsentEntityFavorites();

}
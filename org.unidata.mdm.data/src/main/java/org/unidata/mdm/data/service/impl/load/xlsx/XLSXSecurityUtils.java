/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.data.service.impl.load.xlsx;

import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;

import org.unidata.mdm.core.type.security.Right;
import org.unidata.mdm.core.type.security.Role;
import org.unidata.mdm.core.type.security.SecurityToken;
import org.unidata.mdm.core.type.security.User;
import org.unidata.mdm.core.util.SecurityUtils;

/**
 * @author Aleksandr Magdenko
 */
public class XLSXSecurityUtils {
    /**
     * Disabling constructor.
     */
    private XLSXSecurityUtils() {
        super();
    }
    /**
     * Check is User allowed for operation "checkRightFunction".
     *
     * @param header XLSXHeader object.
     * @param user User with roles and rights.
     * @param parentResourceName Parent resource name to check if no any custom rights found for attribute.
     * @param checkRightFunction Right predicate to check.
     * @return True if user allowed for operation.
     */
    public static boolean isUserAllowed(XLSXHeader header, String parentResourceName,
                                        Predicate<Right> checkRightFunction) {

        SecurityToken token = SecurityUtils.getSecurityTokenForCurrentUser();
        User user = Objects.isNull(token) ? null : token.getUser();
        if (user != null && !SecurityUtils.isAdminUser()) {

            // Loop over all roles and check if any role allow to resource to read/update (depends on checkRightFunction).
            for (Role role : user.getRoles()) {

                Map<String, Right> roleRights = SecurityUtils.extractRightsMap(role.getRights());

                Optional<Map.Entry<String, Right>> optionalEntry = roleRights.entrySet()
                        .stream()
                        .filter(entry -> header.getSystemHeader().endsWith(entry.getKey()))
                        .findAny();

                boolean usedCustomResourceRight = false;

                if (optionalEntry.isPresent()) {
                    Right attrRight = optionalEntry.get().getValue();

                    // Use current attribute right only if at least ony right equals TRUE.
                    // That means that attribute resource right overrides parent resource right.
                    if (attrRight.isCreate() || attrRight.isRead() || attrRight.isUpdate() || attrRight.isDelete()) {
                        if (checkRightFunction.test(attrRight)) {
                            return true;
                        }

                        usedCustomResourceRight = true;
                    }
                }

                if (!usedCustomResourceRight) {
                    // Check right for parent resource where entityName equals parent resource name.
                    boolean parentRightAllowed = roleRights.entrySet()
                            .stream()
                            .filter(entry -> parentResourceName.equals(entry.getKey()))
                            .findAny()
                            .map(entry -> checkRightFunction.test(entry.getValue()))
                            .orElse(Boolean.FALSE);

                    if (parentRightAllowed) {
                        return true;
                    }
                }
            }

            // No any role found where resource allow to read/update resource.
            return false;
        }

        // Always true for SYSTEM user and admin.
        return true;
    }
}

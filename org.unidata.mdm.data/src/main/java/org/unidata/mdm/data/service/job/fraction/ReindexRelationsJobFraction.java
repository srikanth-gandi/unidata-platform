/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.data.service.job.fraction;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ItemWriteListener;
import org.springframework.batch.core.StepExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.core.service.MetaModelService;
import org.unidata.mdm.core.service.job.JobCommonParameters;
import org.unidata.mdm.core.service.job.step.ItemWriteListenerAdapter;
import org.unidata.mdm.core.type.annotation.JobRef;
import org.unidata.mdm.core.type.job.JobFraction;
import org.unidata.mdm.core.type.job.JobParameterDescriptor;
import org.unidata.mdm.core.type.model.RegisterElement;
import org.unidata.mdm.core.type.model.RelationElement;
import org.unidata.mdm.data.context.UpsertRelationRequestContext;
import org.unidata.mdm.data.context.UpsertRelationRequestContext.UpsertRelationHint;
import org.unidata.mdm.data.context.UpsertRelationsRequestContext;
import org.unidata.mdm.data.context.UpsertRequestContext;
import org.unidata.mdm.data.dao.RelationsDAO;
import org.unidata.mdm.data.module.DataModule;
import org.unidata.mdm.data.service.segments.relations.batch.RelationsUpsertConnectorExecutor;
import org.unidata.mdm.data.service.segments.relations.batch.RelationsUpsertFinishExecutor;
import org.unidata.mdm.data.service.segments.relations.batch.RelationsUpsertPersistenceExecutor;
import org.unidata.mdm.data.service.segments.relations.batch.RelationsUpsertProcessExecutor;
import org.unidata.mdm.data.service.segments.relations.batch.RelationsUpsertStartExecutor;
import org.unidata.mdm.data.service.segments.relations.upsert.RelationUpsertFinishExecutor;
import org.unidata.mdm.data.service.segments.relations.upsert.RelationUpsertIndexingExecutor;
import org.unidata.mdm.data.service.segments.relations.upsert.RelationUpsertStartExecutor;
import org.unidata.mdm.data.service.segments.relations.upsert.RelationUpsertTimelineExecutor;
import org.unidata.mdm.data.type.apply.batch.impl.RecordUpsertBatchSetAccumulator;
import org.unidata.mdm.data.type.apply.batch.impl.RelationUpsertBatchSetAccumulator;
import org.unidata.mdm.meta.configuration.Descriptors;
import org.unidata.mdm.meta.service.impl.data.DataModelMappingComponent;
import org.unidata.mdm.meta.type.RelativeDirection;
import org.unidata.mdm.meta.type.instance.DataModelInstance;
import org.unidata.mdm.system.service.PipelineService;
import org.unidata.mdm.system.service.TextService;
import org.unidata.mdm.system.service.TouchService;
import org.unidata.mdm.system.type.batch.BatchSetAccumulator;
import org.unidata.mdm.system.type.pipeline.Pipeline;
import org.unidata.mdm.system.type.pipeline.connection.PipelineConnection;
import org.unidata.mdm.system.type.touch.Touch;
import org.unidata.mdm.system.type.touch.TouchParams;
import org.unidata.mdm.system.type.touch.Touchable;
import org.unidata.mdm.system.util.TextUtils;

@JobRef("reindexDataJob")
@Component("reindexRelationsFraction")
public class ReindexRelationsJobFraction extends JobFraction implements Touchable {
    /**
     * The logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(ReindexRelationsJobFraction.class);
    /**
     * Relations mapping listener.
     */
    private final ItemWriteListener<String> relationsMappingListener = new ItemWriteListenerAdapter<String>() {
        /**
         * {@inheritDoc}
         */
        @Override
        public void afterWrite(List<? extends String> items) {

            // Update mappings
            DataModelInstance i = metaModelService.instance(Descriptors.DATA);
            for (String entityName : items) {

                LOGGER.info("Processing relations mapping for {}.", entityName);

                // Default storage id will be taken
                if (i.isRegister(entityName)) {

                    RegisterElement re = i.getRegister(entityName).getRegister();
                    Stream<RelationElement> rels = Stream.concat(
                            re.getIncomingRelations().keySet().stream(),
                            re.getOutgoingRelations().keySet().stream());

                    rels.forEach(r -> dataModelMappingComponent.updateRelationMappings(null, entityName, r.getName()));
                }
            }
        }
    };
    /**
     * IWL.
     */
    private Map<String, ItemWriteListener<?>> itemWriteListeners = Map.of("reindexDataJobMappingStep", relationsMappingListener);
    /**
     * Connect reindex data pipeline.
     */
    public static final Touch<PipelineConnection> TOUCH_REINDEX_PIPELINE_CONNECT
        = Touch.builder(PipelineConnection.class)
            .touchName("[touch-reindex-pipeline-connect]")
            .paramType("step-name", String.class)
            .paramType("step-execution", StepExecution.class)
            .build();
    /**
     * Has register data.
     */
    public static final Touch<Void> TOUCH_REINDEX_DATA_ACCUMULATE
        = Touch.builder(Void.class)
            .touchName("[touch-reindex-data-accumulate]")
            .paramType("step-name", String.class)
            .paramType("step-execution", StepExecution.class)
            .paramType("batch-set-accumulator", BatchSetAccumulator.class)
            .build();
    /**
     * This fraction ID.
     */
    private static final String ID = "reindex-relations-fraction";
    /**
     * This fraction display name.
     */
    private static final String DISPLAY_NAME = DataModule.MODULE_ID + ".reindex.relations.name";
    /**
     * This fraction description.
     */
    private static final String DESCRIPTION = DataModule.MODULE_ID + ".reindex.relations.description";
    /**
     * Reindex relations param.
     */
    private static final String REINDEX_RELATIONS_PARAM = "reindexRelations";
    /**
     * Reindex relations param display name.
     */
    private static final String REINDEX_RELATIONS_PARAM_DISPLAY_NAME = DataModule.MODULE_ID + "reindex.relations.param.display.name";
    /**
     * Reindex relations param description.
     */
    private static final String REINDEX_RELATIONS_PARAM_DESCRIPTION = DataModule.MODULE_ID + "reindex.relations.param.description";
    /**
     * This fraction parameters.
     */
    private static final List<JobParameterDescriptor<?>> PARAMETERS = List.of(
            JobParameterDescriptor.bool()
                .defaultSingle(Boolean.TRUE)
                .name(REINDEX_RELATIONS_PARAM)
                .displayName(() -> TextUtils.getText(REINDEX_RELATIONS_PARAM_DISPLAY_NAME))
                .description(() -> TextUtils.getText(REINDEX_RELATIONS_PARAM_DESCRIPTION))
                .build());
    /**
     * Relations DAO.
     */
    @Autowired
    private RelationsDAO relationsDao;
    /**
     * The MMS.
     */
    @Autowired
    private MetaModelService metaModelService;
    /**
     * Model mapping component.
     */
    @Autowired
    private DataModelMappingComponent dataModelMappingComponent;
    /**
     * PLS.
     */
    @Autowired
    private PipelineService pipelineService;
    /**
     * The TS.
     */
    @Autowired
    private TextService textService;
    /**
     * Constructor.
     */
    @Autowired
    public ReindexRelationsJobFraction(final TouchService touchService) {
        super(ID, PARAMETERS);
        touchService.register(this, TOUCH_REINDEX_PIPELINE_CONNECT, TOUCH_REINDEX_DATA_ACCUMULATE);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getDisplayName() {
        return textService.getText(DISPLAY_NAME);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getDescription() {
        return textService.getText(DESCRIPTION);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public int getOrder() {
        return 10;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public Object touch(TouchParams<?> t) {

        if (StringUtils.equals(t.getTouch().getTouchName(), TOUCH_REINDEX_PIPELINE_CONNECT.getTouchName())) {
            return connect(t.getParam("step-execution"));
        } else if (StringUtils.equals(t.getTouch().getTouchName(), TOUCH_REINDEX_DATA_ACCUMULATE.getTouchName())) {
            accumulate(t.getParam("step-execution"), t.getParam("batch-set-accumulator"));
        }

        return null;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public ItemWriteListener<?> itemWriteListener(String stepName) {
        return itemWriteListeners.get(stepName);
    }

    private PipelineConnection connect(StepExecution step) {

        if (unused(step)) {
            return null;
        }

        return PipelineConnection.single()
                .pipeline(Pipeline.start(pipelineService.start(RelationsUpsertStartExecutor.SEGMENT_ID))
                    .with(pipelineService.point(RelationsUpsertProcessExecutor.SEGMENT_ID))
                    .with(pipelineService.point(RelationsUpsertPersistenceExecutor.SEGMENT_ID))
                    .end(pipelineService.finish(RelationsUpsertFinishExecutor.SEGMENT_ID)))
                .segment(pipelineService.connector(RelationsUpsertConnectorExecutor.SEGMENT_ID))
                .build();
    }

    private void accumulate(StepExecution step, BatchSetAccumulator<?, ?> bsa) {

        if (unused(step)) {
            return;
        }

        RecordUpsertBatchSetAccumulator rubsa = (RecordUpsertBatchSetAccumulator) bsa;
        if (CollectionUtils.isEmpty(rubsa.workingCopy())) {
            return;
        }

        List<UpsertRelationsRequestContext> payload = new ArrayList<>(rubsa.workingCopy().size());
        boolean noDrop = BooleanUtils.toBoolean(step.getJobParameters().getString("cleanIndexes", "false"));
        String operationId = step.getJobParameters().getString(JobCommonParameters.PARAM_OPERATION_ID);
        DataModelInstance instance = metaModelService.instance(Descriptors.DATA);

        // We just know, that entity name is set
        // For ALL entity names can be different in the same block/partition
        for (UpsertRequestContext ctx : rubsa.workingCopy()) {

            UpsertRelationsRequestContext result = append(ctx, instance, noDrop, operationId);
            if (Objects.nonNull(result)) {
                payload.add(result);
            }
        }

        charge(rubsa, payload);
    }

    private void charge(RecordUpsertBatchSetAccumulator bsa, List<UpsertRelationsRequestContext> payload) {

        if (CollectionUtils.isEmpty(payload)) {
            return;
        }

        RelationUpsertBatchSetAccumulator relbsa = bsa.fragment(RelationUpsertBatchSetAccumulator.ID);
        if (Objects.isNull(relbsa)) {

            // Our fragment pipeline
            Pipeline p = Pipeline.start(pipelineService.start(RelationUpsertStartExecutor.SEGMENT_ID))
                    .with(pipelineService.point(RelationUpsertTimelineExecutor.SEGMENT_ID))
                    .with(pipelineService.point(RelationUpsertIndexingExecutor.SEGMENT_ID))
                    .end(pipelineService.finish(RelationUpsertFinishExecutor.SEGMENT_ID));

            relbsa = new RelationUpsertBatchSetAccumulator(bsa.workingCopy().size(), false);
            relbsa.setPipeline(p);

            // Add to carrier
            bsa.fragment(relbsa);
        }

        relbsa.charge(payload);
    }

    private UpsertRelationsRequestContext append(UpsertRequestContext ctx, DataModelInstance instance, boolean noDrop, String operationId) {

        RegisterElement el = instance.getRegister(ctx.getEntityName());
        if (Objects.nonNull(el)) {

            Map<String, List<UpsertRelationRequestContext>> result = new HashMap<>();
            if (MapUtils.isNotEmpty(el.getOutgoingRelations())) {

                Map<String, List<UUID>> fromIds
                    = relationsDao.loadMappedRelationEtalonIds(
                            UUID.fromString(ctx.getEtalonKey()), Collections.emptyList(), RelativeDirection.FROM);

                fromIds.forEach((k, v) ->
                    result.put(k, v.stream()
                            .map(id ->
                                UpsertRelationRequestContext.builder()
                                    .relationEtalonKey(id.toString())
                                    .relationName(k)
                                    .recalculateWholeTimeline(true)
                                    .skipIndexDrop(noDrop)
                                    .operationId(operationId)
                                    .hint(UpsertRelationHint.HINT_PROCESSING_SIDE, RelativeDirection.FROM)
                                    .build()
                            )
                            .collect(Collectors.toList())));
            }

            if (MapUtils.isNotEmpty(el.getIncomingRelations())) {

                Map<String, List<UUID>> toIds
                    = relationsDao.loadMappedRelationEtalonIds(
                            UUID.fromString(ctx.getEtalonKey()), null, RelativeDirection.TO);

                toIds.forEach((k, v) ->
                    result.put(k, v.stream()
                            .map(id ->
                                UpsertRelationRequestContext.builder()
                                    .relationEtalonKey(id.toString())
                                    .relationName(k)
                                    .recalculateWholeTimeline(true)
                                    .skipIndexDrop(noDrop)
                                    .operationId(operationId)
                                    .hint(UpsertRelationHint.HINT_PROCESSING_SIDE, RelativeDirection.TO)
                                    .build()
                            )
                            .collect(Collectors.toList())));
            }

            if (MapUtils.isNotEmpty(result)) {
                return UpsertRelationsRequestContext.builder()
                        .relationsFrom(result)
                        .build();
            }
        }

        return null;
    }

    private boolean unused(StepExecution step) {

        // If either one is true, rels will be reindexed
        boolean jobReindexRelations = BooleanUtils.toBoolean(step.getJobParameters().getString(REINDEX_RELATIONS_PARAM, Boolean.FALSE.toString()));
        boolean stepReindexRelations = BooleanUtils.toBoolean(step.getExecutionContext().getString(REINDEX_RELATIONS_PARAM, Boolean.FALSE.toString()));

        return !jobReindexRelations && !stepReindexRelations;
    }
}

/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.data.service.impl.load.xlsx;

/**
 * The Class DataExportService.
 *
 * @author ilya.bykov
 */
//@Qualifier(value = "xlsxExportService")
//@Component(value = "xlsxExportService")
public class XLSXDataExportService extends XLSXProcessor /* implements DataExportService */ {

//    /**
//     * CLSF MM service. Needed to narrow node names.
//     */
//    @Autowired
//    private ClsfService classifierMetadataService;
//
//    /**
//     * The data records service.
//     */
//    @Autowired
//    private DataRecordsService dataRecordsService;
//
//    /**
//     * The cr component.
//     */
//    @Autowired
//    private CommonRecordsComponent crComponent;
//
//    @Autowired
//    private SearchResultHitModifier searchResultHitModifier;
//
//    /**
//     *
//     */
//    @Autowired
//    private UserService userService;
//    /**
//     * Logger.
//     */
//    private static final Logger LOGGER = LoggerFactory.getLogger(XLSXDataExportService.class);
//    /**
//     * The display names.
//     */
//    private LoadingCache<DNKey, String> displayNames = CacheBuilder.newBuilder().expireAfterWrite(100, TimeUnit.SECONDS)
//            .build(new DisplayNames());
//
//
//    /*
//     * (non-Javadoc)
//     *
//     * @see
//     * com.unidata.mdm.backend.service.data.DataExportService#exportDataAsXSLX(
//     * com.unidata.mdm.backend.common.context.GetMultipleRequestContext)
//     */
//    @Override
//    public ByteArrayOutputStream exportData(GetMultipleRequestContext ctx) {
//
//        try (ByteArrayOutputStream output = new ByteArrayOutputStream();
//             Workbook wb = createTemplateWorkbook(ctx.getEntityName())) {
//
//            GetRecordsDTO records = dataRecordsService.getRecords(ctx);
//            fillTemplateWbWithData(wb, records, ctx.getEntityName(), ctx.getUserName());
//            wb.write(output);
//            return output;
//
//        } catch (IOException e) {
//            throw new DataProcessingException("Unable to export data for {} to XLS.",
//                    ExceptionId.EX_DATA_EXPORT_UNABLE_TO_EXPORT_XLS, ctx.getEntityName());
//        }
//    }
//
//    /**
//     * Fill template wb with data. Structure must be like: </b>
//     * <ul>
//     * <li>ID</li>
//     * <li>Simple attributes</li>
//     * <li>Classifiers(if any)</li>
//     * <li>Complex attributes</li>
//     * <li>Relations</li>
//     * <p>
//     * </ul>
//     *
//     * @param wb         the wb
//     * @param records    the records
//     * @param entityName entity name
//     */
//    private void fillTemplateWbWithData(Workbook wb, GetRecordsDTO records, String entityName, String userName) {
//        MeasurementPoint.start();
//        try {
//
//            EntityElement el = metaModelService.instance(Descriptors.DATA)
//                    .getElement(entityName);
//
//            UserWithPasswordDTO user = userName == null || SecurityConstants.SYSTEM_USER_NAME.equals(userName) ?
//                    null : userService.getUserByName(userName);
//
//            Map<String, List<LinkedHashMap<String, Attribute>>> list = denormalizeRecords(records);
//            CellStyle cellStyleData = wb.createCellStyle();
//            cellStyleData.setWrapText(true);
//
//            // 1. Fill data for main record
//            Sheet sheet = identifySheet(wb, entityName, OBJECT_TYPE.ENTITY);
//            List<XLSXHeader> recordHeaders = createHeaders(el);
//            List<LinkedHashMap<String, Attribute>> recordRows = list.get(OBJECT_TYPE.ENTITY.value() + entityName);
//            fillSheetWithData(cellStyleData, recordHeaders, sheet, recordRows, user, entityName);
//
//            // 2. Fill data for complex attributes
//            EntityDef entityDef = metaModelService.getEntityByIdNoDeps(entityName);
//            if (entityDef != null && CollectionUtils.isNotEmpty(entityDef.getComplexAttribute())) {
//                entityDef.getComplexAttribute().forEach(comlexAttribute -> {
//                    final NestedEntityDef nestedEntityDef = metaModelService.getNestedEntityByNoDeps(comlexAttribute.getNestedEntityName());
//                    if (nestedEntityDef != null) {
//                        List<XLSXHeader> nestedHeaders = createNestedEntityHeaders(nestedEntityDef);
//                        Sheet nestedSheet = identifySheet(wb, nestedEntityDef.getTouchName(), OBJECT_TYPE.NESTED);
//                        List<LinkedHashMap<String, Attribute>> nestedRows = list.get(OBJECT_TYPE.NESTED.value() + nestedEntityDef.getTouchName());
//                        fillSheetWithData(cellStyleData, nestedHeaders, nestedSheet, nestedRows, user, entityName);
//                    }
//                });
//            }
//
//            // 3. Fill data for relations.
//            Map<RelationDef, EntityDef> relations = metaModelService.getEntityRelations(entityName, false, true);
//            for (Map.Entry<RelationDef, EntityDef> entryRel : relations.entrySet()) {
//                RelationDef relationDef = entryRel.getKey();
//                List<XLSXHeader> relationHeaders = createRelationHeaders(relationDef);
//                Sheet relationSheet = identifySheet(wb, relationDef.getTouchName(), OBJECT_TYPE.RELATION);
//                List<LinkedHashMap<String, Attribute>> relationRows = list.get(OBJECT_TYPE.RELATION.value() + relationDef.getTouchName());
//                fillSheetWithData(cellStyleData, relationHeaders, relationSheet, relationRows, user, entityName);
//            }
//
//            // 4. Fill data for classifiers.
//            List<String> clsfNames = metaModelService.getClassifiersForEntity(entityName);
//            if (CollectionUtils.isNotEmpty(clsfNames)) {
//                clsfNames.forEach(clsfName -> {
//                    final ClsfDTO clsfDTO = classifierService.getClassifierByName(clsfName);
//                    List<XLSXHeader> classifierHeaders = createClassifierHeaders(clsfDTO);
//                    Sheet classifierSheet =  identifySheet(wb, clsfName, OBJECT_TYPE.CLASSIFIER);
//                    List<LinkedHashMap<String, Attribute>> classifierRows = list.get(OBJECT_TYPE.CLASSIFIER.value() + clsfName);
//                    fillSheetWithData(cellStyleData, classifierHeaders, classifierSheet, classifierRows, user, entityName);
//                });
//            }
//        } finally {
//            MeasurementPoint.stop();
//        }
//    }
//
//    private void fillSheetWithData(CellStyle cellStyleData, List<XLSXHeader> headers, Sheet sheet,
//                                   List<LinkedHashMap<String, Attribute>> rows,  UserWithPasswordDTO user,
//                                   String entityName) {
//        if (CollectionUtils.isNotEmpty(rows)) {
//            for (int i = SYSTEM_ROWS_QTY; i < rows.size() + SYSTEM_ROWS_QTY; i++) {
//                Row row = sheet.createRow(i);
//                int columnIdx = 0;
//                LinkedHashMap<String, Attribute> denormalizedRecord = rows.get(i - SYSTEM_ROWS_QTY);
//                for (XLSXHeader header : headers) {
//                    Attribute data = denormalizedRecord.get(header.getSystemHeader());
//                    Cell cell = row.createCell(columnIdx);
//                    cell.setCellStyle(cellStyleData);
//                    fillCellWithData(data, cell, header, user, entityName);
//                    columnIdx++;
//                }
//            }
//
//            headers.stream()
//                    .filter(header -> header.getType() == XLSXHeader.TYPE.SYSTEM)
//                    .forEach(header -> sheet.autoSizeColumn(header.getOrder()));
//        }
//    }
//
//    /**
//     * Fill cell with data.
//     *
//     * @param data   the data
//     * @param cell   the cell
//     * @param header the header
//     */
//    private void fillCellWithData(Attribute data, Cell cell, XLSXHeader header, UserWithPasswordDTO user,
//                                  String entityName) {
//        if (Objects.isNull(data)) {
//            cell.setCellType(Cell.CELL_TYPE_STRING);
//            cell.setCellValue(StringUtils.EMPTY);
//            return;
//        }
//
//        // Make security check only for non-system fields.
//        if (header.getType() != XLSXHeader.TYPE.SYSTEM &&
//                // Parent resourceName equals entityName. Check for 'READ' right.
//                !XLSXSecurityUtils.isUserAllowed(header, user, entityName, Right::isRead)) {
//            cell.setCellValue(StringUtils.EMPTY);
//            return;
//        }
//
//        switch (data.getAttributeType()) {
//            case SIMPLE:
//                fillCellWithSimpleAttributeData(data.narrow(), cell, header);
//                break;
//            case CODE:
//                fillCellWithCodeAttributeData(data.narrow(), cell, header);
//                break;
//            case ARRAY:
//                fillCellWithArrayAttributeData(data.narrow(), cell, header);
//                break;
//            default:
//                // TODO throw for complex attribute. Programming error.
//                break;
//        }
//    }
//
//    /**
//     * Fills cell with simple attribute data.
//     *
//     * @param attr   the attribute
//     * @param cell   the cell
//     * @param header the header
//     */
//    private void fillCellWithSimpleAttributeData(SimpleAttribute<?> attr, Cell cell, XLSXHeader header) {
//
//        if (Objects.isNull(attr.getValue())) {
//            cell.setCellType(CellType.STRING);
//            cell.setCellValue(StringUtils.EMPTY);
//            return;
//        }
//
//        switch (attr.getDataType()) {
//            case STRING:
//            case ENUM:
//            case LINK:
//                cell.setCellType(CellType.STRING);
//                cell.setCellValue((String) attr.castValue());
//                break;
//            case BLOB:
//                cell.setCellType(CellType.STRING);
//                cell.setCellValue(((BinaryLargeValue) attr.getValue()).getFileName());
//                break;
//            case BOOLEAN:
//                cell.setCellType(CellType.BOOLEAN);
//                cell.setCellValue((Boolean) attr.getValue());
//                break;
//            case CLOB:
//                cell.setCellType(CellType.STRING);
//                cell.setCellValue(((CharacterLargeValue) attr.getValue()).getFileName());
//                break;
//            case DATE:
//                cell.setCellType(CellType.STRING);
//                cell.setCellValue(EXCEL_DATE_FORMAT.format(attr.castValue()));
//                break;
//            case INTEGER:
//                cell.setCellType(CellType.NUMERIC);
//                cell.setCellValue((Long) attr.getValue());
//                break;
//            case NUMBER:
//            case MEASURED:
//                cell.setCellType(CellType.NUMERIC);
//                cell.setCellValue((Double) attr.getValue());
//                break;
//            case TIME:
//                cell.setCellType(CellType.STRING);
//                cell.setCellValue(EXCEL_TIME_FORMAT.format(attr.castValue()));
//                break;
//            case TIMESTAMP:
//                cell.setCellType(CellType.STRING);
//                cell.setCellValue(EXCEL_DATE_TIME_FORMAT.format(attr.castValue()));
//                break;
//            default:
//                break;
//        }
//    }
//
//    /**
//     * Fills cell with code attribute data.
//     *
//     * @param attr   the attribute
//     * @param cell   the cell
//     * @param header the header
//     */
//    private void fillCellWithCodeAttributeData(CodeAttribute<?> attr, Cell cell, XLSXHeader header) {
//
//        if (Objects.isNull(attr.getValue())) {
//            cell.setCellType(CellType.STRING);
//            cell.setCellValue(StringUtils.EMPTY);
//            return;
//        }
//
//        switch (attr.getDataType()) {
//            case STRING:
//                cell.setCellType(CellType.STRING);
//                cell.setCellValue((String) attr.castValue());
//                break;
//            case INTEGER:
//                cell.setCellType(CellType.NUMERIC);
//                cell.setCellValue((Long) attr.getValue());
//                break;
//            default:
//                break;
//        }
//    }
//
//    /**
//     * Fills cell with simple attribute data.
//     *
//     * @param attr   the attribute
//     * @param cell   the cell
//     * @param header the header
//     */
//    private void fillCellWithArrayAttributeData(ArrayAttribute<?> attr, Cell cell, XLSXHeader header) {
//
//        if (attr.isEmpty()) {
//            cell.setCellType(CellType.STRING);
//            cell.setCellValue(StringUtils.EMPTY);
//            return;
//        }
//
//        switch (header.getAttributeHolder().getValueType()) {
//            case STRING:
//            case INTEGER:
//            case NUMBER:
//                cell.setCellType(CellType.STRING);
//                cell.setCellValue(AttributeUtils.joinArrayValues(attr.toArray(), header.getAttributeHolder().getExchangeSeparator()));
//                break;
//            case DATE:
//                cell.setCellType(CellType.STRING);
//                cell.setCellValue(AttributeUtils.joinArrayValues(
//                        Arrays.stream(attr.toArray()).map(v -> EXCEL_DATE_FORMAT.format((LocalDate) v)).toArray(),
//                        header.getAttributeHolder().getExchangeSeparator()));
//                break;
//            case TIME:
//                cell.setCellType(CellType.STRING);
//                cell.setCellValue(AttributeUtils.joinArrayValues(
//                        Arrays.stream(attr.toArray()).map(v -> EXCEL_TIME_FORMAT.format((LocalTime) v)).toArray(),
//                        header.getAttributeHolder().getExchangeSeparator()));
//                break;
//            case TIMESTAMP:
//                cell.setCellType(CellType.STRING);
//                cell.setCellValue(AttributeUtils.joinArrayValues(
//                        Arrays.stream(attr.toArray()).map(v -> EXCEL_DATE_TIME_FORMAT.format((LocalDateTime) v)).toArray(),
//                        header.getAttributeHolder().getExchangeSeparator()));
//                break;
//            default:
//                break;
//        }
//    }
//
//    /**
//     * Denormalize goldens.
//     *
//     * @param records the records
//     * @return the list
//     */
//    private Map<String, List<LinkedHashMap<String, Attribute>>> denormalizeRecords(GetRecordsDTO records) {
//        Date now = new Date();
//        Map<String, List<LinkedHashMap<String, Attribute>>> result = new HashMap<>();
//        Set<String> processedRelationPeriods = new HashSet<>();
//        Set<String> processedClassifierPeriods = new HashSet<>();
//
//        if (CollectionUtils.isEmpty(records.getEtalons())) {
//            return result;
//        }
//
//        for (EtalonRecord record : records.getEtalons()) {
//            denormalizeRecord(now, record,
//                    MapUtils.isEmpty(records.getRelations()) ? null : records.getRelations().get(record),
//                    MapUtils.isEmpty(records.getClassifiers()) ? null : records.getClassifiers().get(record),
//                    result,
//                    processedRelationPeriods,
//                    processedClassifierPeriods);
//        }
//
//        return result;
//    }
//
//    /**
//     * Denormalize golden.
//     *
//     * @param record      the record
//     * @param relations   relations map
//     * @param classifiers classifiers map
//     * @param result      the result
//     * @return the list
//     */
//    @SuppressWarnings("unchecked")
//    private Map<String, List<LinkedHashMap<String, Attribute>>> denormalizeRecord(
//            final Date now,
//            final EtalonRecord record,
//            final Map<RelationStateDTO, List<GetRelationDTO>> relations,
//            final Map<String, List<GetClassifierDTO>> classifiers,
//            final Map<String, List<LinkedHashMap<String, Attribute>>> result,
//            final Set<String> processedRelationPeriods,
//            final Set<String> processedClassifierPeriods
//    ) {
//
//        if (record == null) {
//            return result;
//        }
//
//        LinkedHashMap<String, Attribute> mainData = new LinkedHashMap<>();
//        LinkedHashMap<String, Attribute> recordData = new LinkedHashMap<>();
//
//
//        SimpleAttribute<?> id = new StringSimpleAttributeImpl(ETALON_ID, record.getInfoSection().getEtalonKey().getId());
//        mainData.put(ETALON_ID, id);
//        // for each row create default column 'EXTERNAL_ID'
//        // for export it is empty
//        SimpleAttribute<?> externalId = new StringSimpleAttributeImpl(EXTERNAL_ID, "");
//        mainData.put(EXTERNAL_ID, externalId);
//
//        // for each row create default column 'ORIGIN_KEYS'
//        StringBuilder originKeysString = new StringBuilder("");
//        RecordKeys keys = crComponent.identify(record.getInfoSection().getEtalonKey());
//        boolean notFirst = false;
//
//        for (RecordOriginKey originKey : keys.getSupplementaryKeysWithoutEnrichments()) {
//
//            if (notFirst) {
//                originKeysString.append("\n");
//            }
//            originKeysString.append(originKey.getSourceSystem())
//                    .append(" | ")
//                    .append(originKey.getExternalId());
//            notFirst = true;
//        }
//        SimpleAttribute<?> originKeys = new StringSimpleAttributeImpl(EXTERNAL_KEYS, originKeysString.toString());
//        mainData.put(EXTERNAL_KEYS, originKeys);
//
//        // for each row create default column 'FROM'
//        SimpleAttribute<?> from = new TimestampSimpleAttributeImpl(FROM, ConvertUtils.date2LocalDateTime(record.getInfoSection().getValidFrom()));
//        recordData.put(FROM, from);
//
//        // for each row create default column 'TO'
//        SimpleAttribute<?> to = new TimestampSimpleAttributeImpl(TO, ConvertUtils.date2LocalDateTime(record.getInfoSection().getValidTo()));
//        recordData.put(TO, to);
//
//        // for each row create default column 'is_active'
//        SimpleAttribute<?> active = new BooleanSimpleAttributeImpl(IS_ACTIVE, keys.isActive());
//        recordData.put(IS_ACTIVE, active);
//        recordData.putAll(mainData);
//
//        // add each first level attribute to the row
//        Collection<Attribute> attributes = record.getAttributeValues();
//        for (Attribute attr : attributes) {
//            // Collect simple, array and code attribute values only.
//            if (attr.getAttributeType() == AttributeType.COMPLEX) {
//                continue;
//            }
//
//            recordData.put(record.getInfoSection().getEntityName() +
//                    SYSTEM_PATH_DELIMITER + attr.getName(), attr);
//        }
//
//        addDenormalizedRows(result, OBJECT_TYPE.ENTITY.value() + record.getInfoSection().getEntityName(), Collections.singletonList(recordData));
//
//        // Complex entities
//        // iterate over all complex attributes
//        Collection<ComplexAttribute> complexAttributes = record.getComplexAttributes();
//
//        for (ComplexAttribute complexAttribute : complexAttributes) {
//
//            List<LinkedHashMap<String, Attribute>> nestedRows = new ArrayList<>();
//
//            // iterate over all nested records inside complex attribute
//            for (DataRecord nestedRecord : complexAttribute) {
//
//                Collection<Attribute> nestedAttrs = nestedRecord.getAttributeValues();
//                LinkedHashMap<String, Attribute> nestedRow = new LinkedHashMap<>();
//                // copy fields from record
//                nestedRow.putAll(mainData);
//                nestedRow.put(FROM, from);
//                nestedRow.put(TO, to);
//
//                for (Attribute nestedAttr : nestedAttrs) {
//                    if (nestedAttr.getAttributeType() == AttributeType.COMPLEX) {
//                        continue;
//                    }
//
//                    nestedRow.put(complexAttribute.getName() + SYSTEM_PATH_DELIMITER + nestedAttr.getName(),
//                            nestedAttr);
//                }
//
//                nestedRows.add(nestedRow);
//            }
//
//            addDenormalizedRows(result, OBJECT_TYPE.NESTED.value() + complexAttribute.getName(), nestedRows);
//        }
//
//        if (MapUtils.isNotEmpty(relations)) {
//
//            for (Entry<RelationStateDTO, List<GetRelationDTO>> entry : relations.entrySet()) {
//
//                entry.getValue().forEach(dto -> {
//                    if (dto.getEtalon() == null || BooleanUtils.isFalse(dto.getPeriodActive())) {
//                        return;
//                    }
//
//                    String periodId = new StringBuilder(dto.getEtalon().getInfoSection().getRelationEtalonKey())
//                            .append("_")
//                            .append(dto.getEtalon().getInfoSection().getPeriodId())
//                            .toString();
//                    if (processedRelationPeriods.contains(periodId)) {
//                        return;
//                    } else {
//                        processedRelationPeriods.add(periodId);
//                    }
//                    List<LinkedHashMap<String, Attribute>> relationDataRows = new ArrayList<>();
//
//                    LinkedHashMap<String, Attribute> relationDataRow = new LinkedHashMap<>();
//                    relationDataRow.putAll(mainData);
//                    relationDataRows.add(relationDataRow);
//                    try {
//                        // add relation from and to fields
//                        relationDataRow.put(FROM, new TimestampSimpleAttributeImpl(FROM,
//                                ConvertUtils.date2LocalDateTime(dto.getEtalon().getInfoSection().getValidFrom())));
//                        relationDataRow.put(TO, new TimestampSimpleAttributeImpl(TO,
//                                ConvertUtils.date2LocalDateTime(dto.getEtalon().getInfoSection().getValidTo())));
//
//                        String entityName = crComponent.identify(dto.getEtalon().getInfoSection().getToEtalonKey()).getEntityName();
//                        String name = TO_ETALON_ID;
//
//                        StringSimpleAttributeImpl attr = new StringSimpleAttributeImpl(
//                                name,
//                                dto.getEtalon().getInfoSection().getToEtalonKey().getId());
//
//                        relationDataRow.put(name, attr);
//
//                        name = TO_DISPLAY_NAME;
//                        attr = new StringSimpleAttributeImpl(
//                                name,
//                                displayNames.get(DNKey.newInstance()
//                                        .withValidFrom(dto.getEtalon().getInfoSection().getValidFrom())
//                                        .withValidTo(dto.getEtalon().getInfoSection().getValidTo())
//                                        .withEtalonId(dto.getEtalon().getInfoSection().getToEtalonKey().getId())
//                                        .withEntityName(dto.getRelName())));
//                        relationDataRow.put(name, attr);
//
//
//                        // add each first level attribute to the row
//                        for (SimpleAttribute<?> simpleAttribute : dto.getEtalon().getSimpleAttributes()) {
//                            if (RelationType.CONTAINS == dto.getEtalon().getInfoSection().getRelationType()) {
//                                name = dto.getEtalon().getInfoSection().getToEntityName() + SYSTEM_PATH_DELIMITER
//                                        + simpleAttribute.getName();
//                            } else {
//                                name = dto.getEtalon().getInfoSection().getRelationName() + SYSTEM_PATH_DELIMITER
//                                        + simpleAttribute.getName();
//                            }
//
//                            relationDataRow.put(name, simpleAttribute);
//
//                        }
//
//                        // add each array attributes
//                        for (ArrayAttribute<?> arrayAttribute : dto.getEtalon().getArrayAttributes()) {
//                            if (RelationType.CONTAINS == dto.getEtalon().getInfoSection().getType()) {
//                                name = dto.getEtalon().getInfoSection().getToEntityName() + SYSTEM_PATH_DELIMITER
//                                    + arrayAttribute.getName();
//                            } else {
//                                name = dto.getEtalon().getInfoSection().getRelationName() + SYSTEM_PATH_DELIMITER
//                                    + arrayAttribute.getName();
//                            }
//
//                            relationDataRow.put(name, arrayAttribute);
//                        }
//
//                    } catch (Exception e) {
//                        LOGGER.error("Error occured while trying to retrieve display name from the search service", e);
//                    }
//
//                    addDenormalizedRows(result, OBJECT_TYPE.RELATION.value() + dto.getEtalon().getInfoSection().getRelationName(),
//                            relationDataRows);
//                });
//            }
//        }
//
//        if (MapUtils.isNotEmpty(classifiers)) {
//            for (Entry<String, List<GetClassifierDTO>> entry : classifiers.entrySet()) {
//                List<LinkedHashMap<String, Attribute>> classifierDataRows = new ArrayList<>();
//                entry.getValue().forEach(cls -> {
//
//                    String periodId = new StringBuilder(cls.getEtalon().getInfoSection().getClassifierEtalonKey())
//                            .append("_")
//                            // todo add period Id for classifier
//                            //.append(dto.getEtalon().getInfoSection().getPeriodId())
//                            .toString();
//                    if (processedClassifierPeriods.contains(periodId)) {
//                        return;
//                    } else {
//                        processedClassifierPeriods.add(periodId);
//                    }
//                    LinkedHashMap<String, Attribute> classifierDataRow = new LinkedHashMap<>();
//                    classifierDataRow.putAll(mainData);
//                    classifierDataRows.add(classifierDataRow);
//
//                    // add classifier from and to fields
//                    classifierDataRow.put(FROM, new TimestampSimpleAttributeImpl(FROM,
//                            ConvertUtils.date2LocalDateTime(cls.getEtalon().getInfoSection().getValidFrom())));
//                    classifierDataRow.put(TO, new TimestampSimpleAttributeImpl(TO,
//                            ConvertUtils.date2LocalDateTime(cls.getEtalon().getInfoSection().getValidTo())));
//                    // Node id
//                    SimpleAttribute<?> nodeIdAttr
//                            = new StringSimpleAttributeImpl(CLASSIFIER_NODE_ID, cls.getClassifierKeys().getOriginKey().getNodeId());
//                    classifierDataRow.put(CLASSIFIER_NODE_ID, nodeIdAttr);
//
//                    // Node name
//                    // TODO: Fix me
//                    SimpleAttribute<?> nodeNameAttr
//                            = new StringSimpleAttributeImpl(CLASSIFIER_NODE_NAME, "TODO: query node name vie cache!");
//                    classifierDataRow.put(CLASSIFIER_NODE_NAME, nodeNameAttr);
//                    // Classifier is active status
//                    SimpleAttribute<?> classifierIsActive
//                            = new BooleanSimpleAttributeImpl(CLASSIFIER_IS_ACTIVE, cls.getClassifierKeys().isActive());
//                    classifierDataRow.put(CLASSIFIER_IS_ACTIVE, classifierIsActive);
//
//                    // Simple attributes
//                    for (SimpleAttribute<?> sa : cls.getEtalon().getSimpleAttributes()) {
//                        classifierDataRow.put(cls.getClassifierKeys().getClassifierName() + SYSTEM_PATH_DELIMITER + sa.getTouchName(), sa);
//                    }
//
//                    // Array attributes
//                    for (ArrayAttribute<?> aa : cls.getEtalon().getArrayAttributes()) {
//                        classifierDataRow.put(
//                                cls.getClassifierKeys().getClassifierName() + SYSTEM_PATH_DELIMITER + aa.getTouchName(),
//                                aa
//                        );
//                    }
//                });
//                addDenormalizedRows(result, OBJECT_TYPE.CLASSIFIER.value() + entry.getKey(), classifierDataRows);
//            }
//        }
//
//        return result;
//    }
//
//    private void addDenormalizedRows(Map<String, List<LinkedHashMap<String, Attribute>>> workbook,
//                                     String sheetName,
//                                     List<LinkedHashMap<String, Attribute>> rows) {
//        if (CollectionUtils.isNotEmpty(rows)) {
//            workbook.computeIfAbsent(sheetName, s -> new ArrayList<>()).addAll(rows);
//        }
//    }
//
//
//    /**
//     * The Class DisplayNames.
//     */
//    private class DisplayNames extends CacheLoader<DNKey, String> {
//
//        /**
//         * Load.
//         *
//         * @param key the key
//         * @return the string
//         * @throws Exception the exception
//         */
//        /*
//         * (non-Javadoc)
//         *
//         * @see com.google.common.cache.CacheLoader#load(java.lang.Object)
//         */
//        @Override
//        public String load(DNKey key) throws Exception {
//            return searchResultHitModifier.extractDisplayName(searchResultHitModifier.getDisplayNamesForRelationTo(key.getEntityName(), Collections.singletonList(Triple.of(key.getEtalonId(), key.getValidFrom(), key.getValidTo()))),
//                    key.getEtalonId(), key.getValidFrom(), key.getValidTo(), new Date());
//        }
//
//    }


}

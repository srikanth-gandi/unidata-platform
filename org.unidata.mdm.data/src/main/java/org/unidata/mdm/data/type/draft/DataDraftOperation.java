package org.unidata.mdm.data.type.draft;

/**
 * @author Mikhail Mikhailov on Sep 24, 2020
 * Operations, that are supported by the data module via draft UPSERT call.
 */
public enum DataDraftOperation {
    /**
     * Upsert period data (new or existing record).
     */
    UPSERT_DATA,
    /**
     * Re-activate a whole record.
     */
    RESTORE_RECORD,
    /**
     * Restore particular period while preserving record's state ACTIVE.
     */
    RESTORE_PERIOD,
    /**
     * Inactivate a whole record.
     */
    DELETE_RECORD,
    /**
     * Delete particular period while preserving record's state ACTIVE.
     */
    DELETE_PERIOD
}

/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.data.service.segments.relations.get;

import java.util.Collections;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.core.service.MetaModelService;
import org.unidata.mdm.core.type.model.RelationElement;
import org.unidata.mdm.core.type.timeline.Timeline;
import org.unidata.mdm.data.context.GetRelationRequestContext;
import org.unidata.mdm.data.module.DataModule;
import org.unidata.mdm.data.service.segments.AttributesPostProcessingSupport;
import org.unidata.mdm.data.type.data.EtalonRelation;
import org.unidata.mdm.data.type.data.OriginRelation;
import org.unidata.mdm.data.type.keys.RelationKeys;
import org.unidata.mdm.meta.configuration.Descriptors;
import org.unidata.mdm.meta.service.LookupService;
import org.unidata.mdm.system.type.pipeline.Point;
import org.unidata.mdm.system.type.pipeline.Start;

/**
 * @author Dmitry Kopin
 */
@Component(RelationGetAttributesPostProcessingExecutor.SEGMENT_ID)
public class RelationGetAttributesPostProcessingExecutor extends Point<GetRelationRequestContext>
    implements AttributesPostProcessingSupport {
    /**
     * This segment ID.
     */
    public static final String SEGMENT_ID = DataModule.MODULE_ID + "[RELATION_GET_POSTPROCESSING]";
    /**
     * Localized message code.
     */
    public static final String SEGMENT_DESCRIPTION = DataModule.MODULE_ID + ".relation.get.post.process.description";
    /**
     * Meta model service.
     */
    @Autowired
    private MetaModelService metaModelService;


    @Autowired
    protected LookupService lookupService;
    /**
     * Constructor.
     */
    public RelationGetAttributesPostProcessingExecutor() {
        super(SEGMENT_ID, SEGMENT_DESCRIPTION);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public MetaModelService metaModelService() {
        return metaModelService;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public LookupService lookupService() {
        return lookupService;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void point(GetRelationRequestContext ctx) {

        Timeline<OriginRelation> current = ctx.currentTimeline();
        if (Objects.nonNull(current)) {

            EtalonRelation er = current.isEmpty() ? null : current.first().getCalculationResult();
            if (Objects.isNull(er)) {
                return;
            }

            RelationKeys keys = current.getKeys();
            RelationElement el = metaModelService.instance(Descriptors.DATA).getRelation(keys.getRelationName());
            process(el.isContainment() ? el.getRight() : el, Collections.singletonList(er));
        }
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean supports(Start<?, ?> start) {
        return GetRelationRequestContext.class.isAssignableFrom(start.getInputTypeClass());
    }
}

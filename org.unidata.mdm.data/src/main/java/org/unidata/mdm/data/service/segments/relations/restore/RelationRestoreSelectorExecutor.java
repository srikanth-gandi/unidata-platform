/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.data.service.segments.relations.restore;

import org.springframework.stereotype.Component;
import org.unidata.mdm.data.context.RelationRestoreContext;
import org.unidata.mdm.data.dto.RestoreRelationDTO;
import org.unidata.mdm.data.module.DataModule;
import org.unidata.mdm.system.type.pipeline.Outcome;
import org.unidata.mdm.system.type.pipeline.Selector;
import org.unidata.mdm.system.type.pipeline.Start;
/**
 * @author Mikhail Mikhailov on Dec 27, 2020
 * Record draft/regular selector.
 */
@Component(RelationRestoreSelectorExecutor.SEGMENT_ID)
public class RelationRestoreSelectorExecutor extends Selector<RelationRestoreContext, RestoreRelationDTO> {
    /**
     * This segment ID.
     */
    public static final String SEGMENT_ID = DataModule.MODULE_ID + "[RELATION_RESTORE_SELECTOR]";
    /**
     * Localized message code.
     */
    public static final String SEGMENT_DESCRIPTION = DataModule.MODULE_ID + ".relation.restore.selector.description";
    /**
     * The draft outcome.
     */
    protected static final Outcome DRAFT = new Outcome(
            "[DRAFT]",
            DataModule.MODULE_ID + ".relation.restore.selector.draft.outcome.description",
            RelationRestoreContext.class,
            RestoreRelationDTO.class);
    /**
     * The regular outcome.
     */
    protected static final Outcome REGULAR = new Outcome(
            "[REGULAR]",
            DataModule.MODULE_ID + ".relation.restore.selector.regular.outcome.description",
            RelationRestoreContext.class,
            RestoreRelationDTO.class);
    /**
     * Constructor.
     * @param id
     * @param description
     */
    public RelationRestoreSelectorExecutor() {
        super(SEGMENT_ID, SEGMENT_DESCRIPTION, RestoreRelationDTO.class, DRAFT, REGULAR);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public Outcome select(RelationRestoreContext ctx) {

        if (ctx.isDraftOperation()) {
            return DRAFT;
        }

        return REGULAR;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean supports(Start<?, ?> start) {
        return RelationRestoreContext.class.isAssignableFrom(start.getInputTypeClass());
    }
}

package org.unidata.mdm.draft.po;

import java.util.Date;

/**
 * @author Mikhail Mikhailov on Sep 9, 2020
 * The draft editions table.
 */
public class EditionPO {
    /**
     * Table name.
     */
    public static final String TABLE_NAME = "editions";
    /**
     * The draft ID.
     */
    public static final String FIELD_DRAFT_ID = "draft_id";
    /**
     * Provider's name.
     */
    public static final String FIELD_PROVIDER = "provider";
    /**
     * Revision.
     */
    public static final String FIELD_REVISION = "revision";
    /**
     * Data.
     */
    public static final String FIELD_CONTENT = "content";
    /**
     * Create date.
     */
    public static final String FIELD_CREATE_DATE = "create_date";
    /**
     * Created by.
     */
    public static final String FIELD_CREATED_BY = "created_by";
    /**
     * The draft ID.
     */
    private long draftId;
    /**
     * Provider ID.
     */
    private String provider;
    /**
     * Revision.
     */
    private int revision;
    /**
     * Data.
     */
    private byte[] content;
    /**
     * Creator's user name.
     */
    private String createdBy;
    /**
     * Create date.
     */
    private Date createDate;
    /**
     * Constructor.
     */
    public EditionPO() {
        super();
    }
    /**
     * @return the draftId
     */
    public long getDraftId() {
        return draftId;
    }
    /**
     * @param draftId the draftId to set
     */
    public void setDraftId(long draftId) {
        this.draftId = draftId;
    }
    /**
     * @return the provider
     */
    public String getProvider() {
        return provider;
    }
    /**
     * @param provider the provider to set
     */
    public void setProvider(String provider) {
        this.provider = provider;
    }
    /**
     * @return the revision
     */
    public int getRevision() {
        return revision;
    }
    /**
     * @param revision the revision to set
     */
    public void setRevision(int revision) {
        this.revision = revision;
    }
    /**
     * @return the content
     */
    public byte[] getContent() {
        return content;
    }
    /**
     * @param content the content to set
     */
    public void setContent(byte[] data) {
        this.content = data;
    }
    /**
     * @return the createDate
     */
    public Date getCreateDate() {
        return createDate;
    }
    /**
     * @param createDate the createDate to set
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
    /**
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }
    /**
     * @param createdBy the createdBy to set
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
}

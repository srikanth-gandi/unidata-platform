/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.draft.type;

import org.apache.commons.lang3.StringUtils;

/**
 * @author Mikhail Mikhailov on Apr 30, 2021
 * Simple tags support.
 */
public class DraftTags {
    /**
     * Tags key <-> value separator.
     */
    public static final String TAGS_SEPARATOR = ":";
    /**
     * Constructor.
     */
    private DraftTags() {
        super();
    }
    /**
     * Creates tag from the given key and value using tags separator internally.
     * @param key the key
     * @param value the value
     * @return tag
     */
    public static String toTag(String key, String value) {
        return StringUtils.join(key, TAGS_SEPARATOR, value);
    }
}

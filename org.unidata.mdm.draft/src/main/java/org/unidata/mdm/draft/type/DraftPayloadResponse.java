package org.unidata.mdm.draft.type;

import org.unidata.mdm.draft.dto.DraftGetResult;

/**
 * @author Mikhail Mikhailov on Sep 24, 2020
 * This is a marker interface that has to be implemented by inner DTO/responses.
 * Those are carried by different draft response types, such as {@link DraftGetResult}.
 */
public interface DraftPayloadResponse {}

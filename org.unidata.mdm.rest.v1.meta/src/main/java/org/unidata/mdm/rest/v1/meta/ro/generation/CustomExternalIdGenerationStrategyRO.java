/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.v1.meta.ro.generation;

/**
 * Rest object for custom external id generation strategy
 * @author Dmitry Kopin on 11.10.2018.
 */
public class CustomExternalIdGenerationStrategyRO extends ExternalIdGenerationStrategyRO {
    /**
     * Strategy identifier
     * Replaced by className, scheduled for removal
     */
    private String strategyId;
    /**
     * Strategy name
     */
    private String name;
    /**
     * Strategy implementation class name.
     */
    private String className;
    /**
     * Strategy description
     */
    private String description;
    /**
     * {@inheritDoc}
     */
    @Override
    public ExternalIdGenerationTypeRO getStrategyType() {
        return ExternalIdGenerationTypeRO.CUSTOM;
    }

    /**
     * @return the className
     */
    public String getClassName() {
        return className;
    }

    /**
     * @param className the className to set
     */
    public void setClassName(String className) {
        this.className = className;
    }

    /**
     * @deprecated Replaced by className, scheduled for removal
     * @return id
     */
    @Deprecated(since = "6.0", forRemoval = true)
    public String getStrategyId() {
        return strategyId;
    }

    /**
     * @deprecated Replaced by className, scheduled for removal
     */
    @Deprecated(since = "6.0", forRemoval = true)
    public void setStrategyId(String strategyId) {
        this.strategyId = strategyId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}


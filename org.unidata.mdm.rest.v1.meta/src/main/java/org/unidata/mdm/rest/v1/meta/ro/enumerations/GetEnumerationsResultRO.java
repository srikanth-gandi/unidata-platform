package org.unidata.mdm.rest.v1.meta.ro.enumerations;

import java.util.List;

import org.unidata.mdm.rest.system.ro.DetailedOutputRO;

/**
 * Get enumeration result
 *
 * @author Alexandr Serov
 * @since 24.11.2020
 **/
public class GetEnumerationsResultRO extends DetailedOutputRO  {

    private List<EnumerationDefinitionRO> enumerations;

    public List<EnumerationDefinitionRO> getEnumerations() {
        return enumerations;
    }

    public void setEnumerations(List<EnumerationDefinitionRO> enumerations) {
        this.enumerations = enumerations;
    }
}

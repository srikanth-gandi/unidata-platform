/**
 * Meta model REST module V1
 *
 * @author Alexandr Serov
 * @since 18.11.2020
 **/
package org.unidata.mdm.rest.v1.meta;
package org.unidata.mdm.rest.v1.meta.ro.groups;

import java.util.List;

import org.unidata.mdm.rest.system.ro.DetailedOutputRO;

/**
 * Get filed groups result
 *
 * @author Alexandr Serov
 * @since 27.11.2020
 **/
public class GetFiledGroupsResultRO extends DetailedOutputRO {

    private List<FilledEntityGroupRO> filedEntityGroups;


    public List<FilledEntityGroupRO> getFiledEntityGroups() {
        return filedEntityGroups;
    }

    public void setFiledEntityGroups(List<FilledEntityGroupRO> filedEntityGroups) {
        this.filedEntityGroups = filedEntityGroups;
    }
}

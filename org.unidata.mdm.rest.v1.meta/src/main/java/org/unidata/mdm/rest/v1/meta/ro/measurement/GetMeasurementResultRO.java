package org.unidata.mdm.rest.v1.meta.ro.measurement;

import org.unidata.mdm.rest.system.ro.DetailedOutputRO;

/**
 * Get measurement result
 *
 * @author Alexandr Serov
 * @since 24.11.2020
 **/
public class GetMeasurementResultRO extends DetailedOutputRO {

    private MeasurementCategoryRO measurement;

    public MeasurementCategoryRO getMeasurement() {
        return measurement;
    }

    public void setMeasurement(MeasurementCategoryRO measurement) {
        this.measurement = measurement;
    }

}

package org.unidata.mdm.rest.v1.meta.ro.measurement;

import org.unidata.mdm.rest.system.ro.DetailedOutputRO;

/**
 * Delete measured result
 *
 * @author Alexandr Serov
 * @since 25.11.2020
 **/
public class DeleteMeasurementResultRO extends DetailedOutputRO {

    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}

package org.unidata.mdm.rest.v1.meta.converter.graph;


import org.unidata.mdm.meta.type.input.meta.MetaAction;
import org.unidata.mdm.rest.v1.meta.ro.MetaActionRO;

/**
 * The Class MetaActionROToDTOConverter.
 * @author ilya.bykov
 */
public class MetaActionROToDTOConverter {

/**
 * Convert.
 *
 * @param source the source
 * @return the meta action
 */
public static MetaAction convert(MetaActionRO source){
	if(source==null){
		return null;
	}
    return MetaAction.valueOf(source.name());
}
}

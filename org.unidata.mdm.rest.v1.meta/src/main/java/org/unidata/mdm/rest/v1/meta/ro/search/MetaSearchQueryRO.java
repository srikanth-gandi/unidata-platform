package org.unidata.mdm.rest.v1.meta.ro.search;

import java.util.List;

/**
 * Search Query
 *
 * @author Alexandr Serov
 * @since 26.11.2020
 **/
public class MetaSearchQueryRO {

    private String text;
    private List<String> fields;
    private int page;
    private int count;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<String> getFields() {
        return fields;
    }

    public void setFields(List<String> fields) {
        this.fields = fields;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}

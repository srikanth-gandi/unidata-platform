package org.unidata.mdm.rest.v1.meta.ro.enumerations;

/**
 * Upsert enumeration result
 *
 * @author Alexandr Serov
 * @since 24.11.2020
 **/
public class UpsertEnumerationRequestRO {

    private EnumerationDefinitionRO enumeration;

    public EnumerationDefinitionRO getEnumeration() {
        return enumeration;
    }

    public void setEnumeration(EnumerationDefinitionRO enumeration) {
        this.enumeration = enumeration;
    }
}

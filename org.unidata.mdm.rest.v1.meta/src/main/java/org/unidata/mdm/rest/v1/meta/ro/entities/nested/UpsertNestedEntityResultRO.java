package org.unidata.mdm.rest.v1.meta.ro.entities.nested;

import org.unidata.mdm.rest.system.ro.DetailedOutputRO;

/**
 * Upsert nested entity result
 *
 * @author Alexandr Serov
 * @since 26.11.2020
 **/
public class UpsertNestedEntityResultRO extends DetailedOutputRO {

    private Long draftId;
    // TODO: coming soon, wait response from upsert
//    private NestedEntityRO nestedEntity;

    public Long getDraftId() {
        return draftId;
    }

    public void setDraftId(Long draftId) {
        this.draftId = draftId;
    }

}

package org.unidata.mdm.rest.v1.meta.ro.entities.register;

import java.util.List;

import org.unidata.mdm.rest.v1.meta.ro.entities.RegisterEntityRO;

/**
 * Get register result
 *
 * @author Alexandr Serov
 * @since 26.11.2020
 **/
public class GetRegistersResultRO {

    private List<RegisterEntityRO> registerEntities;

    public List<RegisterEntityRO> getRegisterEntities() {
        return registerEntities;
    }

    public void setRegisterEntities(List<RegisterEntityRO> registerEntities) {
        this.registerEntities = registerEntities;
    }
}

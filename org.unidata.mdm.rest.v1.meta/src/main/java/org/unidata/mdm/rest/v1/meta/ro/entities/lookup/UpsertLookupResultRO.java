package org.unidata.mdm.rest.v1.meta.ro.entities.lookup;

import org.unidata.mdm.rest.system.ro.DetailedOutputRO;

/**
 * Upsert lookup result
 *
 * @author Alexandr Serov
 * @since 26.11.2020
 **/
public class UpsertLookupResultRO extends DetailedOutputRO {

    private Long draftId;
    // TODO: coming soon, wait response from upsert
//    private LookupEntityRO lookupEntity;

    public Long getDraftId() {
        return draftId;
    }

    public void setDraftId(Long draftId) {
        this.draftId = draftId;
    }
}

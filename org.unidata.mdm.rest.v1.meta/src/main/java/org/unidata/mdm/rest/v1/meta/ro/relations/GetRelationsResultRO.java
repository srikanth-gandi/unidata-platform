package org.unidata.mdm.rest.v1.meta.ro.relations;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

import org.unidata.mdm.rest.system.ro.DetailedOutputRO;

/**
 * Get Relations Result
 *
 * @author Alexandr Serov
 * @since 25.11.2020
 **/
public class GetRelationsResultRO extends DetailedOutputRO {

    private List<RelationRO> relations;

    public GetRelationsResultRO() {
        super();
    }

    public GetRelationsResultRO(List<RelationRO> relations) {
        super();
        this.relations = relations;
    }

    public List<RelationRO> getRelations() {
        return Objects.isNull(relations) ? Collections.emptyList() : relations;
    }

    public void setRelations(List<RelationRO> relations) {
        this.relations = relations;
    }
}

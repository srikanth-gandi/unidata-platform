package org.unidata.mdm.rest.v1.meta.converter;


import org.unidata.mdm.meta.type.input.meta.MetaExistence;
import org.unidata.mdm.rest.v1.meta.ro.MetaExistenceRO;

/**
 * The Class MetaStatusROToDTOConverter.
 * @author ilya.bykov
 */
public class MetaExistenceROToDTOConverter {

	/**
	 * Convert.
	 *
	 * @param source
	 *            the source
	 * @return the meta type
	 */
	public static MetaExistence convert(MetaExistenceRO source) {
		if (source == null) {
			return null;
		}
		return MetaExistence.valueOf(source.name());
	}
}

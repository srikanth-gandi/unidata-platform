/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.system.service;

import java.util.Collection;

import org.unidata.mdm.system.type.namespace.NameSpace;

/**
 * @author Mikhail Mikhailov on Mar 4, 2021
 */
public interface NameSpaceService extends ModulePostProcessor {
    /**
     * Gets namespace by id.
     * @param id the id
     * @return namespace or null
     */
    NameSpace getById(String id);
    /**
     * Returns true, if the service knows namespace with the given id.
     * @param id the id
     * @return true, if the service knows namespace with the given id
     */
    boolean hasNameSpace(String id);
    /**
     * Gets all registered namespaces.
     * @return all registered namespaces
     */
    Collection<NameSpace> getNameSpaces();
}

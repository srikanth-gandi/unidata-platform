/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.system.dao.impl;

import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.unidata.mdm.system.configuration.SystemConfigurationConstants;
import org.unidata.mdm.system.context.DatabaseMigrationContext;
import org.unidata.mdm.system.dao.NodesDAO;
import org.unidata.mdm.system.migration.Migrations;
import org.unidata.mdm.system.service.DatabaseMigrationService;

import nl.myndocs.database.migrator.MigrationScript;

/**
 * @author Mikhail Mikhailov on Sep 1, 2021
 */
@Repository
public class NodesDAOImpl extends BaseDAOImpl implements NodesDAO, InitializingBean {
    /**
     * Since the DAO is used on early stages, we need to have our migrations here separately.
     */
    private static final MigrationScript[] LOCAL_MIGRATIONS = {
            Migrations.of(
                    "UN-17594-symbolic-node-name",
                    "mikhail.mikhailov",
                    "create extension if not exists \"uuid-ossp\" with cascade",
                    "create table if not exists nodes_info(" +
                            "node_name text, " +
                            "node_sequence bytea not null, " +
                            "constraint pk_nodes_info_node_name primary key (node_name))",
                    "create index if not exists ix_nodes_info_node_sequence on nodes_info using hash (node_sequence)",
                    "create or replace function ud_generate_node_sequence() returns bytea as $$\n"
                    + "declare \n"
                    + "    val bytea;\n"
                    + "    val_exists boolean;\n"
                    + "begin\n"
                    + "    loop\n"
                    + "\n"
                    + "        val := set_bit(('\\x' || right(uuid_generate_v4()::text, 12))::bytea, 0, 1);\n"
                    + "        val_exists := coalesce((select distinct true from nodes_info where node_sequence = val), false);\n"
                    + "\n"
                    + "        if val_exists = false then\n"
                    + "            return val;\n"
                    + "        end if;\n"
                    + "\n"
                    + "    end loop;\n"
                    + "end\n"
                    + "$$ language plpgsql;")
    };
    /**
     * Since the DAO is used on early stages, we need to have our migrations here separately.
     */
    private final DatabaseMigrationService migrationService;
    /**
     * Processes node info and id sequence.
     */
    private final String processSQL;
    /**
     * Constructor.
     */
    @Autowired
    public NodesDAOImpl(
            @Qualifier("systemDataSource") final DataSource dataSource,
            @Qualifier("nodes-sql") final Properties sql,
            DatabaseMigrationService migrationService) {
        super(dataSource);
        this.processSQL = sql.getProperty("processSQL");
        this.migrationService = migrationService;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void afterPropertiesSet() throws Exception {
        migrationService.migrate(DatabaseMigrationContext.builder()
                .schemaName(SystemConfigurationConstants.SYSTEM_SCHEMA_NAME)
                .logName(SystemConfigurationConstants.SYSTEM_MIGRATION_LOG_NAME)
                .dataSource(getDefaultDataSource())
                .migrations(LOCAL_MIGRATIONS)
                .build());
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public byte[] process(String name) {
        return getJdbcTemplate().queryForObject(processSQL, byte[].class, name);
    }
}

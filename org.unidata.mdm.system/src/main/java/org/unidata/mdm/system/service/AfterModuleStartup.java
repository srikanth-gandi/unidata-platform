package org.unidata.mdm.system.service;

/**
 * Runs methods after {@link org.unidata.mdm.system.type.module.Module#start()}.
 * @author Mikhail Mikhailov on Apr 1, 2020
 */
public interface AfterModuleStartup {
    /**
     * Runs method after {@link org.unidata.mdm.system.type.module.Module#start()}.
     */
    void afterModuleStartup();
}

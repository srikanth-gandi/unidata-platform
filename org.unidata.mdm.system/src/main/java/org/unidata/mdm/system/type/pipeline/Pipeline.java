/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.system.type.pipeline;

import java.util.ArrayList;
import java.util.Collections;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.collections4.SetUtils;
import org.apache.commons.lang3.StringUtils;
import org.unidata.mdm.system.configuration.SystemConfigurationConstants;
import org.unidata.mdm.system.exception.PipelineException;
import org.unidata.mdm.system.exception.SystemExceptionIds;
import org.unidata.mdm.system.type.pipeline.connection.MultiPipelineConnection;
import org.unidata.mdm.system.type.pipeline.connection.OutcomesPipelineConnection;
import org.unidata.mdm.system.type.pipeline.connection.PipelineConnection;
import org.unidata.mdm.system.type.pipeline.connection.PipelineConnection.ConnectionType;
import org.unidata.mdm.system.type.pipeline.connection.PipelineId;
import org.unidata.mdm.system.type.pipeline.connection.SinglePipelineConnection;

/**
 * @author Mikhail Mikhailov
 * Pipeline instance object.
 */
public final class Pipeline {
    /**
     * Empty pipeline instance.
     */
    private static final Pipeline EMPTY = new Pipeline(null, null, null, false, false);
    /**
     * The start segment id.
     */
    private final String startId;
    /**
     * The subject id. May be null/empty string.
     */
    private final String subjectId;
    /**
     * The description, either supplied or generated.
     */
    private final String description;
    /**
     * Marks a pipeline as batched pipeline.
     */
    private final boolean batched;
    /**
     * Marks a pipeline as anonymous pipeline.
     */
    private final boolean anonymous;
    /**
     * Just the indicator that .end has already been called and the PL is closed.
     */
    private boolean finished;
    /**
     * Marks pipelines, that were created by autoinstall and was not modified by user.
     */
    private boolean automatic;
    /**
     * Collected segments.
     */
    private final List<Segment> segments = new ArrayList<>();
    /**
     * Fallbacks.
     */
    private final List<Segment> fallbacks = new ArrayList<>();
    /**
     * Connected pipelines.
     */
    private Map<Segment, PipelineConnection> connections;
    /**
     * Constructor.
     * @param startId the start segment id.
     * @param subjectId the subject id. May be null/blank.
     * @param description the description.
     * @param batched the batched mark
     */
    private Pipeline(String startId, String subjectId, String description, boolean batched, boolean anonymous) {
        super();
        this.startId = startId;
        this.subjectId = subjectId;
        this.description = description;
        this.batched = batched;
        this.anonymous = anonymous;
    }
    /**
     * Gets the pipeline id for this pipeline.
     * @return pipeline id for this pipeline
     */
    public PipelineId getPipelineId() {
        return new PipelineId(startId, subjectId);
    }
    /**
     * Gets pipeline ID. Must be unique accross the system.
     * @return ID
     */
    public String getStartId() {
        return startId;
    }
    /**
     * @return the subjectId
     */
    public String getSubjectId() {
        return subjectId;
    }
    /**
     * Gets type description.
     * @return description
     */
    public String getDescription() {
        return description;
    }
    /**
     * Gets the participating segments collection.
     * @return segments
     */
    public List<Segment> getSegments() {
        return segments;
    }
    /**
     * Gets the pipeline fallback functions.
     * @return fallbacks
     */
    public List<Segment> getFallbacks() {
        return fallbacks;
    }
    /**
     * Gets pipeline connection associated with the given segment or null.
     * @param <C> the precise connection type
     * @param s the segment
     * @return connection or null, if not associated
     */
    @SuppressWarnings("unchecked")
    @Nullable
    public<C extends PipelineConnection> C getConnection(Segment s) {
        return (C) getConnections().get(s);
    }
    /**
     * Gets the output type class of this pipeline.
     * @return class or null, if the pipeline was not started (is empty)
     */
    @Nullable
    public Class<?> getOutputTypeClass() {

        if (isEmpty()) {
            return null;
        }

        return ((Start<?, ?>) segments.get(0)).getOutputTypeClass();
    }
    /**
     * Gets the input type class of this pipeline.
     * @return class or null, if the pipeline was not started (is empty)
     */
    @Nullable
    public Class<?> getInputTypeClass() {

        if (isEmpty()) {
            return null;
        }

        return ((Start<?, ?>) segments.get(0)).getInputTypeClass();
    }
    /**
     * Returns true, if the pipeline p is the same as this pipeline.
     * @param p the pipeline
     * @return true, if the pipeline p is the same as this pipeline
     */
    public boolean isSame(Pipeline p) {

        if (Objects.isNull(p)) {
            return false;
        } else if (p == this) {
            return true;
        }

        if (!Objects.equals(this.startId, p.startId)
         || !Objects.equals(this.subjectId, p.subjectId)
         || !Objects.equals(this.description, p.description)
         || (this.batched != p.batched)
         || (this.anonymous != p.anonymous)
         || (this.finished != p.finished)
         || (this.automatic != p.automatic)
         || (this.segments.size() != p.segments.size())
         || (this.fallbacks.size() != p.fallbacks.size())) {
            return false;
        }

        for (int i = 0; i < this.segments.size(); i++) {

            Segment mine = this.segments.get(i);
            Segment theirs = p.segments.get(i);

            // Same singletons are expected to be used.
            // Can just compare references.
            if (!isSameSegment(mine, theirs)) {
                return false;
            }
        }

        for (int i = 0; i < this.fallbacks.size(); i++) {

            Segment mine = this.fallbacks.get(i);
            Segment theirs = p.fallbacks.get(i);

            // Same singletons are expected to be used.
            // Can just compare references.
            if (!isSameSegment(mine, theirs)) {
                return false;
            }
        }

        return hasSameConnections(p);
    }

    /**
     * Returns true, if the segment is connected with another pipeline(s).
     * @param s the segment
     * @return true, if the segment is connected with another pipeline(s)
     */
    public boolean isConnected(Segment s) {
        return getConnections().containsKey(s);
    }
    /**
     * Returns finished state.
     * @return finished state
     */
    public boolean isFinished() {
        return finished;
    }
    /**
     * Returns batched state.
     * @return batched state
     */
    public boolean isBatched() {
        return batched;
    }
    /**
     * Tells whether this pipeline is an anonymous one.
     * @return true, if anonymous, false otherwise
     */
    public boolean isAnonymous() {
        return anonymous;
    }
    /**
     * Tells whether this pipeline is empty.
     * @return true, if empty, false otherwise
     */
    public boolean isEmpty() {
        return segments.isEmpty();
    }
    /**
     * @return the automatic
     */
    public boolean isAutomatic() {
        return automatic;
    }
    /**
     * Puts this pipeline to ready state.
     */
    public void ready() {
        getConnections().forEach((k, v) -> v.connect(k));
    }
    /**
     * @param automatic the automatic to set
     */
    public Pipeline automatic(boolean automatic) {
        this.automatic = automatic;
        return this;
    }
    /**
     * Adds a point to this pipeline.
     * @param p the point
     * @return self
     */
    public Pipeline with(@Nonnull Point<? extends PipelineInput> p) {

        Objects.requireNonNull(p, "Point segment is null");

        throwIfPipelineClosed();
        throwIfPipelineBatchedMismatch(p);

        segments.add(p);
        return this;
    }
    /**
     * Adds a connector with pipeline selection at runtime.
     * @param c the connector
     * @return self
     */
    public Pipeline with(@Nonnull Connector<? extends PipelineInput, ? extends PipelineOutput> c) {

        Objects.requireNonNull(c, "Connector segment is null");

        throwIfPipelineClosed();
        throwIfPipelineBatchedMismatch(c);

        segments.add(c);
        return this;
    }
    /**
     * Adds a connector with pipeline selection at runtime.
     * @param c the connector
     * @param p the connected pipeline
     * @return self
     */
    public Pipeline with(@Nonnull Connector<? extends PipelineInput, ? extends PipelineOutput> c, @Nonnull PipelineConnection p) {

        Objects.requireNonNull(c, "Connector segment is null");
        Objects.requireNonNull(p, "Connector pipeline connection is null");

        throwIfPipelineClosed();
        throwIfPipelineBatchedMismatch(c);

        segments.add(c);

        ensureConnections().put(c, p);
        return this;
    }
    /**
     * Adds a splitter with pipeline selection at runtime.
     * @param s a {@link Splitter} instance
     * @param p a supplier, returning splitting outcomes
     * @return self
     */
    public Pipeline with(@Nonnull Splitter<? extends PipelineInput, ? extends PipelineOutput> s, PipelineConnection p) {

        Objects.requireNonNull(s, "Splitter segment is null");
        Objects.requireNonNull(p, "Splitter outcomes connection is null");

        throwIfPipelineClosed();
        throwIfPipelineBatchedMismatch(s);

        segments.add(s);

        ensureConnections().put(s, p);
        return this;
    }
    /**
     * Adds a selector with pipeline selection at runtime.
     * @param s a {@link Selector} instance
     * @param p a supplier, returning splitting outcomes
     * @return self
     */
    public Pipeline with(@Nonnull Selector<? extends PipelineInput, ? extends PipelineOutput> s, PipelineConnection p) {

        Objects.requireNonNull(s, "Selector segment is null");
        Objects.requireNonNull(p, "Selector outcomes connection is null");

        throwIfPipelineClosed();
        throwIfPipelineBatchedMismatch(s);

        segments.add(s);

        ensureConnections().put(s, p);
        return this;
    }
    /**
     * Connects using the supplied connection.
     * Connected segment must not be null!
     * @param p the connection
     * @return self
     */
    public Pipeline with(@Nonnull PipelineConnection p) {

        Objects.requireNonNull(p, "Pipeline connection is null");

        Segment segment = p.getSegment();
        Objects.requireNonNull(segment, "Connected segment is null");

        switch (segment.getType()) {
        case CONNECTOR:
            return with((Connector<?, ?>) segment, p);
        case SELECTOR:
            return with((Selector<?, ?>) segment, p);
        case SPLITTER:
            return with((Splitter<?, ?>) segment, p);
        default:
            break;
        }

        return this;
    }
    /**
     * Add a fallback function to pipeline.
     * @param fallback the fallback function
     * @return self
     */
    public Pipeline with(@Nonnull Fallback<? extends PipelineInput> fallback) {

        Objects.requireNonNull(fallback, "Fallback segment is null");

        throwIfPipelineBatchedMismatch(fallback);

        fallbacks.add(fallback);
        return this;
    }
    /**
     * Adds a connector with pipeline selection at runtime.
     * @param f the finish segment
     * @return self
     */
    public Pipeline end(@Nonnull Finish<? extends PipelineInput, ? extends PipelineOutput> f) {

        Objects.requireNonNull(f, "Finish segment is null");

        throwIfPipelineClosed();
        throwIfPipelineBatchedMismatch(f);

        segments.add(f);

        verify();

        finished = true;
        return this;
    }

    private boolean isSameSegment(Segment mine, Segment theirs) {
        return mine == theirs
            || (Objects.nonNull(mine) && Objects.nonNull(theirs) && StringUtils.equals(mine.getId(), theirs.getId()));
    }

    private boolean hasSameConnections(Pipeline p) {

        if (MapUtils.size(this.connections) != MapUtils.size(p.connections)) {
            return false;
        }

        for (Entry<Segment, PipelineConnection> entry : this.getConnections().entrySet()) {

            boolean matches = false;
            PipelineConnection other = p.getConnections().get(entry.getKey());
            if (Objects.nonNull(other) && other.getType() == entry.getValue().getType()) {
                matches =
                        (other.getType() == ConnectionType.SINGLE && isSameSingleConnection((SinglePipelineConnection) entry.getValue(), (SinglePipelineConnection) other))
                     || (other.getType() == ConnectionType.MULTIPLE && isSameMultiConnection((MultiPipelineConnection) entry.getValue(), (MultiPipelineConnection) other))
                     || (other.getType() == ConnectionType.OUTCOME && isSameOutcomeConnection((OutcomesPipelineConnection) entry.getValue(), (OutcomesPipelineConnection) other));
            }

            if (!matches) {
                return false;
            }
        }

        return true;
    }

    private boolean isSameSingleConnection(SinglePipelineConnection mine, SinglePipelineConnection theirs) {

        boolean initial = mine == theirs || mine != null && theirs != null;
        if (!initial) {
            return false;
        }

        if (Objects.nonNull(mine)) {

            if (!Objects.equals(mine.getId(), theirs.getId())) {
                return false;
            }

            if (Objects.nonNull(mine.getInitial()) && !mine.getInitial().isSame(theirs.getInitial())) {
                return false;
            }
        }

        return true;
    }

    private boolean isSameMultiConnection(MultiPipelineConnection mine, MultiPipelineConnection theirs) {

        boolean initial = mine == theirs || mine != null && theirs != null;
        if (!initial) {
            return false;
        }

        if (Objects.isNull(mine)) {
            return true;
        }

        if (mine.getIds().size() != theirs.getIds().size()) {
            return false;
        }

        for (int i = 0; i < mine.getIds().size(); i++) {
            if (!Objects.equals(mine.getIds().get(i), theirs.getIds().get(i))) {
                return false;
            }
        }

        if (mine.getInitial().size() != theirs.getInitial().size()) {
            return false;
        }

        for (int i = 0; i < mine.getInitial().size(); i++) {
            if (!mine.getInitial().get(i).isSame(theirs.getInitial().get(i))) {
                return false;
            }
        }

        return true;
    }

    private boolean isSameOutcomeConnection(OutcomesPipelineConnection mine, OutcomesPipelineConnection theirs) {

        boolean initial = mine == theirs || mine != null && theirs != null;
        if (!initial) {
            return false;
        }

        if (Objects.isNull(mine)) {
            return true;
        }

        if (!SetUtils.isEqualSet(mine.getIds().keySet(), theirs.getIds().keySet())) {
            return false;
        }

        for (Entry<String, PipelineId> entry : mine.getIds().entrySet()) {
            if (!Objects.equals(entry.getValue(), theirs.getIds().get(entry.getKey()))) {
                return false;
            }
        }

        if (!SetUtils.isEqualSet(mine.getInitial().keySet(), theirs.getInitial().keySet())) {
            return false;
        }

        for (Entry<Outcome, Pipeline> entry : mine.getInitial().entrySet()) {
            if (!entry.getValue().isSame(theirs.getInitial().get(entry.getKey()))) {
                return false;
            }
        }

        return true;
    }

    private void verify() {

        // 1. Every pipeline must support starting point input type in each segment
        Start<?, ?> start = (Start<?, ?>) segments.get(0);
        for (Segment s : segments) {

            Class<?> outputType = null;
            switch (s.getType()) {
            case SELECTOR:
                outputType = ((Selector<?, ?>) s).getOutputTypeClass();
                break;
            case SPLITTER:
                outputType = ((Splitter<?, ?>) s).getOutputTypeClass();
                break;
            case FINISH:
                outputType = ((Finish<?, ?>) s).getOutputTypeClass();
                break;
            default:
                break;
            }

            if (!s.supports(start)) {
                throw new PipelineException("Segment [{}] does not support pipeline's starting point [{}].",
                        SystemExceptionIds.EX_PIPELINE_START_SEGMENT_NOT_SUPPORTED, s.getId(), start.getId());
            }

            if (Objects.nonNull(outputType) && !start.getOutputTypeClass().isAssignableFrom(outputType)) {
                throw new PipelineException("[{}] output type does not match pipeline's output type [{}].",
                        SystemExceptionIds.EX_PIPELINE_OUTPUT_TYPES_DONT_MATCH, s.getId(), start.getOutputTypeClass().getCanonicalName());
            }
        }
    }

    private Map<Segment, PipelineConnection> ensureConnections() {

        if (Objects.isNull(connections)) {
            connections = new IdentityHashMap<>();
        }

        return connections;
    }

    private Map<Segment, PipelineConnection> getConnections() {
        return Objects.isNull(connections) ? Collections.emptyMap() : connections;
    }

    /**
     * Throws if this PL is already closed.
     */
    private void throwIfPipelineClosed() {
        if (finished) {
            throw new PipelineException("This pipeline is already finished.", SystemExceptionIds.EX_PIPELINE_ALREADY_FINISHED);
        }
    }
    /**
     * Throws if this PL is already closed.
     */
    private void throwIfPipelineBatchedMismatch(Segment s) {
        if (s.isBatched() != batched) {
            throw new PipelineException("Attempt to add a non batched segment to a batched pipeline or vice versa.",
                    SystemExceptionIds.EX_PIPELINE_BATCHED_MISMATCH);
        }
    }
    /**
     * Returns the empty pipeline singleton.
     * @return empty pipeline
     */
    public static Pipeline empty() {
        return EMPTY;
    }
    /**
     * Returns a pipeline instance.
     * @param s the starting point
     * @param anonymous build an anonymous pipeline, if true
     * @return pipeline
     */
    public static Pipeline start(@Nonnull Start<? extends PipelineInput, ? extends PipelineOutput> s, boolean anonymous) {

        Objects.requireNonNull(s, "Start segment is null");
        if (anonymous) {
            Pipeline p = new Pipeline(s.getId(), null, null, s.isBatched(), true);
            p.segments.add(s);
            return p;
        }

        return start(s);
    }
    /**
     * Starts a pipeline from starting point with no subject.
     * Description will be taken from the starting point.
     * @param s the starting point
     * @return a pipeline instance
     */
    public static Pipeline start(@Nonnull Start<? extends PipelineInput, ? extends PipelineOutput> s) {
        return start(s, null, null);
    }
    /**
     * Starts a pipeline from starting point and subject.
     * Description will be taken from the starting point.
     * @param s the starting point
     * @param subjectId the subject id, on which this pipeline overrides the default one
     * @return a pipeline instance
     */
    public static Pipeline start(@Nonnull Start<? extends PipelineInput, ? extends PipelineOutput> s, String subjectId) {
        return start(s, subjectId, null);
    }
    /**
     * Starts a named pipeline from starting point, using subject and description.
     * @param s the starting point
     * @param subjectId the subject id, on which this pipeline overrides the default one
     * @param description the description
     * @return a pipeline instance
     */
    public static Pipeline start(@Nonnull Start<? extends PipelineInput, ? extends PipelineOutput> s, String subjectId, String description) {
        Objects.requireNonNull(s, "Start segment is null");
        Pipeline p = new Pipeline(
                s.getId(),
                StringUtils.isBlank(subjectId) ? SystemConfigurationConstants.NON_SUBJECT : subjectId,
                StringUtils.isBlank(description) ? s.getDescription() : description,
                s.isBatched(),
                false);
        p.segments.add(s);
        return p;
    }
}

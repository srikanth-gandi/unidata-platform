/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.system.service.impl.spring;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Nullable;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.unidata.mdm.system.type.spring.ModularApplicationContext;

/**
 * @author Mikhail Mikhailov on Sep 22, 2021
 * Copied from modular web context, since those must be different inheritance sub-hierarchies.
 */
public class ModularAnnotationConfigApplicationContext
    extends AnnotationConfigApplicationContext
    implements ModularApplicationContext {
    /**
     * Modules stack.
     */
    private final List<AbstractApplicationContext> stack = new ArrayList<>(32);
    /**
     * Was already refreshed.
     */
    private boolean ready;
    /**
     * Constructor.
     */
    public ModularAnnotationConfigApplicationContext() {
        super();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public AbstractApplicationContext getFirst() {
        return stack.get(0);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public AbstractApplicationContext getLast() {
        return stack.get(stack.size() - 1);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void addChild(AbstractApplicationContext context) {
        stack.add(context);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    @Nullable
    public AbstractApplicationContext getChild(String id) {
        return stack.stream()
                .filter(c -> c.getId().equals(id))
                .findFirst()
                .orElse(null);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<AbstractApplicationContext> getChildren() {
        return stack;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public int getSize() {
        return stack.size();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<String> getIds() {
        return stack.stream()
                .map(AbstractApplicationContext::getId)
                .collect(Collectors.toList());
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isReady() {
        return ready;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void setReady(boolean ready) {
        this.ready = ready;
    }
}

/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.meta.ro;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Michael Yashin. Created on 25.05.2015.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ComplexAttributeRO extends AbstractAttributeRO {
    /**
     * Minimum appearance count.
     */
    protected Integer minCount;
    /**
     * Maximum appearance count.
     */
    protected Integer maxCount;
    /**
     * Sub entity key attribute.
     */
    protected String nestedEntityKeyAttribute;

    protected String nestedEntityName;

    protected int order;

    /**
     * @return the nestedEntityName
     */
    public String getNestedEntityName() {
        return nestedEntityName;
    }

    /**
     * @param nestedEntityName the nestedEntityName to set
     */
    public void setNestedEntityName(String nestedEntityName) {
        this.nestedEntityName = nestedEntityName;
    }

    public Integer getMinCount() {
        return minCount;
    }

    public void setMinCount(Integer minCount) {
        this.minCount = minCount;
    }

    public Integer getMaxCount() {
        return maxCount;
    }

    public void setMaxCount(Integer maxCount) {
        this.maxCount = maxCount;
    }

    public String getNestedEntityKeyAttribute() {
        return nestedEntityKeyAttribute;
    }

    public void setNestedEntityKeyAttribute(String subEntityKeyAttribute) {
        this.nestedEntityKeyAttribute = subEntityKeyAttribute;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }
}

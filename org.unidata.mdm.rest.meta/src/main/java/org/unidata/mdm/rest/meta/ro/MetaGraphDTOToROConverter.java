package org.unidata.mdm.rest.meta.ro;


import org.unidata.mdm.meta.type.input.meta.MetaGraph;

/**
 * The Class MetaGraphDTOToROConverter.
 * @author ilya.bykov
 */
public class MetaGraphDTOToROConverter {

    private MetaGraphDTOToROConverter() {
    }

    /**
     * Convert.
     *
     * @param source the source
     * @return the meta graph RO
     */
    public static MetaGraphRO convert(MetaGraph source) {
        if (source == null) {
            return null;
        }
        MetaGraphRO target = new MetaGraphRO(
                source.getId(), source.getFileName(),
                MetaVertexDTOToROConverter.convert(source.vertexSet()),
                MetaEdgeDTOToROConverter.convert(source.edgeSet()),
                source.isImportRoles(),
                source.isImportUsers()
        );
        target.setOverride(source.isOverride());
        return target;
    }
}

/**
 * Meta model REST bits.
 * @author Mikhail Mikhailov on Feb 13, 2020
 */
package org.unidata.mdm.rest.meta.type;
/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.meta.ro.references;

import javax.annotation.Nonnull;

/**
 * Unique key of relation help identifying relation
 */
public class RelationReferenceRO implements UniqueReferenceRO {
    /**
     * The type.
     */
    public static final ReferenceType TYPE = new ReferenceType("RELATION", "связь");

    @Nonnull
    private final String relName;

    public RelationReferenceRO(@Nonnull String relName) {
        this.relName = relName;
    }

    @Nonnull
    public String getRelName() {
        return relName;
    }

    @Override
    public ReferenceType keyType() {
        return TYPE;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        RelationReferenceRO that = (RelationReferenceRO) o;

        return relName.equals(that.relName);

    }

    @Override
    public int hashCode() {
        return relName.hashCode();
    }

    @Override
    public String toString() {
        return "{" +
                "relName='" + relName + '\'' +
                '}';
    }
}

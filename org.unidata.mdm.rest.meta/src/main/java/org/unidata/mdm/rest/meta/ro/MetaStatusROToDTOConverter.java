package org.unidata.mdm.rest.meta.ro;


import org.unidata.mdm.meta.type.input.meta.MetaStatus;

/**
 * The Class MetaStatusROToDTOConverter.
 * @author ilya.bykovF
 */
public class MetaStatusROToDTOConverter {

	private MetaStatusROToDTOConverter() {
	}

	/**
	 * Convert.
	 *
	 * @param source the source
	 * @return the meta status
	 */
	public static MetaStatus convert(MetaStatusRO source) {
		if (source == null) {
			return null;
		}
		return MetaStatus.valueOf(source.name());
	}
}

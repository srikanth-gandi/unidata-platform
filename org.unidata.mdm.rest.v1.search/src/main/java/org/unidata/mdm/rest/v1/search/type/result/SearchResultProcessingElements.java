package org.unidata.mdm.rest.v1.search.type.result;

public enum SearchResultProcessingElements {
    LOOKUP, MEASURED, ENUM, DATE
}
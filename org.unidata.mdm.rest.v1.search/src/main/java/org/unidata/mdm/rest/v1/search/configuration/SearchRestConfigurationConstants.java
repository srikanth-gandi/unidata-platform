/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.v1.search.configuration;

public class SearchRestConfigurationConstants {

    private SearchRestConfigurationConstants() {
        super();
    }
    /**
     * Parameter 'text'.
     */
    public static final String SEARCH_PARAM_TEXT = "text";
    /**
     * Parameter 'fields'.
     */
    public static final String SEARCH_PARAM_FIELDS = "fields";
    /**
     * Parameter 'count'.
     */
    public static final String SEARCH_PARAM_COUNT = "count";
    /**
     * Parameter 'page'.
     */
    public static final String SEARCH_PARAM_PAGE = "page";
    /**
     * Parameter 'request content'.
     */
    public static final String SEARCH_PARAM_EXPORT_REQUEST_CONTENT = "request_content";
    /**
     * Search root path.
     */
    public static final String SEARCH_PATH_SEARCH = "search";
//    /**
//     * Form search path 'param'.
//     */
//    public static final String SEARCH_PATH_FORM = "form";
//    /**
//     * Combo search path 'param'.
//     */
//    public static final String SEARCH_PATH_COMBO = "combo";
    /**
     * Complex search path 'param'.
     */
    public static final String SEARCH_PATH_COMPLEX = "complex";
//    /**
//     * Simple search path 'param'.
//     */
//    public static final String SEARCH_PATH_SIMPLE = "simple";
    /**
     * Search path 'param'.
     */
    public static final String SEARCH_PATH_SAYT = "sayt";
    /**
     * Search path 'xls-export-simple'.
     */
    public static final String SEARCH_PATH_XLS_EXPORT_SIMPLE = "xls-export-simple";
    /**
     * Search path 'xls-export-form'.
     */
    public static final String SEARCH_PATH_XLS_EXPORT_FORM = "xls-export-form";
    /**
     * Search path 'meta'.
     */
    public static final String SEARCH_PATH_META = "meta";
    /**
     * Default number of objects to return (page size), if nothing is set.
     */
    public static final String DEFAULT_OBJ_COUNT_VALUE = "10";
    /**
     * Default page number (offset).
     */
    public static final String DEFAULT_PAGE_NUMBER_VALUE = "0";

    // ========================================== Message block ========================================================

    public static final String SEARCH_BEFORE_USER_EXIT_EXCEPTION = "app.search.before.user.exit.exception";

    public static final String SEARCH_AFTER_USER_EXIT_EXCEPTION = "app.search.after.user.exit.exception";
}

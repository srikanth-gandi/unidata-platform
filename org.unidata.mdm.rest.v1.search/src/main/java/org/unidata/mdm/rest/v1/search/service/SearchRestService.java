/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.v1.search.service;

import java.util.Objects;
import javax.ws.rs.Consumes;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import org.apache.cxf.jaxrs.ext.xml.ElementClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.unidata.mdm.rest.system.service.AbstractRestService;
import org.unidata.mdm.rest.v1.search.ro.SearchRequestRO;
import org.unidata.mdm.rest.v1.search.ro.SearchResponseRO;
import org.unidata.mdm.rest.v1.search.type.rendering.SearchRestInputRenderingAction;
import org.unidata.mdm.rest.v1.search.type.rendering.SearchRestOutputRenderingAction;
import org.unidata.mdm.search.context.ComplexSearchRequestContext;
import org.unidata.mdm.search.context.ComplexSearchRequestContext.ComplexSearchRequestContextBuilder;
import org.unidata.mdm.search.service.SearchService;
import org.unidata.mdm.system.dto.OutputContainer;
import org.unidata.mdm.system.service.RenderingService;
import org.unidata.mdm.system.type.runtime.MeasurementContextName;
import org.unidata.mdm.system.type.runtime.MeasurementPoint;

/**
 * @author Mikhail Mikhailov
 * Search REST service.
 */
@Path("/")
@Consumes({"application/json"})
@Produces({"application/json"})
public class SearchRestService extends AbstractRestService {
    /**
     * Logger.
     */
    public static final Logger LOGGER = LoggerFactory.getLogger(SearchRestService.class);
    /**
     * Search service.
     */
    @Autowired
    private SearchService searchService;
    /**
     * Rendering support.
     */
    @Autowired
    private RenderingService renderingService;

    /**
     * Search form request
     *
     * @return result
     */
    @POST
    @ElementClass(response = SearchResponseRO.class)
    @Operation(description = "Search", method = HttpMethod.POST, tags = "search")
    public SearchResponseRO search(@Parameter(description = "Search query") SearchRequestRO request) {

        Objects.requireNonNull(request, "Request can't be null");
        MeasurementPoint.init(MeasurementContextName.MEASURE_UI_SEARCH_SIMPLE);
        MeasurementPoint.start();
        try {
            ComplexSearchRequestContextBuilder collector = ComplexSearchRequestContext.builder();
            renderingService.renderInput(SearchRestInputRenderingAction.SIMPLE_SEARCH_INPUT, collector, request);
            OutputContainer container = searchService.search(collector.build());
            SearchResponseRO output = new SearchResponseRO();
            renderingService.renderOutput(SearchRestOutputRenderingAction.SIMPLE_SEARCH_OUTPUT, container, output);
            return output;
        } finally {
            MeasurementPoint.stop();
        }
    }

}

/**
 * Rendering REST search requests.
 * @author Mikhail Mikhailov on Feb 28, 2020
 */
package org.unidata.mdm.rest.v1.search.type.rendering;
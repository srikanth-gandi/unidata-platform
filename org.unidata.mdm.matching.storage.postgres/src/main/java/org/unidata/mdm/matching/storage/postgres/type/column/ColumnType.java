/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.storage.postgres.type.column;

import org.unidata.mdm.matching.core.type.data.MatchingDataType;

/**
 * @author Sergey Murskiy on 17.08.2021
 */
public enum ColumnType {
    /**
     * String column.
     */
    STRING("varchar(512)"),
    /**
     * Double column.
     */
    DOUBLE("numeric"),
    /**
     * Long column.
     */
    LONG("bigint"),
    /**
     * Boolean column.
     */
    BOOLEAN("boolean"),
    /**
     * Byte column.
     */
    BYTES("bytea"),
    /**
     * Date column.
     */
    LOCAL_DATE("date"),
    /**
     * Time column.
     */
    LOCAL_TIME("timetz"),
    /**
     * Timestamp column.
     */
    LOCAL_DATE_TIME("timestamptz");

    /**
     * Postgres type.
     */
    private final String postgresType;

    /**
     * Constructor.
     *
     * @param postgresType the postgres type to set
     */
    ColumnType(String postgresType) {
        this.postgresType = postgresType;
    }

    /**
     * Gets postgres type.
     *
     * @return the postgres type
     */
    public String getPostgresType() {
        return postgresType;
    }

    /**
     * Convert matching data type to column type.
     *
     * @param dataType matching data type
     * @return column type
     */
    public static ColumnType fromDataType(MatchingDataType dataType) {
        switch (dataType) {
            case STRING:
            case ENUM:
            case LINK:
            case DICTIONARY:
                return STRING;
            case BOOLEAN:
                return BOOLEAN;
            case NUMBER:
            case MEASURED:
                return DOUBLE;
            case BLOB:
            case CLOB:
                return BYTES;
            case INTEGER:
                return LONG;
            case DATE:
                return LOCAL_DATE;
            case TIME:
                return LOCAL_TIME;
            case TIMESTAMP:
                return LOCAL_DATE_TIME;
            default:
                return null;
        }
    }
}

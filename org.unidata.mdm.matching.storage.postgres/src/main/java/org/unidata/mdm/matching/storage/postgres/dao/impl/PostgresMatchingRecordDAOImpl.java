/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.storage.postgres.dao.impl;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.unidata.mdm.matching.storage.postgres.converter.MatchingRecordKeyConverter;
import org.unidata.mdm.matching.storage.postgres.dao.PostgresMatchingRecordDAO;
import org.unidata.mdm.matching.storage.postgres.exception.MatchingStorageExceptionIds;
import org.unidata.mdm.matching.storage.postgres.exception.MatchingStorageRuntimeException;
import org.unidata.mdm.matching.storage.postgres.po.MatchingRecordKeyPO;
import org.unidata.mdm.matching.storage.postgres.po.MatchingRecordPO;
import org.unidata.mdm.matching.storage.postgres.type.column.Column;
import org.unidata.mdm.system.dao.impl.BaseDAOImpl;
import org.unidata.mdm.system.type.runtime.MeasurementPoint;

import javax.sql.DataSource;
import java.sql.Array;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * @author Sergey Murskiy on 18.08.2021
 */
@Repository
public class PostgresMatchingRecordDAOImpl extends BaseDAOImpl implements PostgresMatchingRecordDAO {

    /**
     * Logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(PostgresMatchingRecordDAOImpl.class);

    /**
     * SQLs.
     */
    private final String insertSQL;
    private final String deleteSQL;

    /**
     * Constructor.
     */
    @Autowired
    public PostgresMatchingRecordDAOImpl(
            @Qualifier("postgresMatchingStorageDatasource") final DataSource dataSource,
            @Qualifier("matching-record-sql") final Properties sql) {
        super(dataSource);

        this.insertSQL = sql.getProperty("insertSQL");
        this.deleteSQL = sql.getProperty("deleteSQL");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void insert(String tableName, List<Column> columns, List<MatchingRecordPO> records) {
        if (CollectionUtils.isEmpty(records)) {
            return;
        }

        MeasurementPoint.start();
        try {
            String statement = fillInsertRecordTemplate(tableName, columns);

            int[][] updates = getJdbcTemplate()
                    .batchUpdate(statement, records, records.size(), (ps, po) -> {
                        Map<String, Object> values = po.getColumnValues();
                        for (Column column : columns) {
                            processRow(ps, column, values.get(column.getName()));
                        }
                    });

            if (updates.length == 0 || updates[0].length != records.size()) {
                throw new MatchingStorageRuntimeException("Batch insert matching records [{}] failed.",
                        MatchingStorageExceptionIds.EX_CE_STORAGE_INSERT_DATA_BATCH_FAILED,
                        records.size());
            }
        } finally {
            MeasurementPoint.stop();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void delete(String tableName, List<MatchingRecordKeyPO> keys) {

        String statement = fillDeleteRecordTemplate(tableName);

        try (Connection c = getBareConnection()) {

            Array ia = c.createArrayOf("varchar", MatchingRecordKeyConverter.keyPOToId(keys).toArray());
            getJdbcTemplate().update(statement, ia);

        } catch (SQLException e) {
            LOGGER.warn("Cannot create record ids array.", e);
        }
    }

    private void processRow(PreparedStatement ps, Column column, Object value) throws SQLException {
        switch (column.getType()) {
            case STRING: {
                ps.setString(column.getOrder(), (String) value);
                break;
            }
            case LONG: {
                ps.setLong(column.getOrder(), (Long) value);
                break;
            }
            case DOUBLE: {
                ps.setDouble(column.getOrder(), (Double) value);
                break;
            }
            case BOOLEAN: {
                ps.setBoolean(column.getOrder(), (Boolean) value);
                break;
            }
            case LOCAL_DATE: {
                ps.setDate(column.getOrder(), Date.valueOf((LocalDate) value));
                break;
            }
            case LOCAL_TIME: {
                ps.setTime(column.getOrder(), Time.valueOf((LocalTime) value));
                break;
            }
            case LOCAL_DATE_TIME: {
                if (value instanceof java.util.Date) {
                    ps.setTimestamp(column.getOrder(), new Timestamp(((java.util.Date) value).getTime()));
                } else {
                    ps.setTimestamp(column.getOrder(), Timestamp.valueOf((LocalDateTime) value));
                }

                break;
            }
            case BYTES: {
                ps.setBytes(column.getOrder(), (byte[]) value);
                break;
            }
        }
    }

    private String fillInsertRecordTemplate(String tableName, List<Column> columns) {

        String tableColumns = StringUtils.join(columns.stream()
                .map(Column::getName)
                .toArray(), ", ");

        String columnPlaceHolders = StringUtils.join(columns.stream()
                .map(column -> "?")
                .toArray(), ", ");

        return insertSQL
                .replace("#{tableName}", tableName)
                .replace("#{tableColumns}", tableColumns)
                .replace("#{columnPlaceHolders}", columnPlaceHolders);
    }

    private String fillDeleteRecordTemplate(String tableName) {

        return deleteSQL
                .replace("#{tableName}", tableName);
    }
}

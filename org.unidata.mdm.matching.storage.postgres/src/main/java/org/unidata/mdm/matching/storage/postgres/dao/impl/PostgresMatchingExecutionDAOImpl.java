/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.storage.postgres.dao.impl;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.unidata.mdm.matching.storage.postgres.converter.MatchingRecordKeyConverter;
import org.unidata.mdm.matching.storage.postgres.dao.PostgresMatchingExecutionDAO;
import org.unidata.mdm.matching.storage.postgres.dao.rm.ClustersRowMapper;
import org.unidata.mdm.matching.storage.postgres.po.ClusterPO;
import org.unidata.mdm.matching.storage.postgres.po.MatchingRecordKeyPO;
import org.unidata.mdm.matching.storage.postgres.type.column.Column;
import org.unidata.mdm.system.dao.impl.BaseDAOImpl;

import javax.sql.DataSource;
import java.sql.Array;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

/**
 * @author Sergey Murskiy on 18.08.2021
 */
@Repository
public class PostgresMatchingExecutionDAOImpl extends BaseDAOImpl implements PostgresMatchingExecutionDAO {
    /**
     * Logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(PostgresMatchingExecutionDAOImpl.class);

    /**
     * Execution SQL.
     */
    private final String executionForIdsSQL;
    private final String executionForAllSQL;

    /**
     * Constructor.
     */
    @Autowired
    public PostgresMatchingExecutionDAOImpl(
            @Qualifier("postgresMatchingStorageDatasource") final DataSource dataSource,
            @Qualifier("matching-execution-sql") final Properties sql) {
        super(dataSource);

        this.executionForIdsSQL = sql.getProperty("executionForIdsSQL");
        this.executionForAllSQL = sql.getProperty("executionForAllSQL");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ClusterPO> execute(String tableName, List<Column> columns, List<MatchingRecordKeyPO> keys) {

        String statement = fillExecutionForIdsSQL(tableName, columns);

        try (Connection c = getBareConnection()) {

            Array ia = c.createArrayOf("varchar", keys.stream()
                    .map(MatchingRecordKeyConverter::keyPOToId)
                    .toArray());

            return getJdbcTemplate().query(statement, ClustersRowMapper.DEFAULT_ROW_MAPPER, ia);

        } catch (SQLException e) {
            LOGGER.warn("Cannot create record ids array.", e);
        }

        return Collections.emptyList();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ClusterPO> execute(String tableName, List<Column> columns) {
        String statement = fillExecutionForAllSQL(tableName, columns);

        return getJdbcTemplate().query(statement, ClustersRowMapper.DEFAULT_ROW_MAPPER);
    }

    private String fillExecutionForIdsSQL(String tableName, List<Column> columns) {

        String columnsWithPrefix = StringUtils.join(columns.stream()
                .map(c -> StringUtils.join("t1.", c.getName()))
                .toArray(), ", ");
        String columnsAsString = StringUtils.join(columns.stream()
                .map(Column::getName)
                .toArray(), ", ");
        StringBuilder condition = new StringBuilder();
        columns.forEach(column -> condition.append(" AND t1.").append(column.getName()).append(" = t2.").append(column.getName()));

        return executionForIdsSQL
                .replace("#{tableName}", tableName)
                .replace("#{columns}", columnsAsString)
                .replace("#{columnsWithPrefix}", columnsWithPrefix)
                .replace("#{condition}", condition.toString());
    }

    private String fillExecutionForAllSQL(String tableName, List<Column> columns) {

        String columnsWithT1Prefix = StringUtils.join(columns.stream()
                .map(c -> StringUtils.join("t1.", c.getName()))
                .toArray(), ", ");

        String columnsWithT2Prefix = StringUtils.join(columns.stream()
                .map(c -> StringUtils.join("t2.", c.getName()))
                .toArray(), ", ");

        StringBuilder condition = new StringBuilder();
        columns.forEach(column -> condition.append(" AND t1.").append(column.getName()).append(" = t2.").append(column.getName()));

        return executionForAllSQL
                .replace("#{tableName}", tableName)
                .replace("#{columnsWithT1Prefix}", columnsWithT1Prefix)
                .replace("#{columnsWithT2Prefix}", columnsWithT2Prefix)
                .replace("#{condition}", condition.toString());
    }
}

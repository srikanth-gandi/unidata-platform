/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.storage.postgres.configuration;

import org.unidata.mdm.matching.core.type.storage.MatchingStorageDescriptor;
import org.unidata.mdm.matching.storage.postgres.service.impl.algorithm.ExactAlgorithm;
import org.unidata.mdm.matching.storage.postgres.service.impl.refresh.PostgresMatchingModelRefreshListener;
import org.unidata.mdm.system.configuration.ModuleConfiguration;
import org.unidata.mdm.system.util.TextUtils;

import java.util.List;

/**
 * @author Sergey Murskiy on 07.09.2021
 */
public final class PostgresMatchingStorageDescriptors {

    /**
     * Display name.
     */
    private static final String MATCHING_STORAGE_DISPLAY_NAME = "org.unidata.mdm.matching.storage.postgres.display.name";
    /**
     * Description.
     */
    private static final String MATCHING_STORAGE_DESCRIPTION = "org.unidata.mdm.matching.storage.postgres.description";

    /**
     * The matching storage descriptor.
     */
    public static final MatchingStorageDescriptor MATCHING_STORAGE = MatchingStorageDescriptor.of(
            PostgresMatchingStorageConfigurationConstants.MATCHING_STORAGE_NAME,
            () -> TextUtils.getText(MATCHING_STORAGE_DISPLAY_NAME),
            () -> TextUtils.getText(MATCHING_STORAGE_DESCRIPTION),
            List.of(ExactAlgorithm.class.getName()),
            List.of(ModuleConfiguration.getBean(PostgresMatchingModelRefreshListener.class))
    );

    /**
     * Constructor.
     */
    private PostgresMatchingStorageDescriptors() {
        super();
    }
}

/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.storage.postgres.po;

import org.unidata.mdm.core.po.ObjectPO;

import java.util.Date;
import java.util.Objects;

/**
 * @author Sergey Murskiy on 17.08.2021
 */
public class MatchingRecordKeyPO implements ObjectPO {
    /**
     * Namespace.
     */
    private String namespace;
    /**
     * Type name.
     */
    private String typeName;
    /**
     * Subject id.
     */
    private String subjectId;
    /**
     * Valid from.
     */
    private Date validFrom;
    /**
     * Valid to.
     */
    private Date validTo;

    /**
     * Gets namespace.
     *
     * @return the namespace
     */
    public String getNamespace() {
        return namespace;
    }

    /**
     * Sets namespace.
     *
     * @param namespace the namespace to set
     */
    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    /**
     * Gets type name.
     *
     * @return the type name
     */
    public String getTypeName() {
        return typeName;
    }

    /**
     * Sets type name
     *
     * @param typeName the type name to set
     */
    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    /**
     * Gets subject id.
     *
     * @return the subject id
     */
    public String getSubjectId() {
        return subjectId;
    }

    /**
     * Sets subject id.
     *
     * @param subjectId the subject id
     */
    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }

    /**
     * Gets valid from.
     *
     * @return the valid from
     */
    public Date getValidFrom() {
        return validFrom;
    }

    /**
     * Sets valid from.
     *
     * @param validFrom the valid from to set
     */
    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    /**
     * Gets valid to.
     *
     * @return the valid to
     */
    public Date getValidTo() {
        return validTo;
    }

    /**
     * Sets valid to.
     *
     * @param validTo the valid to to set
     */
    public void setValidTo(Date validTo) {
        this.validTo = validTo;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return Objects.hash(
                getNamespace(),
                getTypeName(),
                getSubjectId(),
                getValidFrom(),
                getValidTo());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        MatchingRecordKeyPO that = (MatchingRecordKeyPO) o;
        return Objects.equals(namespace, that.namespace) &&
                Objects.equals(typeName, that.typeName) &&
                Objects.equals(subjectId, that.subjectId) &&
                Objects.equals(validFrom, that.validFrom) &&
                Objects.equals(validTo, that.validTo);
    }
}

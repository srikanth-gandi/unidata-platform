/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.storage.postgres.converter;

import org.apache.commons.collections4.CollectionUtils;
import org.unidata.mdm.matching.core.type.data.MatchingRecord;
import org.unidata.mdm.matching.core.type.data.MatchingRecordKey;
import org.unidata.mdm.matching.core.type.data.field.MatchingRecordField;
import org.unidata.mdm.matching.storage.postgres.po.MatchingRecordPO;
import org.unidata.mdm.matching.storage.postgres.type.column.Column;
import org.unidata.mdm.matching.storage.postgres.type.column.SystemColumns;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author Sergey Murskiy on 18.08.2021
 */
public class MatchingRecordConverter {

    /**
     * Constructor.
     */
    private MatchingRecordConverter() {
        super();
    }

    /**
     * Convert matching record to PO.
     *
     * @param columns        matching table columns
     * @param matchingRecord matching record
     * @return po
     */
    public static MatchingRecordPO recordToPO(List<Column> columns, MatchingRecord matchingRecord) {

        if (CollectionUtils.isEmpty(columns) || Objects.isNull(matchingRecord)) {
            return null;
        }

        Map<String, Object> values = new HashMap<>(columns.size());
        columns.forEach(column -> values.put(column.getName(), processValue(column,
                matchingRecord.getField(column.getAlias()))));

        appendSystemFields(matchingRecord, values);

        MatchingRecordPO po = new MatchingRecordPO();
        po.setColumnValues(values);

        return po;
    }

    /**
     * Convert matching records to POs.
     *
     * @param columns         matching table columns
     * @param matchingRecords matching records
     * @return pos
     */
    public static List<MatchingRecordPO> recordToPO(List<Column> columns, List<MatchingRecord> matchingRecords) {
        if (CollectionUtils.isEmpty(columns) || CollectionUtils.isEmpty(matchingRecords)) {
            return Collections.emptyList();
        }

        return matchingRecords.stream()
                .map(matchingRecord -> recordToPO(columns, matchingRecord))
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    private static Object processValue(Column column, MatchingRecordField<?> field) {
        if (Objects.isNull(field)) {
            return null;
        }

        if (column.hasPreprocessing()) {
            return column.getPreprocessingRule().process(field);
        } else {
            return field.getValue();
        }
    }

    private static void appendSystemFields(MatchingRecord matchingRecord, Map<String, Object> values) {
        MatchingRecordKey key = matchingRecord.getKey();

        values.put(SystemColumns.ID.getName(), MatchingRecordKeyConverter.keyToId(key));
        values.put(SystemColumns.NAMESPACE.getName(), key.getNamespace());
        values.put(SystemColumns.TYPE_NAME.getName(), key.getTypeName());
        values.put(SystemColumns.SUBJECT_ID.getName(), key.getSubjectId());
        values.put(SystemColumns.VALID_FROM.getName(), key.getValidFrom());
        values.put(SystemColumns.VALID_TO.getName(), key.getValidTo());
    }
}

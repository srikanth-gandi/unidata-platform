/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.storage.postgres.migration;

import nl.myndocs.database.migrator.MigrationScript;
import org.unidata.mdm.system.migration.Migrations;
import org.unidata.mdm.system.util.ResourceUtils;

/**
 * @author Sergey Murskiy on 23.08.2021
 */
public class PostgresMatchingStorageMigrations {

    /**
     * Constructor.
     */
    private PostgresMatchingStorageMigrations() {
        super();
    }

    /**
     * Migrations.
     */
    private static final MigrationScript[] INSTALL = {
            Migrations.of(
                    "UN-16971-postgres-matching-storage-install-schema",
                    "sergey.murskiy",
                    ResourceUtils.asString("classpath:/migration/UN-16971-postgres-matching-storage-install-schema.sql")
            ),
    };

    /**
     * Gets install migration.
     *
     * @return migrations
     */
    public static MigrationScript[] install() {
        return INSTALL;
    }
}

/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.storage.postgres.dao.rm;

import org.postgresql.util.PGobject;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.unidata.mdm.matching.storage.postgres.po.ClusterPO;
import org.unidata.mdm.matching.storage.postgres.po.MatchingRecordKeyPO;

import java.sql.Array;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * @author Sergey Murskiy on 17.08.2021
 */
public class ClustersRowMapper implements RowMapper<ClusterPO> {

    /**
     * Default reusable row mapper.
     */
    public static final ClustersRowMapper DEFAULT_ROW_MAPPER = new ClustersRowMapper();

    /**
     * Extracts first result or returns null.
     */
    public static final ResultSetExtractor<ClusterPO> DEFAULT_FIRST_RESULT_EXTRACTOR = rs -> rs.next()
            ? DEFAULT_ROW_MAPPER.mapRow(rs, rs.getRow())
            : null;

    /**
     * Constructor.
     */
    private ClustersRowMapper() {
        super();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ClusterPO mapRow(ResultSet rs, int rowNum) throws SQLException {
        ClusterPO po = new ClusterPO();

        po.setRecordKeys(getKeys(rs.getArray(1)));

        return po;
    }

    /**
     * Unmarshall keys array.
     *
     * @param keys the keys array
     * @return list of keys
     */
    private Set<MatchingRecordKeyPO> getKeys(Array keys) throws SQLException {

        if (Objects.isNull(keys)) {
            return Collections.emptySet();
        }

        try {

            Object[] lines = (Object[]) keys.getArray();
            Set<MatchingRecordKeyPO> result = new HashSet<>();
            for (Object line : lines) {

                PGobject l = (PGobject) line;
                MatchingRecordKeyPO po = MatchingRecordIdRowTokenizer.DEFAULT_MATCHING_RECORD_ID_TOKENIZER.process(l);
                if (Objects.nonNull(po)) {
                    result.add(po);
                }
            }

            return result;
        } finally {
            keys.free();
        }
    }
}

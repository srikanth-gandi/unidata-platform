/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.storage.postgres.configuration;

import org.springframework.beans.factory.config.PropertiesFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.unidata.mdm.matching.storage.postgres.module.PostgresMatchingStorageModule;
import org.unidata.mdm.system.configuration.ModuleConfiguration;
import org.unidata.mdm.system.util.DataSourceUtils;

import javax.sql.DataSource;
import java.util.Properties;

/**
 * @author Sergey Murskiy on 18.08.2021
 */
@Configuration
public class PostgresMatchingStorageConfiguration extends ModuleConfiguration {
    /**
     * Configuration id.
     */
    private static final ConfigurationId ID = () -> "POSTGRES_MATCHING_STORAGE_CONFIGURATION";

    /**
     * {@inheritDoc}
     */
    @Override
    public ConfigurationId getId() {
        return ID;
    }

    @Bean("postgresMatchingStorageDatasource")
    public DataSource postgresMatchingStorageDatasource() {
        Properties properties = getPropertiesByPrefix(
                PostgresMatchingStorageModule.MODULE_ID + ".datasource.",
                true);
        return DataSourceUtils.newPoolingXADataSource(properties);
    }

    @Bean("matching-execution-sql")
    public PropertiesFactoryBean matchingExecutionSql() {
        PropertiesFactoryBean bean = new PropertiesFactoryBean();
        bean.setLocation(new ClassPathResource("db/matching-execution-sql.xml"));
        return bean;
    }

    @Bean("matching-table-sql")
    public PropertiesFactoryBean matchingTableSql() {
        PropertiesFactoryBean bean = new PropertiesFactoryBean();
        bean.setLocation(new ClassPathResource("db/matching-table-sql.xml"));
        return bean;
    }

    @Bean("matching-record-sql")
    public PropertiesFactoryBean matchingRecordSql() {
        PropertiesFactoryBean bean = new PropertiesFactoryBean();
        bean.setLocation(new ClassPathResource("db/matching-record-sql.xml"));
        return bean;
    }
}

/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.storage.postgres.dao;

import org.unidata.mdm.matching.storage.postgres.po.ClusterPO;
import org.unidata.mdm.matching.storage.postgres.po.MatchingRecordKeyPO;
import org.unidata.mdm.matching.storage.postgres.type.column.Column;

import java.util.List;

/**
 * @author Sergey Murskiy on 18.08.2021
 */
public interface PostgresMatchingExecutionDAO {

    /**
     * Execute matching for input matching record keys.
     *
     * @param tableName the matching table name
     * @param columns   grouping columns
     * @param keys      keys
     * @return clusters
     */
    List<ClusterPO> execute(String tableName, List<Column> columns, List<MatchingRecordKeyPO> keys);

    /**
     * Execute matching for all matching table data.
     *
     * @param tableName the matching table name
     * @param columns   grouping columns
     * @return clusters
     */
    List<ClusterPO> execute(String tableName, List<Column> columns);
}

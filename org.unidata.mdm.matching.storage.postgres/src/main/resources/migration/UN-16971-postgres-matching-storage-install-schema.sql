create type matching_record_key as
(
    namespace  varchar(256),
    type_name  varchar(256),
    subject_id varchar(256),
    valid_from timestamptz,
    valid_to   timestamptz
);

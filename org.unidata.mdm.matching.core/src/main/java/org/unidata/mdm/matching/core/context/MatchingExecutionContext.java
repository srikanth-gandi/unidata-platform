/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.context;

import org.unidata.mdm.matching.core.type.execution.MatchingRealTimeInput;
import org.unidata.mdm.matching.core.type.model.instance.MatchingRuleMappingElement;
import org.unidata.mdm.matching.core.type.model.instance.MatchingRuleSetElement;

import java.util.Date;

/**
 * @author Sergey Murskiy on 09.08.2021
 */
public class MatchingExecutionContext extends AbstractMatchingContext implements MatchingDeploymentHolderContext {
    /**
     * Matching real-time input.
     */
    private final MatchingRealTimeInput realTimeInput;
    /**
     * Executed matching rule set.
     */
    private final MatchingRuleSetElement set;
    /**
     * Executed matching rule.
     */
    private final MatchingRuleMappingElement ruleMapping;
    /**
     * Matching date.
     */
    private final Date matchingDate;
    /**
     * Is skip result calculation.
     */
    private final boolean skipResultCalculation;
    /**
     * Is skip computing cluster updates.
     */
    private final boolean skipClusterUpdates;
    /**
     * Is clean cluster storage.
     */
    private final boolean cleanClusterStorage;

    /**
     * Constructor.
     *
     * @param b the builder
     */
    private MatchingExecutionContext(MatchingExecutionContextBuilder b) {
        super(b);

        this.realTimeInput = b.realTimeInput;
        this.set = b.set;
        this.ruleMapping = b.ruleMapping;
        this.matchingDate = b.matchingDate;
        this.skipResultCalculation = b.skipResultCalculation;
        this.skipClusterUpdates = b.skipClusterUpdates;
        this.cleanClusterStorage = b.cleanClusterStorage;
    }

    /**
     * Gets matching real-time input.
     *
     * @return matching real-time input
     */
    public MatchingRealTimeInput getRealTimeInput() {
        return realTimeInput;
    }

    /**
     * Gets executing matching set.
     *
     * @return matching set
     */
    public MatchingRuleSetElement getSet() {
        return set;
    }

    /**
     * Gets execution matching rule mapping.
     *
     * @return rule mapping
     */
    public MatchingRuleMappingElement getRuleMapping() {
        return ruleMapping;
    }

    /**
     * Gets matching date.
     *
     * @return the matching date
     */
    public Date getMatchingDate() {
        return matchingDate;
    }

    /**
     * Gets 'skipResultCalculation' flag.
     *
     * @return is skip result calculation
     */
    public boolean isSkipResultCalculation() {
        return skipResultCalculation;
    }

    /**
     * Gets 'skipClusterUpdates' flag.
     *
     * @return is skip cluster updates
     */
    public boolean isSkipClusterUpdates() {
        return skipClusterUpdates;
    }

    /**
     * Gets 'cleanClusterStorage' flag.
     *
     * @return is clean cluster storage
     */
    public boolean isCleanClusterStorage() {
        return cleanClusterStorage;
    }

    /**
     * Gets builder.
     *
     * @return the builder
     */
    public static MatchingExecutionContextBuilder builder() {
        return new MatchingExecutionContextBuilder();
    }

    /**
     * Builder class.
     */
    public static class MatchingExecutionContextBuilder extends AbstractMatchingContextBuilder<MatchingExecutionContextBuilder> {
        /**
         * Matching real-time input.
         */
        private MatchingRealTimeInput realTimeInput;
        /**
         * Executed matching rule set.
         */
        private MatchingRuleSetElement set;
        /**
         * Executed matching rule.
         */
        private MatchingRuleMappingElement ruleMapping;
        /**
         * Matching date.
         */
        private Date matchingDate;
        /**
         * Is skip result calculation.
         */
        private boolean skipResultCalculation;
        /**
         * Is skip computing cluster updates.
         */
        private boolean skipClusterUpdates;
        /**
         * Is clean cluster storage.
         */
        private boolean cleanClusterStorage;

        /**
         * Sets matching rule set.
         *
         * @param set matching rule set
         * @return self
         */
        public MatchingExecutionContextBuilder set(MatchingRuleSetElement set) {
            this.set = set;
            return self();
        }

        /**
         * Sets matching rule.
         *
         * @param rule matching rule to set
         * @return self
         */
        public MatchingExecutionContextBuilder rule(MatchingRuleMappingElement rule) {
            this.ruleMapping = rule;
            return self();
        }

        /**
         * Sets real time matching input.
         *
         * @param input real-time matching input
         * @return self
         */
        public MatchingExecutionContextBuilder realTimeInput(MatchingRealTimeInput input) {
            this.realTimeInput = input;
            return self();
        }

        /**
         * Sets matching date.
         *
         * @param matchingDate the matching date
         * @return self
         */
        public MatchingExecutionContextBuilder matchingDate(Date matchingDate) {
            this.matchingDate = matchingDate;
            return self();
        }

        /**
         * Sets 'skipResultCalculation' flag
         *
         * @param skipResultCalculation the 'skipResultCalculation' to set
         * @return self
         */
        public MatchingExecutionContextBuilder skipResultCalculation(boolean skipResultCalculation) {
            this.skipResultCalculation = skipResultCalculation;

            return self();
        }

        /**
         * Sets 'skipClusterUpdates' flag.
         *
         * @param skipClusterUpdates the 'skipClusterUpdates' to set
         * @return self
         */
        public MatchingExecutionContextBuilder skipClusterUpdates(boolean skipClusterUpdates) {
            this.skipClusterUpdates = skipClusterUpdates;
            return self();
        }

        /**
         * Sets 'cleanClusterStorage' flag.
         *
         * @param cleanClusterStorage the 'cleanClusterStorage' flag
         * @return self
         */
        public MatchingExecutionContextBuilder cleanClusterStorage(boolean cleanClusterStorage) {
            this.cleanClusterStorage = cleanClusterStorage;
            return self();
        }

        /**
         * {@inheritDoc}
         */
        public MatchingExecutionContext build() {
            return new MatchingExecutionContext(this);
        }
    }
}

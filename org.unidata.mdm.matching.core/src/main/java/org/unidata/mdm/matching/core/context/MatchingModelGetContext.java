/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.context;

import org.unidata.mdm.core.context.AbstractModelGetContext;
import org.unidata.mdm.core.service.segments.ModelGetStartExecutor;
import org.unidata.mdm.matching.core.configuration.MatchingModelIds;
import org.unidata.mdm.system.context.DraftAwareContext;

import java.util.List;

/**
 * @author Sergey Murskiy on 14.06.2021
 */
public class MatchingModelGetContext extends AbstractModelGetContext implements DraftAwareContext {
    /**
     * SVUID.
     */
    private static final long serialVersionUID = 5269536086664782582L;

    /**
     * A possibly set draft id.
     */
    private final Long draftId;
    /**
     * A possibly set parent draft id.
     */
    private final Long parentDraftId;
    /**
     * Requested algorithm ids.
     */
    private final List<String> algorithmIds;
    /**
     * Requested matching table ids.
     */
    private final List<String> matchingTableIds;
    /**
     * Requested rule ids.
     */
    private final List<String> matchingRuleIds;
    /**
     * Requested rule set ids.
     */
    private final List<String> matchingRuleSetIds;
    /**
     * Assignments assignment keys.
     */
    private final List<String> assignmentByKeys;
    /**
     * Fetch all algorithms source.
     */
    private final boolean allAlgorithms;
    /**
     * Fetch all matching tables source.
     */
    private final boolean allMatchingTables;
    /**
     * All rules requested.
     */
    private final boolean allMatchingRules;
    /**
     * All rule sets requested.
     */
    private final boolean allMatchingRuleSets;
    /**
     * All assignments.
     */
    private final boolean allAssignments;

    /**
     * Constructor.
     *
     * @param b the builder.
     */
    private MatchingModelGetContext(MatchingModelGetContextBuilder b) {
        super(b);
        this.algorithmIds = b.algorithmIds;
        this.allAlgorithms = b.allAlgorithms;
        this.matchingTableIds = b.matchingTableIds;
        this.allMatchingTables = b.allMatchingTables;
        this.matchingRuleIds = b.matchingRuleIds;
        this.allMatchingRules = b.allMatchingRules;
        this.matchingRuleSetIds = b.matchingRuleSetIds;
        this.allMatchingRuleSets = b.allMatchingRuleSets;
        this.assignmentByKeys = b.assignmentByKeys;
        this.allAssignments = b.allAssignments;
        this.draftId = b.draftId;
        this.parentDraftId = b.parentDraftId;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public Long getDraftId() {
        return draftId;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long getParentDraftId() {
        return parentDraftId;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getTypeId() {
        return MatchingModelIds.MATCHING;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getInstanceId() {
        return MatchingModelIds.DEFAULT_MODEL_INSTANCE_ID;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getStartTypeId() {
        return ModelGetStartExecutor.SEGMENT_ID;
    }

    /**
     * @return algorithm ids
     */
    public List<String> getAlgorithmIds() {
        return algorithmIds;
    }

    /**
     * @return matching table ids
     */
    public List<String> getMatchingTableIds() {
        return matchingTableIds;
    }

    /**
     * @return matching rule ids
     */
    public List<String> getMatchingRuleIds() {
        return matchingRuleIds;
    }

    /**
     * @return matching rule set ids
     */
    public List<String> getMatchingRuleSetIds() {
        return matchingRuleSetIds;
    }

    /**
     * @return assignment keys
     */
    public List<String> getAssignmentByKeys() {
        return assignmentByKeys;
    }

    /**
     * @return is all algorithms
     */
    public boolean isAllAlgorithms() {
        return allAlgorithms;
    }

    /**
     * @return is all matching tables
     */
    public boolean isAllMatchingTables() {
        return allMatchingTables;
    }

    /**
     * @return is all matching rules
     */
    public boolean isAllMatchingRules() {
        return allMatchingRules;
    }

    /**
     * @return is all matching rule sets
     */
    public boolean isAllMatchingRuleSets() {
        return allMatchingRuleSets;
    }

    /**
     * @return is all assignments
     */
    public boolean isAllAssignments() {
        return allAssignments;
    }

    /**
     * Gets a builder instance.
     * @return builder instance
     */
    public static MatchingModelGetContextBuilder builder() {
        return new MatchingModelGetContextBuilder();
    }
    /**
     * The builder for this context.
     * @author Mikhail Mikhailov on Nov 28, 2019
     */
    public static class MatchingModelGetContextBuilder extends AbstractModelGetContextBuilder<MatchingModelGetContextBuilder> {
        /**
         * Requested algorithm ids.
         */
        private List<String> algorithmIds;
        /**
         * All algorithms requested.
         */
        private boolean allAlgorithms;
        /**
         * Requested matching table ids.
         */
        private List<String> matchingTableIds;
        /**
         * All matching tables requested.
         */
        private boolean allMatchingTables;
        /**
         * Requested rule ids.
         */
        private List<String> matchingRuleIds;
        /**
         * All rules requested.
         */
        private boolean allMatchingRules;
        /**
         * Requested rule set ids.
         */
        private List<String> matchingRuleSetIds;
        /**
         * All rule sets requested.
         */
        private boolean allMatchingRuleSets;
        /**
         * Assignments assignment keys.
         */
        private List<String> assignmentByKeys;
        /**
         * All assignments.
         */
        private boolean allAssignments;
        /**
         * The draft id.
         */
        private Long draftId;
        /**
         * The parent draft id.
         */
        private Long parentDraftId;

        /**
         * Constructor.
         */

        protected MatchingModelGetContextBuilder() {
            super();
        }
        /**
         * Sets draft id
         * @param draftId the draft id
         * @return self
         */
        public MatchingModelGetContextBuilder draftId(Long draftId) {
            this.draftId = draftId;
            return self();
        }

        /**
         * Sets parent draft id
         * @param parentDraftId the parent draft id
         * @return self
         */
        public MatchingModelGetContextBuilder parentDraftId(Long parentDraftId) {
            this.parentDraftId = parentDraftId;
            return self();
        }

        /**
         * @param algorithmIds the algorithm ids to set
         */
        public MatchingModelGetContextBuilder algorithmIds(List<String> algorithmIds) {
            this.algorithmIds = algorithmIds;
            return self();
        }

        /**
         * @param allAlgorithms the all algorithms to set
         */
        public MatchingModelGetContextBuilder allAlgorithms(boolean allAlgorithms) {
            this.allAlgorithms = allAlgorithms;
            return self();
        }

        /**
         * @param matchingTableIds the matching table ids to set
         */
        public MatchingModelGetContextBuilder matchingTableIds(List<String> matchingTableIds) {
            this.matchingTableIds = matchingTableIds;
            return self();
        }

        /**
         * @param allMatchingTables the all matching tables to set
         */
        public MatchingModelGetContextBuilder allMatchingTables(boolean allMatchingTables) {
            this.allMatchingTables = allMatchingTables;
            return self();
        }

        /**
         * @param matchingRuleIds the matching rule ids to set
         */
        public MatchingModelGetContextBuilder matchingRuleIds(List<String> matchingRuleIds) {
            this.matchingRuleIds = matchingRuleIds;
            return self();
        }

        /**
         * @param allMatchingRules the all matching rules to set
         */
        public MatchingModelGetContextBuilder allMatchingRules(boolean allMatchingRules) {
            this.allMatchingRules = allMatchingRules;
            return self();
        }

        /**
         * @param matchingRuleSetIds the matching rule set ids to set
         */
        public MatchingModelGetContextBuilder matchingRuleSetIds(List<String> matchingRuleSetIds) {
            this.matchingRuleSetIds = matchingRuleSetIds;
            return self();
        }

        /**
         * @param allMatchingRuleSets the all matching rule sets to set
         */
        public MatchingModelGetContextBuilder allMatchingRuleSets(boolean allMatchingRuleSets) {
            this.allMatchingRuleSets = allMatchingRuleSets;
            return self();
        }

        /**
         * @param assignmentKeys the assignmentKeys to set
         */
        public MatchingModelGetContextBuilder assignmentByKeys(List<String> assignmentKeys) {
            this.assignmentByKeys = assignmentKeys;
            return self();
        }

        /**
         * @param allAssignments the allAssignments to set
         */
        public MatchingModelGetContextBuilder allAssignments(boolean allAssignments) {
            this.allAssignments = allAssignments;
            return self();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public MatchingModelGetContext build() {
            return new MatchingModelGetContext(this);
        }
    }
}

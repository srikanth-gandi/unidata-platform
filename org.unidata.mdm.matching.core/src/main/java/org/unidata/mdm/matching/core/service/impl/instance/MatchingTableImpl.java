/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.service.impl.instance;

import org.unidata.mdm.core.type.model.instance.AbstractNamedDisplayableCustomPropertiesImpl;
import org.unidata.mdm.matching.core.type.data.MatchingDataType;
import org.unidata.mdm.matching.core.type.model.instance.MatchingColumnElement;
import org.unidata.mdm.matching.core.type.model.instance.MatchingTableElement;
import org.unidata.mdm.matching.core.type.model.source.table.MatchingColumnSource;
import org.unidata.mdm.matching.core.type.model.source.table.MatchingTableSource;

import java.time.OffsetDateTime;
import java.util.Collection;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author Sergey Murskiy on 20.06.2021
 */
public class MatchingTableImpl extends AbstractNamedDisplayableCustomPropertiesImpl
        implements MatchingTableElement {

    /**
     * Source.
     */
    private final MatchingTableSource source;
    /**
     * Matching table columns.
     */
    private final Map<String, MatchingColumnElement> columns;

    /**
     * Constructor.
     *
     * @param source matching table source
     */
    public MatchingTableImpl(MatchingTableSource source) {
        super(source.getName(), source.getDisplayName(), source.getDescription(), source.getCustomProperties());

        this.source = source;

        columns = source.getColumns().stream()
                .map(MatchingColumnImpl::new)
                .collect(Collectors.toMap(MatchingColumnElement::getName, Function.identity()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<MatchingColumnElement> getColumns() {
        return columns.values();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MatchingColumnElement getColumn(String id) {
        return columns.get(id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OffsetDateTime getCreateDate() {
        return source.getCreateDate();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCreatedBy() {
        return source.getCreatedBy();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OffsetDateTime getUpdateDate() {
        return source.getUpdateDate();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getUpdatedBy() {
        return source.getUpdatedBy();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getId() {
        return getName();
    }

    /**
     * Gets matching table source.
     *
     * @return source
     */
    public MatchingTableSource getSource() {
        return source;
    }

    /**
     * Matching column implementation.
     */
    private static class MatchingColumnImpl implements MatchingColumnElement {
        /**
         * Source.
         */
        private final MatchingColumnSource source;

        /**
         * Constructor.
         *
         * @param source matching column source
         */
        public MatchingColumnImpl(MatchingColumnSource source) {
            this.source = source;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String getName() {
            return source.getName();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String getDisplayName() {
            return source.getDisplayName();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String getId() {
            return getName();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public MatchingDataType getType() {
            return source.getType();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }

            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            MatchingColumnImpl that = (MatchingColumnImpl) o;

            return Objects.equals(getName(), that.getName())
                    && Objects.equals(getDisplayName(), that.getDisplayName())
                    && Objects.equals(getType(), that.getType());
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public int hashCode() {
            return Objects.hash(getName(), getDisplayName(), getType());
        }
    }
}

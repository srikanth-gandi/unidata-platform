/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.service.impl.instance;

import org.unidata.mdm.matching.core.type.algorithm.AlgorithmParameterType;
import org.unidata.mdm.matching.core.type.model.instance.AlgorithmParameterElement;
import org.unidata.mdm.matching.core.type.model.source.algorithm.AlgorithmParameterSource;

import java.util.Objects;

/**
 * @author Sergey Murskiy on 22.06.2021
 */
public class AlgorithmParameterImpl implements AlgorithmParameterElement {
    /**
     * Source.
     */
    private final AlgorithmParameterSource source;

    /**
     * Constructor.
     *
     * @param source the source
     */
    public AlgorithmParameterImpl(AlgorithmParameterSource source) {
        this.source = source;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getName() {
        return source.getName();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getId() {
        return getName();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AlgorithmParameterType getType() {
        return source.getType();
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    @Override
    public <V> V getValue() {
        return Objects.isNull(source.getValue())
                ? null
                : (V) source.getValue().getValue();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AlgorithmParameterImpl that = (AlgorithmParameterImpl) o;

        return Objects.equals(getName(), that.getName())
                && Objects.equals(getValue(), that.getValue());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return Objects.hash(getName(), getValue());
    }
}

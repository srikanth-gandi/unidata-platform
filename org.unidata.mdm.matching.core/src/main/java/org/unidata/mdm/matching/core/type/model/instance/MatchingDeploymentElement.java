package org.unidata.mdm.matching.core.type.model.instance;

import java.util.List;
import java.util.Map;

/**
 * @author Sergey Murskiy on 09.08.2021
 */
public interface MatchingDeploymentElement {
    /**
     * Gets matching storage id.
     *
     * @return the matching storage id
     */
    String getMatchingStorageId();

    /**
     * Gets matching table name.
     *
     * @return the matching table name
     */
    String getMatchingTableName();

    /**
     * Gets matching table.
     *
     * @return the matching table
     */
    MatchingTableElement getMatchingTable();

    /**
     * Gets matching table columns assignments.
     *
     * @return matching table columns assignment.
     */
    Map<MatchingColumnElement, List<AlgorithmSettingsElement>> getColumnAssignments();

    /**
     * Gets matching table column assignments.
     *
     * @param column matching column
     * @return matching table column assignment.
     */
    List<AlgorithmSettingsElement> getColumnAssignment(MatchingColumnElement column);
}

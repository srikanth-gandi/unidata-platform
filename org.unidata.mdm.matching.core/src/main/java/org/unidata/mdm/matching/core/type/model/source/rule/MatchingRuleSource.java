/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.type.model.source.rule;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import org.apache.commons.collections4.CollectionUtils;
import org.unidata.mdm.core.type.model.source.AbstractCustomPropertiesHolder;
import org.unidata.mdm.matching.core.type.model.source.algorithm.AlgorithmSettingsSource;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

/**
 * @author Sergey Murskiy on 12.06.2021
 */
public class MatchingRuleSource extends AbstractCustomPropertiesHolder<MatchingRuleSource> {
    /**
     * SVUID.
     */
    private static final long serialVersionUID = -2166681868436919519L;

    @JacksonXmlProperty(isAttribute = true, localName = "name")
    private String name;

    @JacksonXmlProperty(isAttribute = true, localName = "displayName")
    private String displayName;

    @JacksonXmlProperty(isAttribute = true, localName = "description")
    private String description;

    @JacksonXmlProperty(isAttribute = true, localName = "matchingStorageId")
    private String matchingStorageId;

    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "algorithm")
    private List<AlgorithmSettingsSource> algorithms;

    @JacksonXmlProperty(isAttribute = true)
    private OffsetDateTime createDate;

    @JacksonXmlProperty(isAttribute = true)
    private String createdBy;

    @JacksonXmlProperty(isAttribute = true)
    private OffsetDateTime updateDate;

    @JacksonXmlProperty(isAttribute = true)
    private String updatedBy;

    /**
     * Gets the name.
     *
     * @return the name.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name
     *
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets the display name.
     *
     * @return the display name
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * Sets the display name.
     *
     * @param displayName the display name to set
     */
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the description.
     *
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets matching storage id.
     *
     * @return the matching storage id
     */
    public String getMatchingStorageId() {
        return matchingStorageId;
    }

    /**
     * Sets matching storage id.
     *
     * @param matchingStorageId the matching storage id to set
     */
    public void setMatchingStorageId(String matchingStorageId) {
        this.matchingStorageId = matchingStorageId;
    }

    /**
     * Gets algorithms.
     *
     * @return algorithms
     */
    public List<AlgorithmSettingsSource> getAlgorithms() {
        if (Objects.isNull(algorithms)) {
            algorithms = new ArrayList<>();
        }

        return this.algorithms;
    }

    /**
     * Sets algorithms.
     *
     * @param algorithms algorithms to set
     */
    public void setAlgorithms(List<AlgorithmSettingsSource> algorithms) {
        this.algorithms = algorithms;
    }

    /**
     * Gets the create date.
     *
     * @return create date
     */
    public OffsetDateTime getCreateDate() {
        return createDate;
    }

    /**
     * Sets the create date property.
     *
     * @param createDate the create date to set
     */
    public void setCreateDate(OffsetDateTime createDate) {
        this.createDate = createDate;
    }

    /**
     * Gets the created by.
     *
     * @return the created by
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * Sets the created by property.
     *
     * @param createdBy the created by to set
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    /**
     * Gets the update date.
     *
     * @return the update date
     */
    public OffsetDateTime getUpdateDate() {
        return updateDate;
    }

    /**
     * Sets the update date property.
     *
     * @param updateDate the update date to set
     */
    public void setUpdateDate(OffsetDateTime updateDate) {
        this.updateDate = updateDate;
    }

    /**
     * Gets the updated by.
     *
     * @return the updated by
     */
    public String getUpdatedBy() {
        return updatedBy;
    }

    /**
     * Sets the updated by property.
     *
     * @param updatedBy the updated by to set
     */
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    /**
     * Rule with name.
     *
     * @param name the name
     * @return self
     */
    public MatchingRuleSource withName(String name) {
        setName(name);
        return this;
    }

    /**
     * Rule with display name.
     *
     * @param displayName the display name
     * @return self
     */
    public MatchingRuleSource withDisplayName(String displayName) {
        setDisplayName(displayName);
        return this;
    }

    /**
     * Rule with description.
     *
     * @param description the description
     * @return self
     */
    public MatchingRuleSource withDescription(String description) {
        setDescription(description);
        return this;
    }

    /**
     * Rule with matching storage id.
     *
     * @param matchingStorageId the matching storage id
     * @return self
     */
    public MatchingRuleSource withMatchingStorageId(String matchingStorageId) {
        setMatchingStorageId(matchingStorageId);
        return this;
    }

    /**
     * Rule with algorithms.
     *
     * @param values algorithms
     * @return self
     */
    public MatchingRuleSource withAlgorithms(Collection<AlgorithmSettingsSource> values) {
        if (CollectionUtils.isNotEmpty(values)) {
            getAlgorithms().addAll(values);
        }

        return this;
    }

    /**
     * Rule with update date.
     *
     * @param value the update date
     * @return self
     */
    public MatchingRuleSource withUpdateDate(OffsetDateTime value) {
        setUpdateDate(value);
        return this;
    }

    /**
     * Rule with create date.
     *
     * @param value the create date
     * @return self
     */
    public MatchingRuleSource withCreateDate(OffsetDateTime value) {
        setCreateDate(value);
        return this;
    }

    /**
     * Rule with updated by.
     *
     * @param value the update by
     * @return self
     */
    public MatchingRuleSource withUpdatedBy(String value) {
        setUpdatedBy(value);
        return this;
    }

    /**
     * Rule with created by.
     *
     * @param value the created by
     * @return self
     */
    public MatchingRuleSource withCreatedBy(String value) {
        setCreatedBy(value);
        return this;
    }
}

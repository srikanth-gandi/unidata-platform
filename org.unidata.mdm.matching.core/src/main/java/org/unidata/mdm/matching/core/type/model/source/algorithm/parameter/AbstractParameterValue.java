/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.type.model.source.algorithm.parameter;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.unidata.mdm.matching.core.type.algorithm.AlgorithmParameterType;

import java.io.Serializable;

/**
 * @author Sergey Murskiy on 17.06.2021
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.EXISTING_PROPERTY, property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = BooleanParameterValue.class, name = "BOOLEAN"),
        @JsonSubTypes.Type(value = DateParameterValue.class, name = "DATE"),
        @JsonSubTypes.Type(value = IntegerParameterValue.class, name = "INTEGER"),
        @JsonSubTypes.Type(value = NumberParameterValue.class, name = "NUMBER"),
        @JsonSubTypes.Type(value = StringParameterValue.class, name = "STRING"),
        @JsonSubTypes.Type(value = EnumParameterValue.class, name = "ENUM")
})
public abstract class AbstractParameterValue<X extends AbstractParameterValue<X, V>, V extends Serializable>
        implements Serializable, AlgorithmParameterValue<V> {
    /**
     * SVUID.
     */
    private static final long serialVersionUID = -4553665326737282236L;

    /**
     * The value
     */
    protected V value;

    /**
     * Constructor.
     */
    protected AbstractParameterValue() {
        super();
    }

    /**
     * Gets the value of the type property.
     *
     * @return possible object is {@link AlgorithmParameterType }
     */
    @Override
    @JsonProperty("type")
    public abstract AlgorithmParameterType getType();

    /**
     * {@inheritDoc}
     */
    @Override
    public V getValue() {
        return value;
    }

    /**
     * Sets the value.
     *
     * @param value the value to set
     */
    public void setValue(V value) {
        this.value = value;
    }

    /**
     * Algorithm parameter with value.
     *
     * @param value the value to set
     * @return self
     */
    public X withValue(V value) {
        setValue(value);
        return self();
    }

    /**
     * Gets self.
     *
     * @return self
     */
    @SuppressWarnings("unchecked")
    protected X self() {
        return (X) this;
    }
}

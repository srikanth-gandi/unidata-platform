/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.type.model.source.rule;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import org.apache.commons.collections4.CollectionUtils;
import org.unidata.mdm.matching.core.type.model.source.algorithm.AlgorithmMappingSource;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

/**
 * @author Sergey Murskiy on 14.06.2021
 */
public class MatchingRuleMappingSource implements Serializable {
    /**
     * SVUID.
     */
    private static final long serialVersionUID = -2202335159712626828L;

    @JacksonXmlProperty(isAttribute = true, localName = "ruleName")
    private String ruleName;

    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "mapping")
    private List<AlgorithmMappingSource> mappings;

    /**
     * Gets the rule name.
     *
     * @return the rule name
     */
    public String getRuleName() {
        return ruleName;
    }

    /**
     * Sets the rule name.
     *
     * @param ruleName the rule name to set
     */
    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }

    /**
     * Gets algorithms mappings.
     *
     * @return algorithms mappings
     */
    public List<AlgorithmMappingSource> getMappings() {
        if (Objects.isNull(mappings)) {
            this.mappings = new ArrayList<>();
        }

        return mappings;
    }

    /**
     * Sets algorithms mappings.
     *
     * @param mappings algorithms mappings to set
     */
    public void setMappings(List<AlgorithmMappingSource> mappings) {
        this.mappings = mappings;
    }

    /**
     * Mapping with rule name.
     *
     * @param ruleName the rule name to set
     * @return self
     */
    public MatchingRuleMappingSource withRuleName(String ruleName) {
        setRuleName(ruleName);
        return this;
    }

    /**
     * Mapping with algorithms mappings.
     *
     * @param mappings algorithms mappings to set
     * @return self
     */
    public MatchingRuleMappingSource withMappings(Collection<AlgorithmMappingSource> mappings) {
        if (CollectionUtils.isNotEmpty(mappings)) {
            getMappings().addAll(mappings);
        }

        return this;
    }
}

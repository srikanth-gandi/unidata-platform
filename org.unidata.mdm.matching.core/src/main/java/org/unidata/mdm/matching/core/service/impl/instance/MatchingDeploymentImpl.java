package org.unidata.mdm.matching.core.service.impl.instance;

import org.unidata.mdm.matching.core.type.model.instance.AlgorithmSettingsElement;
import org.unidata.mdm.matching.core.type.model.instance.MatchingColumnElement;
import org.unidata.mdm.matching.core.type.model.instance.MatchingDeploymentElement;
import org.unidata.mdm.matching.core.type.model.instance.MatchingTableElement;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @author Sergey Murskiy on 23.08.2021
 */
public class MatchingDeploymentImpl implements MatchingDeploymentElement {
    /**
     * Matching storage id.
     */
    private final String matchingStorageId;
    /**
     * Matching table.
     */
    private final MatchingTableElement matchingTable;
    /**
     * Matching column assignments.
     */
    private final Map<MatchingColumnElement, List<AlgorithmSettingsElement>> assignments;

    /**
     * Constructor.
     *
     * @param matchingStorageId the matching storage id
     * @param table             the matching table
     * @param assignments       columns assignments
     */
    public MatchingDeploymentImpl(String matchingStorageId, MatchingTableElement table,
                                  Map<MatchingColumnElement, List<AlgorithmSettingsElement>> assignments) {
        this.matchingStorageId = matchingStorageId;
        this.matchingTable = table;
        this.assignments = assignments;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getMatchingStorageId() {
        return matchingStorageId;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getMatchingTableName() {
        return matchingTable.getName();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MatchingTableElement getMatchingTable() {
        return matchingTable;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<MatchingColumnElement, List<AlgorithmSettingsElement>> getColumnAssignments() {
        return Objects.isNull(assignments)
                ? Collections.emptyMap()
                : assignments;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<AlgorithmSettingsElement> getColumnAssignment(MatchingColumnElement column) {
        return assignments.get(column);
    }
}

/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.type.model.source.table;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.io.Serializable;

/**
 * @author Sergey Murskiy on 15.06.2021
 */
public class MatchingColumnMappingSource implements Serializable {
    /**
     * SVUID.
     */
    private static final long serialVersionUID = 4758072898318394481L;

    @JacksonXmlProperty(isAttribute = true, localName = "columnName")
    private String columnName;

    @JacksonXmlProperty(isAttribute = true, localName = "path")
    private String path;

    /**
     * Gets the column name.
     *
     * @return the column name
     */
    public String getColumnName() {
        return columnName;
    }

    /**
     * Sets the column name.
     *
     * @param columnName the column name to set
     */
    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    /**
     * Gets the input path.
     *
     * @return the input path
     */
    public String getPath() {
        return path;
    }

    /**
     * Sets the input path.
     *
     * @param path the input path to set
     */
    public void setPath(String path) {
        this.path = path;
    }

    /**
     * Matching column mapping with column name.
     *
     * @param columnName the column name to set
     * @return self
     */
    public MatchingColumnMappingSource withColumnName(String columnName) {
        setColumnName(columnName);
        return this;
    }

    /**
     * Matching column mapping with input path.
     *
     * @param path the input path to set
     * @return self
     */
    public MatchingColumnMappingSource withPath(String path) {
        setPath(path);
        return this;
    }
}

/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.dto;

import org.apache.commons.collections4.MapUtils;
import org.unidata.mdm.matching.core.context.ClusterUpdateContext;
import org.unidata.mdm.matching.core.type.cluster.Cluster;
import org.unidata.mdm.matching.core.type.data.MatchingRecordKey;

import java.util.Map;

/**
 * @author Sergey Murskiy on 06.08.2021
 */
public class MatchingExecutionResult {
    /**
     * Matching result.
     */
    private Map<MatchingRecordKey, Cluster> result;
    /**
     * Cluster updates.
     */
    private ClusterUpdateContext clusterUpdate;

    /**
     * Gets calculated results.
     *
     * @return calculated clusters.
     */
    public Map<MatchingRecordKey, Cluster> getResult() {
        return MapUtils.emptyIfNull(result);
    }

    /**
     * Sets calculated results.
     *
     * @param result calculated clusters.
     */
    public void setResult(Map<MatchingRecordKey, Cluster> result) {
        this.result = result;
    }

    /**
     * Gets cluster updates.
     *
     * @return cluster update context
     */
    public ClusterUpdateContext getClusterUpdate() {
        return clusterUpdate;
    }

    /**
     * Sets cluster updates.
     *
     * @param clusterUpdate cluster update context
     */
    public void setClusterUpdate(ClusterUpdateContext clusterUpdate) {
        this.clusterUpdate = clusterUpdate;
    }
}

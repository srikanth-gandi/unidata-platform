/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.type.model.instance;

import org.unidata.mdm.core.type.model.IdentityElement;
import org.unidata.mdm.system.type.namespace.NameSpace;

import java.util.Collection;

/**
 * @author Sergey Murskiy on 20.06.2021
 */
public interface NamespaceAssignmentElement extends IdentityElement {
    /**
     * Gets the namespace object it is assigned to.
     * @return namespace object it is assigned to
     */
    NameSpace getNameSpace();

    /**
     * Gets type name, mapped by this assignment.
     * @return type name, mapped by this assignment
     */
    String getTypeName();

    /**
     * Gets assignment key.
     *
     * @return assignment key
     */
    String getAssignmentKey();

    /**
     * Gets a collection of matching rule sets.
     *
     * @return collection of matching rule sets
     */
    Collection<MatchingRuleSetElement> getAssignedSets();

    /**
     * Gets assigned matching rule set by rule set name.
     *
     * @param setName the rule set name
     * @return matching rule set element
     */
    MatchingRuleSetElement getAssignedSet(String setName);

    /**
     * Gets assigned matching table by matching table name.
     *
     * @param matchingTableName the matching table name
     * @return matching table element
     */
    MatchingTableElement getAssignedTable(String matchingTableName);

    /**
     * Gets all matching tables elements.
     *
     * @return matching table elements
     */
    Collection<MatchingTableElement> getAssignedTables();

    /**
     * Gets a collection of matching table mappings.
     *
     * @return collection of matching table mappings
     */
    Collection<MatchingTableMappingElement> getAssignedMappings();

    /**
     * Gets matching table mapping, associated with the given matching table name.
     *
     * @param matchingTableName the matching table name
     * @return matching table mapping
     */
    MatchingTableMappingElement getAssignedMapping(String matchingTableName);
}

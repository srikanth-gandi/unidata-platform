/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.service;

import org.unidata.mdm.matching.core.context.ClusterDeleteContext;
import org.unidata.mdm.matching.core.context.ClusterGetContext;
import org.unidata.mdm.matching.core.context.ClusterSearchContext;
import org.unidata.mdm.matching.core.context.ClusterUpdateContext;
import org.unidata.mdm.matching.core.dto.ClusterGetResult;

/**
 * @author Sergey Murskiy on 26.06.2021
 */
public interface ClusterService {

    /**
     * Process clusters updates (delete, create and update).
     *
     * @param ctx cluster update context
     */
    void update(ClusterUpdateContext ctx);

    /**
     * Delete founded clusters.
     *
     * @param ctx cluster delete context
     */
    void delete(ClusterDeleteContext ctx);

    /**
     * Get cluster by id.
     *
     * @param ctx cluster get context
     * @return cluster get result
     */
    ClusterGetResult get(ClusterGetContext ctx);

    /**
     * Search clusters.
     *
     * @param ctx cluster search context
     * @return cluster get result
     */
    ClusterGetResult search(ClusterSearchContext ctx);
}

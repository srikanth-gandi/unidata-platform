/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.converter;

import org.apache.commons.collections4.MapUtils;
import org.springframework.stereotype.Component;
import org.unidata.mdm.matching.core.type.cluster.Cluster;
import org.unidata.mdm.matching.core.type.cluster.ClusterRecord;
import org.unidata.mdm.matching.core.type.search.ClusterChildIndexType;
import org.unidata.mdm.matching.core.type.search.ClusterHeadIndexType;
import org.unidata.mdm.matching.core.type.search.ClusterHeaderField;
import org.unidata.mdm.matching.core.type.search.ClusterIndexId;
import org.unidata.mdm.matching.core.type.search.ClusterRecordHeaderField;
import org.unidata.mdm.matching.core.type.search.ClusterRecordIndexId;
import org.unidata.mdm.search.dto.SearchResultHitDTO;
import org.unidata.mdm.search.type.indexing.Indexing;
import org.unidata.mdm.search.type.indexing.IndexingField;
import org.unidata.mdm.search.type.indexing.impl.IndexingRecordImpl;
import org.unidata.mdm.search.util.SearchUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Sergey Murskiy on 20.07.2021
 */
@Component
public class ClusterConverter {

    /**
     * Fetch cluster from search result hit.
     *
     * @param hit search result hit
     * @return cluster
     */
    public Cluster clusterFromHit(SearchResultHitDTO hit) {
        Cluster cluster = new Cluster();

        cluster.setId(hit.getFieldFirstValue(ClusterHeaderField.FIELD_ID.getName()));
        cluster.setRuleName(hit.getFieldFirstValue(ClusterHeaderField.FIELD_RULE_NAME.getName()));
        cluster.setRuleDisplayName(hit.getFieldFirstValue(ClusterHeaderField.FIELD_RULE_DISPLAY_NAME.getName()));
        cluster.setSetName(hit.getFieldFirstValue(ClusterHeaderField.FIELD_SET_NAME.getName()));
        cluster.setSetDisplayName(hit.getFieldFirstValue(ClusterHeaderField.FIELD_SET_DISPLAY_NAME.getName()));
        cluster.setMatchingDate(SearchUtils.parse(hit.getFieldFirstValue(ClusterHeaderField.FIELD_MATCHING_DATE.getName())));
        cluster.setRecordsCount(hit.getFieldFirstValue(ClusterHeaderField.FIELD_RECORDS_COUNT.getName()));

        ClusterRecord owner = new ClusterRecord();
        owner.setNamespace(hit.getFieldFirstValue(ClusterHeaderField.FIELD_OWNER_NAMESPACE.getName()));
        owner.setTypeName(hit.getFieldFirstValue(ClusterHeaderField.FIELD_OWNER_TYPE_NAME.getName()));
        owner.setSubjectId(hit.getFieldFirstValue(ClusterHeaderField.FIELD_OWNER_SUBJECT_ID.getName()));
        owner.setValidFrom(SearchUtils.parse(hit.getFieldFirstValue(ClusterHeaderField.FIELD_OWNER_VALID_FROM.getName())));
        owner.setValidTo(SearchUtils.parse(hit.getFieldFirstValue(ClusterHeaderField.FIELD_OWNER_VALID_TO.getName())));

        cluster.setOwner(owner);

        return cluster;
    }

    /**
     * Fetch cluster record from search result hit.
     *
     * @param hit search result hit
     * @return cluster record
     */
    public ClusterRecord clusterRecordFromHit(SearchResultHitDTO hit) {
        ClusterRecord clusterRecord = new ClusterRecord();

        clusterRecord.setNamespace(hit.getFieldFirstValue(ClusterRecordHeaderField.FIELD_NAMESPACE.getName()));
        clusterRecord.setTypeName(hit.getFieldFirstValue(ClusterRecordHeaderField.FIELD_TYPE_NAME.getName()));
        clusterRecord.setSubjectId(hit.getFieldFirstValue(ClusterRecordHeaderField.FIELD_SUBJECT_ID.getName()));
        clusterRecord.setValidFrom(SearchUtils.parse(hit.getFieldFirstValue(ClusterRecordHeaderField.FIELD_VALID_FROM.getName())));
        clusterRecord.setValidTo(SearchUtils.parse(hit.getFieldFirstValue(ClusterRecordHeaderField.FIELD_VALID_TO.getName())));
        clusterRecord.setMatchingRate(hit.getFieldFirstValue(ClusterRecordHeaderField.FIELD_MATCHING_RATE.getName()));


        return clusterRecord;
    }

    /**
     * Convert cluster to indexing.
     *
     * @param cluster cluster
     * @return indexing
     */
    public Collection<Indexing> toIndexing(Cluster cluster) {
        List<Indexing> indexing = new ArrayList<>();

        Indexing clusterIndexing = new Indexing(ClusterHeadIndexType.CLUSTER, ClusterIndexId.of(cluster.getId()))
                .withFields(IndexingField.of(ClusterHeaderField.FIELD_ID.getName(),
                        cluster.getId()))
                .withFields(IndexingField.of(ClusterHeaderField.FIELD_RULE_NAME.getName(),
                        cluster.getRuleName()))
                .withFields(IndexingField.of(ClusterHeaderField.FIELD_SET_NAME.getName(),
                        cluster.getSetName()))
                .withFields(IndexingField.of(ClusterHeaderField.FIELD_RULE_DISPLAY_NAME.getName(),
                        cluster.getRuleDisplayName()))
                .withFields(IndexingField.of(ClusterHeaderField.FIELD_SET_DISPLAY_NAME.getName(),
                        cluster.getSetDisplayName()))
                .withFields(IndexingField.of(ClusterHeaderField.FIELD_OWNER_NAMESPACE.getName(),
                        cluster.getOwner().getNamespace()))
                .withFields(IndexingField.of(ClusterHeaderField.FIELD_OWNER_TYPE_NAME.getName(),
                        cluster.getOwner().getTypeName()))
                .withFields(IndexingField.of(ClusterHeaderField.FIELD_OWNER_SUBJECT_ID.getName(),
                        cluster.getOwner().getSubjectId()))
                .withFields(IndexingField.of(ClusterHeaderField.FIELD_OWNER_VALID_FROM.getName(),
                        cluster.getOwner().getValidFrom()))
                .withFields(IndexingField.of(ClusterHeaderField.FIELD_OWNER_VALID_TO.getName(),
                        cluster.getOwner().getValidTo()))
                .withFields(IndexingField.of(ClusterHeaderField.FIELD_RECORDS_COUNT.getName(),
                        Long.valueOf(cluster.getClusterRecords().size())))
                .withFields(IndexingField.of(ClusterHeaderField.FIELD_MATCHING_DATE.getName(),
                        cluster.getMatchingDate()));

        if (MapUtils.isNotEmpty(cluster.getAdditionalParameters())) {
            clusterIndexing.withFields(IndexingField.ofRecords(ClusterHeaderField.FIELD_ADDITIONAL_PARAMETERS.getPath(),
                    cluster.getAdditionalParameters().entrySet().stream()
                            .map(param ->
                                    List.of(
                                            IndexingField.of(ClusterHeaderField.FIELD_ADDITIONAL_PARAMETER_NAME.getName(), param.getKey()),
                                            IndexingField.of(ClusterHeaderField.FIELD_ADDITIONAL_PARAMETER_VALUE.getName(), String.valueOf(param.getValue()))))
                            .map(IndexingRecordImpl::new)
                            .collect(Collectors.toList())));
        }

        indexing.add(clusterIndexing);

        cluster.getClusterRecords().forEach(clusterRecord -> indexing.add(toIndexing(cluster.getId(), clusterRecord)));

        return indexing;
    }

    public Indexing toIndexing(String clusterId, ClusterRecord clusterRecord) {
        ClusterRecordIndexId id = ClusterRecordIndexId.of(clusterId, clusterRecord.getNamespace(),
                clusterRecord.getTypeName(), clusterRecord.getSubjectId(), clusterRecord.getValidTo());

        return new Indexing(ClusterChildIndexType.CLUSTER_RECORD, id)
                .withFields(IndexingField.of(ClusterRecordHeaderField.FIELD_CLUSTER_ID.getName(),
                        clusterId))
                .withFields(IndexingField.of(ClusterRecordHeaderField.FIELD_SUBJECT_ID.getName(),
                        clusterRecord.getSubjectId()))
                .withFields(IndexingField.of(ClusterRecordHeaderField.FIELD_NAMESPACE.getName(),
                        clusterRecord.getNamespace()))
                .withFields(IndexingField.of(ClusterRecordHeaderField.FIELD_TYPE_NAME.getName(),
                        clusterRecord.getTypeName()))
                .withFields(IndexingField.of(ClusterRecordHeaderField.FIELD_VALID_FROM.getName(),
                        clusterRecord.getValidFrom()))
                .withFields(IndexingField.of(ClusterRecordHeaderField.FIELD_VALID_TO.getName(),
                        clusterRecord.getValidTo()))
                .withFields(IndexingField.of(ClusterRecordHeaderField.FIELD_MATCHING_RATE.getName(),
                        Long.valueOf(clusterRecord.getMatchingRate()))
                );
    }
}

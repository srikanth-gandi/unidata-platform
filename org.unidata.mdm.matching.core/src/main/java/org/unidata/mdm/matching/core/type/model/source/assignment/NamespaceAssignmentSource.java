/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.type.model.source.assignment;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.unidata.mdm.matching.core.type.model.source.table.MatchingTableMappingSource;
import org.unidata.mdm.system.util.NameSpaceUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

/**
 * @author Sergey Murskiy on 15.06.2021
 */
public class NamespaceAssignmentSource implements Serializable {
    /**
     * SVUID.
     */
    private static final long serialVersionUID = 7735878412786047333L;

    @JacksonXmlProperty(isAttribute = true, localName = "nameSpace")
    private String nameSpace;

    @JacksonXmlProperty(isAttribute = true, localName = "typeName")
    private String typeName;

    @JacksonXmlElementWrapper(localName = "matchingTables")
    @JacksonXmlProperty(localName = "matchingTable")
    private List<String> matchingTables;

    @JacksonXmlElementWrapper(localName = "sets")
    @JacksonXmlProperty(localName = "set")
    private List<String> sets;

    @JacksonXmlElementWrapper(localName = "mappings")
    @JacksonXmlProperty(localName = "mapping")
    private List<MatchingTableMappingSource> mappings;

    /**
     * Gets the value of the namespace property.
     *
     * @return namespace
     */
    public String getNameSpace() {
        return nameSpace;
    }

    /**
     * Sets the value of the nameSpace property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setNameSpace(String value) {
        this.nameSpace = value;
    }

    /**
     * Gets the value of the typeName property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getTypeName() {
        return typeName;
    }

    /**
     * Sets the value of the typeName property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setTypeName(String value) {
        this.typeName = value;
    }

    /**
     * Gets assigned matching tables.
     *
     * @return assigned matching tables
     */
    public List<String> getMatchingTables() {
        if (Objects.isNull(matchingTables)) {
            this.matchingTables = new ArrayList<>();
        }

        return matchingTables;
    }

    /**
     * Sets assigned matching tables.
     *
     * @param matchingTables assigned matching tables to set
     */
    public void setMatchingTables(List<String> matchingTables) {
        this.matchingTables = matchingTables;
    }

    /**
     * Gets assigned matching rule sets.
     *
     * @return assigned matching rule sets
     */
    public List<String> getSets() {
        if (Objects.isNull(sets)) {
            this.sets = new ArrayList<>();
        }

        return sets;
    }

    /**
     * Sets assigned matching rule sets.
     *
     * @param sets assigned matching rule sets to set
     */
    public void setSets(List<String> sets) {
        this.sets = sets;
    }

    /**
     * Gets matching table mappings.
     *
     * @return matching table mappings
     */
    public List<MatchingTableMappingSource> getMappings() {
        if (Objects.isNull(mappings)) {
            mappings = new ArrayList<>();
        }

        return mappings;
    }

    /**
     * Sets matching table mappings.
     *
     * @param mappings matching table mappings to set
     */
    public void setMappings(List<MatchingTableMappingSource> mappings) {
        this.mappings = mappings;
    }

    /**
     * Gets the value of the assignment key.
     *
     * @return possible object is
     * {@link String }
     */
    public String getAssignmentKey() {
        return NameSpaceUtils.join(nameSpace, typeName);
    }

    /**
     * Namespace assignment with namespace.
     *
     * @param value namespace to set
     * @return self
     */
    public NamespaceAssignmentSource withNameSpace(String value) {
        setNameSpace(value);
        return this;
    }

    /**
     * Namespace assignment with type name.
     *
     * @param value the type name to set
     * @return self
     */
    public NamespaceAssignmentSource withTypeName(String value) {
        setTypeName(value);
        return this;
    }

    /**
     * Namespace assignment with matching tables.
     *
     * @param values matching table names to set
     * @return self
     */
    public NamespaceAssignmentSource withMatchingTables(String... values) {
        if (ArrayUtils.isNotEmpty(values)) {
            return withMatchingTables(Arrays.asList(values));
        }

        return this;
    }

    /**
     * Namespace assignment with matching tables.
     *
     * @param values matching table names to set
     * @return self
     */
    public NamespaceAssignmentSource withMatchingTables(Collection<String> values) {
        if (CollectionUtils.isNotEmpty(values)) {
            getMatchingTables().addAll(values);
        }

        return this;
    }

    /**
     * Namespace assignment with matching rule sets.
     *
     * @param values matching rule sets to set
     * @return self
     */
    public NamespaceAssignmentSource withSets(String... values) {
        if (ArrayUtils.isNotEmpty(values)) {
            return withSets(Arrays.asList(values));
        }

        return this;
    }

    /**
     * Namespace assignment with matching rule sets.
     *
     * @param values matching rule sets to set
     * @return self
     */
    public NamespaceAssignmentSource withSets(Collection<String> values) {
        if (CollectionUtils.isNotEmpty(values)) {
            getSets().addAll(values);
        }

        return this;
    }

    /**
     * Namespace assignment with matching table mappings.
     *
     * @param values matching table mappings to set
     * @return self
     */
    public NamespaceAssignmentSource withMappings(MatchingTableMappingSource... values) {
        if (ArrayUtils.isNotEmpty(values)) {
            return withMappings(Arrays.asList(values));
        }

        return this;
    }

    /**
     * Namespace assignment with matching table mappings.
     *
     * @param values matching table mappings to set
     * @return self
     */
    public NamespaceAssignmentSource withMappings(Collection<MatchingTableMappingSource> values) {
        if (CollectionUtils.isNotEmpty(values)) {
            getMappings().addAll(values);
        }

        return this;
    }
}

/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.type.algorithm;

import org.apache.commons.collections4.CollectionUtils;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Supplier;

/**
 * @author Sergey Murskiy on 23.06.2021
 */
public class AlgorithmParamConfiguration<T> {
    /**
     * Name.
     */
    private final String name;
    /**
     * Display name.
     */
    private final Supplier<String> displayName;
    /**
     * Description.
     */
    private final Supplier<String> description;
    /**
     * Value required or not.
     */
    private final boolean required;
    /**
     * Parameter type.
     */
    private final AlgorithmParameterType type;
    /**
     * Default value.
     */
    private final T defaultValue;
    /**
     * Enum values.
     */
    private final List<EnumValue> enumValues;

    /**
     * Constructor.
     */
    private AlgorithmParamConfiguration(AlgorithmParamConfigurationBuilder<T> b) {
        super();

        Objects.requireNonNull(b.name, "Parameter name must not be null.");

        this.name = b.name;
        this.displayName = Objects.nonNull(b.displayNameSupplier) ? b.displayNameSupplier : () -> b.displayName;
        this.description = Objects.nonNull(b.descriptionSupplier) ? b.descriptionSupplier : () -> b.description;
        this.required = b.required;
        this.type = b.type;
        this.defaultValue = b.defaultValue;
        this.enumValues = b.enumValues;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the name
     */
    public Supplier<String> getDisplayName() {
        return displayName;
    }

    /**
     * @return the description
     */
    public Supplier<String> getDescription() {
        return description;
    }

    /**
     * @return the required
     */
    public boolean isRequired() {
        return required;
    }

    /**
     * @return parameter type
     */
    public AlgorithmParameterType getType() {
        return type;
    }

    /**
     * @return default value
     */
    public T getDefaultValue() {
        return defaultValue;
    }

    /**
     * @return enum values
     */
    public List<EnumValue> getEnumValues() {
        return enumValues;
    }

    /**
     * Gets string parameter builder.
     *
     * @return builder
     */
    public static AlgorithmParamConfigurationBuilder<String> string() {
        return new AlgorithmParamConfigurationBuilder<String>()
                .type(AlgorithmParameterType.STRING);
    }

    /**
     * Gets integer parameter builder.
     *
     * @return builder
     */
    public static AlgorithmParamConfigurationBuilder<Long> integer() {
        return new AlgorithmParamConfigurationBuilder<Long>()
                .type(AlgorithmParameterType.INTEGER);
    }

    /**
     * Gets number parameter builder.
     *
     * @return builder
     */
    public static AlgorithmParamConfigurationBuilder<Double> number() {
        return new AlgorithmParamConfigurationBuilder<Double>()
                .type(AlgorithmParameterType.NUMBER);
    }

    /**
     * Gets bool parameter builder.
     *
     * @return builder
     */
    public static AlgorithmParamConfigurationBuilder<Boolean> bool() {
        return new AlgorithmParamConfigurationBuilder<Boolean>()
                .type(AlgorithmParameterType.BOOLEAN);
    }

    /**
     * Gets enumeration parameter builder.
     *
     * @return builder
     */
    public static AlgorithmParamConfigurationBuilder<String> enumeration(List<EnumValue> enumValues) {
        return new AlgorithmParamConfigurationBuilder<String>(enumValues)
                .type(AlgorithmParameterType.ENUM);
    }

    /**
     * Gets date parameter builder.
     *
     * @return builder
     */
    public static AlgorithmParamConfigurationBuilder<LocalDate> date() {
        return new AlgorithmParamConfigurationBuilder<LocalDate>()
                .type(AlgorithmParameterType.DATE);
    }

    /**
     * Parameter builder.
     */
    public static class AlgorithmParamConfigurationBuilder<T> {
        /**
         * Name of the parameter.
         */
        private String name;
        /**
         * Plain display name.
         */
        private String displayName;
        /**
         * Display name supplier to support i18n.
         */
        private Supplier<String> displayNameSupplier;
        /**
         * Description in plain text.
         */
        private String description;
        /**
         * Description as supplier.
         */
        private Supplier<String> descriptionSupplier;
        /**
         * Value required or not.
         */
        private boolean required;
        /**
         * Algorithm parameter type.
         */
        private AlgorithmParameterType type;
        /**
         * Default value.
         */
        private T defaultValue;
        /**
         * Enum values.
         */
        private final List<EnumValue> enumValues = new ArrayList<>();

        /**
         * Constructor.
         */
        private AlgorithmParamConfigurationBuilder() {
            super();
        }

        /**
         * Constructor.
         */
        private AlgorithmParamConfigurationBuilder(List<EnumValue> enumValues) {
            if (CollectionUtils.isNotEmpty(enumValues)) {
                this.enumValues.addAll(enumValues);
            }
        }

        /**
         * @param name the name to set
         */
        public AlgorithmParamConfigurationBuilder<T> name(String name) {
            this.name = name;
            return this;
        }

        /**
         * @param displayName the displayName to set
         */
        public AlgorithmParamConfigurationBuilder<T> displayName(String displayName) {
            this.displayName = displayName;
            return this;
        }

        /**
         * @param displayNameSupplier the displayNameSupplier to set
         */
        public AlgorithmParamConfigurationBuilder<T> displayName(Supplier<String> displayNameSupplier) {
            this.displayNameSupplier = displayNameSupplier;
            return this;
        }

        /**
         * @param description the description to set
         */
        public AlgorithmParamConfigurationBuilder<T> description(String description) {
            this.description = description;
            return this;
        }

        /**
         * @param descriptionSupplier the descriptionSupplier to set
         */
        public AlgorithmParamConfigurationBuilder<T> description(Supplier<String> descriptionSupplier) {
            this.descriptionSupplier = descriptionSupplier;
            return this;
        }

        /**
         * @param required the required to set
         */
        public AlgorithmParamConfigurationBuilder<T> required(boolean required) {
            this.required = required;
            return this;
        }

        /**
         * @param type the parameter type
         */
        public AlgorithmParamConfigurationBuilder<T> type(AlgorithmParameterType type) {
            this.type = type;
            return this;
        }

        /**
         * @param defaultValue the default value
         */
        public AlgorithmParamConfigurationBuilder<T> defaultValue(T defaultValue) {
            this.defaultValue = defaultValue;
            return this;
        }

        /**
         * Builds port.
         *
         * @return port
         */
        public AlgorithmParamConfiguration<T> build() {
            return new AlgorithmParamConfiguration<>(this);
        }
    }
}

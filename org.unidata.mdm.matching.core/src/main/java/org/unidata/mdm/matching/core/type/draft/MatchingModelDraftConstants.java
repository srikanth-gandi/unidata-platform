/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.type.draft;

/**
 * @author Sergey Murskiy on 22.06.2021
 */
public final class MatchingModelDraftConstants {
    /**
     * The storage id variable name.
     */
    public static final String STORAGE_ID = "STORAGE_ID";
    /**
     * The model start version number variable name.
     */
    public static final String DRAFT_START_VERSION = "DRAFT_START_VERSION";
    /**
     * The model draft initiator variable name.
     */
    public static final String DRAFT_INITIATOR = "DRAFT_INITIATOR";

    /**
     * Constructor.
     */
    private MatchingModelDraftConstants() {
        super();
    }
}

/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.serialization;

import org.unidata.mdm.matching.core.exception.MatchingExceptionIds;
import org.unidata.mdm.matching.core.exception.MatchingRuntimeException;
import org.unidata.mdm.matching.core.type.model.MatchingModel;
import org.unidata.mdm.system.serialization.xml.XmlObjectSerializer;
import org.unidata.mdm.system.util.CompressUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * @author Sergey Murskiy on 22.06.2021
 */
public class MatchingSerializer {

    /**
     * Matching model zip entry name.
     */
    public static final String MATCHING_MODEL_ZIP_ENTRY = "matching-model";

    /**
     * Constructor.
     */
    private MatchingSerializer() {
        super();
    }

    /**
     * Does decompress default ZIP compressed matching model XML representation.
     *
     * @param data the compressed data
     * @return model
     */
    public static MatchingModel modelFromCompressedXml(byte[] data) {

        byte[] uncompressed;
        try {
            uncompressed = CompressUtils.unzip(data, MATCHING_MODEL_ZIP_ENTRY);
        } catch (IOException e) {
            throw new MatchingRuntimeException("Cannot decompress matching model.", e,
                    MatchingExceptionIds.EX_MATCHING_CANNOT_DECOMPRESS_DATA);
        }

        return XmlObjectSerializer
                .getInstance()
                .fromXmlString(MatchingModel.class, new String(uncompressed, StandardCharsets.UTF_8));
    }

    /**
     * Does compress model to default ZIP compressed XML representation.
     *
     * @param model the model
     * @return bytes
     */
    public static byte[] modelToCompressedXml(MatchingModel model) {

        String content = XmlObjectSerializer.getInstance().toXmlString(model, false);
        byte[] compressed;
        try {
            compressed = CompressUtils.zip(content.getBytes(StandardCharsets.UTF_8), MATCHING_MODEL_ZIP_ENTRY);
        } catch (IOException e) {
            throw new MatchingRuntimeException("Cannot compress data model.", e,
                    MatchingExceptionIds.EX_MATCHING_CANNOT_COMPRESS_DATA);
        }

        return compressed;
    }
}

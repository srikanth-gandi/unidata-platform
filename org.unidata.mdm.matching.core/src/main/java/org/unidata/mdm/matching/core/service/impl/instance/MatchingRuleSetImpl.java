/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.service.impl.instance;

import org.unidata.mdm.core.type.model.instance.AbstractNamedDisplayableCustomPropertiesImpl;
import org.unidata.mdm.matching.core.type.model.instance.AlgorithmMappingElement;
import org.unidata.mdm.matching.core.type.model.instance.AlgorithmSettingsElement;
import org.unidata.mdm.matching.core.type.model.instance.MatchingColumnElement;
import org.unidata.mdm.matching.core.type.model.instance.MatchingModelInstance;
import org.unidata.mdm.matching.core.type.model.instance.MatchingRuleElement;
import org.unidata.mdm.matching.core.type.model.instance.MatchingRuleMappingElement;
import org.unidata.mdm.matching.core.type.model.instance.MatchingRuleSetElement;
import org.unidata.mdm.matching.core.type.model.instance.MatchingTableElement;
import org.unidata.mdm.matching.core.type.model.source.algorithm.AlgorithmMappingSource;
import org.unidata.mdm.matching.core.type.model.source.rule.MatchingRuleMappingSource;
import org.unidata.mdm.matching.core.type.model.source.rule.MatchingRuleSetSource;

import java.time.OffsetDateTime;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author Sergey Murskiy on 20.06.2021
 */
public class MatchingRuleSetImpl extends AbstractNamedDisplayableCustomPropertiesImpl
        implements MatchingRuleSetElement {

    /**
     * Source.
     */
    private final MatchingRuleSetSource source;
    /**
     * Matching table.
     */
    private final MatchingTableElement matchingTable;
    /**
     * Mappings.
     */
    private final Map<String, MatchingRuleMappingElement> mappings;

    /**
     * Constructor.
     *
     * @param source matching rule set source
     */
    public MatchingRuleSetImpl(MatchingRuleSetSource source, MatchingModelInstance instance) {
        super(source.getName(), source.getDisplayName(), source.getDescription(), source.getCustomProperties());

        this.source = source;
        this.matchingTable = instance.getTable(source.getMatchingTable());

        mappings = new HashMap<>(source.getMappings().size());

        for (MatchingRuleMappingSource m : source.getMappings()) {
            mappings.put(m.getRuleName(), new MatchingRuleMappingImpl(m, instance));
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MatchingTableElement getMatchingTable() {
        return matchingTable;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<MatchingRuleMappingElement> getMappings() {
        return mappings.values();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MatchingRuleMappingElement getMapping(String ruleName) {
        return mappings.get(ruleName);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OffsetDateTime getCreateDate() {
        return source.getCreateDate();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCreatedBy() {
        return source.getCreatedBy();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OffsetDateTime getUpdateDate() {
        return source.getUpdateDate();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getUpdatedBy() {
        return source.getUpdatedBy();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getId() {
        return getName();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getMatchingStorageId() {
        return source.getMatchingStorageId();
    }

    /**
     * Gets matching rule set source.
     *
     * @return source
     */
    public MatchingRuleSetSource getSource() {
        return source;
    }

    /**
     * Matching rule mapping implementation.
     */
    private class MatchingRuleMappingImpl implements MatchingRuleMappingElement {
        /**
         * Source.
         */
        private final MatchingRuleElement rule;

        /**
         * Algorithms mappings.
         */
        private final Map<String, AlgorithmMappingElement> mappings;

        /**
         * Constructor.
         *
         * @param source matching rule mapping source
         */
        public MatchingRuleMappingImpl(MatchingRuleMappingSource source, MatchingModelInstance instance) {
            this.rule = instance.getRule(source.getRuleName());

            mappings = source.getMappings().stream()
                    .map(s -> new AlgorithmMappingImpl(s, matchingTable, rule))
                    .collect(Collectors.toMap(AlgorithmMappingElement::getAlgorithmSettingsId, Function.identity()));
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public MatchingRuleElement getRule() {
            return rule;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public Collection<AlgorithmMappingElement> getMappings() {
            return mappings.values();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public AlgorithmMappingElement getMapping(String algorithmId) {
            return mappings.get(algorithmId);
        }
    }

    /**
     * Algorithm mapping implementation.
     */
    public static class AlgorithmMappingImpl implements AlgorithmMappingElement {
        /**
         * Matching column.
         */
        private final MatchingColumnElement column;
        /**
         * Matching algorithm settings.
         */
        private final AlgorithmSettingsElement algorithmSettings;

        /**
         * Constructor.
         *
         * @param source        matching rule mapping source
         * @param matchingTable matching table
         * @param rule          matching rule
         */
        public AlgorithmMappingImpl(AlgorithmMappingSource source, MatchingTableElement matchingTable,
                                    MatchingRuleElement rule) {
            this.column = matchingTable.getColumn(source.getColumnName());
            this.algorithmSettings = rule.getAlgorithm(source.getSettingsId());
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String getAlgorithmSettingsId() {
            return algorithmSettings.getId();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public AlgorithmSettingsElement getAlgorithmSettings() {
            return algorithmSettings;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String getColumnName() {
            return column.getName();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public MatchingColumnElement getColumn() {
            return column;
        }
    }
}

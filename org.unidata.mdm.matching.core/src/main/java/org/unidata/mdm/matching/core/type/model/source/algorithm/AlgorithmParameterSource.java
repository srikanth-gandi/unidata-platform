/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.type.model.source.algorithm;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import org.unidata.mdm.matching.core.type.algorithm.AlgorithmParameterType;
import org.unidata.mdm.matching.core.type.model.source.algorithm.parameter.AbstractParameterValue;
import org.unidata.mdm.matching.core.type.model.source.algorithm.parameter.AlgorithmParameterValue;

import java.io.Serializable;
import java.util.Objects;

/**
 * Algorithm parameter holder.
 *
 * @author Sergey Murskiy on 19.06.2021
 */
public class AlgorithmParameterSource implements Serializable {
    /**
     * SVUID.
     */
    private static final long serialVersionUID = 2095012349999680895L;

    @JacksonXmlProperty(localName = "name")
    private String name;

    @JacksonXmlProperty(localName = "type")
    private AlgorithmParameterType type;

    @JacksonXmlProperty(localName = "value")
    private AbstractParameterValue<?, ?> value;

    /**
     * Gets parameter name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets parameter name.
     *
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets parameter type.
     *
     * @return parameter type
     */
    public AlgorithmParameterType getType() {
        return type;
    }

    /**
     * Sets parameter type.
     *
     * @param type the parameter type
     */
    public void setType(AlgorithmParameterType type) {
        this.type = type;
    }

    /**
     * @return the single
     */
    @SuppressWarnings("unchecked")
    public <X> AlgorithmParameterValue<X> getValue() {
        return (AlgorithmParameterValue<X>) value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(AbstractParameterValue<?, ?> value) {
        this.value = value;
    }

    /**
     * @return is empty
     */
    public boolean isEmpty() {
        return Objects.isNull(value);
    }

    /**
     * Algorithm parameter with name.
     *
     * @param value the name to set
     * @return self
     */
    public AlgorithmParameterSource withName(String value) {
        setName(value);
        return this;
    }

    /**
     * Algorithm parameter with type.
     *
     * @param value the type to set
     * @return self
     */
    public AlgorithmParameterSource withType(AlgorithmParameterType value) {
        setType(value);
        return this;
    }

    /**
     * Algorithm parameter with value.
     *
     * @param value the value to set
     * @return self
     */
    public AlgorithmParameterSource withValue(AbstractParameterValue<?, ?> value) {
        setValue(value);
        return this;
    }
}

/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.service.impl.instance;

import org.apache.commons.lang3.StringUtils;
import org.unidata.mdm.core.service.UPathService;
import org.unidata.mdm.core.type.upath.UPath;
import org.unidata.mdm.matching.core.type.model.instance.MatchingColumnMappingElement;
import org.unidata.mdm.matching.core.type.model.instance.MatchingRuleSetElement;
import org.unidata.mdm.matching.core.type.model.instance.MatchingTableElement;
import org.unidata.mdm.matching.core.type.model.instance.MatchingTableMappingElement;
import org.unidata.mdm.matching.core.type.model.instance.NamespaceAssignmentElement;
import org.unidata.mdm.matching.core.type.model.source.assignment.NamespaceAssignmentSource;
import org.unidata.mdm.matching.core.type.model.source.table.MatchingColumnMappingSource;
import org.unidata.mdm.matching.core.type.model.source.table.MatchingTableMappingSource;
import org.unidata.mdm.system.type.namespace.NameSpace;
import org.unidata.mdm.system.util.NameSpaceUtils;

import java.util.Collection;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author Sergey Murskiy on 20.06.2021
 */
public class NamespaceAssignmentImpl implements NamespaceAssignmentElement {
    /**
     * The source.
     */
    private final NamespaceAssignmentSource source;
    /**
     * Matching tables.
     */
    private final Map<String, MatchingTableElement> assignedTables;
    /**
     * Matching rule sets.
     */
    private final Map<String, MatchingRuleSetElement> assignedSets;
    /**
     * Matching table mappings.
     */
    private final Map<String, MatchingTableMappingElement> mappings;

    /**
     * Constructor.
     */
    public NamespaceAssignmentImpl(MatchingModelInstanceImpl mmi, NamespaceAssignmentSource source,
                                   UPathService uPathService) {
        super();
        this.source = source;

        assignedSets = source.getSets().stream()
                .collect(Collectors.toMap(Function.identity(), mmi::getSet));

        assignedTables = source.getMatchingTables().stream()
                .collect(Collectors.toMap(Function.identity(), mmi::getTable));

        mappings = source.getMappings().stream()
                .map(m -> new MatchingTableMappingImpl(m, uPathService))
                .collect(Collectors.toMap(MatchingTableMappingElement::getMatchingTableName, Function.identity()));
    }

    /**
     * Gets the source.
     *
     * @return the source
     */
    public NamespaceAssignmentSource getSource() {
        return source;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NameSpace getNameSpace() {
        return NameSpaceUtils.find(source.getNameSpace());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getTypeName() {
        return source.getTypeName();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getId() {
        return getAssignmentKey();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getAssignmentKey() {
        return source.getAssignmentKey();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<MatchingRuleSetElement> getAssignedSets() {
        return assignedSets.values();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MatchingRuleSetElement getAssignedSet(String setName) {
        return assignedSets.get(setName);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MatchingTableElement getAssignedTable(String matchingTableName) {
        return assignedTables.get(matchingTableName);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<MatchingTableElement> getAssignedTables() {
        return assignedTables.values();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<MatchingTableMappingElement> getAssignedMappings() {
        return mappings.values();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MatchingTableMappingElement getAssignedMapping(String matchingTableName) {
        return mappings.get(matchingTableName);
    }

    /**
     * Matching table mapping implementation.
     */
    public static class MatchingTableMappingImpl implements MatchingTableMappingElement {
        /**
         * Source.
         */
        private final MatchingTableMappingSource source;
        /**
         * Mappings.
         */
        private final Map<String, MatchingColumnMappingElement> mappings;

        /**
         * Constructor.
         *
         * @param source       matching table mapping source
         * @param uPathService uPath service
         */
        public MatchingTableMappingImpl(MatchingTableMappingSource source, UPathService uPathService) {
            this.source = source;

            mappings = source.getMappings().stream()
                    .map(s -> new MatchingColumnMappingImpl(s, uPathService))
                    .collect(Collectors.toMap(MatchingColumnMappingElement::getColumnName, Function.identity()));
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String getMatchingTableName() {
            return source.getMatchingTableName();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public Collection<MatchingColumnMappingElement> getMappings() {
            return mappings.values();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public MatchingColumnMappingElement getMapping(String columnName) {
            return mappings.get(columnName);
        }
    }

    public static class MatchingColumnMappingImpl implements MatchingColumnMappingElement {
        /**
         * Source.
         */
        private final MatchingColumnMappingSource source;
        /**
         * Input path.
         */
        private final UPath path;

        /**
         * Constructor.
         *
         * @param source       matching column mapping source
         * @param uPathService uPath service
         */
        public MatchingColumnMappingImpl(MatchingColumnMappingSource source, UPathService uPathService) {
            this.source = source;

            path = StringUtils.isNotBlank(source.getPath())
                    ? uPathService.upathCreate(source.getPath())
                    : null;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String getColumnName() {
            return source.getColumnName();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public boolean isPath() {
            return Objects.nonNull(path);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public UPath getPath() {
            return path;
        }
    }
}

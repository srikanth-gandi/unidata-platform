/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.module;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.unidata.mdm.matching.core.configuration.MatchingCoreConfiguration;
import org.unidata.mdm.matching.core.configuration.MatchingCoreConfigurationConstants;
import org.unidata.mdm.matching.core.migration.MatchingCoreMigrations;
import org.unidata.mdm.system.configuration.SystemConfigurationConstants;
import org.unidata.mdm.system.context.DatabaseMigrationContext;
import org.unidata.mdm.system.service.DatabaseMigrationService;
import org.unidata.mdm.system.type.annotation.ConfigurationRef;
import org.unidata.mdm.system.type.configuration.ConfigurationValue;
import org.unidata.mdm.system.type.module.AbstractModule;
import org.unidata.mdm.system.type.module.Dependency;
import org.unidata.mdm.system.type.pipeline.Segment;
import org.unidata.mdm.system.util.DataSourceUtils;

import javax.sql.DataSource;
import java.util.Arrays;
import java.util.Collection;

/**
 * Matching core module.
 *
 * @author Sergey Murskiy on 08.06.2021
 */
public class MatchingCoreModule extends AbstractModule {
    /**
     * Logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(MatchingCoreModule.class);

    /**
     * Module id.
     */
    public static final String MODULE_ID = "org.unidata.mdm.matching.core";

    /**
     * Module dependencies.
     */
    private static final Collection<Dependency> DEPENDENCIES = Arrays.asList(
            new Dependency("org.unidata.mdm.core", "6.0"),
            new Dependency("org.unidata.mdm.system", "6.0"),
            new Dependency("org.unidata.mdm.draft", "6.0"),
            new Dependency("org.unidata.mdm.search", "6.0")
    );

    /**
     * Matching core data source.
     */
    @Autowired
    @Qualifier("matchingDataSource")
    private DataSource matchingDataSource;
    /**
     * Migration service.
     */
    @Autowired
    private DatabaseMigrationService migrationService;
    /**
     * Matching core configuration.
     */
    @Autowired
    private MatchingCoreConfiguration matchingCoreConfiguration;

    /**
     * Whether we run in developer mode.
     */
    @ConfigurationRef(SystemConfigurationConstants.PROPERTY_DEVELOPER_MODE)
    private ConfigurationValue<Boolean> developerMode;

    /**
     * Is module installed.
     */
    private boolean install;

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<Dependency> getDependencies() {
        return DEPENDENCIES;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getId() {
        return MODULE_ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getVersion() {
        return "6.3";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getName() {
        return "Matching Core";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getDescription() {
        return "Matching Core module";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void install() {
        LOGGER.info("Install");
        migrate();
        install = true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void uninstall() {
        LOGGER.info("Uninstall");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void start() {
        LOGGER.info("Start");

        if (Boolean.TRUE.equals(developerMode.getValue()) && !install) {
            migrate();
        }

        addSegments(matchingCoreConfiguration.getBeansOfType(Segment.class).values());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void stop() {
        LOGGER.info("Stop");
        DataSourceUtils.shutdown(matchingDataSource);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String[] getResourceBundleBasenames() {
        return new String[]{"matching_core_messages"};
    }

    private void migrate() {
        migrationService.migrate(DatabaseMigrationContext.builder()
                .schemaName(MatchingCoreConfigurationConstants.MATCHING_CORE_SCHEMA_NAME)
                .logName(MatchingCoreConfigurationConstants.MATCHING_CORE_LOG_NAME)
                .dataSource(matchingDataSource)
                .migrations(MatchingCoreMigrations.install())
                .build());
    }
}

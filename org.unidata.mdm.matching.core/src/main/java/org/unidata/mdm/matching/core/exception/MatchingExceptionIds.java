/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.exception;

import org.unidata.mdm.system.exception.ExceptionId;

/**
 * @author Sergey Murskiy on 22.06.2021
 */
public final class MatchingExceptionIds {
    /**
     * Constructor.
     */
    private MatchingExceptionIds() {
        super();
    }

    public static final ExceptionId EX_MATCHING_CANNOT_DECOMPRESS_DATA =
            new ExceptionId("EX_MATCHING_CANNOT_DECOMPRESS_DATA", "app.matching.cannot. decompress.data");

    public static final ExceptionId EX_MATCHING_CANNOT_COMPRESS_DATA =
            new ExceptionId("EX_MATCHING_CANNOT_COMPRESS_DATA", "app.matching.cannot.compress.data");

    public static final ExceptionId EX_MATCHING_CANNOT_INSTALL_MATCHING_ALGORITHMS =
            new ExceptionId("EX_MATCHING_CANNOT_INSTALL_MATCHING_ALGORITHMS", "app.matching.cannot.install.matching.algorithms");

    public static final ExceptionId EX_MATCHING_UPSERT_MATCHING_MODEL_CONSISTENCY =
            new ExceptionId("EX_MATCHING_UPSERT_MATCHING_MODEL_CONSISTENCY", "app.matching.upsert.matching.model.consistency");

    public static final ExceptionId EX_MATCHING_UPSERT_MATCHING_MODEL_VALIDATION =
            new ExceptionId("EX_MATCHING_UPSERT_MATCHING_MODEL_VALIDATION", "app.matching.upsert.matching.model.validation");

    public static final ExceptionId EX_MATCHING_UPSERT_MATCHING_MODEL_REVISION_EXISTS =
            new ExceptionId("EX_MATCHING_UPSERT_MATCHING_MODEL_REVISION_EXISTS", "app.matching.upsert.matching.model.revision.exists");

    public static final ExceptionId EX_MATCHING_LIBRARY_CANNOT_BE_CREATED =
            new ExceptionId("EX_MATCHING_LIBRARY_CANNOT_BE_CREATED", "app.matching.library.cannot.be.created");

    public static final ExceptionId EX_MATCHING_ALGORITHM_CANNOT_BE_CREATED =
            new ExceptionId("EX_MATCHING_ALGORITHM_CANNOT_BE_CREATED", "app.matching.algorithm.cannot.be.created");

    public static final ExceptionId EX_MATCHING_LIBRARY_NOT_FOUND =
            new ExceptionId("EX_MATCHING_LIBRARY_NOT_FOUND", "app.matching.library.not.found");
}

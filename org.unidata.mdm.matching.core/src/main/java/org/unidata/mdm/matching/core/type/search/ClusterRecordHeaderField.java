/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.type.search;

import org.unidata.mdm.search.type.FieldType;
import org.unidata.mdm.search.type.IndexField;

/**
 * @author Sergey Murskiy on 01.07.2021
 */
public enum ClusterRecordHeaderField implements IndexField {
    /**
     * Cluster id.
     */
    FIELD_CLUSTER_ID("$clusterId", FieldType.INTEGER),
    /**
     * Cluster record id.
     */
    FIELD_SUBJECT_ID("$subjectId", FieldType.STRING),
    /**
     * Namespace.
     */
    FIELD_NAMESPACE("$namespace", FieldType.STRING),
    /**
     * Type name.
     */
    FIELD_TYPE_NAME("$typeName", FieldType.STRING),
    /**
     * Valid from.
     */
    FIELD_VALID_FROM("$validFrom", FieldType.TIMESTAMP),
    /**
     * Valid to.
     */
    FIELD_VALID_TO("$validTo", FieldType.TIMESTAMP),
    /**
     * Matching rate.
     */
    FIELD_MATCHING_RATE("$matchingRate", FieldType.INTEGER);

    /**
     * Constructor.
     *
     * @param field field name
     * @param type field type
     */
    ClusterRecordHeaderField(String field, FieldType type) {
        this.field = field;
        this.type = type;
    }

    /**
     * The name.
     */
    private final String field;
    /**
     * The type.
     */
    private final FieldType type;

    /**
     * {@inheritDoc}
     */
    @Override
    public FieldType getFieldType() {
        return type;
    }
    /**
     * @return the field
     */
    @Override
    public String getName() {
        return field;
    }
}

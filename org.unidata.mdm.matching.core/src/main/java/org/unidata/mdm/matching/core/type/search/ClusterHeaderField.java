/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.type.search;

import org.unidata.mdm.search.type.FieldType;
import org.unidata.mdm.search.type.IndexField;

/**
 * @author Sergey Murskiy on 01.07.2021
 */
public enum ClusterHeaderField implements IndexField {
    /**
     * Join field.
     */
    FIELD_JOIN("$j", FieldType.JOIN),
    /**
     * Cluster id.
     */
    FIELD_ID("$id", FieldType.STRING),
    /**
     * Rule name.
     */
    FIELD_RULE_NAME("$ruleName", FieldType.STRING),
    /**
     * Set name.
     */
    FIELD_SET_NAME("$setName", FieldType.STRING),
    /**
     * Rule name.
     */
    FIELD_RULE_DISPLAY_NAME("$ruleDisplayName", FieldType.STRING),
    /**
     * Set name.
     */
    FIELD_SET_DISPLAY_NAME("$setDisplayName", FieldType.STRING),
    /**
     * Owner namespace.
     */
    FIELD_OWNER_NAMESPACE("$ownerNamespace", FieldType.STRING),
    /**
     * Owner type name.
     */
    FIELD_OWNER_TYPE_NAME("$ownerTypeName", FieldType.STRING),
    /**
     * Owner subject id.
     */
    FIELD_OWNER_SUBJECT_ID("$ownerSubjectId", FieldType.STRING),
    /**
     * Owner valid from.
     */
    FIELD_OWNER_VALID_FROM("$ownerValidFrom", FieldType.TIMESTAMP),
    /**
     * Owner valid to.
     */
    FIELD_OWNER_VALID_TO("$ownerValidTo", FieldType.TIMESTAMP),
    /**
     * Cluster records count.
     */
    FIELD_RECORDS_COUNT("$recordsCount", FieldType.INTEGER),
    /**
     * Matching date.
     */
    FIELD_MATCHING_DATE("$matchingDate", FieldType.TIMESTAMP),
    /**
     * Additional parameters.
     */
    FIELD_ADDITIONAL_PARAMETERS("$additionalParameters", FieldType.COMPOSITE),
    /**
     * Additional parameter name.
     */
    FIELD_ADDITIONAL_PARAMETER_NAME("$name", FieldType.STRING) {
        @Override
        public String getPath() {
            return FIELD_ADDITIONAL_PARAMETERS.getName() + "." + getName();
        }
    },
    /**
     * Additional parameter value.
     */
    FIELD_ADDITIONAL_PARAMETER_VALUE("$value", FieldType.ANY) {
        @Override
        public String getPath() {
            return FIELD_ADDITIONAL_PARAMETERS.getName() + "." + getName();
        }
    };

    /**
     * Constructor.
     *
     * @param field field name
     * @param type field type
     */
    ClusterHeaderField(String field, FieldType type) {
        this.field = field;
        this.type = type;
    }

    /**
     * The name.
     */
    private final String field;
    /**
     * The type.
     */
    private final FieldType type;

    /**
     * {@inheritDoc}
     */
    @Override
    public FieldType getFieldType() {
        return type;
    }
    /**
     * @return the field
     */
    @Override
    public String getName() {
        return field;
    }
}

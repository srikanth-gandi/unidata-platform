/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.dto;

import org.apache.commons.collections4.CollectionUtils;
import org.unidata.mdm.matching.core.type.model.source.table.MatchingTableSource;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * @author Sergey Murskiy on 22.06.2021
 */
public class MatchingTablesModelGetResult {
    /**
     * Matching tables, if asked as standalone.
     */
    private List<MatchingTableSource> matchingTables;

    /**
     * Constructor.
     * @param matchingTables matching tables
     */
    public MatchingTablesModelGetResult(List<MatchingTableSource> matchingTables) {
        super();
        this.matchingTables = matchingTables;
    }

    /**
     * Constructor.
     *
     * @param matchingTable matching table
     */
    public MatchingTablesModelGetResult(MatchingTableSource matchingTable) {
        super();
        this.matchingTables = Collections.singletonList(matchingTable);
    }

    /**
     * @return the matching tables
     */
    public List<MatchingTableSource> getMatchingTables() {
        return Objects.isNull(matchingTables) ? Collections.emptyList() : matchingTables;
    }

    /**
     * @param matchingTables the matching tables to set
     */
    public void setMatchingTables(List<MatchingTableSource> matchingTables) {
        this.matchingTables = matchingTables;
    }

    /**
     * Gets first value.
     *
     * @return matching table
     */
    public MatchingTableSource singleValue() {
        return CollectionUtils.isNotEmpty(matchingTables)
                ? matchingTables.get(0)
                : null;
    }
}

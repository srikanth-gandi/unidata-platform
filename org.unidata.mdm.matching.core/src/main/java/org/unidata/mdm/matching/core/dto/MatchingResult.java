/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.dto;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.unidata.mdm.matching.core.type.cluster.Cluster;
import org.unidata.mdm.matching.core.type.model.instance.MatchingRuleElement;
import org.unidata.mdm.matching.core.type.model.instance.MatchingRuleSetElement;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @author Sergey Murskiy on 09.08.2021
 */
public class MatchingResult {
    /**
     * Collected state, keyed by rule rule set name.
     */
    private final Map<MatchingRuleSetElement, List<RuleExecutionResult>> results = new HashMap<>();
    /**
     * The payload to save.
     */
    private Object payload;

    /**
     * Constructor.
     */
    public MatchingResult() {
        super();
    }

    /**
     * Adds a state to result.
     *
     * @param state the state to add
     */
    public void add(RuleExecutionResult state) {
        if (Objects.nonNull(state)) {

            results
                    .computeIfAbsent(state.getSet(), k -> new ArrayList<>())
                    .add(state);
        }
    }

    /**
     * Adds a collection of states to result.
     *
     * @param states the states to add
     */
    public void addAll(Collection<RuleExecutionResult> states) {
        if (CollectionUtils.isNotEmpty(states)) {
            for (RuleExecutionResult e : states) {
                add(e);
            }
        }
    }

    /**
     * Gets rule execution states by rule rule set name.
     *
     * @param setName the set name
     * @return collection
     */
    public List<RuleExecutionResult> getResults(String setName) {

        for (Map.Entry<MatchingRuleSetElement, List<RuleExecutionResult>> entry : results.entrySet()) {
            if (StringUtils.equals(setName, entry.getKey().getName())) {
                return entry.getValue();
            }
        }

        return Collections.emptyList();
    }

    /**
     * Gets all rule execution states, keyed by rule rule set names.
     *
     * @return all rule execution states, keyed by rule rule set names
     */
    public Map<MatchingRuleSetElement, List<RuleExecutionResult>> getResults() {
        return results;
    }

    /**
     * @return the payload
     */
    @SuppressWarnings("unchecked")
    public <T> T getPayload() {
        return (T) payload;
    }

    /**
     * @param payload the payload to set
     */
    public void setPayload(Object payload) {
        this.payload = payload;
    }

    /**
     * Rule execution result.
     */
    public static class RuleExecutionResult {
        /**
         * The name of the rule.
         */
        private final MatchingRuleElement rule;
        /**
         * The name of the rule rule set.
         */
        private final MatchingRuleSetElement set;
        /**
         * Cluster, collected for the rule.
         */
        private Cluster cluster;
        /**
         * Skip indicator.
         */
        private boolean skipped;

        /**
         * Constructor.
         *
         * @param s the set
         * @param r the rule
         */
        public RuleExecutionResult(MatchingRuleSetElement s, MatchingRuleElement r) {
            super();

            Objects.requireNonNull(r, "Rule rule must not be null.");
            Objects.requireNonNull(s, "Matching rule set must not be null.");

            this.rule = r;
            this.set = s;
        }

        /**
         * Constructor.
         *
         * @param s    the set
         * @param r    the rule
         * @param skip is skip
         */
        public RuleExecutionResult(MatchingRuleSetElement s, MatchingRuleElement r, boolean skip) {
            this(s, r);
            this.skipped = skip;
        }

        /**
         * @return the rule
         */
        public MatchingRuleElement getRule() {
            return rule;
        }

        /**
         * @return the set
         */
        public MatchingRuleSetElement getSet() {
            return set;
        }

        /**
         * Adds an cluster to result.
         *
         * @param cluster the cluster to add
         */
        public void cluster(Cluster cluster) {
            if (Objects.nonNull(cluster)) {
                this.cluster = cluster;
            }
        }

        /**
         * @return the cluster
         */
        public Cluster getCluster() {
            return cluster;
        }

        /**
         * Returns true, if this rule execution has cluster.
         *
         * @return true, if this rule execution has cluster
         */
        public boolean hasCluster() {
            return Objects.nonNull(cluster);
        }

        /**
         * @return the skipped
         */
        public boolean isSkipped() {
            return skipped;
        }

        /**
         * Sets skip indicator to true.
         */
        public void skip() {
            skipped = true;
        }
    }
}

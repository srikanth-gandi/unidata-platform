/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.service.segments;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.core.util.SecurityUtils;
import org.unidata.mdm.draft.context.DraftUpsertContext;
import org.unidata.mdm.draft.dto.DraftUpsertResult;
import org.unidata.mdm.draft.type.Draft;
import org.unidata.mdm.draft.type.Edition;
import org.unidata.mdm.matching.core.context.MatchingModelUpsertContext;
import org.unidata.mdm.matching.core.module.MatchingCoreModule;
import org.unidata.mdm.matching.core.service.impl.MatchingModelComponent;
import org.unidata.mdm.matching.core.type.draft.MatchingModelDraftConstants;
import org.unidata.mdm.matching.core.type.model.MatchingModel;
import org.unidata.mdm.matching.core.type.model.instance.MatchingModelInstance;
import org.unidata.mdm.system.type.pipeline.Finish;
import org.unidata.mdm.system.type.pipeline.Start;
import org.unidata.mdm.system.type.variables.Variables;

import java.util.Date;

/**
 * @author Sergey Murskiy on 22.06.2021
 */
@Component(MatchingDraftUpsertFinishExecutor.SEGMENT_ID)
public class MatchingDraftUpsertFinishExecutor extends Finish<DraftUpsertContext, DraftUpsertResult> {
    /**
     * This segment ID.
     */
    public static final String SEGMENT_ID = MatchingCoreModule.MODULE_ID + "[MATCHING_DRAFT_UPSERT_FINISH]";
    /**
     * Localized message code.
     */
    public static final String SEGMENT_DESCRIPTION = MatchingCoreModule.MODULE_ID + ".matching.draft.upsert.finish.description";

    /**
     * The matching model component.
     */
    @Autowired
    private MatchingModelComponent matchingModelComponent;

    /**
     * Constructor.
     */
    public MatchingDraftUpsertFinishExecutor() {
        super(SEGMENT_ID, SEGMENT_DESCRIPTION, DraftUpsertResult.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DraftUpsertResult finish(DraftUpsertContext ctx) {

        Draft draft = ctx.currentDraft();
        DraftUpsertResult result = new DraftUpsertResult(true);

        // 1. Set draft to result
        result.setDraft(draft);

        // 2. Take snapshot of current upon draft creation
        if (!draft.isExisting()) {

            String storageId = ctx.hasParameter(MatchingModelDraftConstants.STORAGE_ID)
                    ? ctx.getParameter(MatchingModelDraftConstants.STORAGE_ID)
                    : SecurityUtils.getCurrentUserStorageId();

            MatchingModelInstance i = matchingModelComponent.instance(storageId, null);

            result.setVariables(variables(i));
            result.setEdition(snapshot(i));
        }

        // 3. The following stuff runs only, if some data were supplied
        if (ctx.hasPayload()) {
            result.setEdition(edit(draft, ctx.getPayload()));
        }

        return result;
    }

    protected Edition snapshot(MatchingModelInstance i) {

        Edition next = new Edition();
        next.setContent(i.toSource());
        next.setCreateDate(new Date(System.currentTimeMillis()));
        next.setCreatedBy(SecurityUtils.getCurrentUserName());

        return next;
    }

    protected Edition edit(Draft draft, MatchingModelUpsertContext input) {

        input.currentDraft(draft);
        MatchingModel changes = matchingModelComponent.assemble(input);

        Edition next = new Edition();
        next.setContent(changes);
        next.setCreateDate(new Date(System.currentTimeMillis()));
        next.setCreatedBy(SecurityUtils.getCurrentUserName());

        return next;
    }

    protected Variables variables(MatchingModelInstance i) {

        return new Variables()
                .add(MatchingModelDraftConstants.DRAFT_INITIATOR, SecurityUtils.getCurrentUserName())
                .add(MatchingModelDraftConstants.STORAGE_ID, i.getStorageId())
                .add(MatchingModelDraftConstants.DRAFT_START_VERSION, i.getVersion());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean supports(Start<?, ?> start) {
        return DraftUpsertContext.class.isAssignableFrom(start.getInputTypeClass());
    }
}

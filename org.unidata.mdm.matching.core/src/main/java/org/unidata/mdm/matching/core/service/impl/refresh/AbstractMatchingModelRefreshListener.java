package org.unidata.mdm.matching.core.service.impl.refresh;

import org.springframework.beans.factory.annotation.Autowired;
import org.unidata.mdm.core.context.ModelRefreshContext;
import org.unidata.mdm.core.context.PreviousModelStateContext;
import org.unidata.mdm.core.service.MetaModelService;
import org.unidata.mdm.core.service.UPathService;
import org.unidata.mdm.core.util.SecurityUtils;
import org.unidata.mdm.matching.core.configuration.MatchingDescriptors;
import org.unidata.mdm.matching.core.configuration.MatchingModelIds;
import org.unidata.mdm.matching.core.dao.MatchingModelDAO;
import org.unidata.mdm.matching.core.po.MatchingModelPO;
import org.unidata.mdm.matching.core.serialization.MatchingSerializer;
import org.unidata.mdm.matching.core.type.model.refresh.MatchingModelRefreshListener;
import org.unidata.mdm.matching.core.service.impl.MatchingAlgorithmsCacheComponent;
import org.unidata.mdm.matching.core.service.impl.instance.MatchingModelInstanceImpl;
import org.unidata.mdm.matching.core.type.model.instance.MatchingModelInstance;

import java.util.Objects;

/**
 * @author Sergey Murskiy on 29.08.2021
 */
public abstract class AbstractMatchingModelRefreshListener implements MatchingModelRefreshListener {
    /**
     * Matching model DAO.
     */
    @Autowired
    private MatchingModelDAO matchingModelDAO;
    /**
     * Upath service.
     */
    @Autowired
    private UPathService uPathService;
    /**
     * Matching algorithms cache component.
     */
    @Autowired
    private MatchingAlgorithmsCacheComponent algorithmsCacheComponent;
    /**
     * Meta model service.
     */
    @Autowired
    protected MetaModelService metaModelService;

    /**
     * {@inheritDoc}
     */
    @Override
    public String getTypeId() {
        return MatchingModelIds.MATCHING;
    }

    /**
     * Gets previous matching model instance.
     *
     * @param ctx model refresh context
     * @return previous matching model instance
     */
    protected MatchingModelInstance getPreviousState(ModelRefreshContext ctx) {

        MatchingModelInstance previousInstance = null;
        if (ctx instanceof PreviousModelStateContext) {
            previousInstance = ((PreviousModelStateContext) ctx).prevousState();
        }

        if (Objects.isNull(previousInstance)) {

            String storageId = SecurityUtils.getStorageId(ctx);
            MatchingModelInstance fresh = metaModelService.instance(MatchingDescriptors.MATCHING, storageId, null);

            if (fresh.getVersion() == 1) {
                return null;
            }

            MatchingModelPO prev = matchingModelDAO.previous(storageId, fresh.getVersion() - 1);
            previousInstance = new MatchingModelInstanceImpl(
                    MatchingSerializer.modelFromCompressedXml(prev.getContent()),
                    uPathService,
                    algorithmsCacheComponent);

            if (ctx instanceof PreviousModelStateContext) {
                ((PreviousModelStateContext) ctx).prevousState(previousInstance);
            }
        }

        return previousInstance;
    }
}

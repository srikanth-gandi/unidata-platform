/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.context;

/**
 * @author Sergey Murskiy on 06.09.2021
 */
public class ClusterGetContext {
    /**
     * Cluster id.
     */
    private final String id;
    /**
     * Fetch cluster records.
     */
    private final boolean fetchClusterRecords;

    /**
     * Constructor.
     *
     * @param b builder
     */
    private ClusterGetContext(ClusterGetContextBuilder b) {
        this.id = b.id;
        this.fetchClusterRecords = b.fetchClusterRecords;
    }

    /**
     * Gets cluster id.
     *
     * @return the cluster id
     */
    public String getId() {
        return id;
    }

    /**
     * Is fetch cluster records.
     *
     * @return is fetch cluster records
     */
    public boolean isFetchClusterRecords() {
        return fetchClusterRecords;
    }

    /**
     * Gets builder.
     *
     * @return the builder
     */
    public static ClusterGetContextBuilder builder() {
        return new ClusterGetContextBuilder();
    }

    /**
     * Builder class.
     */
    public static class ClusterGetContextBuilder {
        /**
         * Cluster id.
         */
        private String id;
        /**
         * Fetch cluster records.
         */
        private boolean fetchClusterRecords;

        /**
         * Sets cluster id.
         *
         * @param id the cluster id
         * @return self
         */
        public ClusterGetContextBuilder id(String id) {
            this.id = id;
            return this;
        }

        /**
         * Sets fetchClusterRecords.
         *
         * @param fetchClusterRecords the 'fetchClusterRecords' to set
         * @return self
         */
        public ClusterGetContextBuilder fetchClusterRecords(boolean fetchClusterRecords) {
            this.fetchClusterRecords = fetchClusterRecords;
            return this;
        }

        /**
         * Build.
         *
         * @return cluster get context
         */
        public ClusterGetContext build() {
            return new ClusterGetContext(this);
        }
    }
}

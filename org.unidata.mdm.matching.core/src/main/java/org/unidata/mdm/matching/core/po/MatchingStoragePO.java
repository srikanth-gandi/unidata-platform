package org.unidata.mdm.matching.core.po;

import org.unidata.mdm.matching.core.type.storage.MatchingStorageStatus;

/**
 * @author Sergey Murskiy on 07.09.2021
 */
public class MatchingStoragePO {
    /**
     * Table name.
     */
    public static final String TABLE_NAME = "matching_storages_info";
    /**
     * Storage ID field.
     */
    public static final String FIELD_ID = "id";
    /**
     * Revision.
     */
    public static final String FIELD_STATUS = "status";

    /**
     * Matching storage id.
     */
    private String id;
    /**
     * Matching storage status.
     */
    private MatchingStorageStatus status;

    /**
     * Gets matching storage id.
     *
     * @return the matching storage id
     */
    public String getId() {
        return id;
    }

    /**
     * Sets matching storage id.
     *
     * @param id the matching storage id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Gets matching storage status.
     *
     * @return status
     */
    public MatchingStorageStatus getStatus() {
        return status;
    }

    /**
     * Sets matching storage status.
     *
     * @param status status to set
     */
    public void setStatus(MatchingStorageStatus status) {
        this.status = status;
    }
}

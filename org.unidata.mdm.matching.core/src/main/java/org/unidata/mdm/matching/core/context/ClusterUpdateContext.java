/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.context;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.unidata.mdm.matching.core.type.cluster.Cluster;
import org.unidata.mdm.matching.core.type.cluster.ClusterRecord;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @author Sergey Murskiy on 06.09.2021
 */
public class ClusterUpdateContext {
    /**
     * Cluster id to delete.
     */
    private final List<String> clusterDelete;
    /**
     * Clusters to insert.
     */
    private final List<Cluster> clusterInsert;
    /**
     * Clusters to update.
     */
    private final List<Cluster> clusterUpdate;
    /**
     * Cluster records to delete.
     */
    private final Map<String, ClusterRecord> clusterRecordDelete;
    /**
     * Cluster records to insert.
     */
    private final Map<String, ClusterRecord> clusterRecordInsert;

    /**
     * Constructor.
     *
     * @param b builder
     */
    public ClusterUpdateContext(ClusterUpdateContextBuilder b) {
        this.clusterDelete = b.clusterDelete;
        this.clusterUpdate = b.clusterUpdate;
        this.clusterInsert = b.clusterInsert;
        this.clusterRecordDelete = b.clusterRecordDelete;
        this.clusterRecordInsert = b.clusterRecordInsert;
    }

    /**
     * Get cluster ids to delete.
     *
     * @return cluster ids to delete
     */
    public List<String> getClusterDelete() {
        return ListUtils.emptyIfNull(clusterDelete);
    }

    /**
     * Gets clusters to insert.
     *
     * @return clusters to insert
     */
    public List<Cluster> getClusterInsert() {
        return ListUtils.emptyIfNull(clusterInsert);
    }

    /**
     * Gets clusters to update.
     *
     * @return clusters to update
     */
    public List<Cluster> getClusterUpdate() {
        return ListUtils.emptyIfNull(clusterUpdate);
    }

    /**
     * Gets cluster records delete.
     *
     * @return cluster records to delete
     */
    public Map<String, ClusterRecord> getClusterRecordDelete() {
        return MapUtils.emptyIfNull(clusterRecordDelete);
    }

    /**
     * Gets cluster records to insert.
     *
     * @return cluster records to insert
     */
    public Map<String, ClusterRecord> getClusterRecordInsert() {
        return MapUtils.emptyIfNull(clusterRecordInsert);
    }

    /**
     * Gets context builder.
     *
     * @return the builder
     */
    public static ClusterUpdateContextBuilder builder() {
        return new ClusterUpdateContextBuilder();
    }

    /**
     * Builder class.
     */
    public static class ClusterUpdateContextBuilder {
        /**
         * Cluster ids to delete.
         */
        private List<String> clusterDelete;
        /**
         * Clusters to insert.
         */
        private List<Cluster> clusterInsert;
        /**
         * Clusters to update.
         */
        private List<Cluster> clusterUpdate;
        /**
         * Cluster records to delete.
         */
        private Map<String, ClusterRecord> clusterRecordDelete;
        /**
         * Cluster records to insert.
         */
        private Map<String, ClusterRecord> clusterRecordInsert;

        /**
         * Add cluster id to delete.
         *
         * @param clusterId cluster id
         * @return self
         */
        public ClusterUpdateContextBuilder clusterDelete(String clusterId) {
            if (StringUtils.isBlank(clusterId)) {
                return this;
            }

            return clusterDelete(Collections.singletonList(clusterId));
        }

        /**
         * Add cluster ids to delete.
         *
         * @param clusterIds cluster ids
         * @return self
         */
        public ClusterUpdateContextBuilder clusterDelete(Collection<String> clusterIds) {
            if (CollectionUtils.isEmpty(clusterIds)) {
                return this;
            }

            if (Objects.isNull(clusterDelete)) {
                clusterDelete = new ArrayList<>();
            }

            clusterDelete.addAll(clusterIds);

            return this;
        }

        /**
         * Add cluster to insert.
         *
         * @param cluster the cluster
         * @return self
         */
        public ClusterUpdateContextBuilder clusterInsert(Cluster cluster) {
            if (Objects.isNull(cluster)) {
                return this;
            }

            return clusterInsert(Collections.singletonList(cluster));
        }

        /**
         * Add clusters to insert.
         *
         * @param clusters clusters
         * @return self
         */
        public ClusterUpdateContextBuilder clusterInsert(Collection<Cluster> clusters) {
            if (CollectionUtils.isEmpty(clusters)) {
                return this;
            }

            if (Objects.isNull(clusterInsert)) {
                clusterInsert = new ArrayList<>();
            }

            clusterInsert.addAll(clusters);

            return this;
        }

        /**
         * Add cluster to update.
         *
         * @param cluster the cluster
         * @return self
         */
        public ClusterUpdateContextBuilder clusterUpdate(Cluster cluster) {
            if (Objects.isNull(cluster)) {
                return this;
            }

            return clusterUpdate(Collections.singletonList(cluster));
        }

        /**
         * Add clusters to update.
         *
         * @param clusters clusters
         * @return self
         */
        public ClusterUpdateContextBuilder clusterUpdate(Collection<Cluster> clusters) {
            if (CollectionUtils.isEmpty(clusters)) {
                return this;
            }

            if (Objects.isNull(clusterUpdate)) {
                clusterUpdate = new ArrayList<>();
            }

            clusterUpdate.addAll(clusters);

            return this;
        }

        /**
         * Add cluster record to insert.
         *
         * @param clusterId     the cluster id
         * @param clusterRecord the cluster record
         * @return self
         */
        public ClusterUpdateContextBuilder clusterRecordInsert(String clusterId, ClusterRecord clusterRecord) {

            if (StringUtils.isBlank(clusterId) || Objects.isNull(clusterRecord)) {
                return this;
            }

            return clusterRecordInsert(Collections.singletonMap(clusterId, clusterRecord));
        }

        /**
         * Add cluster records to insert.
         *
         * @param clusterRecords cluster records
         * @return self
         */
        public ClusterUpdateContextBuilder clusterRecordInsert(Map<String, ClusterRecord> clusterRecords) {

            if (MapUtils.isEmpty(clusterRecords)) {
                return this;
            }

            if (Objects.isNull(clusterRecordInsert)) {
                clusterRecordInsert = new HashMap<>();
            }

            clusterRecordInsert.putAll(clusterRecords);

            return this;
        }

        /**
         * Add cluster record to delete.
         *
         * @param clusterId     the cluster id
         * @param clusterRecord the cluster record
         * @return self
         */
        public ClusterUpdateContextBuilder clusterRecordDelete(String clusterId, ClusterRecord clusterRecord) {

            if (StringUtils.isBlank(clusterId) || Objects.isNull(clusterRecord)) {
                return this;
            }

            return clusterRecordDelete(Collections.singletonMap(clusterId, clusterRecord));
        }

        /**
         * Add cluster records to delete.
         *
         * @param clusterRecords cluster records
         * @return self
         */
        public ClusterUpdateContextBuilder clusterRecordDelete(Map<String, ClusterRecord> clusterRecords) {

            if (MapUtils.isEmpty(clusterRecords)) {
                return this;
            }

            if (Objects.isNull(clusterRecordDelete)) {
                clusterRecordDelete = new HashMap<>();
            }

            clusterRecordDelete.putAll(clusterRecords);

            return this;
        }

        /**
         * Add cluster records to delete.
         *
         * @param clusterId      the cluster id
         * @param clusterRecords cluster records
         * @return self
         */
        public ClusterUpdateContextBuilder clusterRecordDelete(String clusterId,
                                                               Collection<ClusterRecord> clusterRecords) {

            if (StringUtils.isBlank(clusterId) || CollectionUtils.isEmpty(clusterRecords)) {
                return this;
            }

            if (Objects.isNull(clusterRecordDelete)) {
                clusterRecordDelete = new HashMap<>();
            }

            clusterRecords.forEach(clusterRecord -> clusterRecordDelete.put(clusterId, clusterRecord));

            return this;
        }

        /**
         * Build.
         *
         * @return cluster update context
         */
        public ClusterUpdateContext build() {
            return new ClusterUpdateContext(this);
        }
    }
}

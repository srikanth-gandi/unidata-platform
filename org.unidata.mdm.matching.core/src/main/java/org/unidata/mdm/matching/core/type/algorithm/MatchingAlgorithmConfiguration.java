/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.type.algorithm;

import org.apache.commons.collections4.CollectionUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * @author Sergey Murskiy on 23.06.2021
 */
public class MatchingAlgorithmConfiguration {
    /**
     * Algorithm parameters.
     */
    private final List<AlgorithmParamConfiguration<?>> parameters;

    /**
     * Constructor.
     */
    public MatchingAlgorithmConfiguration(MatchingAlgorithmConfigurationBuilder b) {
        super();
        this.parameters = b.parameters;
    }

    /**
     * @return the parameters
     */
    public List<AlgorithmParamConfiguration<?>> getParameters() {
        if (CollectionUtils.isEmpty(parameters)) {
            return Collections.emptyList();
        }

        return parameters;
    }

    /**
     * Algorithm configuration builder instance.
     *
     * @return builder instance
     */
    public static MatchingAlgorithmConfigurationBuilder configuration() {
        return new MatchingAlgorithmConfigurationBuilder();
    }

    /**
     * @author Mikhail Mikhailov on Feb 4, 2021
     */
    public static class MatchingAlgorithmConfigurationBuilder {
        /**
         * Parameters.
         */
        private List<AlgorithmParamConfiguration<?>> parameters;

        /**
         * Constructor.
         */
        private MatchingAlgorithmConfigurationBuilder() {
            super();
        }

        /**
         * @param param the parameter to set
         */
        public MatchingAlgorithmConfigurationBuilder parameter(AlgorithmParamConfiguration<?> param) {
            if (Objects.nonNull(param)) {
                if (Objects.isNull(this.parameters)) {
                    this.parameters = new ArrayList<>();
                }
                this.parameters.add(param);
            }
            return this;
        }

        /**
         * @param params the parameters to set
         */
        public MatchingAlgorithmConfigurationBuilder parameter(List<AlgorithmParamConfiguration<?>> params) {
            if (CollectionUtils.isNotEmpty(params)) {
                if (Objects.isNull(this.parameters)) {
                    this.parameters = new ArrayList<>();
                }
                this.parameters.addAll(params);
            }
            return this;
        }

        /**
         * Builds configuration instance.
         *
         * @return configuration instance
         */
        public MatchingAlgorithmConfiguration build() {
            return new MatchingAlgorithmConfiguration(this);
        }
    }
}

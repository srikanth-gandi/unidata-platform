create table matching_storages_info
(
    id     varchar(256) not null,
    status varchar(256),
    constraint pk_matching_storages_info_id primary key (id)
);

package org.unidata.mdm.meta.configuration;

import org.unidata.mdm.core.type.model.ModelImplementation;

/**
 * @author Mikhail Mikhailov on Oct 11, 2020
 * Model type ids this module exports.
 */
public final class TypeIds {
    /**
     * Data model type id.
     * The corresponding {@link ModelImplementation} singleton will have this qualifier.
     */
    public static final String DATA_MODEL = "DATA";
    /**
     * Source systems model type id.
     * The corresponding {@link ModelImplementation} singleton will have this qualifier.
     */
    public static final String SOURCE_SYSTEMS_MODEL = "SOURCE_SYSTEMS";
    /**
     * Enumerations model type id.
     * The corresponding {@link ModelImplementation} singleton will have this qualifier.
     */
    public static final String ENUMERATIONS_MODEL = "ENUMERATIONS";
    /**
     * Measurement units model type id.
     * The corresponding {@link ModelImplementation} singleton will have this qualifier.
     */
    public static final String MEASUREMENT_UNITS_MODEL = "MEASUREMENT_UNITS";
    /**
     * Constructor.
     */
    private TypeIds() {
        super();
    }
}

package org.unidata.mdm.meta.type.model.attributes;

import org.unidata.mdm.meta.type.model.SimpleDataType;

/**
 * Attribute with simple data type
 *
 * @author Alexandr Serov
 * @see SimpleDataType
 * @since 26.08.2020
 **/
public interface SimpleTypeMetaModelAttribute extends SearchableMetaModelAttribute {

    SimpleDataType getSimpleDataType();

    void setSimpleDataType(SimpleDataType value);

}

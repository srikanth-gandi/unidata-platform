package org.unidata.mdm.meta.type.model.strategy;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import org.unidata.mdm.meta.type.model.ValueGenerationStrategyType;

import static org.unidata.mdm.meta.type.model.ModelNamespace.META_MODEL_NAMESPACE;


@JsonPropertyOrder({"attributes", "separator"})
public class ConcatenatedValueGenerationStrategy extends AbstractValueGenerationStrategy implements Serializable {

    private final static long serialVersionUID = 987654321L;

    @JacksonXmlProperty(namespace = META_MODEL_NAMESPACE)
    protected List<String> attributes;

    protected String separator;

    public ConcatenatedValueGenerationStrategy() {
        super(ValueGenerationStrategyType.CONCAT);
    }

    public List<String> getAttributes() {
        if (attributes == null) {
            attributes = new ArrayList<String>();
        }
        return this.attributes;
    }

    public String getSeparator() {
        return separator;
    }

    public void setSeparator(String value) {
        this.separator = value;
    }

    public ConcatenatedValueGenerationStrategy withAttributes(String... values) {
        if (values!= null) {
            for (String value: values) {
                getAttributes().add(value);
            }
        }
        return this;
    }

    public ConcatenatedValueGenerationStrategy withAttributes(Collection<String> values) {
        if (values!= null) {
            getAttributes().addAll(values);
        }
        return this;
    }

    public ConcatenatedValueGenerationStrategy withSeparator(String value) {
        setSeparator(value);
        return this;
    }

}

/**
 * @author Mikhail Mikhailov on Dec 1, 2020
 * Types related to SOURCE SYSTEMS.
 */
package org.unidata.mdm.meta.service.impl.sourcesystems;
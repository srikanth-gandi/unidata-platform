package org.unidata.mdm.meta.type.model.strategy;

import java.io.Serializable;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import org.unidata.mdm.meta.type.model.ValueGenerationStrategyType;


public class CustomValueGenerationStrategy extends AbstractValueGenerationStrategy implements Serializable {

    private final static long serialVersionUID = 987654321L;

    @JacksonXmlProperty(isAttribute = true)
    protected String className;

    public CustomValueGenerationStrategy() {
        super(ValueGenerationStrategyType.CUSTOM);
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String value) {
        this.className = value;
    }

    public CustomValueGenerationStrategy withClassName(String value) {
        setClassName(value);
        return this;
    }

}

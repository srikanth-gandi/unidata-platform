package org.unidata.mdm.meta.configuration;

import org.unidata.mdm.core.type.model.ModelDescriptor;
import org.unidata.mdm.meta.type.instance.DataModelInstance;
import org.unidata.mdm.meta.type.instance.EnumerationsInstance;
import org.unidata.mdm.meta.type.instance.MeasurementUnitsInstance;
import org.unidata.mdm.meta.type.instance.SourceSystemsInstance;

/**
 * @author Mikhail Mikhailov on Oct 8, 2020
 * Base meta model constants.
 */
public final class Descriptors {
    /**
     * The data descriptor.
     */
    public static final ModelDescriptor<DataModelInstance> DATA
        = ModelDescriptor.of(TypeIds.DATA_MODEL, DataModelInstance.class);
    /**
     * The SS descriptor.
     */
    public static final ModelDescriptor<SourceSystemsInstance> SOURCE_SYSTEMS
        = ModelDescriptor.of(TypeIds.SOURCE_SYSTEMS_MODEL, SourceSystemsInstance.class);
    /**
     * The enumerations descriptor.
     */
    public static final ModelDescriptor<EnumerationsInstance> ENUMERATIONS
        = ModelDescriptor.of(TypeIds.ENUMERATIONS_MODEL, EnumerationsInstance.class);
    /**
     * The measurement units descriptor.
     */
    public static final ModelDescriptor<MeasurementUnitsInstance> MEASUREMENT_UNITS
        = ModelDescriptor.of(TypeIds.MEASUREMENT_UNITS_MODEL, MeasurementUnitsInstance.class);
    /**
     * Constructor.
     */
    private Descriptors() {
        super();
    }
}

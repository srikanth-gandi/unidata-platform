package org.unidata.mdm.meta.type.model.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import static org.unidata.mdm.meta.type.model.ModelNamespace.META_MODEL_NAMESPACE;

public class RelationGroup implements Serializable {

    private final static long serialVersionUID = 987654321L;

    @JacksonXmlProperty(namespace = META_MODEL_NAMESPACE)
    protected List<String> relations;
    @JacksonXmlProperty(isAttribute = true)
    protected int row;
    @JacksonXmlProperty(isAttribute = true)
    protected int column;
    @JacksonXmlProperty(isAttribute = true)
    protected RelType relType;
    @JacksonXmlProperty(isAttribute = true)
    protected String title;

    public List<String> getRelations() {
        if (relations == null) {
            relations = new ArrayList<>();
        }
        return this.relations;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int value) {
        this.row = value;
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int value) {
        this.column = value;
    }

    public RelType getRelType() {
        return relType;
    }

    public void setRelType(RelType value) {
        this.relType = value;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String value) {
        this.title = value;
    }

    public RelationGroup withRelations(String... values) {
        if (values!= null) {
            Collections.addAll(getRelations(), values);
        }
        return this;
    }

    public RelationGroup withRelations(Collection<String> values) {
        if (values!= null && !values.isEmpty()) {
            getRelations().addAll(values);
        }
        return this;
    }

    public RelationGroup withRow(int value) {
        setRow(value);
        return this;
    }

    public RelationGroup withColumn(int value) {
        setColumn(value);
        return this;
    }

    public RelationGroup withRelType(RelType value) {
        setRelType(value);
        return this;
    }

    public RelationGroup withTitle(String value) {
        setTitle(value);
        return this;
    }

}

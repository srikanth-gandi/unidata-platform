/**
 * @author Mikhail Mikhailov on Dec 1, 2020
 * Data instance types.
 */
package org.unidata.mdm.meta.service.impl.data.instance;
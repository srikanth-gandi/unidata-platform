package org.unidata.mdm.meta.type.model;

import java.util.List;

import org.unidata.mdm.meta.type.model.attributes.ArrayMetaModelAttribute;
import org.unidata.mdm.meta.type.model.attributes.AttributeGroup;
import org.unidata.mdm.meta.type.model.attributes.SimpleMetaModelAttribute;
import org.unidata.mdm.meta.type.model.entities.RelationGroup;
import org.unidata.mdm.meta.type.model.merge.MergeSettings;

/**
 * Model entity interface
 *
 * @author Alexandr Serov
 * @since 27.08.2020
 **/
public interface MetaModelEntity extends DisplayableElement {

    MergeSettings getMergeSettings();

    void setMergeSettings(MergeSettings value);

    PeriodBoundary getValidityPeriod();

    void setValidityPeriod(PeriodBoundary value);

    List<AttributeGroup> getAttributeGroups();

    List<RelationGroup> getRelationGroups();

    List<SimpleMetaModelAttribute> getSimpleAttribute();

    List<ArrayMetaModelAttribute> getArrayAttribute();

    void setSimpleAttribute(List<SimpleMetaModelAttribute> simpleAttribute);

    void setArrayAttribute(List<ArrayMetaModelAttribute> arrayAttribute);
}

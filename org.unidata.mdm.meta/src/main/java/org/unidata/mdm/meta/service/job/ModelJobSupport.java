/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.meta.service.job;

import static java.util.stream.Collectors.toCollection;
import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.unidata.mdm.core.service.MetaModelService;
import org.unidata.mdm.core.type.model.EntityElement;
import org.unidata.mdm.core.util.JobUtils;
import org.unidata.mdm.meta.configuration.Descriptors;
import org.unidata.mdm.meta.type.instance.DataModelInstance;
import org.unidata.mdm.search.util.SearchUtils;

/**
 * @author Mikhail Mikhailov on Dec 19, 2019
 */
public interface ModelJobSupport {

    MetaModelService metaModelService();

    default List<String> getAllEntitiesList() {

        DataModelInstance i = metaModelService().instance(Descriptors.DATA);
        return Stream.concat(
                i.getRegisters().stream(),
                i.getLookups().stream())
            .map(EntityElement::getName)
            .sorted(String::compareTo)
            .collect(Collectors.toList());
    }

    default List<String> getJustEntitiesList() {
        return metaModelService().instance(Descriptors.DATA)
                .getRegisters().stream()
                .map(EntityElement::getName)
                .collect(toList());
    }

    default List<String> getJustLookupEntitiesList() {
        return metaModelService().instance(Descriptors.DATA)
                .getLookups().stream()
                .map(EntityElement::getName)
                .collect(toList());
    }

    default Collection<String> ensureEntitiesList(Collection<String> names) {

        if (CollectionUtils.isEmpty(names)) {
            return Collections.emptyList();
        } else if (names.contains(JobUtils.JOB_ALL)) {
            return metaModelService()
                    .instance(Descriptors.DATA)
                    .getElements()
                    .stream()
                    .filter(el -> el.isLookup() || el.isRegister())
                    .map(EntityElement::getName)
                    .collect(Collectors.toList());
        }

        return names;
    }

    /**
     * @return collection of name of entities
     */
    default List<String> getEntityList(String entityNames) {

        List<String> reindexTypes = new ArrayList<>();
        boolean reindexAll = StringUtils.contains(entityNames, JobUtils.JOB_ALL);
        if (reindexAll) {
            metaModelService().instance(Descriptors.DATA)
                            .getRegisters()
                            .stream()
                            .map(EntityElement::getName)
                            .collect(toCollection(() -> reindexTypes));

            metaModelService().instance(Descriptors.DATA)
                            .getLookups()
                            .stream()
                            .map(EntityElement::getName)
                            .collect(toCollection(() -> reindexTypes));
        } else {
            if (entityNames != null) {
                String[] tokens = entityNames.split(SearchUtils.COMMA_SEPARATOR);
                Collections.addAll(reindexTypes, tokens);
            }
        }

        return reindexTypes;
    }
}

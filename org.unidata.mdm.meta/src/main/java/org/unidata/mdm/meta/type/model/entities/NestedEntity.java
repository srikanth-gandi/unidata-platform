package org.unidata.mdm.meta.type.model.entities;

import java.io.Serializable;

public class NestedEntity extends ComplexAttributesHolderEntity<NestedEntity> implements Serializable {
    /**
     * GSVUID.
     */
    private static final long serialVersionUID = -2154224099200030942L;
}

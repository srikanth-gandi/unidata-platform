package org.unidata.mdm.meta.type.model.entities;

import static org.unidata.mdm.meta.type.model.ModelNamespace.META_MODEL_NAMESPACE;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.unidata.mdm.meta.type.model.attributes.ComplexMetaModelAttribute;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public abstract class ComplexAttributesHolderEntity<X extends ComplexAttributesHolderEntity<X>> extends AbstractEntity<X> {
    /**
     * GSVUID.
     */
    private static final long serialVersionUID = -8297550992004927681L;

    @JacksonXmlProperty(namespace = META_MODEL_NAMESPACE)
    private List<ComplexMetaModelAttribute> complexAttribute;

    public List<ComplexMetaModelAttribute> getComplexAttribute() {
        if (complexAttribute == null) {
            complexAttribute = new ArrayList<>();
        }
        return this.complexAttribute;
    }

    public void setComplexAttributes(List<ComplexMetaModelAttribute> values) {
        withComplexAttribute(values);
    }

    public X withComplexAttribute(ComplexMetaModelAttribute... values) {
        if (ArrayUtils.isNotEmpty(values)) {
            Collections.addAll(getComplexAttribute(), values);
        }
        return self();
    }

    public X withComplexAttribute(Collection<ComplexMetaModelAttribute> values) {
        if (CollectionUtils.isNotEmpty(values)) {
            getComplexAttribute().addAll(values);
        }
        return self();
    }
}

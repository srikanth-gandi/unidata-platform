/**
 * Types related to MM rendering.
 *
 * @author Mikhail Mikhailov on Feb 13, 2020
 */
package org.unidata.mdm.meta.type.rendering;
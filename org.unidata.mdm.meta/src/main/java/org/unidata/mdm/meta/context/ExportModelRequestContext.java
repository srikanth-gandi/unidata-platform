/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.meta.context;

import java.io.Serializable;
import java.nio.file.Path;

import org.unidata.mdm.meta.service.segments.io.ModelExportStartExecutor;
import org.unidata.mdm.system.context.AbstractCompositeRequestContext;
import org.unidata.mdm.system.type.pipeline.PipelineInput;

public class ExportModelRequestContext
        extends AbstractCompositeRequestContext
        implements PipelineInput, Serializable {

    private final String storageId;

    private final Path path;
    private final ExportContext exportContext;

    private ExportModelRequestContext(Builder b) {
        super(b);
        this.storageId = b.storageId;
        this.exportContext = b.exportContext;
        this.path = b.path;
    }


    @Override
    public String getStartTypeId() {
        return ModelExportStartExecutor.SEGMENT_ID;
    }


    public static Builder builder() {
        return new Builder();
    }

    public String getStorageId() {
        return storageId;
    }

    public Path getPath() {
        return path;
    }

    public ExportContext getExportContext() {
        return exportContext;
    }

    public static class Builder extends AbstractCompositeRequestContextBuilder<Builder> {


        private String storageId;
        private Path path;
        private ExportContext exportContext;

        @Override
        public ExportModelRequestContext build() {
            return new ExportModelRequestContext(this);
        }


        public Builder storageId(String storageId) {
            this.storageId = storageId;
            return this;
        }

        public Builder path(java.nio.file.Path path) {
            this.path = path;
            return this;
        }

        public Builder exportContext(ExportContext exportContext) {
            this.exportContext = exportContext;

            return this;
        }
    }
}

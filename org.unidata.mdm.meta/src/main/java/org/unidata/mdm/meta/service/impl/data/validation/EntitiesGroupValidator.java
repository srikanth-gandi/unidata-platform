/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.meta.service.impl.data.validation;

import java.util.ArrayList;
import java.util.Collection;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.springframework.stereotype.Component;
import org.unidata.mdm.meta.type.model.entities.EntitiesGroup;
import org.unidata.mdm.system.exception.ValidationResult;

@Component
public class EntitiesGroupValidator extends AbstractDataModelElementValidator<EntitiesGroup> {

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public DataModelElementType getSupportedElementType() {
        return DataModelElementType.ENTITIES_GROUP;
    }

    @Nullable
    @Override
    public String getModelElementId(@Nonnull EntitiesGroup modelElement) {
        return modelElement.getName();
    }

    @Override
    public Collection<ValidationResult> checkElement(EntitiesGroup modelElement) {
        final Collection<ValidationResult> errors = new ArrayList<>();
        errors.addAll(super.checkElement(modelElement));
        modelElement.getInnerGroups().forEach(el -> errors.addAll(checkElement(el)));
        return errors;
    }
}

/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.meta.configuration;

import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.beans.factory.config.PropertiesFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.unidata.mdm.system.configuration.ModuleConfiguration;
import org.unidata.mdm.system.util.DataSourceUtils;

/**
 * @author Alexander Malyshev
 */
@Configuration
@EnableTransactionManagement
public class MetaConfiguration extends ModuleConfiguration {
    /**
	 * This id.
	 */
	public static final ConfigurationId ID = () -> "META_CONFIGURATION";
	/**
	 * {@inheritDoc}
	 */
    @Override
    public ConfigurationId getId() {
		return ID;
	}

    @Bean(name = "metaDataSource")
    public DataSource metaDataSource() {
    	Properties properties = getPropertiesByPrefix(MetaConfigurationConstants.META_DATASOURCE_PROPERTIES_PREFIX, true);
    	return DataSourceUtils.newPoolingXADataSource(properties);
    }

    @Bean("data-model-sql")
    public PropertiesFactoryBean modelsSql() {
        final PropertiesFactoryBean propertiesFactoryBean = new PropertiesFactoryBean();
        propertiesFactoryBean.setLocation(new ClassPathResource("/db/data-model-sql.xml"));
        return propertiesFactoryBean;
    }

    @Bean("source-systems-model-sql")
    public PropertiesFactoryBean sourceSystemsSql() {
        final PropertiesFactoryBean propertiesFactoryBean = new PropertiesFactoryBean();
        propertiesFactoryBean.setLocation(new ClassPathResource("/db/source-systems-model-sql.xml"));
        return propertiesFactoryBean;
    }

    @Bean("enumerations-model-sql")
    public PropertiesFactoryBean enumerationsSql() {
        final PropertiesFactoryBean propertiesFactoryBean = new PropertiesFactoryBean();
        propertiesFactoryBean.setLocation(new ClassPathResource("/db/enumerations-model-sql.xml"));
        return propertiesFactoryBean;
    }

    @Bean("measurement-units-model-sql")
    public PropertiesFactoryBean measurementUnitsSql() {
        final PropertiesFactoryBean propertiesFactoryBean = new PropertiesFactoryBean();
        propertiesFactoryBean.setLocation(new ClassPathResource("/db/measurement-units-model-sql.xml"));
        return propertiesFactoryBean;
    }
}

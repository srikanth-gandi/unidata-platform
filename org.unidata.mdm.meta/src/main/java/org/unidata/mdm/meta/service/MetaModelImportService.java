/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 * 
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.meta.service;

import java.io.InputStream;
import java.nio.file.Path;

import org.unidata.mdm.core.service.ImportStructureService;
import org.unidata.mdm.meta.dto.FullModelDTO;
import org.unidata.mdm.meta.type.input.meta.MetaExistence;
import org.unidata.mdm.meta.type.input.meta.MetaGraph;
import org.unidata.mdm.meta.type.input.meta.MetaType;

/**
 * @author maria.chistyakova
 */
public interface MetaModelImportService extends ImportStructureService<FullModelDTO> {
    void importModel(InputStream inputStream, boolean recreate);

    void importMeasureUnits(InputStream metaModelInputStream);

    MetaGraph apply(MetaGraph graph);

    void fillResponseMetaGraphWithCurrentData(MetaGraph result);

    Path getRootPath(Path pathToZipFile, String fileName);

    void cleanupTree(Path zipFile, Path rootFolder);

    void enrich(MetaGraph metaGraph, MetaExistence aNew, MetaType[] values);
}

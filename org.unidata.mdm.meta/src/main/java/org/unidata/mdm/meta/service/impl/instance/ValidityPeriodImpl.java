package org.unidata.mdm.meta.service.impl.instance;

import java.util.Date;

import org.unidata.mdm.core.type.model.ValidityPeriodElement;
import org.unidata.mdm.meta.type.model.PeriodBoundary;
import org.unidata.mdm.meta.util.ValidityPeriodUtils;
import org.unidata.mdm.system.util.ConvertUtils;

/**
 * @author Mikhail Mikhailov on Oct 16, 2020
 * Validity period wrapper.
 */
public class ValidityPeriodImpl implements ValidityPeriodElement {
    /**
     * Period start.
     */
    private final Date start;
    /**
     * Period end.
     */
    private final Date end;
    /**
     * Was defined.
     */
    private final boolean defined;
    /**
     * The mode.
     */
    private final Granularity granularity;
    /**
     * Constructor.
     */
    public ValidityPeriodImpl(PeriodBoundary b) {
        super();
        this.defined = b != null;
        this.start = b != null ? ConvertUtils.localDateTime2Date(b.getStart()) : null;
        this.end = b != null ? ConvertUtils.localDateTime2Date(b.getEnd()): null;
        this.granularity = b != null && b.getMode() != null
                ? Granularity.valueOf(b.getMode().name())
                : ValidityPeriodUtils.getGlobalDateGranularityMode();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public Granularity getGranularity() {
        return granularity;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isDefined() {
        return defined;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public Date getFrom() {
        return start;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public Date getTo() {
        return end;
    }
}
